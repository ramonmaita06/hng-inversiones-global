<div class="modal fullscreen-modal" id="kyc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

<div class="modal-dialog modal-lg">

<!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header text-center">
      <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
      <i class="fa fa-shield  text-danger" ></i> <span class="text-danger" style="font-weight: bold;" id="modaltitle1">VERIFICAR IDENTIDAD</span> <i class="fa fa-user  text-danger"></i>

    </div>
    <div class="modal-body" id="div-camara">

      <div class="cortina_kyc">
        <div class="" style="position: fixed;top: 40%;left: 40%;">
          <img src="images/cargar.gif" alt="">
        </div>
      </div>
    
        <?php 

          function mostrar_camara(){
            echo '<div class="row">
                <div class="col-md-12 jumbotrom" >
                  <div class="text-center" id="pc">
                    <div class="row">
                      <div class="col-md-4">
                        <select name="listaDeDispositivos" id="listaDeDispositivos" class="form-control">
                          <option value="">NO HAY CAMARAS CONECTADAS</option>
                        </select>
                      </div>
                      <div class="col-md-8 text-right">
                        <div class="btn-group">
                          <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Activar Camara" id="boton-c"><i class="fa fa-play"></i></button>
                          <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Tomar Foto" id="boton"><i class="fa fa-camera"></i></button>
                          <button type="button" class="btn btn-primary" data-toggle="tooltip" title="Apagar Camara" id="boton-a"><i class="fa fa-stop"></i></button>
                        </div>
                        <!-- <button id="boton" class="btn btn-info"> <i class="fa fa-camera"></i>Tomar foto</button>
                        <p id="estado"></p>
                        <button id="boton-c" class="btn btn-info"> <i class="fa fa-camera"></i>Activar Camara</button>
                        <button id="boton-a" class="btn btn-info"> <i class="fa fa-camera"></i>Apagar Camara</button> -->
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12 text-center text-success">
                        <p id="estado"></p>
                      </div>
                    </div>
                    <br>
                    <video muted="muted" id="video"></video>
                    <canvas id="canvas" style="display: none; width: 100% !important; height: 100px !important:" width="100%" style="border:10px solid #000000;"></canvas>
                  </div>
                  <div id="tel">
                    <img src="" alt="" id="miimagen1">
                    <input id="imagen"  name="imagen" data-toggle="tooltip" title="Debe seleccionar un archivo de imagen"  type="file" class="file-loading file-input"  accept="image/*" capture="capture" onchange="preview(this.value)">
                    <a href="#"  data-toggle="tooltip" id="bsubir"  class="btn btn-md btn-success" onclick="return  CamaraMovil()" title="Pulse para subir la imagen"><i class="fa fa-picture-o"></i> Subir Imagen <i class="fa fa-upload"></i></a>
                  </div>
                </div>
              </div>';
          }

          $verificacion_estatus = mysql_query("SELECT * FROM verificar_identidad WHERE cod_cliente='".$_SESSION['cod_id']."' AND estatus IN ('1','2')");

          if (mysql_num_rows($verificacion_estatus) > 0) {
            while ($verifca = mysql_fetch_array($verificacion_estatus)) {
              if ($verifca['estatus'] == 1) {
                echo '<div class="alert alert-info">
                        <h4><i class="icon fa fa-info"></i> Aviso!</h4>
                        Estimado cliente su foto se encuentra en el proceso de verificación por nuestro equipo.
                      </div>';
                echo '
                  <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                      <img src="'.substr($verifca["foto"],3).'" alt="" width="" height="" class="img-responsive img-thumbnail zoom">
                    </div>
                  </div>

                ' ;
              }elseif($verifca['estatus'] == 2){

              echo '<div class="alert alert-danger">
                        <h4><i class="icon fa fa-ban"></i> Aviso!</h4>
                        Estimado cliente su foto no se ha podido verificar, por favor cargue nuevamente su foto con su documento de identidad.
                      </div>';
              }
              if ($verifca['estatus'] != 1) {
                mostrar_camara();
              }
            }
          }else{
            echo '<div class="alert" style="background:#E2F7FC">
              <h4><i class="icon fa fa-info-circle"></i> Aviso!</h4>
              Estimado cliente le informamos HNG se encuentra verificando la identidad de nuestros clientes, con el objetivo de <b>evitar estafas y usurpación de identidad</b>. <br>
                <b>Para verificar es necesario que permita acceso a la camara de su dispositivo (
                <i class="fa fa-video-camera"></i>
                <i class="fa fa-mobile"></i>
                <i class="fa fa-laptop"></i>
                <i class="fa fa-desktop"></i>
                )</b>.<br><br>
                A continuación una guia para realizar la selfie de identidad, pude identificarse con los siguiente documentos que muestren fotos de si mismo, como por ejemplo, <ul><li>Cedula de Identidad</i><li> DNI</li> <li>Pasaporte</li> <li>Licencia de Conducir</li> <li>Etc.</li></ul><br>
                <center><img src="images/kyc.png"  style="width:320px" title="Imagen extraida de https://paacapital.com/knowledgebase-category/kyc/"></center>
            </div>';
            mostrar_camara();
          }

      ?>

    </div>
    
    <div class="modal-footer">
      <button onclick="javascript:location.href='php/salir.php';" class="pull-right font btn btn-danger" ><i class="glyphicon glyphicon-off"></i> Cerrar Sesión</button>
      <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
    </div>
  </div>
  
</div>
</div>


<div class="modal fullscreen-modal" id="ModalContrato" tabindex="-189" role="dialog" aria-labelledby="myModalLabe2l">

  <div class="modal-dialog modal-lg">

  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <i class="fa fa-refresh  text-danger" ></i> <span class="text-danger" style="font-weight: bold;" id="modaltitle1">RENOVAR CONTRATO</span> <i class="fa fa-file  text-danger"></i>

      </div>
      <div class="modal-body">

        <div class="cortina_kyc">
          <div class="" style="position: fixed;top: 40%;left: 40%;">
            <center>
              
              <img src="images/cargar.gif" alt="">
              <br>
              Cargando..!
            </center>
          </div>
        </div>
        <div class="container-fluid">
          
          <div class="col-md-12">
            <div class="alert alert-warning">
              <h4><i class="icon fa fa-warning"></i> Alerta!</h4>
              ESTIMADO CLIENTE LE INFORMAMOS QUE SU CONTRATO SE ENCUENTRA VENCIDO DEBE RENOVARLO PARA CONTINUAR CON NUESTROS SERVICIOS.
            </div>
           
          </div>

          <div class="col-md-12 text-center" id="monto-renovacion">
            <?php  
              $consulta = mysql_query("SELECT * FROM monedas where estatus = 1");

              if (mysql_num_rows($consulta) > 0) {
                $cartera_inscripcion = '';
                while ($monedas = mysql_fetch_object($consulta)) {
                  $cartera = $monedas->cartera;
                  $c = mysql_query("select cliente.cod_id, mis_depositos$cartera.* from cliente,mis_depositos$cartera where mis_depositos$cartera.id_c = cliente.id_c and cliente.cod_id = '".$_SESSION['cod_id']."' and mis_depositos$cartera.tipot ='INSCRIPCION' ");
                  if (mysql_num_rows($c) > 0) {
                    $cartera_inscripcion = $cartera;
                    break;
                  }
                }
              }

              $consul2 = mysql_query("SELECT * FROM monedas where cartera = '$cartera_inscripcion'");

              $moneda = mysql_fetch_object($consul2);
              echo 'EL MONTO DE RENOVACION DEL CONTRATO ES DE <big class="label label-primary" style="font-size: 18pt;">'.masmenos($moneda->inscripcion*10).' '.$moneda->moneda."</big>";

              echo '<input type="hidden" id="carteraR" value="'.$moneda->cartera.'"><input type="hidden" id="divisaR" value="'.$moneda->moneda.'">';

              $table = '<table class="table table-hover table-striped">

              <thead>
                <tr>
                  <th>Pais</th>
                  <th>Disponible Para Renovar</th>
                  <th>Accion</th>
                </tr>
              </thead>

              <tr>
                <td><img src="images/monedas_paises/'.$moneda->pais.'.gif" class="img-responsive" style="width:64px"></td>
                <td id="portafolio-act" style="font-size:18pt;"></td>
                <td id="btn-acc"></td>
              </tr>


              </table>';

              echo $table;
            ?>
              

          </div>
          
          <div class="col-md-12" id="portafolio-act">
            
          </div>

          <div class="col-md-12"  style="display: none;"  id="div-recargar">
            <?php include("app_5x5/recargar_renovar_contrato.php") ?>
          </div>

          <div class="col-md-12" style="display: none;"  id="div-contrato">
            <div class="row">
              <div class="col-md-12">
                <table class="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>Fecha de Inicio</th>
                      <th>Fecha de vencimiento</th>
                      <th>Fecha a renovar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 

                      $fecha_inicio = date('d-m-Y');
                      $mes_inicio = date('m');
                      $y = date('Y');
                      if (intval($mes_inicio) < 9 ) {
                        $y = intval($y) + 1;
                        $fecha_vencimiento = date_format(date_create("$y-01-14"),"d-m-Y");
                      }else{
                        $y = intval($y) + 2;
                        $fecha_vencimiento = date_format(date_create("$y-01-14"),"d-m-Y");
                      }
                      $fecha_renovacion = date_format(date_create("$y-01-15"),"d-m-Y");

                    ?>
                    <tr>
                      <td><?php echo $fecha_inicio; ?></td>
                      <td><?php echo $fecha_vencimiento; ?></td>
                      <td><?php echo $fecha_renovacion; ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <iframe src="<?php echo 'php/proceso_renovar_contrato.php?cod_id='.$_SESSION['cod_id']; ?>" frameborder="0" width="100%"  height="500"></iframe>
            <div class="row" style="margin-top: 15px;">
              
              <!-- <div class="col-md-2">
              </div> -->
              <div class="col-md-6">
                <input type="checkbox" id="aceptar" value="acepto" style="width: 20px; height: 20px; display: inline-block;">

                <label for="aceptar" class="col-md-6 control-label" style="font-size: 12pt; width: 100% !important; display: contents;"> Aceptar Terminos y Condiciones

                </label>
              </div>
              <div class="col-md-6">
                <button class="btn btn-success bnt-flat" id="btn-renovar-c" disabled="disabled" style="float: right">SI, DESEO RENOVAR EL CONTRATO</button>
              </div>
            </div>
          </div>
        </div>
        

      </div>
      
      <div class="modal-footer">
        <button onclick="javascript:location.href='php/salir.php';" class="pull-right font btn btn-danger" ><i class="glyphicon glyphicon-off"></i> Cerrar Sesión</button>
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button> -->
      </div>
    </div>
    
  </div>
</div>