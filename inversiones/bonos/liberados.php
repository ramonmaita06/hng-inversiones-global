<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" type="text/css" href="bootstrap/datatables/bootstrap/css/bootstrap.min.css">
	<script type="text/javascript" src="bootstrap/datatables/jquery.min.js"></script>
	<script type="text/javascript" src="bootstrap/datatables/bootstrap/js/bootstrap.min.js"></script>

  <link rel="stylesheet" type="text/css" href="bootstrap/datatables/datatables/datatables.min.css">
  <script type="text/javascript" src="bootstrap/datatables/datatables/datatables.min.js" ></script>
</head>
<body>
<div class="panel panel-success">
<div class="panel-heading">
  <center><b>BONOS LIBERADOS</b></center>
</div>
<div class="panel-body">

     <div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group">
         
          <div class="col-md-12" style="font-size:32px; text-align:center; font-weight:bold;">
			 MONTO	<i class="fa fa-money"></i> 
			 <span id="monto_libera">0.00</span>		  
			</div>
          </div>
       </div>  
       </div>  
<hR>
     
	 <div id="listado_bonos">	 </div>
	 
	 
	 
</div>	
</div>	


<script>
  $(document).ready(function(){
  	setTimeout(function(){

    	
		$('.datatable').DataTable({
			"order": [[ 10, "desc" ]],
	    	language: {
	        "decimal": "",
	        "emptyTable": "No hay información",
	        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	        "infoPostFix": "",
	        "thousands": ",",
	        "lengthMenu": "Mostrar _MENU_ Entradas",
	        "loadingRecords": "Cargando...",
	        "processing": "Procesando...",
	        "search": "Buscar:",
	        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
    		}
		});
	},5000);
  });

parent.cerrar_carga();
cargar_bonos_libera();
</script>
</body>
</html>