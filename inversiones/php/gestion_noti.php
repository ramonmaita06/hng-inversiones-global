<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
 

<link href="css/sweetalert.css" rel="stylesheet">
<script src="js/proceso_notifica.js"></script>

<script>
<?php if($_GET["opc"]=="crear"){echo "abrir_nota();";} ?>
<?php if($_GET["opc"]=="leer"){echo "abrir_nota();";} ?>
</script>


<style>
.imgr{
    -webkit-animation: 3s rotate linear infinite;
    animation: 3s rotate linear infinite;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}

.hover_noti{ cursor:pointer;}
.hover_noti:hover{background:#E4F2DF;}

#cortina_nota{
	display:none;
	position:fixed;
	width:100%;
	height:100%;
	left:0%;
	top:0%;
	z-index:99;
	background:black;
	opacity:.7;
}

#ventana_nota{
	display:none;
	position:absolute;
	width:80%;
	left:10%;
	top:5%;
	z-index:100;
	background:white;
}
</style>
</head>
<body>
<div>

<div class="panel panel-success">
<div class="panel-heading text-center"><big><big><i class="fa fa-bell-o"></i> GESTIÓN DE NOTIFICACIONES</big></big></div>
<div class="panel-body">


<table class="table">
<tr><th style="width:30%">NOTIFICACIÓN  &nbsp;&nbsp;<i class="fa fa-folder-o" style="font-size:20px; cursor:pointer; text-success" title="Notificaciones sin leer" onclick="return filtronoti(0)"></i> <span id="slnotas">0</span> <span style="color:#eee">|</span> 
<i class="fa fa-folder-open-o text-warning" style="font-size:20px; cursor:pointer;" title="Notificaciones leidas" onclick="return filtronoti(1)"></i> <span id="lnotas" style="color:grey">0</span>
<a href="#" title="Pulse para recargar las notificaciones" class="text-success"><i class="fa fa-refresh" style="float:right; font-size:24px" onclick="mostrar_notas_gestion(), parent.mostrar_notas();"></i></a></th>
<th style="width:55%">MENSAJE</th><th align="right"><a href="#"class="btn btn-ms btn-success" title="Crear notificación" onclick="return abrir_nota()"><i class="fa fa-edit "></i></a></th></tr>
<tr>
<td style="background:#F9FCF8">
<div id="notas" style="overflow-y:auto; height:400px;"></div>
</td>
<td colspan="2">
<div id="cuerpo_noti" style="overflow-y:auto; height:400px;">
<br><br>
<center>

<i class="fa fa-comments-o text-success" style="font-size:48px"></i>
No hay notificaciones selecciondas<bR>
</center>
</div>
</td>
</tr>
</table>


</div>
</div>

<div id="cortina_nota"></div>
<div id="ventana_nota">
<div class="panel panel-success">
<div class="panel-heading"><b><big><big><i class="fa fa-plus"></i> NOTIFICACIONES</big></big></b>
<a href="#" title="Pulse para cerrar la ventana" class="text-success"><i class="fa fa-times" style="float:right" onclick="$('#cortina_nota,#ventana_nota').hide(500,'linear'),stop_video();"></i></a>
</div>
<div class="panel-body">

<table class="table table-bordered">
<?php if($_SESSION['cod_id']!='HNGI-0001'){ ?>
<tr><th>PARA</th><td>HOMBRES DE NEGOCIOS GLOBAL C.A.<input type="hidden" id="para" value="HNGI-0001"></td></tr>
<?php }else{ 

include("cnx.php");

$cons=mysql_query("select * from cliente order by cod_id, nombre asc");
$lista='<select id="para" class="form-control">';
while($info=mysql_fetch_array($cons)){
	$lista.='<option value="'.$info['cod_id'].'">'.$info['cod_id'].' - '.$info['nombre'].' '.$info['apellido'].'</option>';
}
$lista.='</select>';
?>
<tr><th>PARA</th><td><?php echo $lista; ?></td></tr>
<?php } ?>
<tr><th>DE</th><td><?php echo $_SESSION['cliente']." <b>".$_SESSION['cod_id']."</b>"; ?></td></tr>
<tr><th>ASUNTO</th><td><input id="asunto" class="form-control" type="text" maxlength="160"></td></tr>
<tr><th colspan="2">NOTA</th></tr>
<tr><td colspan="2"><textarea id="nota" class="form-control" style="width:100%; height:120px;" placeholder="Escriba la nota..."></textarea></td></tr>
<tr><th colspan="2"><center><button onclick="return crear_nota()" class="btn btn-success"><i class="fa fa-send"></i> Enviar</button></center></th></tr>
</table>
</div>
</div>

</div>



<script>parent.cerrar_carga(); mostrar_notas_gestion();</script>

    <script src="js/sweetalert.min.js"></script>
    <script src="js/sweetalert.dev.js"></script>
    <script src="js/wow.min.js"></script>

</body>
</html>