<?php  
	session_start();
	$_SESSION['usd'] = 1;
	$_SESSION['euro'] = 1.11;
	$_SESSION['bs'] = 75000;
	$_SESSION['estatero'] = 0.33
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
	<script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script>
	<script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
</head>
<body>
	<div class="container">
		
	<form action="" class="form-horizontal col-md-8 col-md-offset-2">
		<div class="form-group">
			<label for="">USD</label>
			<input type="number" id="USD" value="<?php echo $_SESSION['usd'];  ?>">
			<label for="">Euro</label>
			<input type="number" id="Euro" value="<?php echo $_SESSION['euro'];  ?>">
			<label for="">BsS</label>
			<input type="number" id="BsS" value="<?php echo $_SESSION['bs'];  ?>">
			<label for="">Estateros</label>
			<input type="number" id="Estateros" value="<?php echo $_SESSION['estatero'];  ?>">
		</div>
		<div class="form-group">
			<label for="">Cartera</label>
			<select name="divisa" id="divisa" class="form-control">
				<option value="USD">USD</option>
				<option value="BsS">BsS</option>
				<option value="Euro">Euro</option>	
			</select>
		</div>
		<div class="form-group">
			<label for="">Estatero</label>
			<input type="checkbox" id="check-estatero" class="form-control" >
		</div>

		<div class="form-group">
			<label for="">Monto</label>
			<input type="number" id="monto" step="0.01" class="form-control" >
		</div>

		<div class="form-group">
			<label for="">Cantidad de Estateros</label>
			<input type="number" step="0.00000001" name="estateros" id="cant_estatero" class="form-control">
		</div>
		<div class="form-group">
			<label for="">Cantidad de Dolares</label>
			<input type="number" step="0.00000001" name="estateros" id="cant_dolares" class="form-control">
		</div>

		<div class="form-group">
			<label for="">Duracion / %</label>
			<select name="Duracion" id="duracion" class="form-control">
				
			</select>
		</div>
	</form>
	</div>



	<script>
		$(function() {
			$(document).on('click', '#check-estatero', function(event) {
				// event.preventDefault();
				if( $('#check-estatero').prop('checked') ) {
					$('#monto').attr('step', '0.00000001');
				}else{
					$('#monto').attr('step', '0.01');

				}
			});
			$(document).on('keyup', '#monto', function(event) {
				event.preventDefault();
				if( $('#check-estatero').prop('checked') ) {
					var monto = parseFloat($('#monto').val());
					var val_estatero = parseFloat($('#Estateros').val());
					var cant_dolares = monto*val_estatero;
					var cant_estateros = cant_dolares/val_estatero;
					// cant_estateros = parseFloat(cant_estateros)
					console.log(cant_estateros)
					$('#cant_estatero').val(cant_estateros);
					$('#cant_dolares').val(cant_dolares);
					$.ajax({
							url: 'prueba_inversion_estatero.php',
							type: 'POST',
							dataType: 'json',
							data: {monto: monto,opc:2},
						})
						.done(function(data) {
							console.log("success");
							if (data.success == true) {
								$('#duracion').html(data.lista);
							}
						})
						.fail(function(e) {
							console.log("error");
							console.log(e);

						})
						.always(function() {
							console.log("complete");
						});
				}else{

					var cartera = $('#divisa').val();
					// alert('assd')
					if (cartera == 'BsS') {
						var monto = parseFloat($('#monto').val());
						var equivale_dolar = parseFloat($('#BsS').val());
						console.log(monto)

						var cant_dolares = monto/equivale_dolar;
						var val_estatero = parseFloat($('#Estateros').val());
						var cant_estateros = cant_dolares/val_estatero;
						// cant_estateros = parseFloat(cant_estateros)
						console.log(cant_estateros)
						$('#cant_estatero').val(cant_estateros);
						$('#cant_dolares').val(cant_dolares);

						$.ajax({
							url: 'prueba_inversion_estatero.php',
							type: 'POST',
							dataType: 'json',
							data: {monto: cant_dolares,opc:1},
						})
						.done(function(data) {
							console.log("success");
							if (data.success == true) {
								$('#duracion').html(data.lista);
							}
						})
						.fail(function(e) {
							console.log("error");
							console.log(e);

						})
						.always(function() {
							console.log("complete");
						});
						

					}else{
						var monto = parseFloat($('#monto').val());
						var equivale_dolar = parseFloat($('#USD').val());
						console.log(monto)

						var cant_dolares = monto/equivale_dolar;
						var val_estatero = parseFloat($('#Estateros').val());
						var cant_estateros = cant_dolares/val_estatero;
						// cant_estateros = parseFloat(cant_estateros)
						console.log(cant_estateros)
						$('#cant_dolares').val(cant_dolares);
						$('#cant_estatero').val(cant_estateros);
						$.ajax({
							url: 'prueba_inversion_estatero.php',
							type: 'POST',
							dataType: 'json',
							data: {monto: cant_dolares,opc:1},
						})
						.done(function(data) {
							console.log("success");
							if (data.success == true) {
								$('#duracion').html(data.lista);
							}
						})
						.fail(function(e) {
							console.log("error");
							console.log(e);

						})
						.always(function() {
							console.log("complete");
						});
					}
				}
			});
		});
	</script>
</body>
</html>