
 
<div class="panel panel-default">
    <div class="panel-heading">
   <a data-toggle="collapse" data-parent="#accordion" href="#collapse1c"> 
   <h4 class="panel-title">
		<span class="glyphicon glyphicon-briefcase"></span> Condiciones
   </h4>
   </a>
   
    </div>
    <div id="collapse1c" class="panel-collapse collapse out">
      <div class="panel-body">
        <table style="width:100%;" class="texto" cellspacing="0" cellspadding="3">
		<tr>
		<td style="font-size:14px; color:black; text-align:justify">
		<h4>DEBE TENER EN CUENTA</h4>
<p>
<br>
<b>A –</b> Usted compra la cantidad en prendas o joyas de oro de inversión con un pago a cuenta y dispone de 12 meses para recomendar las mismas cantidades de los mismos Planes de Amigos / Planes Duales a nuevos clientes, quedando de esta manera cerrado el contrato y cobrar su cantidad en oro en concepto de DESCUENTO.
<br>
<br>

<b>B –</b> El cliente, en el supuesto de no tener sus 6 referidos, podrá abonar la diferencia entre el pago a cuenta y la reserva, recibiendo de esta manera la cantidad reservada en oro, sin perder nunca su pago a cuenta, en este caso se aplica un honorario por compra directa de oro, del 12% + 12.5% descuento aplicado al Sponsor, total 24.5%.
<br>
<br>

<b>C –</b> En el caso que desee cancelar su transacción, esto tiene que solicitarse dentro de los 14 Días naturales siguientes al pago. En este caso, la cantidad abonada será devuelta en los 20 días hábiles después de la cancelación de la transacción, dicha cantidad será devuelta en la misma tarjeta donde se haya efectuado la compra.
<br>
<br>

<b>D –</b> Recuerda poner el ID de la persona que te ha presentado esta oportunidad (Sponsor).
<br>
<br>

<b>E –</b> Todos los campos a rellenar son obligatorios.
<br>
<br>

<b>F –</b> En el supuesto que el Plan de Amigo / Dual Plan pagado por un cliente haya servido para cerrar cualquiera de los Planes de Amigos de otro cliente, NO SE DEVOLVERÁ CANTIDAD ALGUNA en los siguientes 14 días después del pago por falta de ganancias por parte de la Empresa, añadiendo inclusive los posible gastos administrativos que haya conllevado esta situación.
<br>
</p>
		</td>
		</tr>
		<tr style="font-size:12px;">
		<td>
    		<hr>
		<center>
		<button onclick="return print()" class="btn btn-info btn-md font"><i class="glyphicon glyphicon-print"></i> Imprimir</button>
		</center>
		</td>
		</tr>
		</table>	
	  </div>
    </div>
  </div>  
	
	
	
<?php
 include("planes_proceso.php"); 
?>
