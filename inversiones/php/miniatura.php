<?php session_name("hng"); session_start();

$dirOriginal=$_SESSION["dirComprobante"];
// $dirOriginal="../comprobantes/";
$archivo = basename($_SESSION["imagend"]);
miniatura($archivo,$dirOriginal);

function miniatura($nombrearchivo,$dirOriginal){

set_time_limit(-1);
ini_set('memory_limit', '-1');

//Ruta de la carpeta donde se guardarán las imagenes

$patch=$_SESSION["desComprobante"]; //ruta destino
// $patch='../comprobantes_min/'; //ruta destino

//Parámetros optimización, resolución máxima permitida
$max_ancho = 860;
$max_alto = 860;	
	
//Redimensionar
$rtOriginal=$dirOriginal.$nombrearchivo;

$extension = strtolower(pathinfo($nombrearchivo, PATHINFO_EXTENSION));


if(($extension=='jpeg')||($extension=='jpg')){
$original = imagecreatefromjpeg($rtOriginal);
}
else if($extension=='png'){
$original = imagecreatefrompng($rtOriginal);
}
else if($extension=='gif'){
$original = imagecreatefromgif($rtOriginal);
}


 
list($ancho,$alto)=getimagesize($rtOriginal);

$x_ratio = $max_ancho / $ancho;
$y_ratio = $max_alto / $alto;


if( ($ancho <= $max_ancho) && ($alto <= $max_alto) ){
    $ancho_final = $ancho;
    $alto_final = $alto;
}
elseif (($x_ratio * $alto) < $max_alto){
    $alto_final = ceil($x_ratio * $alto);
    $ancho_final = $max_ancho;
}
else{
    $ancho_final = ceil($y_ratio * $ancho);
    $alto_final = $max_alto;
}

$lienzo=imagecreatetruecolor($ancho_final,$alto_final); 

imagecopyresampled($lienzo,$original,0,0,0,0,$ancho_final, $alto_final,$ancho,$alto);
 
//imagedestroy($original);
 
$cal=8;



if(($extension=='jpeg')||($extension=='jpg')){
imagejpeg($lienzo,$patch."/".$nombrearchivo);
}
else if($extension=='png'){
imagepng($lienzo,$patch."/".$nombrearchivo);
}
else if($extension=='gif'){
imagegif($lienzo,$patch."/".$nombrearchivo);
}

unlink($rtOriginal);

return true;

}


?>