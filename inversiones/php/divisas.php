<link href="css/sweetalert.css" type="text/css" rel="stylesheet">
<script src="js/sweetalert.min.js"></script>
<script src="js/procesos_divisa.js"></script>

<div class="panel panel-success">
<div class="panel-heading"><big><big><b>COMPRA DE DIVISAS</b></big></big></div>
<div class="panel-body">

	<div class="row">
		<div class="col-sm-12 col-lg-12">
			<div class=" form-group">
				<label class="col-md-12">Mis portafolios <span class="text-success">(Disponible)</span></label>
			</div>
			<div class=" form-group">
				<div class="col-md-6">
					<div  class="form-control" id="div_bolivar"></div>
				</div>
				<div class="col-md-6">
					<div  class="form-control" id="div_dolar"></div>
				</div>
			</div>
		</div>
	</div>
<hr>
	<div class="row">
		<div class="col-sm-12 col-lg-12">
			<div class=" form-group">
				<label class="col-md-12">Tasa de Compra-Venta (<span id="div_fecha"></span>)</label>
			</div>
			<div class=" form-group">
				<div class="col-md-6">
					<b class="text-primary">HNG te Vende los USD$</b> <div  class="form-control" id="tasa_div_compra">...</div>
				</div>
				<div class="col-md-6">
					<b class="text-success">HNG te Compra los USD$</b> <div  class="form-control" id="tasa_div_venta">...</div>
				</div>
			</div>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-sm-12 col-lg-12">
			<div class=" form-group">
				<div class="col-md-3"> 
				</div>
				<div class="col-md-6 box" style="background: #F9F3CF; color: #9A0808; text-align: justify;"><b>Nota:</b>
				Los dolares canjeado a través de codigos precargados fuera de las operaciones del sistemas de HNG,
				serán generados bajo el monto de la tasa del día en referencia al dolar promedio de <a href="https://www.instagram.com/monitordolarvzla__/?hl=es-la" target="https://www.instagram.com/monitordolarvzla__/?hl=es-la" class="text-success fa fa-link"><span class="text-success"> Monitor Dolar Venezuela</span></a>.

	
						<a href="https://www.instagram.com/monitordolarvzla__/?hl=es-la" target="https://www.instagram.com/monitordolarvzla__/?hl=es-la"><img src="images/monitorDolar.png" style="width:32px"></a>
				
						<b class="text-success">Tasa: </b>
					
						<span style="color:green; font-weight:bold; font-size: 16px;"  id="tasa_monitor">...</span> BsS.
					En fecha: 
						<span style="color:black; font-weight:bold; font-size: 16px;"  id="fecha_tasa_monitor"></span>
					
			</div>
				<div class="col-md-3"> </div>
			</div>
		</div>
	</div>
<hr>
	<div class="row">
		<div class="col-sm-3 col-lg-3">
			<div class=" form-group">
				<label class="col-md-12">Monto a Canjear</label>
			</div>
			<div class=" form-group">
				<div class="col-md-12"><input type="number"  id="monto_divisa" title="Monto a Canjear" data-toggle="tooltip" onkeyup="return cambio_bs_dolar()" placeholder="0,00" step="0.01"  class="form-control" value="1000"></div>
			</div>
		</div>
		<div class="col-sm-4 col-lg-4">
			<div class=" form-group">
				<label class="col-md-10">Conversión</label>
			</div>
			<div class=" form-group">
				<select id="tipo" class="form-control" onchange="return cambio_divisa()">
					<option value="1">De Bolivares a Dolares</option>
					<option value="2">De Dolares a Bolivares </option>
				</select>
				
			</div>
		</div>
		<div class="col-sm-3 col-lg-3">
			<div class=" form-group">
				<label class="col-md-12">Resultado del Canje</label>
			</div>
			<div class=" form-group">
				<div class="col-md-12"><div id="resultado_divisa" title="Monto resultante" data-toggle="tooltip" class="form-control" style="color:green; background:white">
				0,00
				</div>
				</div>
			</div>
		</div>

		<div class="col-sm-2 col-lg-2">
		<div class=" form-group">
			<label class="col-md-12">&nbsp;</label>
		</div>
			<div class=" form-group text-center">
				<button class="btn btn-warning" onclick="return cambio_divisa()"><i class="fa fa-retweet"></i> Calcular</button>
			</div>
		</div>
	</div>
	


<!--mostar resultado-->	

<hr>
	<div class="row">
		<div class="col-sm-12 col-lg-12" >
			<div class=" form-group">
				<label class="col-md-12"><b>Nota:</b></label>
			</div>
			<div class=" form-group">
				<div class="col-md-12 text-justify" id="proceso" >
					Este proceso permitirá comprar divisas en (Dolares y Bolivares) y ser debitado y acreditado al portafolio correspondiente a la misma.<br> La tasa de compra y venta será aplicada por la empresa <b>Hombres de Negocios Global  Internacional Gold INC.</b>
					<br><br>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-lg-12" id="miresultado" style="display:none">
			<div class=" form-group">
				<label class="col-md-12"><b>Resultado:</b></label>
			</div>
			<div class=" form-group">
				<div class="col-md-12 text-justify" id="proceso" >
					<p>
					<span id="compra" class="text-success" style="font-weight:bold;"></span>
					&nbsp;&nbsp;
					<button class="btn btn-success btn-sm" id="btn_comprar" onclick="return comprar_divisas()"><i class="fa fa-shopping-cart"></i> Comprar</button>
					</p>
					<span id="montoletra"></span>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

