<?php session_name("hng"); session_start(); 

if(isset($_SESSION['us']) && $_SESSION['id_c']<=2){
?>

 <html>
  <head>
  <meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
	 <link rel="icon" href="images/favicon.gif" type="image/gif">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	
	    <!-- jQuery 2.1.3 -->
	<script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script>
	<link href="../css/sweetalert.css" type="text/css" rel="stylesheet">
	<script src="../js/number_format.js"></script>
	<script src="../js/procesos_divisa.js"></script>

	
	
<div class="context">
	<div class="panel panel-success">
	<div class="panel-heading text-center"><big><big><b>GESTOR DE DIVISAS</b></big></big>

	<div >Bienvenid@: <?php echo $_SESSION["cliente"]; ?></div>
	</div>
	
<div class="panel-body">
	<div class="btn-group pull-right">
		<button class="btn btn-sm btn-success" onclick="$('#formulario').show(500,'linear'),setTimeout(function(){location.href='#formulario';},1000);" title="Modificar Divisa"><i class="fa fa-edit"></i> Actualizar </button>	
		<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
			<span class="fa fa-gear" style="height:16px"> Opciones</span>
		</button>
		<ul class="dropdown-menu" role="menu">
			
			<li class="divider"></li>
			<li><a class="fa fa-sign-out" onclick="location.href='salir_divisa';" >Salir</a></li>
		</ul>
    </div>
	<br>
	<br>
	<div class="row text-center">
		<div class=" form-group ">	
			<div class="col-md-12 text-center ">
				<big><div   id="div_fecha"></div></big>
			</div>
		</div>			
	</div>			
<hr>	






<div class="row text-center">
<div class="box bg-purple col-md-3">
	<div class="inner">
	  <h4 style="font-size:28px"><div id="tasa_actual"></div></h4>
	  <p><label class="text-center text-default" >Tasa Actual</label></p>
	</div>
</div>

<div class="small-box bg-red col-md-3">
	<div class="inner">
	  <h4 style="font-size:28px"><div id="tasa_div_compra"></div></h4>
	  <p><label class="text-center text-default" >Tasa de Compra</label></p>
	</div>
	<div class="icon">
	  <i class="fa fa-usd"  style=" margin-top:10px"></i>
	</div>
</div>

<div class="small-box bg-green col-md-3">
	<div class="inner">
	  <h4 style="font-size:28px"><div  class="" id="tasa_div_venta"></div></h4>
	  <p><label class="text-center text-default" >Tasa de Venta</label></p>
	</div>
	<div class="icon">
	  <i class="fa fa-usd"  style=" margin-top:10px"></i>
	</div>
</div>


<div class="small-box bg-blue col-md-3">
	<div class="inner">
	  <h3 style="font-size:47px"><div id="cporc_tasa_compra"></div></h3>
	  <p><label class="text-center text-default" >Porcentaje de Compra</label></p>
	</div>
	<div class="icon">
	  <i class="fa fa-bar-chart-o" style="margin-top:10px"></i>
	</div>
<bR>
</div>

<div class="small-box bg-orange col-md-3">
	<div class="inner">
	  <h3 style="font-size:47px"><div  class="" id="cporc_tasa_venta"></div></h3>
	  <p><label class="text-center text-default" >Porcentaje de Venta</label></p>
	</div>
	<div class="icon">
	  <i class="fa fa-bar-chart-o" style="margin-top:10px"></i>
	</div>
	<br>
</div>
	
	</div>



<hr>
		<div class="row text-center text-warning" style="display:none" id="formulario">
			
			
			<div class="form-group">
				<div  class="col-md-12">
					<div class="col-md-2 ">
						<label class="text-center" >Fecha actual</label>
						<strong><input   id="fechadivisa" title="Fecha del dia de hoy" data-toggle="tooltip"    class="form-control" type="date" value="<?php echo date("d/m/Y"); ?>"></strong>
					</div>
					<div class="col-md-2">
						<label class="text-center" >Tasa de Compra</label>
						<input type="number"  id="tasa_compra" title="Monto de la tasa de compra" data-toggle="tooltip"    class="form-control" value="">
					</div>
					<div class="col-md-2">
						<label class="text-center" >Tasa de Venta</label>
						<input type="number"  id="tasa_venta" title="Monto de la tasa de venta" data-toggle="tooltip"    class="form-control " value="">
					</div>
					<div class="col-md-2  ">
						<label class="text-center" >Porcentaje de Compra </label>
						<input type="number" id="tasa_porc_compra" title="Porcentaje de compra (desde Clientes a HNG)" data-toggle="tooltip"    class="form-control" value="">
					</div>
					<div class="col-md-2">
						<label class="text-center" >Porcentaje de Venta</label>
						<input type="number" id="tasa_porc_venta" title="porcentaje de venta (desde HNG a Clientes)" data-toggle="tooltip"    class="form-control" value="">
					</div>		
					<div class="col-md-2  text-center">
						<div class=" form-group">
							<label class="col-md-12">&nbsp;</label>
						</div>
						<button class="btn btn-success btn-sm" onclick="return modificar_divisa()" title="Pulse para cambiar la informacion de la divisa"><i class="fa fa-refresh"></i>  Cambiar</button>
						<button class="btn btn-warning" onclick="$('#formulario').hide(500,'linear')" title="Pulse para cancelar la actualizacion de divisas"><i class="fa fa-ban"></i> </button>
					</div>
				</div>	
			</div>	
		</div>
	</div>
</div>
<a name="formulario">&nbsp;</a>
<hr>
	

	
	
	
	
	
	
	
	

    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
	<script src="../js/sweetalert.min.js"></script>
    <script>
	  mis_cofres_gestion();
    </script>
  </body>
</html>
<?php
}else{
	include("salir_divisa.php");
}
?>
	  


