<?php  session_name("hng"); session_start(); 

if(isset($_SESSION['us'])){
	
?>
<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Archivos de calendario-->
	<script src="js/src/js/jscal2.js"></script>
    <script src="js/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="js/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="js/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="js/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  
  <link rel="stylesheet" href="js/bootstrap3/css/bootstrap.min.css" media="screen">
  <link rel="stylesheet" href="css/estilo_reciclaje.css">
  <link rel="stylesheet" href="css/estilo_input.css">
  <script src="js/jq.min.js"></script>
  <script src="js/bootstrap3/js/bootstrap.min.js"></script>
  <script src="js/control/proceso.js"></script>
  


</head>
<body>

<div class="container">
 

 
 <!--
<div class="pfixed" style="background:#fff; width:100%;"> 
 <p><br><br> 
 <br><br> </p>
<table width="100%">
<td width="70%"><span class="ftg1">MI DEPOSITO</span></td>
<td width="15%" align="left" class="ftm2" style="" class="ftm2"><button id="impDP" disabled onclick="return impDP(this.value);" value="1" class="ftpb btn btn-success btn-sm" title="Pulse para imprimir la informacion de mi deposito"><span class="glyphicon glyphicon-print"></span> Imprimir</button></td>
</table>
</div>

<p><br><br><br></p>
<p><br><br><br></p>
	<div class="table-responsive">  
    <table class="table-hover table">
    <thead>
      <tr class="tpequeno table-reflow ">
        <th class="tcenter">N&deg;</th>
        <th class="tleft">DESECHOS</th>
      </tr>
    </thead>
    <tbody id="listado"></tbody>
  </table>
</div>


<div class="ttitulom1 tleft pfixed" style="z-index:1000001">
<div class="tcenter">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td width="20%" align="left"><img class="imagen" src="<?php echo 'fotos/'.$_SESSION['imagend'];?>" width="40"></td>
<td align="center">
<span class="ftm tcenter" style="color:white">HNG-RECICLAJE</span>
</td>
<td width="20%">
<div align="right" style="width:auto"><span onclick="$('#misubmenu').show();" id="mimenu" class='dey glyphicon glyphicon-menu-hamburger btn-lg' style="height:100%; width:100%;" title="Menu principal">
<div id="misubmenu">
<ol class="tleft">
<li onclick="return ropcion(1)"><span class='glyphicon glyphicon-shopping-cart'></span> Mi Deposito</li>
<li onclick="return ropcion(2)"><span class='glyphicon glyphicon-piggy-bank'></span> Mis Ventas</span></li>
<li onclick="return ropcion(3)"><span class='glyphicon glyphicon-usd'></span> Vender</li>
<li onclick="return ropcion(4)"><span class='glyphicon glyphicon-book'></span> Mis Movimientos</li>
<li onclick="return location.href='php/salir.php'"><span class='glyphicon glyphicon-off'></span> Salir</li>
</ol>
<div style="height:5px; background:#004500"></div>
</div>
</span>
</div>
</td>
</tr>
<tr>
<td colspan="3" class="ftp" align="left" style="background:#fff;">
<?php echo "<span class='ftm1'><span class='glyphicon glyphicon-leaf' aria-hidden='true'></span>&nbsp;".$_SESSION["cod_idR"]."</span> <span class='ftp'>".$_SESSION["cliente"]."</span>"; ?>
<div style="height:4px; background:#004500"></div>
</td>
</tr>
</table>
</div>
</div>-->





</div>
		<form onsubmit="return false;">
		<div  id="gestion">
		<input id="id_ds" type="hidden">
		<table class="table" style="border:none;  border-radius:10px; ">
		<tr style="background-image:url(images/barras/barra1.png); border:none;">
		<td  colspan="2" class="tright">
		<a href="#"><span title="Pulse para cerrar la ventana" class="glyphicon glyphicon-remove-sign" onclick="$('#gestion,#cortina').hide(200,'linear')"></span></a>
		</td>
		</tr>
		<tr class="ttitulom">
		<td colspan="2" id="tdesecho">
		...No Hay Desechos Seleccionados
		</td>
		</tr>
		<tr style="background:#fff;">
		<td>
		<center><span class="ftp">CANTIDAD NUEVA</span><br>
		<input  type="number" step="0.01" min="0" max="100000" id="cant" class="form-control hg wm tg">
		<br><span  class="ftp" id="unidad1"></span></center>
		</td>
        <td scope="raw">
		<center><span class="ftp">CANTIDAD ACUMULADA</span><br>
		<input type="number" readonly step="0.01" id="acumulado"  class="form-control hg wm tg">
		<br><span class="ftp"  id="unidad2"></span></center>
		</td>
		</tr>
		<tr class="tpie">
        <td colspan="2">
		<div class="btn-group" align="center">
		<button id="agrega" onclick="return agregar(1)" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus"></span></button>
		<button id="quitar" onclick="return agregar(0)" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-minus"></span></button>
		</div>
		</td>
		</tr>
		</table></div>
		</form>

		
<div id="v2">
<div style="height:32px; background-image:url(images/barras/barra1.png); border:none; padding:6px;">
<table width="100%">
<td align="left" width="65%"><span class="ftm2">MIS VENTAS</span> </td>
<td width="15%" align="left" class="ftm2" style="top:2px; position:absolute;" class="ftm2"><button id="impLVTAS" disabled onclick="return impLVTAS(this.value);" value="1" class="ftpb btn btn-success btn-sm" title="Pulse para imprimir la informacion de mis ventas"><span class="glyphicon glyphicon-print"></span> Imprimir</button></td>
<td width="10" align="right"><a href="#" onclick="$('#v2,#cortina').hide(200,'linear'); $('#listado').show(200,'linear'); " ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
</table>
</div>

<table style="width:100%"><td>
<div class="input-group">
<span class="input-group-addon"><b>Filtro</b></span>
<input id="filter" type="text" class="form-control" placeholder="Buscar ventas">
<span onclick="return buscarVTAS(document.getElementById('filter'));"  class="input-group-addon ftpb btn btn-default btn-sm" title="Pulse para buscar los desechos vendidos"><span class="glyphicon glyphicon-search" style="font-size:16px; color:green;"></span></span>
</div>
</td>
<td style="width:48px;">&nbsp;<button id="actualizar" onclick="return listarVTAS();"  class="ftpb btn btn-success btn-sm" title="Pulse para actualizar ventas realizadas de desechos"><span class="glyphicon glyphicon-refresh" style="font-size:16px;"></span></button></td>
</table>

<?php include("php/gestion/misventas.php"); ?>
</div>	
	
<div id="v3">
<div style="height:32px; background-image:url(images/barras/barra1.png); border:none; padding:6px;">
<table width="100%">
<td align="left"><span class="ftm2">VENDER</span> </td>
<td width="10" align="right"><a href="#" onclick="$('#v3,#cortina').hide(200,'linear'); $('#listado').show(200,'linear'); " ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
</table>
</div>
<?php include("php/gestion/vender.php"); ?>
</div>	
	
<div id="v4">
<div style="height:32px; background-image:url(images/barras/barra1.png); border:none; padding:6px;">
<table width="100%" border="0">
<td align="left" width="65%"><span class="ftm2">MIS MOVIMIENTOS </span> </td>
<td width="15%" align="left" class="ftm2" style="top:2px; position:absolute;"><button id="impMTV" value="1" disabled onclick="return impMTV(this.value);"  class="ftpb btn btn-success btn-sm" title="Pulse para imprimir los detalles de la venta"><span class="glyphicon glyphicon-print"></span> Imprimir</button></td>
<td width="5%" align="right"><a href="#" onclick="$('#v4,#detalles,#cortina').hide(200,'linear'); $('#listado').show(200,'linear'); " ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
</table>
</div>
<table style="width:100%">
<td>

<div class="input-group">
<span class="input-group-addon"><b>A</b></span>
<input id="fi" type="text" readonly class="form-control" placeholder="Desde" style="background:#fff; font-size:10px; color:blue;">
<span id="bfi" class="input-group-addon ftpb btn btn-default btn-sm" title="Buscar fecha desde"><span class="glyphicon glyphicon-calendar" style="font-size:16px; color:green;"></span></span>

<span class="input-group-addon"><b>B</b></span>
<input id="ff" type="text" readonly class="form-control" placeholder="Hasta" style="background:#fff; font-size:10px; color:blue;">
<span id="bff" class="input-group-addon ftpb btn btn-default btn-sm" title="Buscar fecha hasta"><span class="glyphicon glyphicon-calendar" style="font-size:16px; color:green;"></span></span>
</div>
<script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fi",
        trigger    : "bfi",
        onSelect   : function() { this.hide()	},
       // showTime   : 12,
        dateFormat : "%d/%m/%Y"
		  });//]]>	
		  Calendar.setup({
        inputField : "ff",
        trigger    : "bff",
        onSelect   : function() { this.hide()},
       // showTime   : 12,
        dateFormat : "%d/%m/%Y"
		  });//]]>		  
</script>
</td>


<td style="width:48px;">&nbsp;<button id="mbuscar" onclick="return buscarMTV();"  class="ftpb btn btn-success btn-sm" title="Pulse para buscar los movimientos realizados de los desechos"><span class="glyphicon glyphicon-search" style="font-size:16px;"></span></button></td>
</table>
<?php include("php/gestion/movimientos.php"); ?>
</div>	

<div id="detalles" style="display:none; position:fixed; top:90px; left:0%; z-index:900091; background:#fff; width:100%;">
<div style="height:32px; background-image:url(images/barras/barra1.png); border:none; padding:6px;">
<table border="0" style="width:100%"><td align="left" class="ftm2" style="width:60%">DETALLES</td>
<td align="right" class="ftm2" style="width:30%; top:2px; position:absolute;"><button id="impVTAS" disabled onclick="return impVTAS();"  class="ftpb btn btn-success btn-sm" title="Pulse para imprimir los detalles de la venta"><span class="glyphicon glyphicon-print"></span> Imprimir</button></td>
<td align="right"><a href="#" onclick="$('#detalles').hide(200,'linear');" ><span class="glyphicon glyphicon-remove-sign"></span></a></td>
</table></div>
<div id="detalleVTAS"></div>
</div>
		
</body>
</html>
<?php }else{echo "<script>location.href='./';</script>";}?>