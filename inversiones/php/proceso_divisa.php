<?php session_name("hng"); session_start();

include("proceso_cofre.php");
include("cnx.php");
include("funciones.php");
include("cifra_letras1.php");
include("cifra_letrasD.php");




class divisa{
	


	public function mis_cofres(){
	    
	    $this->info_config();
	    
		echo json_encode(array("salida"=>1
		,"cofre"=>masmenos1(miscofre::cofre_divisas('mis_depositos','BsS'))
		,"cofre_dolar"=>masmenos1(miscofre::cofre_disponible('mis_depositos_divisa','USD'))
		,"pctasa_compra"=>masmenos($_SESSION['pctasa_compra'])." BsS (x 1 USD$)"
		,"pctasa_venta"=>masmenos($_SESSION['pctasa_venta'])." BsS (x 1 USD$)"
		,"fecha_conf"=>cambiar_fecha_es($_SESSION['cfecha_conf'])
		,"fecha_conf1"=>$_SESSION['cfecha_conf']
		,"cporc_tasa_compra"=>round(($_SESSION['cporc_tasa_compra']*100),2)."%"
		,"cporc_tasa_venta"=>($_SESSION['ctasa_porc_venta']*100)."%"
		/*para modificacion*/
		,"cporc_tasa_compra1"=>($_SESSION['cporc_tasa_compra']*100)
		,"cporc_tasa_venta1"=>($_SESSION['ctasa_porc_venta']*100)
		,"ptasa_compra"=>($_SESSION['ptasa_compra'])
		,"ptasa_venta"=>($_SESSION['ptasa_venta'])
		
		,"tasa_monitor"=>(masmenos($_SESSION['tasa_compra']))
		,"fecha_tasa_monitor"=>cambiar_fecha_es($_SESSION['cfecha_conf'])

		));
	}
	
	public function info_config(){

		
		$cons = mysql_query("select * from configuracion where id_conf = 1");
		if(mysql_num_rows($cons)>0){
			$info = mysql_fetch_array($cons);
            $_SESSION['cinscripcion'] = $info['inscripcion'];
            $_SESSION['cbonos'] = $info['bonos'];
            $_SESSION['cporcentaje_patro'] = $info['porcentaje_patro'];
            $_SESSION['cfecha_conf'] = $info['fecha_conf'];
            $_SESSION['ctasa_compra'] = $info['tasa_compra'];
            $_SESSION['ctasa_venta'] = $info['tasa_venta']; 
            
            $_SESSION['pctasa_compra'] = $info['tasa_compra'] + ($info['tasa_compra'] *  $info['porc_tasa_compra']);
            $_SESSION['ptasa_compra'] = $info['tasa_compra'];
            
            $_SESSION['pctasa_venta'] = $info['tasa_venta'] + ($info['tasa_venta'] * $info['tasa_porc_venta']);
            $_SESSION['ptasa_venta'] = $info['tasa_venta'];
            
            $_SESSION['cporc_tasa_compra'] = $info['porc_tasa_compra'];
            $_SESSION['ctasa_porc_venta'] = $info['tasa_porc_venta'];
            $_SESSION['cinv_minima_bolivar'] = $info['inv_minima_bolivar'];
            $_SESSION['cinv_minima_dolar'] = $info['inv_minima_dolar'];
            $_SESSION['cretiro_bolivar'] = $info['retiro_bolivar'];
            $_SESSION['cretiro_dolar'] = $info['retiro_dolar'];
		}
				

	}
	
	
	public function cambio_bs_dolar(){
		$mensaje = '';
		$resp = 0;
		$monto = 0;
		
		if($_POST['monto'] <= 0){
			$mensaje = 'El monto a convertir debe ser mayor a 0,00 .';
			echo json_encode(array("salida"=>0,"mensaje"=>$mensaje));
			return ;
		}
		if($_POST['monto'] > floatval(miscofre::cofre_divisas('mis_depositos','BsS'))){
			$mensaje = 'No posee fondos suficientes para realizar la compra, por favor aumente o recargue su portafolio.';
			echo json_encode(array("salida"=>0,"mensaje"=>$mensaje));
			return ;
		}
		
		$cons = mysql_query("select * from configuracion where id_conf = 1");
		if(mysql_num_rows($cons)>0){
			$info = mysql_fetch_array($cons);
			/*HNG le vende al cliente los dolares*/
			$monto = $_POST['monto'] / ($info['tasa_compra'] + ($info['tasa_compra'] * $info['porc_tasa_compra']));
			$resp = 1;
			$_SESSION['monto_divisa'] = $monto;
			$_SESSION['tipo_divisa'] = $_POST['tipo'];
			$_SESSION['monto_compra'] = $_POST['monto'];
			$_SESSION['inv_minima_dolar'] = $info['inv_minima_dolar'];
			$_SESSION['compra_minima_dolar'] = $info['compra_minima_dolar'];
		}
				
		echo json_encode(array("salida"=>$resp,"monto"=>number_format($monto,2,',','.')." USD$.","montoletra"=>numtoletrasdolar(round($monto,2))));
	}
	
	public function cambio_dolar_bs(){
		$mensaje = '';
		$resp = 0;
		$monto = 0;
		
		if($_POST['monto'] <= 0){
			$mensaje = 'El monto a convertir debe ser mayor a 0,00 .';
			echo json_encode(array("salida"=>0,"mensaje"=>$mensaje));
			return ;
		}
		if($_POST['monto'] > floatval(miscofre::cofre_disponible('mis_depositos_divisa','USD'))){
			$mensaje = 'No posee fondos suficientes para realizar la compra, por favor aumente o recargue su portafolio.';
			echo json_encode(array("salida"=>0,"mensaje"=>$mensaje));
			return ;
		}
		$cons = mysql_query("select * from configuracion where id_conf = 1");
		if(mysql_num_rows($cons)>0){
			$info = mysql_fetch_array($cons);
			/*HNG le vende al cliente los bolivares*/
			$monto =  $_POST['monto'] * ($info['tasa_venta'] + ($info['tasa_venta'] * $info['tasa_porc_venta']));
			$resp = 1;
			$_SESSION['monto_divisa'] = $monto;
			$_SESSION['tipo_divisa'] = $_POST['tipo'];
			$_SESSION['monto_compra'] = $_POST['monto'];
		}
				
		echo json_encode(array("salida"=>$resp,"monto"=>masmenos($monto)." BsS.","montoletra"=>numtoletras(round($monto,2))));
	}	
	
	
	public function procesar_compra(){
		$resp = 0;
		$salida = 'La compra no fue realizada, es posible que no posea saldo.';
		if($_SESSION['tipo_divisa']==2){
		mysql_query("insert into mis_depositos values (NULL,'".$_SESSION['id_c']."',NOW(),'Compra de ".number_format($_SESSION['monto_divisa'],2,",",".")." bolivares por un monto de ".number_format($_SESSION['monto_compra'],2,",",".")." USD$.',NOW(),'".$this->referencias()."','".$_SESSION['monto_divisa']."',1,'COMPRA')");
				
		mysql_query("insert into mis_depositos_divisa values (NULL,'".$_SESSION['id_c']."',NOW(),'Compra de ".number_format($_SESSION['monto_divisa'],2,",",".")." bolivares por un monto de ".number_format($_SESSION['monto_compra'],2,",",".")." USD$.',NOW(),'".$this->referencias_dolar()."','".$_SESSION['monto_compra']."',2,'RETIRO')");
		
	
		$salida = 'Compra exitosa, revise sus portafolios. **';
		$resp = 1;
		}else if($_SESSION['tipo_divisa']==1){	
		    //var_dump($_SESSION['monto_divisa'], $_SESSION['inv_minima_dolar']);
		    if($_SESSION['monto_divisa']<$_SESSION['compra_minima_dolar']){
		        echo json_encode(array("salida"=>0, "resultado"=>"La compra minima de dolares es de ".$_SESSION['compra_minima_dolar']." USD$ recargue su portafolio en bolivares."));
		        return false;
		    }
		    
		mysql_query("insert into mis_depositos_divisa values (NULL,'".$_SESSION['id_c']."',NOW(),'Compra de ".number_format($_SESSION['monto_divisa'],2,",",".")." dolares  por un monto de ".number_format($_SESSION['monto_compra'],2,",",".")." BsS.',NOW(),'".$this->referencias_dolar()."','".$_SESSION['monto_divisa']."',1,'COMPRA')");
		
		mysql_query("insert into mis_depositos values (NULL,'".$_SESSION['id_c']."',NOW(),'Compra de ".number_format($_SESSION['monto_divisa'],2,",",".")." dolares  por un monto de ".number_format($_SESSION['monto_compra'],2,",",".")." BsS.',NOW(),'".$this->referencias()."','".$_SESSION['monto_compra']."',2,'RETIRO')");		
		
		$salida = 'Compra exitosa, revise sus portafolios. *';
		$resp = 1;
		}
		
		echo json_encode(array("salida"=>$resp,"resultado"=>$salida));
	}	

function referencias(){
	
	$nref=rand(100000, 99999999);
	
	$cons=mysql_query("select * from mis_depositos where referencia='".$nref."'");
	if(mysql_num_rows($cons)>0){
		$this->referencias();
	}else{
		return $nref;	
	}
}

function referencias_dolar(){
	
	$nref=rand(100000, 99999999);
	
	$cons=mysql_query("select * from mis_depositos_divisa where referencia='".$nref."'");
	if(mysql_num_rows($cons)>0){
		$this->referencias_dolar();
	}else{
		return $nref;	
	}
}



public function modificar(){
	

	
	$result = mysql_query("update configuracion set 
	porc_tasa_compra='".round(($_POST["pcompra"]/100),2)."', 
	tasa_porc_venta='".round(($_POST["pventa"]/100),2)."',
	tasa_compra='".$_POST["tcompra"]."',
	tasa_venta='".$_POST["tventa"]."',
	fecha_conf='".$_POST["fecha"]."'
	where id_conf = 1");
	$tasa_5poc = $_POST["tventa"]+($_POST["tventa"]*round(($_POST["pventa"]/100),2));

	mysql_query("update monedas set equivale_dolar='".$tasa_5poc."' where indice=1");
	
	if($result){
		$resp = 1;
		$mensaje ="Los datos fueron actualizados exitosamente"; 
	}else{
		$resp = 0;
		$mensaje ="Los datos no fueron actualizados, verifique e intente de nuevo."; 		
	}
	
	echo json_encode(array("salida"=>$resp,"mensaje"=>$mensaje));
}







	
}
$obj = new divisa;


if($_POST['tipo']==1){ $obj->cambio_bs_dolar();}
if($_POST['tipo']==2){ $obj->cambio_dolar_bs();}
if($_POST['tipo']==3){ $obj->procesar_compra();}
if($_POST['opc']==4){ $obj->mis_cofres();}
if($_POST['opc']==5){ $obj->modificar();}