<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <!--Archivos de calendario-->
	<script src="js/src/js/jscal2.js"></script>
    <script src="js/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="js/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="js/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="js/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  
  <link rel="stylesheet" href="js/bootstrap3/css/bootstrap.min.css" media="screen">
  <link rel="stylesheet" href="css/estilo_dey.css" type="text/css">
  <link rel="stylesheet" href="css/estilo_input.css">
  <script src="js/jq.min.js"></script>
  <script src="js/bootstrap3/js/bootstrap.min.js"></script>
  <script src="js/mensajes.js"></script>
  <script src="js/control/proceso.js"></script>
  
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#about">About</a></li>
        <li><a href="#contact">Contact</a></li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>

<div class="container-fluid">

  <form class="form-horizontal" role="form">
    <div class="row">
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputEmail" class="col-md-4 control-label">Email:</label>
          <div class="col-md-8">
            <input type="email" class="form-control" id="inputEmail" placeholder="Email">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputPassword" class="col-md-4 control-label">Password:</label>
          <div class="col-md-8">
            <input type="password" class="form-control" id="inputPassword" placeholder="Password">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel3" class="col-md-4 control-label">Label 3:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="inputLabel3" placeholder="Label 3">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="inputLabel4" class="col-md-4 control-label">Label 4:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="inputLabel4" placeholder="Label 4">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input5" class="col-md-4 control-label">1234567890:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input5" placeholder="input 5">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input6" class="col-md-4 control-label">123456789012:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input6" placeholder="input 6">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input7" class="col-md-4 control-label">12345678901234:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input7" placeholder="input 7">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input8" class="col-md-4 control-label">1234567890123456:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input8" placeholder="input 8">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input9" class="col-md-4 control-label">123456789012345678:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input9" placeholder="input 9">
          </div>
        </div>
      </div>
      <div class="col-sm-6 col-lg-4">
        <div class="form-group">
          <label for="input10" class="col-md-4 control-label">12345678901234567890:</label>
          <div class="col-md-8">
            <input type="text" class="form-control" id="input10" placeholder="input 10">
          </div>
        </div>
      </div>
    </div><!-- /.row this actually does not appear to be needed with the form-horizontal -->
  </form>
  <p>Note: label text will occupy as much space as the text takes regardless of the 
      column size, so be sure to validate your spacing.
  </p>
</div><!-- /.container -->


</body>
</html>