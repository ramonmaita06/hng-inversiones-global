<?php


$infoModal='<!--ventana modal-->			
  <!-- Trigger the modal with a button -->
  
  <!-- Modal -->
  <div class="modal fade" id="modalEncuesta" role="dialog" >
    <div class="modal-dialog" data-backdrop="static" data-keyboard="false">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header"><b><span style="color:#317BB6"><big>ENCUESTA HNG-INTERNET</big></span></b>
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        </div>
        <div class="modal-body">
          
          <div class="row">
            <div class="form-group">
              <div class="col-md-12">
                <div class="col-md-12" id="">
   
   <div class=" alert alert-info" style="text-align: justify; font-size:16px; font-weight:bold;">Estimad@ cliente, a continuación se le realizará una encuesta sencilla con el objetivo de conocer la situación actual de cada localidad referente al funcionamiento del servicio de internet dentro del alcance de su localidad de residencia.</div>
<br>
<br>
<div style="color:#122B40; font-size:16px; font-weight:bold;"><i>

<!--¿Está de acuerdo con adquirir servicio de internet satelital en su localidad de residencia?-->

¿Desearía recibir internet a través de HNG?
</i>
<br>
<table class="table table-hover table-striped">
<tr>
<td align="right">Si</td><td align="left"><input type="radio" name="respE" style="transform: scale(2);" id="resp1" value="Si"></td>
<td align="right">No</td><td align="left"><input type="radio" name="respE" style="transform: scale(2);" id="resp2" value="No"></td>
</tr>
<tr>
<td colspan="4"><hr><center><button onclick="responder()" id="btnEncuesta" class="btn btn-success"><i class="fa fa-send"></i> Responder</button></center></td>
</tr>
</table>

</div>






                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="modal-footer">
          <!--<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
<script>

setTimeout(function(){
 // $("#modalEncuesta").modal("show");
  $("#modalEncuesta").modal({"backdrop": "static"});
},3000);

function responder(){
    $("#btnEncuesta").attr({"disabled":true});
    var respE="";
    if(($("#resp1").prop("checked")==false)&&($("#resp2").prop("checked")==false)){
      swal("","Estimad@ cliente, debe escoger una de las opciones de la pregunta.","info");
      $("#btnEncuesta").attr({"disabled":false});
      return false;
    }
    if($("#resp1").prop("checked")==true){ respE = "Si";}
    if($("#resp2").prop("checked")==true){ respE = "No";}

  $.post("php/proceso_encuesta.php",{opc:1,respE:respE},function(resp){
    if(resp.salida==1){
        $("#btnEncuesta").attr({"disabled":false});
        swal("",resp.msn,"success");
        $("#modalEncuesta").modal("hide");
    }else{
        $("#btnEncuesta").attr({"disabled":false});
        swal("",resp.msn,"error");
    }
  },"json");

}


</script>
  ';


  ?>