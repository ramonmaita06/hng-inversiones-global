
<div class="modal fullscreen-modal fade" id="modalCuentasHNGNacional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-list-alt fa-2x text-danger"></i> <span class="text-danger" style="font-weight: bold;" id="modaltitle1">CUENTAS AUTORIZADAS</span>

        </div>
        <div class="modal-body">
          <div><b><big><center><img src="images/monedas_paises/ve.gif" style="width:32px"> CUENTAS NACIONALES <img src="images/monedas_paises/ve.gif" style="width:32px"></center></big></b></div>
      
            <pre style="background: white">
    <a href="http://bancodevenezuela.com" target="_BLANK"><img src="images/bv.jpg" style="width:120px" title="Pulse para ir al banco"></a>
    BANCO DE VENEZUELA
    JOSE GREGORIO PAREDES ANDRADES (AUTORIZADA)
    C.I: V-13.131.611
    CUENTA CORRIENTE
    <b>N&deg; 01020462310005457991</b>
    joseparedes_empresario@hotmail.com 
    <span style="color:blue"><b>Solo Depositos, Transferencias y Pago Movil</b></span>
            </pre>
        

      
            <pre style="background: white">
    <a href="http://mercantilbanco.com/" target="_BLANK"><img src="images/mercantil.jpg" style="width:120px"  title="Pulse para ir al banco"></a>
    BANCO MERCANTIL
    HOMBRES DE NEGOCIOS GLOBAL C.A
    RIF: J-40452736-2
    CUENTA CORRIENTE
    <b>N&deg; 01050134211134145497</b>
    pagos.hng@gmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>
   
          

    
            <pre style="background: white">
    <a href="http://www.venezolano.com/" target="_BLANK"><img src="images/bvc.png" style="width:120px" title="Pulse para ir al banco"></a>
    BANCO VENEZOLANO DE CREDITO
    HOMBRES DE NEGOCIOS GLOBAL C.A
    RIF: J-40452736-2
    CUENTA CORRIENTE
    <b>N&deg; 01040036830360119874</b>
    pagos.hng@gmail.com 
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>
     
            <pre style="background: white">
    <a href="https://www.provincial.com/" target="_BLANK"><img src="images/provincial.jpg" style="width:120px"  title="Pulse para ir al banco"></a>
    BANCO PROVINCIAL
    HOMBRES DE NEGOCIOS GLOBAL C.A
    RIF: J-40452736-2
    CUENTA CORRIENTE
    <b>N&deg; 01080165660100032528</b>
    pagos.hng@gmail.com 
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>
  
            <pre style="background: white">
    <a href="http://www.bnc.com.ve/" target="_BLANK"><img src="images/bnc.jpg" style="width:120px" title="Pulse para ir al banco"></a>
    BANCO NACIONAL DE CREDITO
    JOSE PAREDES
    CI: 13.131.611
    CUENTA CORRIENTE
    <b>N° 01910074062174000294</b>
    joseparedes_empresario@hotmail.com
    <b>Pago Movil:</b>
    Telefono: 0414-8590115
    <span style="color:blue"><b>Solo Depositos, Transferencias y Pago Movil</b></span>
          </pre>
  
            <pre style="background: white">
    <a href="http://www.banesco.com/" target="_BLANK"><img src="images/banesco.jpg" style="width:120px" title="Pulse para ir al banco"></a>      
    BANCO BANESCO
    JOSE PAREDES
    CI: 13.131.611
    CUENTA CORRIENTE
    <b>N° 01340186171863061020</b>
    joseparedes_empresario@hotmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>

            <pre style="background: white">
    <a href="http://www.bicentenariobu.com/" target="_BLANK"><img src="images/bicentenario.jpg" style="width:120px" title="Pulse para ir al banco"></a> 
    BANCO BICENTENARIO 
    JOSE PAREDES
    CI: 13.131.611 
    CUENTA CORRIENTE
    <b>N° 01750067890000004178</b>
    joseparedes_empresario@hotmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>

            <pre style="background: white">
    <a href="http://www.bod.com.ve/" target="_BLANK"><img src="images/bod.jpg" style="width:120px"  title="Pulse para ir al banco"></a>
    BANCO OCCIDENTAL DE DESCUENTO (BOD)
    HOMBRES DE NEGOCIOS GLOBAL C.A
    RIF: J-40452736-2
    CUENTA CORRIENTE
    <b>N&deg; 01160172330031362117</b>
    pagos.hng@gmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>


            <pre style="background: white">
    <a href="http://www.100x100banco.com" target="_BLANK"><img src="images/100x.png" style="width:120px" title="Pulse para ir al banco"></a>  
    100% BANCO
    JOSE PAREDES
    CI: 13.131.611
    CUENTA CORRIENTE
    <b>N° 01560038120000878272</b>
    joseparedes_empresario@hotmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>
  
            <pre style="background: white">
    <a href="http://www.bancocaroni.com.ve/" target="_BLANK"><img src="images/caroni.jpg" style="width:120px"  title="Pulse para ir al banco"></a>       
    BANCO CARONI  
    JOSE PAREDES 
    CI: 13.131.611 
    CUENTA CORRIENTE
    <b>N° 01280533143310013759</b>
    joseparedes_empresario@hotmail.com
    <span style="color:blue"><b>Solo Depositos y Transferencias</b></span>
          </pre>
  

        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>






<div class="modal fullscreen-modal fade" id="modalCuentasHNG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-list-alt fa-2x text-danger"></i> <span class="text-danger" style="font-weight: bold;" id="modaltitle1">CUENTAS AUTORIZADAS</span>

        </div>
        <div class="modal-body">
          <div><b><big><center>CUENTAS INTERNACIONALES</center></big></b></div>
          <p>
            <pre style="background: white">
    <a href="https://www.banesco.com.pa/" data-toggle="tooltip" title="Ir a  Banesco" target="https://www.banesco.com.pa/"><img src="images/banesco.jpg" width="120"></a>
    <b>BANESCO PANAMÁ</b>
    Titular: Alexis León
    Cuenta Corriente
    Nº Cuenta: 201800143794
    Correo: alexisdld02@gmail.com
    <b>Transferencias y Depositos en <span style="color:blue">Dolares</b></span>
          </pre>
          </p>

          <p>
            <pre style="background: white">
    <a href="https://www.zellepay.com/" data-toggle="tooltip" title="Ir a ZELLE" target="https://www.zellepay.com/"><img src="images/zelle.png" width="120"></a>
    <b>WELLS FARGO / ZELLE</b>
    Titular: Charles Sánchez
    Cuenta Cheque #1010260440635
    Routing Nuber 026012881
    Dirección: 5814 NE 4TH CT APT 102 Miami, FL 331372693
    Código postal 33137
    Correo: charles-sanchez@hotmail.com
    <b>Transferencias en <span style="color:blue">Dolares</b></span>
          </pre>
          </p>
          

          <p>
            <pre style="background: white">
    <a href="https://paypal.me/deydeco" data-toggle="tooltip" title="Ir a Paypal" target="https://paypal.me/deydeco"><img src="images/paypal.png" width="120"></a>
    <b>PayPal</b>
    Titular: Dey Rodriguez
    <b>deydeco@gmail.com</b>
    RIF: V-17.046.042-8
    <b>Transferencias en <span style="color:blue">Dolares y Euros</span></b>
          </pre>
          </p>

          <p>
            <pre style="background: white">
    <a href="https://www.grupobancolombia.com/wps/portal/personas" data-toggle="tooltip" title="Ir a Bancolombia" target="https://www.grupobancolombia.com/wps/portal/personas"><img src="images/bancolombia.png" width="120"></a>
    <b>BANCOLOMBIA</b>
    Titular: DANIELA RODRIGUEZ
    Cuenta Nº 912015282-11
    C.I.: 1.127.051.416
    <b>Transferencias y Depositos en <span style="color:blue">Pesos Colombianos</span></b>
          </pre>
          </p>

      <p>
            <pre style="background: white">
    <a href="https://www.novobanco.pt/site/cms.aspx" data-toggle="tooltip" title="Ir a  Novo Banco" target="https://www.novobanco.pt/site/cms.aspx"><img src="images/novobanco.jpg" width="120"></a>
    <b>NOVO BANCO</b>
    Titular: LUIS PERESTRELO DE VASCONCELOS
    Cuenta Nº 000482122302
    NIB: 0007 0000 00482122302 23
    IBAN: PT50 0007 0000 0048 21223022 3
    SWIFT / BIC: BESCPTPL
    <b>Depositos y Transferencias en <span style="color:blue">Euros</b></span>
          </pre>
          </p>



      <p>
            <pre style="background: white">
    <a href="https://www.santander.pt/en_GB/Personal.html" data-toggle="tooltip" title="Ir a  Santander" target="https://www.santander.pt/en_GB/Personal.html"><img src="images/santander.png" width="120" style="background:red"></a>
    <b>SANTANDER</b>
    Titular: LUIS PERESTRELO DE VASCONCELOS
    NIB: 0018 0003 51684553020 95
    IBAN: PT50 0018 0003 51684553020 95
    SWIFT / BIC: TOTAPTPL
    <b>Depositos y Transferencias en <span style="color:blue">Euros</b></span>
          </pre>
          </p>


        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>





<!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  
  <!-- Modal -->
  <div class="modal fade" id="lecturasModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><big><big><big><b> <i class="fa fa-book text-warning"></i> LECTURAS PARA EL ÉXITO</b></big></big></big></center>
        </div>
        <div class="modal-body" id="">
          <center>
          <pre style="width:240px;"><a href="documentos/El_hombre_mas_rico_de_babilonia.pdf" target="l1"><img src="images/libro1.png" style="width:220px; height:240px"></a></pre>
          <pre style="width:240px;"><a href="documentos/EL_JUEGO_DEL_DINERO_ROBERT_T_KIYOSAKI_KIYOSAKI.pdf" target="l2"><img src="images/libro2.png" style="width:220px; height:240px"></a></pre>
          <pre style="width:240px;"><a href="documentos/EL_SECRETO_DEL_EXITO_EN_EL_TRABAJO_Y_EN_LA_VIDA_DONALD_TRUMP_BILL_ZANKER.pdf" target="l3"><img src="images/libro3.png" style="width:220px; height:240px"></a></pre>
      </center>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>



<!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="vmodal"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>


<!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  
  <!-- Modal -->
  <div class="modal fade" id="modalClientePaises" role="dialog">
    <div class="modal-dialog" style="width: 70%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <center><b><big><big>CLIENTES REGISTRADOS EN HOMBRES DE NEGOCIOS GLOBAL C.A.</big></big><br>Generado en fecha: <?php echo date("d/m/Y H:i:s a"); ?><br>
            Total Clientes Registrados: N&deg; <span id="totalClientePaises" class="text-primary"></span></b></center>
        </div>
        <div class="modal-body" id="ClientePaises">Cargando información...</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>  
  
  <!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModaldivisa" role="dialog">
    <div class="modal-dialog" style="left:0%; width:90%">
    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="vmodal">
    <?php include("php/divisas.php"); ?>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
<div class="modal fade" id="miModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-required="true" style="font-size:16px"><i class="fa fa-times fa-1x"></i></span>
        </button>
        <div>
        <h4 class="modal-title" id="myModalLabel">
        <div class="user-header panel-title text-center bg-info">
      
          <br><strong ><big><big>Asistencia al Evento </big></big></strong>
          </div></h4>
      </div>
      <div class="modal-body">
        <div  class="row" >
          <div class=" table-condensad" >
                    <!-- inner menu: contains the actual data -->
                    <ul class=" text-center">
          <p>
            <b>Cupos Disponibles: <span id="cupos" class="label label-success" style="font-size:14px"></span> Boletos</b></p>
          
          <b>Precio del Boleto: <span id="precio" class="label label-default" style="font-size:14px"></span> BsS. / <span id="precio1" class="label label-default" style="font-size:14px"></span> USD.</b>
          <span id="pre1" style="display:none"></span>
          <span id="pre2" style="display:none"></span>
          
          
          </ul>
          </div>  
          
                      
          <div class=" asistir" >         
          <center>
          
        <table class="table table-condensad table-sm alert alert-warning">         
          <tr>
          <td  class="text-right" style="font-size:20px; font-weight:bold;">Boletos:
         </td>
          <td align="center" >          
          <select class="form-control" style="width:auto; font-size:16px" id="boletos">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>
          <option>7</option>
          <option>8</option>
          <option>9</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
          <option>13</option>
          <option>14</option>
          <option>15</option>
          </select>
          </td>
          <td>
          <button onclick="asistir()" class="btn  btn-default asistir" title="Asistir al evento"><i class="fa fa-check "></i> Reservar</button></td>
          </tr>
          </table>
  
          </center>

<table width="100%">
<tr><td colspan="2" class="bg-success"><center><big><big><b>Otros metodos de pago</b></big></big></center></td></tr>
<td width="50%" class="bg-warning" style="padding: 3px; border-right: 1px #B5E2C0 solid;">  
<b>TRANSFERENCIA / DEPOSITOS</b> <br>
<b>Banco Mercantil</b> <br>
<b>Cuenta Corriente</b><br>
<b>0105-0632-84-1632108283</b><br>
Titular:<b> Ninoska Isturis</b><br>
Cedula de Identidad:<b> V-14.574.450 </b><br>
</td>
<td class="bg-warning" style="padding: 3px;">
<b>PAGO MOVIL:</b><br>
Bancos:<b> Mercantil y Venezuela</b><br>
Telefono: <b>0412-2003377</b><br>
Cedula de Identidad:<b> V-14.574.450</b>  
<br>
<br>&nbsp;
</td>
<tr><td colspan="2" class="bg-success" style="text-align:justify; padding: 4px;"><b>Nota:</b> De usar esta opción, deberá llevar el día del evento el soporte de la transacción bancaria, o si desea cancelar en efectivo en ambas monedas (BsS / USD). <b>HNG no es la encargada de recibir depositos o transferencias bancarias para la compra de boletos, solo la cuenta aquí autorizada por HNG.</b></td></tr>
</table>

          </div>
        
          <center>
          <hr>
          <div class="alert bg-info">
            <div id="portafolioCompra" style="display:none; width:100%;" align="justify">
              <img src="images/cargar6.gif" style="width:24px;"> En espera del portafolio.<br>
            </div>
            <div id="paracomprar" style="display:none; width:100%;" align="justify"></div>
          </div>
          <hr>

          <div id="mostrarboletos" style="display:none; font-size:16px;"><span id="nbol" style="font-size:14px;"></span> <a href="boletos/boletos.php" target="Misboletos" class="btn btn-sm btn-info"><i class="fa fa-print text-default"></i> Imprimir</a>
          </div>
          </center>
          <div class="text-center text-danger">
            <strong></br><img src="images/hng-crucero.jpg" class="img-responsive"></br></strong>
          </div>
                </div>
      </div>
          
    </div>
  </div>
</div>
</div>
    
<!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="vmodal"></div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  <!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  <!-- Modal -->


<!--  fgd-->
<div class="modal fade" id="info">
      <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
    
          <!-- cabecera del diálogo -->
          <div class="modal-header">
            <div>
        <h4 class="modal-title" id="myModalLabel">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-required="true" style="font-size:16px"><i class="fa fa-times fa-1x"></i></span>
        </button>
        <div class="user-header panel-title text-center bg-info">
      
          <br><strong ><big><big>Condiciones que debe saber de HNG </big></big></strong>
          </div></h4>
      </div>
           
          </div>
    
          <!-- cuerpo del diálogo -->
          <div class="modal-body">
            
           
  <!---otro menu-->
 
    <div class="row">
        <div class="col-sm-3 col-md-3">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" ><span class="glyphicon glyphicon-list-alt">
                            </span> Contenido Informativo</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <span class="fa fa-file text-success"></span><a href="#" id="cambia" > Inversiones en Bolivares</a>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <span class="fa fa-file text-success"></span><a href="#" id="cambia1" > Condición única de los retiros de fondos</a>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <span class="fa fa-file text-success"></span><a href="#" id="cambia2" > Condición única de INVERSIONES de fondos</a>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <span class="fa fa-file text-success"></span><a href="#" id="cambia3" > Inversiones En Dólares</a>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        <span class="fa fa-file text-success"></span><a href="#" id="cambia4" > Comunicación General </a>
                                    </td>
                                </tr>
                                
                                
                                
                                
                            </table>
                        </div>
                    </div>
                </div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"  id="cambia5"><span class="glyphicon glyphicon-book">
                            </span> Recomendaciones</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <a href="#" id="cambia6" class="fa fa-file text-success"> Sueños</a> 
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#" id="cambia7" class="fa fa-file text-success"> 4 principios claves para alcanzar tus sueños </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a href="#" id="cambia8" class="fa fa-file text-success"> Metas</a>
                                    </td>
                                </tr>
                               
                            </table>
                        </div>
                    </div>
                </div>
        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a  href="documentos/CondicionesHNG.pdf" download="Condiciones de HNG" ><span class="glyphicon glyphicon-cloud-download">
                            </span> Descargar todo el contenido</a>
                        </h4>
                    </div>
                    
                </div>
        
                         
                         
                        
                
            </div>
        </div>
        <div class="col-sm-9 col-md-9" >
            <div class=" well text-justifygrid-block"> 
          <div id="texto"  style="display:none;">       
      <h3 class="text-center text-primary">Inversiones en Bolivares</h3></br>
      <ul>  <li>Los retiros de los fondos son depositado entre  6 a 72 horas hábil tomado de los días bancarios  (días hábiles Bancarios).</li>
      <li>  El mínimo retiro es el establecido en la interfaz.</li> 
      <li>  Debe tener una cuenta bancaria, y si es de un tercero debe  estas autorizada por el titular y por el afiliado  (solicitante copia de C.I  y la huella,  e igual el Afiliado). </li>
      </ul> 
    </div>
    
    <div id="texto1" style="display:none;">
<h3 class="text-center text-primary">
 Condición única de los retiros de fondos: </h3></br>
<ul><li>  Los retiros a partir del 10 de Diciembre será depositado el siguiente mes… el 10 de Enero.</li>
<li>  Le sugerimos que los retiros generados a partir del 10 de diciembre lo reinvierta para aprovechar el mejor rendimiento de HNG.</li>
</ul>
</div>
<div id="texto2" style="display:none;">
<h3 class="text-center text-primary">
Condición única de INVERSIONES de fondos</h3>    
<ul><li>  A partir de 15 de Diciembre hasta el 15 de Enero tendrá una aplicación única de INVERSION de un rendimiento de 120% por 50 días  para lo que desea invertir en esa fecha. Los afiliado  Tendrá las dos modalidades de inversión  la tradicional y la condición única de inversiones  de fondos en esa fecha.</li>
<li>  Los pagos de regalías  se calculan a partir de las inversiones… es decir hacer la inversión correspondiente no por el hecho que hizo el depósito o está en el portafolio sin invertirlo. </li>
<li>  Mínimos de inversión es el establecido en la interfaz. </li>
<li>  Monto mínimo de rendimiento el 50%  Máximo 120% Plus.</li>
<li>  Otros ingreso, por cada recomendado que invierta en HNG tendrá el 25%  primer nivel, el 2 % por el segundo nivel y  por el 3 nivel 10%  cuarto nivel 1% hasta el nivel 12 por tu línea de profundidad.</li>
<li>  A partir de una inversión de 80.000,00 por 840 (24 periodo de 35 días)  tendrá un rendimientos de 80% por cada periodo. </li>
<li>  Los bonos generados por los referidos serán liberado en 35 días continuo… será tomado de referencia del día que se haga la inversión del referido.  </li>
<li>  Mínimos días de inversión en Bolívares 35 días continuos.  </li>
</ul>
</div>

<div id="texto3" style="display:none;">
<h3 class="text-center text-primary">Inversiones En Dólares</h3>

<ul> <b>Para ganar los bonos (por referido) en Dólares:</b>
<li>  Tener  un mínimo de inversión en Dólares (mínimos 10$) con ese mínimo ganaría hasta el tercer nivel  que sería el 15% ,3%, 5%... </li>
<li>  Para  ganar del primer nivel hasta  el nivel 12 tendría que tener una inversión mayor de 100$ con ganancias del 15% ,3%, 5%... y 1% hasta el nivel 12 por tu línea de profundidad.</li>
<li>  Los bono generado por los referido serán liberado en 100 días continuo… será tomado de referencia del día que haga la inversión el referido.</li>

<li>  Para invertir en Dólares debe tener una inversión en Bolívares de un 5% de la inversión que hará en dólares. </li>
<li>  Cada vez que invierta en Dólares tendrá que invertir el 5% en Bolívares en carácter obligatorio. </li>
<li>  Mínimos días de inversión  en  Dólares 60 días</li>
<li>  El mínimos retiro en Dólares  es  100 Dólares</li>
<li>  Mínimos de inversión de 10 Dólares </li>
<li>  Monto mínimo de rendimiento el 15% en Dólares</li>
<li>  Otros ingresos, por cada recomendado que invierta en HNG tendrá el 15%  en Dólares para el  primer nivel, por el segundo nivel 3%,  por el tercer nivel 5% y por el cuarto nivel 1% hasta el nivel 12.</li>
<li>  Para poder disfrutar la profundidad del  Cuarto nivel hasta el nivel 12 tendrá que tener una Inversión mayor de 100$, tendrá cien (100) días para nivelar tu inversión.</li>  
<li>  HNG tendrá vacaciones colectiva (la administración)  del 10 de Diciembre hasta el 10 de Enero… el sistemas funcionara remotamente y automáticamente. </li>
<li>  Todos los caso generado del 10 de Diciembre hasta el 10 de Enero será resuelto a partir del 11 de Enero (con excepción al servicio de  Exequias e Inversiones que  esta operativamente las 24 hora y los 365 días del años).</li>
<li>  En el caso de las inversiones estará el sistema remotamente. </li>
<li>  HNG se ve en la necesidad de reajustar las condiciones de acuerdo a la aprobación del cuestionario plasmado en el contrato de afiliación.</li></br>
  <b>Nota:</b> todas las condiciones tendrán cambios sin preaviso… Honrando lo que ya están procesado.  </br></br>

<label class="text-center"><b>"El fracaso es huérfano, el éxito tienes doliente… todos querrán ser tus mejores amigos, familiares y socio"</b></label></br>
<label class="text-center">José Paredes</label></ul>
</div>

<div id="texto4" style="display:none;">
<h3 class="text-center text-primary">
Comunicación general</h3> <label class="text-justify" >HNG le agradece por  ser parte de la historia de 2018… esperando lograr nuestro objetivo en 2019… 
 Recuerda que “No son tus condiciones la que cambian o determinan tu destino; si no tus decisiones... Juntos conquistamos tus sueños.”</br> José paredes</label>
 </div>
<div id="texto5" style="display:none;" align="center" class="text-success">
Recomendaciones de sistemas educativos de HNG</br> para lograr nuestras metas en el <?php echo date("Y"); ?>. </br></br> "Primero Ser para  poder tener".
 </div>
<div id="texto6" style="display:none;">
<h3 class="text-center text-primary">Sueños</h3>
<ul>
<li>  Es la razón real. ¿Por qué está dispuesto hacer este negocio? Es como las raíces de un árbol no se ven por estar bajo de la tierra, pero son estas las bases y fundamentos que sostienen el crecimiento del árbol.</li>
<li>  Usted necesita una razón para identificar y construir el propósito de su vida. ¿cuáles son tus sueños? ¿Adónde quieres ir con esta oportunidad? ¿Tienes aun sueños que esperas hacer realidad?</li>
<li>  El soñar es parte natural de la vida, es el punto inicial de todo logro, si usted tiene sueños débiles, su dedicación, perseverancia y acciones serán a un nivel bajo y producirá pocos o ningún resultados.
</li>
</ul>
</div>
<div id="texto7" style="display:none;">
<h3 class="text-center text-primary">
4 principios claves para alcanzar tus sueños </h3>
<ul>
<li>  Un sueño claramente definido, fundamentado en el ardiente deseo de cumplirlo. 
<li>  Un plan definido, expresado en acción continúa. </li>
<li>  Una mente completamente cerrada a todas las influencias negativas y desalentadoras, incluyendo sugerencias negativas de familiares, amigos y conocidos. </li>
<li>  Una alianza amistosa con una o más personas que lo ayudaron a salir adelante con el sueño y el plan.</li>
</ul>
</div>

<div id="texto8" style="display:none;">
<h3 class="text-center text-primary">
Las metas</h3>
<ul><li>  Las metas proveen dirección y para establecerlas debemos determinar primero donde estamos ahora y a donde queremos llegar. Las metas serán los pequeños peldaños o niveles dentro del plan de compensación que debemos ir escalando para lograr nuestros sueños.</li>
<li>  A las metas que queremos lograr se les asignan fechas.</li>
<li>  Fijar una fecha significa comprometerse actuar.</li>
<li>  Metas sin acción son puras ilusiones. </li></ul>
</div>

            </div>
        </div>
    </div>



          </div>
    
          <!-- pie del diálogo -->
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
          </div>
    
        </div>
      </div>
    </div> 
  


<div  onclick="ver_evento(0)" title="Evento, Crucero" data-toggle="tooltip" id="info_eventos" style="display: none; position: fixed; top: 70px; right: 0px; background: rgb(255, 255, 255); border-radius: 5px 0px 0px 5px; padding: 10px 15px; font-size: 16px; z-index: 99999; cursor: pointer; color: rgb(60, 141, 188); box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px;"><i class="fa fa-bullhorn parpadea text-warning fa-2x"></i>
<label class="label label-success" style="position:absolute !important; margin-left:-10px !important; margin-top:-15px !important; font-size:10px !important;">1<label>
</div>



<div  id="contenido" style="display: none; padding: 10px; position: fixed; top: 70px; right: -250px; background: rgb(255, 255, 255); border: 0px solid rgb(221, 221, 221); width: 250px; z-index: 99999; box-shadow: rgba(0, 0, 0, 0.1) 0px 1px 3px;" data-toggle="tooltip" title="Mostrar información del evento 'HNG-Crucero'">
<img src="images/hng-crucero-mini.jpg"  title="Pulse para comprar"  width="200" data-toggle="modal" onclick="return ver_evento(1),consultaboleto();" style="cursor:pointer" data-target="#miModal">
</div>
  
  <script type="text/javascript">
  
  $(document).ready(function(){
      
    <?php
        if($_SESSION['veces']<4){
           echo ' setTimeout(function(){ $("#info").modal("show"); },3000); ';
        }
  ?>
 $("#texto, #texto2").toggle(1000); //activar informacion
 $("#cambia").click(function(){
 $("#texto").toggle(1000);
 });
 $("#cambia1").click(function(){
 $("#texto1").toggle(1000);
 });
 $("#cambia2").click(function(){
 $("#texto2").toggle(1000);
 });
 $("#cambia3").click(function(){
 $("#texto3").toggle(1000);
 });
 $("#cambia4").click(function(){
 $("#texto4").toggle(1000);
 });
 $("#cambia5").click(function(){
 $("#texto5").toggle(1000);
 });
 $("#cambia6").click(function(){
 $("#texto6").toggle(1000);
 });
 $("#cambia7").click(function(){
 $("#texto7").toggle(1000);
 });
 $("#cambia8").click(function(){
 $("#texto8").toggle(1000);
 });
});
  
  
  
</script> 
    
<script>



function ver_evento(n){
 if(n==0){
 $("#info_eventos").attr({"onclick":"ver_evento(1)"});
 $("#info_eventos").animate({"right":"250px"});
 $("#contenido").animate({"right":"0px"});

}else{
 $("#info_eventos").attr({"onclick":"ver_evento(0)"});
 $("#info_eventos").animate({"right":"0px"});
 $("#contenido").animate({"right":"-250px"});
}
}

ver_evento(0);

</script>     