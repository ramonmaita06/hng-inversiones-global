<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HNG</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />     
    <link href="css/sweetalert.css" rel="stylesheet">
    <link href="css/style_dorado.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body>
<form onsubmit="return false;">
<div class="panel panel-success" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading text-center"><big><big>CAMBIO DE CONTRASEÑA</big></big></div>
<div class="panel-body">
<div style="background:#fff;">
<br>
<div class="form-group">
<div class="row font" style="color:black;">
<div class="col-md-12">
<label class="col-md-6"><i class="fa  fa-unlock"></i> Contraseña Actual</label>
<input type="password" class="col-md-8 form-control" style="height:48px; font-size:20px; " id="claveA">
</div>
<div class="col-md-12">
<label class="col-md-6"><i class="fa  fa-lock"></i> Contraseña Nueva</label>
<input type="password" class="col-md-6 form-control" style="height:48px; font-size:20px; " id="claveN">
</div>
<div class="col-md-12">
<label class="col-md-6"><i class="fa  fa-lock"></i> Confirmar Contraseña Nueva</label>
<input type="password" class="col-md-6 form-control " style="height:48px; font-size:20px; " id="claveCN">
</div>
</div>
</div>



<br>

<br>
<center><button class="btn btn-success btn-lg" onclick="return cambiar_clave()"><i class="fa fa-key"></i> Cambiar</button></center>
</div>
</div>
</div>
</form>
<script>parent.cerrar_carga();</script>
<script src="js/jquery.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="js/sweetalert.dev.js"></script>
    <script src="js/wow.min.js"></script>
	
    <!-- jQuery UI 1.11.2 -->
    <script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>

</body>
</html>