<?php session_name("hng"); session_start(); 
date_default_timezone_set('America/Caracas');
if(isset($_SESSION["us"])){
include("php/cnx.php");
include("php/funciones.php");
?>
<!DOCTYPE html>
<html>
  <head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="Last-Modified" content="0">
  <meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
   <link rel="icon" href="images/favicon.gif" type="image/gif">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    

        <!-- FontAwesome 4.3.0 -->
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <!--<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->   
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style_inversiones.css" rel="stylesheet" type="text/css" />
     <link rel="stylesheet" type="text/css" href="bootstrap/datatables/datatables/datatables.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  
  
  
      <!-- jQuery 2.1.3 -->
  <link href="bootstrap/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    
  <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
  <script src="bootstrap/fileinput/js/fileinput.min.js"></script>
  <script src="js/script.js"></script>
  <script src="js/imagen.js"></script>
  <script src="js/number_format.js"></script>
  <script src="js/procesos_app.js"></script>
  <!-- <script src="js/procesos.js"></script> -->
  <!-- <script src="js/procesos_planes.js"></script> -->
  <script src="js/proceso_notifica.js"></script>
  <script src="js/proceso_evento.js"></script>
  <script src="js/proceso_renovar_contrato.js"></script>

    <script>
      $(function() {
        $('.cortina_kyc').hide();;
        $('.zoom').hover(function() {
            $(this).addClass('transition');
        }, function() {
            $(this).removeClass('transition');
        });
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           
            $('#pc').hide();
            $('#tel').show();

        }else{
          $('#tel').hide();
            $('#pc').show();
            // camara();
        }

        $(document).on('click', '#boton-c', function(event) {
          event.preventDefault();
          /* Act on the event */
          camara();
        });

        $(document).on('click', '#boton-a', function(event) {
          event.preventDefault();
          /* Act on the event */
          vidOff();
        });


        


      });

      function vidOff() { 
          video.pause(); 
          video.src = ""; 
          localstream.getTracks()[0].stop(); 
          console.log("Vid off"); 
      } 
      function preloader() {
        $('.cortina_kyc').show('slow/400/fast');
      }
      function cerrar_preloader() {
        setTimeout(function(){
          $('.cortina_kyc').hide('slow/400/fast');
        },1500);
      }

     

    </script>
    <?php 
    $sql = mysql_query("SELECT * FROM verificar_identidad WHERE cod_cliente='".$_SESSION['cod_id']."' AND estatus = 0 "); 
    $identidad_verificada = mysql_num_rows($sql);

    $contrato = mysql_query("SELECT * FROM contratos WHERE cod_cliente='".$_SESSION['cod_id']."' AND estatus = 1");
    if (mysql_num_rows($contrato) <= 0) {
        // $contrato_vencido = 5;
       $contratoc = mysql_query("SELECT * FROM cliente WHERE cod_id='".$_SESSION['cod_id']."' AND fecha_reg < '2019-09-01' ");
       if (mysql_num_rows($contratoc) > 0) {
        $contrato_vencido++;
        $modalC = 1;
       }
    }else{
       $contrato_vencido = 0;
    }



    if ($contrato_vencido > 0  || $identidad_verificada <= 0) {
      $bloqueo_menu = 10;
    }else{
      $bloqueo_menu = 0;

    }
  ?>
  <?php if ($identidad_verificada > 0): ?>
    <script>
          $.ajax({
            url: 'php/planes_proceso_menu.php',
            type: 'POST',
            dataType: 'json',
            data: {opc: 4, divisaR: $('#divisaR').val(), carteraR:$('#carteraR').val()},
          })
          .done(function(data) {
            console.log("success");
            // console.log(data);
            if (data.success==true) {
              $('#portafolio-act').html(data.saldo);
              $('#btn-acc').html(data.boton);
            }

          })
          .fail(function(e) {
            console.log("error");
            console.log(e);
          })
          .always(function() {
            console.log("complete");
            cerrar_preloader();
          });
       $(function() {
          preloader();

          $(document).on('click', '#btn-renovar-c', function(event) {
            event.preventDefault();
            preloader();
            if( $('#aceptar').prop('checked') ) {
              $.ajax({
                url: 'php/proceso_renovar_contrato.php',
                type: 'POST',
                dataType: 'json',
                data: {opc: 3},
              })
              .done(function(data) {
                console.log("success");
                // console.log(data);
                if (data.success==true) {
                  swal({title: "EXITO!", text:"Su contrato se ha renovado" , timer: 4000,  showConfirmButton: true});
                  setTimeout(function(){window.location.reload(); },1000);
                }


                if (data.error == true) {
                   swal({title: "Oops!", text:"Ha ocurrido un error.!" , timer: 4000,  showConfirmButton: true});
                }
              })
              .fail(function(e) {
                console.log("error");
                console.log(e);
              })
              .always(function() {
                console.log("complete");
                cerrar_preloader();
              });
            }else{
              cerrar_preloader();
              alert('debe de aceptar los terminos y condiciones de contrato.');
            }
          });
          // fileinput();
        });
    </script>
    
  <?php endif ?>
  </head>
  
  <style>
.ocultame{display:inline;}

#mensajes{height:350px;}
 @media only screen and (max-width: 930px){
.ocultame{display:none;}
#mensajes{height:300px;}
.chat{width:99%;}
.adaptame{
  background:red;
  width:150%;
  float:center;
}
.content-header{display:none;}
}
.cortina_ayuda{
  position: fixed;
  z-index:99999;
  width: 100%;
  height: 100%;
  background: #000;
  opacity: .6;
  left: 0%;
  top: 0%;
}

.ayuda{
  position:absolute;
  z-index: 100000;
  background: #fff;
  width: 80%;
  left: 10%;
  top: 3%;
  border-radius:5px;
}


.notifi{
  position:absolute;
  z-index: 100000;
  background: #fff;
  width: 50%;
  left: 25%;
  top: 10%;
  border-radius:5px;
}


.estateros{ font-weight: bold; }
.estateros:hover{
  background:#222D32;
}

.cortina_kyc{
  position: fixed;
  z-index:99999;
  width: 100%;
  height: 100%;
  background: #fff;
  opacity: .6;
  left: 0%;
  top: 0%;
}


img.zoom {
    /*width: 350px;*/
    /*height: 200px;*/
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    -o-transition: all .2s ease-in-out;
    -ms-transition: all .2s ease-in-out;
}
 
.transition {
    -webkit-transform: scale(1.8); 
    -moz-transform: scale(1.8);
    -o-transform: scale(1.8);
    transform: scale(1.8);
}
</style>
  
  <body name="tope" class="skin-yellow" <?php if( $bloqueo_menu <= 0) { ?>  onload="return app_inversiones('app_5x5/index.php')" <?php } ?> >

<?php 
  if($_SESSION['mostrar_ayuda']==1){
?>
  <div class="cortina_ayuda"></div>
  <div class="ayuda">
    <?php include("ayuda/index.php"); ?>
  </div>
<?php 
  }else{

    if(comprobar_fecha('2019-10-31 18:00:00')>0){
?>
  <!--<div class="cortina_ayuda cnotifi"></div>-->
  <div class="notifi" >
    <center><img src="images/comunicado_hng.jpg" data-placement="bottom" class="img-responsive" data-toggle="tooltip" title="Pulse para cerrar la notificación" onclick="$(this,'.notifi').hide(500,'linear');"></center>
  </div>
<?php 
  }else{
   // echo '<script>location.href="php/salir.php";</script>';
  }
?>



  <div class="cortina_ayuda" style="display: none"></div>
  <div class="ayuda" style="display: none">
    <?php include("ayuda/index.php"); ?>
  </div>
<?php  
  }
    //echo $_SESSION['encuesta'];
?>
  
  




  
  
  
    <div class="wrapper" style="background:#000">
      
      <header class="main-header">
        <!-- Logo -->
        <!--<a href="../../" class="logo"></a>-->
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">

          <!-- Sidebar toggle button-->


          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Navegación</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <?php 
                if($_SESSION['id_c']==1 || $_SESSION['id_c']==2){
              ?>
                <li class="estateros" data-taggle="tooltip" title="Banco Estatero (Admin)"><a href="#" onclick="app_inversiones('multicajero/')" ><i class="fa fa-bank" style="font-size:16px"></i><span class="ocultame"> Banco Estatero</span></a></li>
              <?php
                }
              ?>
              <?php 
                
                $cons0 = mysql_query("select * from cajero where id_c='".$_SESSION['id_c']."' AND estatus=1");
                if(mysql_num_rows($cons0)>0){
                  $info = mysql_fetch_array($cons0);
               
              ?>
              <li class="estateros" data-taggle="tooltip"  title="Multicajero Estatero HNG"><a href="#" onclick="app_inversiones('multicajero/indexCajero')"><i class="fa fa-barcode" style="font-size:16px"></i><span class="ocultame parpadea" > Multicajero Estatero HNG</span></a></li>

              <?php 
                }
              ?>

              <li class="dropdown notifications-menu" onclick="return recarga_cofreBTN()">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-briefcase" style=" text-shadow: -1px 1px 2px #888;"><span style="font-family: 'Inconsolata'; font-weight: bold; font-size: 16px; color:#E7CC83; font-style:italic"> Mis Portafolios</span></i>
                </a>
                <ul class="dropdown-menu" style="width:500px">
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">

                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">                    
                        <div  id="cofre" class="cofre"  data-placement="bottom" style="color:white;"></div>
                      </a>

                    </ul>
                  </li>
                </ul>
              </li>


              <!-- Messages: style can be found in dropdown.less-->
      <!--<li class="dropdown" style="background:#000;" onclick="return muestraPortafolios()">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="ocultame"></span>
                 <i class="fa fa-briefcase"></i>
                  <span  id="cofre" class="cofre" title="Mis bonos de inversiones" onclick="" data-toggle="tooltip" data-placement="bottom" style="color:white;">0,00 </span>
                </a>
      </li>     --> 




              <li class="dropdown notifications-menu" onclick="return mostrar_notas()">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <span id="mis_notas"><i class="fa fa-bell-o"></i></span>
                  <span class="label label-danger parpadea"  id="nnotifica">0</span>
                </a>
                <ul class="dropdown-menu">
                  <li class="header"  id="nnoti">Tienes 0 notificaciones</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu"  id="lnotifica"></ul>
                  </li>
                  <li class="footer">         <center><table class="table bg-success table-condensad">
          <td align="center"><a href="#" onclick="return mis_carteras('php/gestion_noti.php?opc=crear');" class="btn btn-ms btn-success" title="Crear notificación"><i class="fa fa-edit "></i></a></td>
          <td align="center"><a href="#" onclick="return mis_carteras('php/gestion_noti.php');" class="btn btn-ms btn-warning" title="Ver notificaciónes"><i class="fa  fa-comment-o "></i></a></td>
          </table></center></li>
                </ul>
              </li>


              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img id="imagenp2" src="<?php echo $_SESSION["imagend"]; ?>" class="user-image" />
                  <span class="hidden-xs"><?php echo $_SESSION["cliente"]; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header" style="margin-top:-2px; margin-left:-1px; margin-right:-1px;">
                    <img id="imagenp3" src="<?php echo $_SESSION["imagend"]; ?>" class="img-circle" />
                    <p>
                      <?php echo $_SESSION["cliente"]; ?>
                      <small>Fecha de Afiliación: <b> <?php echo $_SESSION["fecha_reg"]; ?></b></small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <li class="user-body" data-toggle="tooltip" title="Pulse para referenciar personas">
                    <div class="col-xs-12 text-center">
                      <button class="btn-limpio" onclick="return mis_carteras('php/referenciar.php');">Recomendar <i class="fa fa-link"></i></button>
                    </div>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer" style="font-size:12px; font-family:arial;color:black">
                    <!--<button onclick="javascript:location.href='#'" class="pull-left font btn btn-primary btn-sm" ><i class="glyphicon glyphicon-user" class="color:red"></i> Mi Perfil</button>-->
                    
                    
                      <button onclick="javascript:location.href='php/salir.php';" class="pull-right font btn btn-danger btn-sm" ><i class="glyphicon glyphicon-off"></i> Cerrar Sesión</button>
                   
                  </li>                  <!-- Menu Footer-->

                </ul>
              </li>
            </ul>
          </div>
        </nav>

      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar" style="background:#000">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar" >

          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-center image" style="background:white; border-radius:15px; cursor:pointer"  onclick="return mis_carteras('php/imagen.php')" title="Pulse para cambiar la imagen de perfil">
              <img id="imagenp1" src="<?php echo $_SESSION["imagend"]; ?>"  style="width:80%; height:80%; left:10%; position:relative" />
            </div>
            <div class=" info">
              <p style="font-size:12px;" align="center"><?php echo $_SESSION["cliente"]; ?></p>
              <p style="font-size:12px;" align="center"><?php echo $_SESSION["cod_id"]; ?></p>        

              <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
            </div> 

        <a href="https://hombresdenegociosglobalca.com/principal" target="1" title="Ir al portal de HNG" data-toggle="tooltip">
          <!--<img src="images/logo_oficial_hng.png" class="img-responsive">-->
          <center><img class="img-responsive" src="images/logo_oficial_new_acceso.png" style="width:250px;"></center>
        </a>
          </div>

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li title="Pulse para ir a inversiones" class="header text-center" style="cursor:pointer; font-weight:bold; font-size:24px; font-family: 'Inconsolata';  color:#E7CC83; font-style:italic" <?php if( $bloqueo_menu <= 0) { ?>  onclick="return app_inversiones('app_5x5/index.php')" <?php } ?>><i class="fa fa-bar-chart-o text-warning"></i> INVERSIONES</li>
     
        <?php 
        // include("php/cnx.php");
        //echo "SELECT * FROM verificar_identidad WHERE cod_cliente=$_SESSION['cod_id'] AND estatus = 0 ";
        $sql = mysql_query("SELECT * FROM verificar_identidad WHERE cod_cliente='".$_SESSION['cod_id']."' AND estatus = 0 ");
          if($bloqueo_menu <= 0){
        ?>
        <li class="treeview">
              <a href="#">
                <i class="fa fa-folder-open-o"></i> <span>Mis Operaciones</span> <span class="label label-default parpadea hidden" style="color:green;">Nuevo</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="https://hombresdenegociosglobalca.com/inversiones/registro/?cod=<?php echo $_SESSION['cod_id']; ?>" target="inscribir"><i class="fa fa-file-text text-warning"></i> Registrar Referido</a></li>
                <li><a href="#" onclick="return app_inversiones('app_5x5/index.php')"><i class="fa fa-bar-chart-o text-warning"></i> Invertir</a></li>
                
          <li><a href="#" onclick="return app_inversiones('app_5x5/retirar.php')"><i class="fa fa-suitcase text-success"></i> Retirar Fondos</a></li>
        
                <li><a href="#" onclick="return mis_carteras('mis_operaciones/recargas.php')"><i class="fa fa-refresh text-primary"></i> Recargas</a></li>

                <li><a href="#" onclick="return mis_carteras('precargado/app/')"><i class="fa fa-barcode text-default"></i> Codigos Precargados</a></li>
        
                <li><a href="#" onclick="return comprar_divisa()"><i class="fa fa-dollar text-default"></i> Comprar Divisas</a></li>        
        
                <!--<li><a href="#" onclick="return mis_carteras('mis_operaciones/bonos.php')"><i class="fa  fa-gift text-warning"></i> Bonos</a></li>-->
               </ul>
          </li> 
      
      
          <li class="treeview">
              <a href="#">
                <i class="fa fa-gift"></i> <span>Mis Bonos</span> <i class="fa fa-angle-left pull-right"></i>
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="label label-success" id="nbonos">0</span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#" onclick="return mis_carteras('bonos/bloqueados.php')"><i class="fa fa-lock text-danger"></i> Bonos Bloqueados</a></li>
                <li><a href="#" onclick="return mis_carteras('bonos/liberados.php')"><i class="fa fa-unlock text-success"></i> Bonos Liberados</a></li>
               </ul>
          </li>   
 
          <li class="treeview">
              <a href="#">
                <i class="fa fa-user"></i> <span>Mis Datos</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <!--<li><a href="#" onclick="return mis_carteras('php/perfil.php')"><i class="fa fa-edit"></i> Informacion Personal</a></li>-->
                <li><a href="#" onclick="return mis_carteras('usuario/index.php')"><i class="fa fa-edit text-primary"></i> Actualizar Datos</a></li>
                <li><a href="#" onclick="return mis_carteras('php/clave.php')"><i class="fa fa-unlock text-success"></i> Cambio de Contraseña</a></li>
                <li><a href="#" onclick="return mis_carteras('php/imagen.php')"><i class="fa fa-picture-o text-warning"></i> Imagen de Perfil</a></li>
                <li><a href="#"   data-toggle="modal"   data-target="#kyc" data-backdrop="static" data-keyboard="false"><i class="fa fa-picture-o text-warning"></i> KYC</a></li>
                <li><a href="#" onclick="return mis_carteras('usuario/bancaria.php')"><i class="fa fa-credit-card text-danger"></i> Datos Bancarios</a></li>
                <li><a href="#" onclick="return mis_carteras('php/micontrato.php')"><i class="fa fa-file text-warning"></i> Mi Contrato <i class="fa fa-star parpadea" style="color:orange;"></i> </a></li>
               </ul>
          </li>
      

          <li class="treeview">
            <a href="#">
              <i class="fa  fa-cogs"></i> <span>Más Opciones</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li class="text-left">
                <a href="#">
                <i class="fa fa-link text-success"></i>
                <button class="btn-limpio" onclick="return mis_carteras('php/referenciar.php');">Recomendar</button>
                </a>
              </li>
              <li class="text-left">
                <a href="#" onclick="return mis_carteras('php/gestion_noti.php?opc=crear');"  title="Crear notificación"><i class="fa fa-edit text-success"></i> Crear Notificación</a>
              </li>
              <li class="text-left">
                <a href="#" onclick="return mis_carteras('php/gestion_noti.php');"  title="Leer notificaciones"><i class="fa fa-comment-o text-warning"></i> Leer Notificaciones</a>
              </li>


              <?php if(($_SESSION['id_c']==1)||($_SESSION['id_c']==2)){ ?>
              <li><a href="#" onclick="return mis_carteras('actividades/actividades.php')"><i class="fa fa-calendar text-default"></i> Actividades</a></li>
              <li><a href="#" onclick="return mis_carteras('actividades/tareas.php')"><i class="fa  fa-tasks text-default"></i> Tareas</a></li>
              <?php } ?>
            </ul>
          </li> 
          <?php  } ?>    
      
<?php if(($_SESSION['id_c']==1)||($_SESSION['id_c']==2)){ ?>
         <li class="treeview bg-black">
              <a href="#">
                <i class="fa  fa-users"></i> <span>Gestión Administrativa</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
<li><a href="#" class="text-success" onclick="return app_inversiones('http://hombresdenegociosglobalca.com/admin/panel_jp.php')"><i class="fa fa-unlock text-primary"></i> Liberar Pagos</a></li>
<li><a href="#" onclick="return app_inversiones('http://hombresdenegociosglobalca.com/admin/inversiones/index_acceso1.php')"><i class="fa fa-sitemap text-info"></i> Gestión Inversiones</a></li>
<li><a href="#" onclick="return app_inversiones('http://hombresdenegociosglobalca.com/admin/dorado/index_acceso1.php')"><i class="fa fa-sitemap text-warning"></i> Gestión Dorado</a></li>
<li><a href="#" onclick="return app_inversiones('http://hombresdenegociosglobalca.com/admin/reciclaje/index_acceso1.php')"><i class="fa fa-sitemap text-success"></i> Gestión Reciclaje</a></li>
<li><a href="#" onclick="return app_inversiones('http://transfer.hombresdenegociosglobalca.com/app/php/proceso_cliente_admin.php?o=2')"><i class="fa fa-exchange text-success"></i> Gestión de Transferencia</a></li>

               </ul>
            </li>       
<?php } ?>           




 
         <li class="treeview">
              <a href="#">
                <i class="fa fa-bank"></i> <span>Bancos Autorizados</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"  data-toggle="modal"   data-target="#modalCuentasHNGNacional"><i class="fa fa-list" style="color:#EDA040"></i> Cuentas Nacionales</a></li>
                <li><a href="#"  data-toggle="modal"   data-target="#modalCuentasHNG"><i class="fa fa-list text-success"></i> Cuentas Internacionales</a></li>
               </ul>
            </li>




            <li><a href="#"  data-toggle="modal"   data-target="#info"><i class="fa fa-info-circle text-primary"></i> Lo que debe saber</a></li>

  <li><a href="#"  data-toggle="modal"  data-target="#lecturasModal">
              <i class="fa fa-book text-warning"></i> Lecturas para el éxito</a>
            </li>

            <li>
              <a href="#" onclick="$('.ayuda, .cortina_ayuda').show(500,'linear');"    data-target="#info"><i class="fa fa-info-circle text-primary"></i> Mejorar mi oficina &nbsp;<span class="label label-success parpadea">Nuevo</span></a>

            </li>




            <li><a href="https://hombresdenegociosglobalca.com/videos.php" target="videos"><i class="fa fa-video-camera text-primary"></i> Sistema Educativo</a></li>
      
            <li><a href="#" onclick="javascript:location.href='php/salir.php';" ><i class="fa fa-sign-out text-danger"></i> Cerrar Sesión</a></li>


          </ul>
        </section>
        <!-- /.sidebar -->

      </aside>

      <!-- Right side column. Contains the navbar and content of the page -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Panel de Control</li>
            
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->

            <!-- right col (We are only adding the ID to make the widgets sortable)-->
            <section class="col-lg-5 connectedSortable" style="width:100%;">
   

              <!-- Calendar -->
              <div class="box box-solid bg-default-gradient">
        
        
        
                <div class="">

  
<a href="#tope" class="tope"><span onclick="return subir()" class="tope tope_in glyphicon glyphicon-chevron-up"></span></a>
<div class="cortina"></div>
            <div id="cargar" style="position:absolute; display:none;  z-index:1000; background:rgba(0, 0, 0, 0.5); top:0%; left:0%; height:100%; width:100%;">
    <center>
    <img src="images/cargar6.gif" style="top:5%; left:45%; position: absolute; width:5%;">
    </center></div>
    
    <div class="chat" style="">
<?php include("chat/index.php"); ?>
</div>
        <div id="cuerpo" style="width:100%; height:auto; position:inheret; overflow:auto;" >
        <p><br><br>
        <center><img src="images/cargar6.gif"><br>Cargando la información, por favor espere.<br><br><br><br></p></center>
      
        </div>
    
        
                </div><!-- /.box-header -->
        
      
              </div><!-- /.box -->

            </section><!-- right col -->
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.1
        </div>
        <strong>Copyright &copy; 2017.</strong> Todos los derechos reservados.
      </footer>
    </div><!-- ./wrapper -->

  <?php include("php/modal.php"); ?>
    <!-- jQuery UI 1.11.2 -->
    <!--<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.min.js" type="text/javascript"></script>-->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <?php  
      include('modales/modal-kyc-contratos.php');
    ?>
    
    <script>
     // $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      

  
  <script type="text/javascript" src="bootstrap/datatables/datatables/datatables.min.js" ></script>
    <!-- Morris.js charts -->
    <!--<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
    <script src="plugins/morris/morris.min.js" type="text/javascript"></script>
    <!-- Sparkline -->
    <script src="plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <!-- jQuery Knob Chart -->
    <script src="plugins/knob/jquery.knob.js" type="text/javascript"></script>
    <!-- daterangepicker -->
    <script src="plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- datepicker -->
    <script src="plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <!-- Slimscroll -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>

    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard2.js" type="text/javascript"></script>
    
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js" type="text/javascript"></script>
    <script>
      var _gaq=[['_setAccount','UA-20257902-1'],['_trackPageview']];(function(d,t){ var g=d.createElement(t),s=d.getElementsByTagName(t)[0]; g.async=1;g.src='js/sonido/ga.js';s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>
  <script src="js/sonido/audiojs/audio.min.js"></script>
    <!--Archivos de calendario-->
  <script src="js/js_hng/src/js/jscal2.js"></script>
    <script src="js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/steel/steel.css" />

    <script type="text/javascript">
      
        setTimeout(function(){ $('[data-toggle="tooltip"]').tooltip(); },1000);
    </script>
  
    <?php if ($identidad_verificada <= 0) { ?> 
  <script>
    // $(function() {
      $('#kyc').modal({
        keyboard: false,
        show: true,
        backdrop: "static"
      })

      // fileinput();
    // });
  </script>
  <?php } ?> 


   <?php if ($modalC == 1 && $identidad_verificada > 0) { ?> 
  <script>
    // $(function() {
      $('#ModalContrato').modal({
        keyboard: false,
        show: true,
        backdrop: "static"
      })

    // });
  </script>
  <?php } ?>
<!--Fin de Archivos de calendario-->
  </body>
</html>
<?php
 }else{ echo "<script>location.href='./';</script>";} 
 ?>