-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 03, 2020 at 04:24 AM
-- Server version: 5.1.30
-- PHP Version: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hng_5x5`
--

-- --------------------------------------------------------

--
-- Table structure for table `comprobantes`
--

CREATE TABLE IF NOT EXISTS `comprobantes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_recarga` int(11) NOT NULL,
  `cartera` varchar(100) NOT NULL,
  `imagen` varchar(225) NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_recarga` (`id_recarga`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `comprobantes`
--

INSERT INTO `comprobantes` (`id`, `id_recarga`, `cartera`, `imagen`, `fecha`) VALUES
(14, 385772, 'mis_depositos', 'comprobantes_min/385772_38651c4450f87348fcbe1f992746a954_13295-4534534534d.png', '2020-01-30'),
(15, 385773, 'mis_depositos', 'comprobantes_min/385773_059def4514ca1bc7ff5781ac48428572_8203-4534534534d2.png', '2020-01-30');
