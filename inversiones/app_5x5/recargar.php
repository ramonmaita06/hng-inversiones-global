<div class="panel panel-success">
<div class="panel-heading">
  <center><b>TRANSFERENCIA DE BONOS DE INVERSIONES</b></center>
</div>
<div class="panel-body">

<input type="hidden" id="montoportafolio1">
<input type="hidden" id="montoportafolio2">
	   
	   
     <div class="row">
	  <div class="col-sm-10 col-lg-10">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Origen</label>
          <div class="col-md-8">
<select id="portafolio1" class="form-control" onchange="return preparar_tranferencia()">
<option value="">Portafolio General: 0.00</option>
<option value="">Portafolio de Inversiones: 0.00</option>
</select>
			</div>
          </div>
       </div>      
       </div>
<br>	   
<div class="row">	   
      <div class="col-sm-10 col-lg-10">
        <div class="form-group">
		<label for="ape" class="col-md-4 control-label">Destino</label>
          <div class="col-md-8">
<select id="portafolio2" class="form-control" disabled>
<option value="">Portafolio General: 0.00</option>
<option value="">Portafolio de Inversiones: 0.00</option>
</select>
			</div>
          </div>
       </div>
	</div> 	
<br>
<div class="row">     
      <div class="col-sm-10 col-lg-10">
        <div class="form-group">
		<label for="ape" class="col-md-4 control-label">Monto a Transferir</label>
		<div class="col-md-8" align="center">
            <input id="montoinv"  type="number" min="0" step="0.01" class="form-control"  maxlength="20" >
		</div>
       </div>
       </div>
	</div> 
	<hr>
     <div class="row">     
      <div class="col-sm12 col-lg-12">
        <div class="form-group">
		<div class="col-md-12"  align="center">
		<button class="btn btn-md btn-success" id="btntranferir" onclick="return transferir()" disabled><i class="fa  fa-plus-square"></i> <span id="btnt">Espere...</span></button>
		<button class="btn btn-md btn-danger" onclick="$('#recargar,#cortina').hide(100,'linear');"><i class="fa  fa-remove"></i> Cancelar</button>
		</div>
       </div>
       </div>
	</div> 	

</div>	
</div>	


<script>parent.cerrar_carga();</script>
