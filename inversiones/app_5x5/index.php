<?php session_name("hng"); session_start(); 
if(isset($_SESSION["us"])){
?>
<!DOCTYPE html>
<html>
  <head>
   <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../bootstrap/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />




    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
   <link href="../css/sweetalert.css" rel="stylesheet">

    <link href="../css/style_inversion.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	    <!-- jQuery 2.1.3 -->
    <!--Archivos de calendario-->
    <!-- <script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script> -->
	<script src="../js/js_hng/src/js/jscal2.js"></script>
    <script src="../js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->

<style>
.adaptame .icon{font-size:24px;}
.ocultame2{display:inline;}
.adaptame{	width:86px; }
.mimenu{width:100%;}
 @media only screen and (max-width: 600px){
.mimenu{position:absolute;}
.adaptame{width:auto;}
.adaptame .icon{font-size:20px;}

.ocultame2{display:none;}
}

</style>

  <script>
    function mostrar_tabla(){
        $('#myModal').modal("show");
    } 
    function mostrar_tabla_divisa(tabla){
        $('#myModal2').modal("show");
        $(".vertablainversion").attr('src','../images/tabla_inversion_'+tabla+'.jpg');
        $(".avertablainversion").attr('href','../images/tabla_inversion_'+tabla+'.jpg');
    }
</script>
  </head>
  <body name="tope">
      
<!--ventana modal-->			
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style="width:90%; left:0%; z-index:10000;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        
        <a href="../images/tabla_inversion.jpg" target="3">
        <center><img src="../images/tabla_inversion.jpg" title="Pulse para ver imagen en tamaño original" class="img-responsive"></center>
        </a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>

<!--ventana modal-->			
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog" style="width:90%; left:0%; z-index:10000;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        
        <a href="../images/tabla_inversion_dolares.jpg" class="avertablainversion" target="3">
        <center><img src="../images/tabla_inversion_dolares.jpg" class="vertablainversion img-responsive" title="Pulse para ver imagen en tamaño original" class="img-responsive"></center>
        </a>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
  
  
  
<div class="" style=" width:100%;">
  <center>
  <div class="panel panel-success" style=" width:100%; border-bottom:none;border-left:none;border-right:none">
  <div class="panel-heading" style="font-size:20px">
  <div id="ganancias" title="Ganancia de Inversion" onclick="return cofre()" style="cursor:pointer; display:none">Portafolio de Inversiones &nbsp;<i class="fa fa-money btn"></i> 0,00</div>


  <center><i class="fa  fa-money"></i> <b>MIS INVERSIONES</b></center>

  
  </div>
<div class="panel panel-body" style="position:absolute; width:100%; padding:0px;">
	
	
  <input type="hidden" id="ganancias1">
<nav class="btn-group" style="z-index:1000;">
 <!-- <button onclick="return opcionINV(6);"  class="btn btn-warning" title="Gestión de bonos de inversión"><i class="fa fa-money" style="font-size:24px"></i></button>-->
  <button onclick="return opcionINV(1);" class="btn btn-success adaptame" title="Nueva Inversion" data-toggle="tooltip" ><i class=" fa fa-bar-chart-o icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Invertir</span></button>
  
  
    <!--<button onclick="return opcionINV(7),lista_cofres_retiro();"  class="btn btn-info adaptame" title="Gestión de retiro de inversión" data-toggle="tooltip" ><i class="fa fa-money icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Retirar</span></button>-->
   
    <button onclick="return parent.app_inversiones('app_5x5/retirar.php')"  class="btn btn-info adaptame" title="Gestión de retiro de inversión" data-toggle="tooltip" ><i class="fa fa-money icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Retirar</span></button> 
  
    <button onclick="return opcionINV(9);"  class="btn btn-default adaptame" title="Gestión de recargas" data-toggle="tooltip" ><i class="fa fa-refresh icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Recargas</span></button>  
	
    <button  onclick="return parent.comprar_divisa()"  class="btn btn-success adaptame" title="Gestión para la compra de divisas (BsS <-> USD$)" data-toggle="tooltip" ><i class="fa fa-dollar icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Divisas</span></button> 
  
  
    <button onclick="return opcionINV(8);"  class="btn btn-warning adaptame" title="Gestión de bonos de inversión" data-toggle="tooltip" ><i class="fa fa-gift icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Bonos</span></button>  

	
  <button onclick="return parent.mis_carteras('precargado/app/')"  class="btn btn-danger adaptame" title="Gestion de Codigos Precargados" data-toggle="tooltip" ><i class="fa fa-barcode icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Precarga</span></button>
  
  <button onclick="return opcionINV(4);"  class="btn btn-primary adaptame" title="Estado de Inversiones" data-toggle="tooltip" ><i class="fa fa-list-alt icon" ></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Inversiones</span></button>


 </nav>
 </div>
 </div>

<br>
<hr>

     
 
 
 
 <div id="cortina"></div>
 <div id="invertir"><?php include("invertir.php"); ?></div>
 <div id="recargar" style="display:none"><?php include("recargar.php"); ?></div>
 <div id="recargas" style="display:none">
<?php include("recargas1.php"); ?>
 </div>
 <div id="retirar" style="display:none"><?php include("retirar1.php"); ?></div>
 <div id="bonos" style="display:none"><?php include("bonos.php"); ?></div>

 <div  class="panel panel-warning"> 
<div class="panel-heading">
 <div class="row" > 

  <div class="col-sm-12 col-lg-12">

    <div class="form-group">
    <div class="col-md-2" align="center">
    <label>Moneda</label>
  <select id="moneda" class="form-control" onchange="buscar_moneda(this)"><?php echo $_SESSION['lista_moneda'];  ?></select>
    </div> 
    <div class="col-md-2" align="center">
    <label>Estatus</label>
  <select id="estatus" class="form-control" onchange="buscar_inv_status()">
    <option value="1">En proceso</option>
    <option value="5">Guardado</option>
    <option value="0">Todas</option>
  </select>
    </div>     
		<div class="col-md-2" align="center">
		<label>N&deg; Referencia</label>
	<input class="form-control" onkeyup="return buscar_ref(this)" onclick="return buscar_ref(this)" type="search" id="refer">
		</div>
		<div class="col-md-2" align="center">
		<label>Desde</label>
	<input class="form-control" placeholder="dd/mm/aaaa" type="text" id="fci">
			 <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fci",
        trigger    : "fci",
        onSelect   : function() { this.hide()
		},
       // showTime   : 12,
         dateFormat : "%d/%m/%Y"
		  });//]]>
		  </script>
		</div>

		<div class="col-md-2" align="center">
		<label>Hasta</label>
	<input class="form-control" placeholder="dd/mm/aaaa" type="text" id="fcf">
				 <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fcf",
        trigger    : "fcf",
        onSelect   : function() { this.hide()
		},
       // showTime   : 12,
        dateFormat : "%d/%m/%Y"
		  });//]]>
		  </script>
		</div>
		<div class="col-md-2" align="center">
      <br>
<button class="btn btn-success btn-sm" onclick="return buscar_fecha_ref()"><i class="fa fa-search"></i> Buscar</button>
		</div>
       </div>
	 
       </div>

		</div>
<div class="row hide">
	<div class="form-group">
		<div class="col-sm-12">
			<div class="col-sm-4" style="text-align:center;" title="Monto total de inversiones realizadas"><b> Inv. Historica:</b> <i class="fa fa-line-chart"></i> <span id="invhistorica">0.00 Bs.</span></div>
			<div class="col-sm-4" style="text-align:center;" title="Monto total de Inversiones activas"><b> Inv. Activas:</b> <i class="fa fa-line-chart"></i> <span id="invactual">0.00 Bs.</span></div>
			<div class="col-sm-4" style="text-align:center;" title="Monto total de Inversiones actuales"><b> Inv. Actual:</b>  <i class="fa fa-line-chart"></i> <span id="porbusqueda">0.00 Bs.</span></div>
		</div>
	</div>
</div>
	</div>

     </div> 
  
<div id="titulomodulo" align="center"></div>
 
 
 <div id="cuerpoinfo"  style="width:100%; height:500px; overflow:auto;"><center><br><br><img src="../images/inversionista1.png"></center></div>


 
</div>
  
  
  
  
	<script src="../js/jq.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../bootstrap/datatables/datatables/datatables.min.css">
  <script type="text/javascript" src="../bootstrap/datatables/datatables/datatables.min.js" ></script>
 	<script src="../js/sweetalert.min.js"></script>
    <!--<script src="../js/sweetalert.dev.js"></script>-->
    <script src="../js/wow.min.js"></script>
	<script src="../js/number_format.js"></script>
 	<script src="../js/procesos.js"></script>
<script>parent.cerrar_carga();

 setTimeout(function(){

      
    $('.datatable').DataTable({
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
  },2500);
    

</script>
    <script src="../bootstrap/fileinput/js/fileinput.min.js"></script>

<script>
      $(function() {
        
        $("#file").fileinput({
          showPreview:false,
          showCaption: true,
          showUpload: false,
          showRemove: false,
          browseClass: "btn btn-primary btn-lg",
          fileType: "any",
          browseLabel: 'Elegir &hellip;',
              browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;',
              browseClass: 'btn btn-primary',
              removeLabel: 'Quitar Imagen',
              removeIcon: '<i class="glyphicon glyphicon-ban-circle"></i> ',
              removeClass: 'btn btn-default',
              uploadLabel: 'Subir Imagen',
              uploadIcon: '<i class="glyphicon glyphicon-upload"></i> ',
              uploadClass: 'btn btn-default',
              msgLoading: 'Cargando &hellip;',
              msgProgress: 'Cargado {percent}% of {file}',
              msgSelected: '{n} Imagenes Seleccionadas',
              previewFileType: 'image',
              wrapTextLength: 250,
          msgFileTypes: {'image': 'Solo se permiten archivos de tipo imagen [PNG, JPG, JPEG, BMP, GIF]'}
        });
      });
  </script>
  </body>
</html>
<?php
}else{	echo "<script>location.href='./';</script>";} 
 ?>