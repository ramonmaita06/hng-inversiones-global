<?php session_name("hng"); session_start(); 
if(isset($_SESSION["us"])){
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
   <link href="../css/sweetalert.css" rel="stylesheet">
    <link href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/style_inversion.css" rel="stylesheet" type="text/css" />
  </head>
 <body>
<div class="panel panel-info">
<div class="panel-heading" style="font-size:20px">
  <center><i class="fa  fa-briefcase"></i> <b>RETIRO DE FONDOS</b></center>
</div>

  <form class="form-horizontal" role="form" onsubmit="return false">
<div class="panel-body">

     <div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group">
         
          <div class="col-md-12" style="font-size:32px; text-align:center; font-weight:bold;">
			  <div id="pgeneral1" title="Portafolio General de Inversiones" onclick="return lista_cofres_retiro()" style="cursor:pointer; font-size:24px;" class="text-success">Portafolio General &nbsp;<i class="fa fa-money btn"></i> 0,00</div>
			  <input type="hidden" id="pgeneral" value="0">
			</div>
          </div>
       </div>  
  </div>
  <div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group">
          <div class="col-md-2" style="font-size:32px; text-align:center; font-weight:bold;"></div>	
		  
          <div class="col-md-8" style="text-align:center; font-weight:bold;">
<b>Nota:</b> <span style="color:red">Solo use la coma (,) para generar una cifra con decimales.</span> Ejemplo: <b id="retiro_minimo"><?php echo $_SESSION["retiro_bolivar1"]; ?></b><br>
<b><span style="color:blue">Monto minimo a retirar:</span>  <?php echo $_SESSION["retiro_bolivar"]; ?> BsS.</b>
<div class="input-group" style="width:100%;">
<span class="input-group-addon" style="width:100%;  background:#EAF5E5">
<input type="number" placeholder="Monto a retirar"step="0.01" id="monto_retiro" min="100" class="form-control" onkeyup="return letra_cifra(this.value);">		  
</span>
<span class="input-group-addon" style="width:100%;  background:#EAF5E5">
<button onclick="return retirar_fondo()"  id="bretiro"  class="btn btn-md btn-success"><i class="fa fa-money"></i> Retirar</button>	  
</span>
</div>
<div id="cifrar"><br></div>
			</div>
          </div>
       </div>  
       </div>  
</form>	   
<hR>

    
	 <div id="listado_retiros" style="overflow-y:auto; height:500px; overflow-x:hidden">
	 <center> No existen retiros realizados recientemente.</center>
	 </div>
	 <div id="listado_retiros1" style="">... </div>	 

</div>	
</div>

	<script src="../js/jq.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
 	<script src="../js/sweetalert.min.js"></script>
	<script src="../js/number_format.js"></script>
 	<script src="../js/proceso_retiro.js"></script>
<script>
  


parent.cerrar_carga(); listar_retiros();   
</script>
  </body>
</html>
<?php
}else{	echo "<script>location.href='./';</script>";} 
 ?>