<?php session_name("hng"); session_start(); 
if(isset($_SESSION["us"])){


    include('../php/cnx.php');
    include('../php/funciones.php');
    include('../php/proceso_cofre.php');
    $cons = mysql_query("select * from monedas where estatus=1");     
        $lista_moneda = 0;
        $lista_moneda1 = 0;
        $lista_moneda2 = 0;
        $retMin = '';
        $retMin1 = '';
    while($info = mysql_fetch_array($cons)){


      /*-*/
            $moneda = $info['moneda'];  
            $indice = $info['indice'];  
            $cartera = $info['cartera'];  
          $lista_moneda .= '<option value="'.$moneda.'">'.$info['nombre'].'</option>';
          $lista_moneda2 .= '<option value="'.$indice.'">'.$info['nombre'].'</option>';

        if($moneda !='BsS')
          $lista_moneda1 .= '<option value="'.$moneda.'">'.$info['nombre'].': '.masmenos(miscofre::cofre_disponible('mis_depositos'.$cartera,$moneda)).'</option>';
        else
          $lista_moneda1 .= '<option value="'.$moneda.'">'.$info['nombre'].': '.masmenos(miscofre::cofre_divisas('mis_depositos'.$cartera,$moneda)).'</option>';

      /*-*/


      if($info['moneda']=='BsS'){
        $retMin.= '<span id="'.$info['moneda'].'"  class="invMin">'.masmenos($info['minimo_retiro']).' '.$info['moneda'].'</span>';
        $retMin.= '<input type="number" style="display:none" id="IM_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';

        $retMin1.= '<span id="_'.$info['moneda'].'"  class="invMin">'.$info['minimo_retiro'].' '.$info['moneda'].'</span>';
        $retMin1.= '<input type="number" style="display:none" id="IM1_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';
      }
      else{
        $retMin.= '<span id="'.$info['moneda'].'"  class="invMin" style="display:none">'.masmenos($info['minimo_retiro']).' '.$info['moneda'].'</span>';
        $retMin.= '<input type="number" style="display:none" id="IM_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';

        $retMin1.= '<span id="_'.$info['moneda'].'"  class="invMin" style="display:none">'.$info['minimo_retiro'].' '.$info['moneda'].'</span>';
        $retMin1.= '<input type="number" style="display:none" id="IM1_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';
      }
    }
        $_SESSION['lista_moneda'] = $lista_moneda;
        $_SESSION['lista_moneda1'] = $lista_moneda1;
        $_SESSION['lista_moneda2'] = $lista_moneda2;


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
   <link href="../css/sweetalert.css" rel="stylesheet">
    <link href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="../css/style_inversion.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
  function tipo_divisa(a){
  var periodo=$("#duracion");
  periodo.val("");

  $(".invMin").hide();
  if(a.value == 'BsS'){
    $("#BsS").show();
    $("#_BsS").show();
  }else{
    $("#"+a.value ).show();
    $("#_"+a.value ).show();
  }
}

</script>
  </head>
 <body>
<div class="panel panel-success">
<div class="panel-heading" style="font-size:20px">
  <center><i class="fa  fa-briefcase"></i> <b>RETIRO DE FONDOS</b></center>
</div>

  <form class="form-horizontal" role="form" onsubmit="return false">
<div class="panel-body">

     <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
         
          <!--<div class="col-md-12" style="font-size:32px; text-align:center; font-weight:bold;">
        <div id="pgeneral1" title="Portafolio General de Inversiones" onclick="return lista_cofres_retiro()" style="cursor:pointer; font-size:24px;" class="text-success">Portafolio de Inversiones &nbsp;<i class="fa fa-money btn"></i> 0,00</div>
        <input type="hidden" id="pgeneral" value="0">
      </div>-->

      <div id="mret"></div>
          </div>
       </div>  
  </div>
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
          <div class="col-md-2" style="font-size:32px; text-align:center; font-weight:bold;"></div> 
      
          <div class="col-md-8" style="text-align:center; font-weight:bold;">

<b>Nota:</b> <span style="color:red">Solo use la coma (,) para generar una cifra con decimales.</span> Ejemplo: <b><?php echo $retMin1; ?></b><br>
<b><span style="color:blue">Monto minimo a retirar:</span>  <?php echo $retMin; ?></b>
<div class="input-group" style="width:100%;">
<span class="input-group-addon" style="width:40%;  background:#EAF5E5">
  <select id="midivisa" class="form-control"  data-toggle="tooltip" onchange="return tipo_divisa(this);" title="Seleccione la divisa a retirar">
    <?php 
        echo $_SESSION['lista_moneda_retiro'];        
    ?>
  </select>   
</span>
<span class="input-group-addon" style="width:40%;  background:#EAF5E5">
<input type="number" placeholder="Monto a retirar" step="0.01" id="monto_retiro" min="1" class="form-control">     
<!--<input type="number" placeholder="Monto a retirar" step="0.01" id="monto_retiro" min="100" class="form-control" onkeyup="return letra_cifra(this.value)">-->      
</span>
<span class="input-group-addon" style="width:20%;  background:#EAF5E5">
<button onclick="return validar_minimo()"  id="bretiro"  class="btn btn-md btn-success"><i class="fa fa-money"></i> Retirar</button>   
</span>
</div>

<div id="cifrar"><br></div>
      </div>
          </div>
       </div>  
       </div>  
</form>    
<hR>

    
   <div id="listado_retiros"  style="overflow-y:auto; height:600px; overflow-x:hidden">
   <center> No existen retiros realizados recientemente.</center>
   </div>
   <div id="listado_retiros1" style="">... </div>  
   
</div>  
</div>

  
  <script src="../js/jq.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../bootstrap/datatables/datatables/datatables.min.css">
  <script type="text/javascript" src="../bootstrap/datatables/datatables/datatables.min.js" ></script>
  <script src="../js/sweetalert.min.js"></script>
    <script src="../js/wow.min.js"></script>
  <script src="../js/number_format.js"></script>
  <script src="../js/proceso_retiro.js"></script>
<script>
parent.cerrar_carga(); listar_retiros();

function reloadDatatable(){
 setTimeout(function(){

      
    $('.datatable').DataTable({
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
  },2500);
}
reloadDatatable();
</script>
  </body>
</html>
<?php
}else{  echo "<script>location.href='./';</script>";} 
 ?>