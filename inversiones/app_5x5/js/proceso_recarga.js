

function registrar_recarga(){
var msn='';

var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancoe=$("#bancoe").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto1").val();
var md = $("#moneda").val();
var mds = $("#moneda option:selected").text();
var moneda_actual = $("#moneda_"+md).val();
moneda_actual = parseFloat(moneda_actual);

var montop=$("#montop1").val();
var bancoext=$("#bancoexterior").val();
var bancoextrec=$("#bancoexteriorpara").val();
if(tipot=='PAGO DESDE EL EXTERIOR'){
	bancod='DE '+bancoext+' A '+bancoextrec;
	}
else{
	bancod='DE '+bancoe+' A '+bancod;
}

if($("#tipot").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de pago", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipot").focus(); },3000); return false;
}


/*validacion*/
if($("#tipot").val()=="PRECARGADO"){
	if($("#codp").val().length<8){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el codigo precargado", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
	}	

	/*monto de Inscripcion*/
	if(moneda_actual>$("#montop1").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#codp").focus();
		return false;
	}
			
	if($("#seguir").val()==0){
	swal({title: "COMPLETAR INFORMACION", text: "El codigo precargado no es valido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
	}

}

else if($("#tipot").val()=="PAGO DESDE EL EXTERIOR"){

	if($("#bancoexterior").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que emite (De) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoexterior").focus(); },3000); return false;
	}
	if($("#bancoexteriorpara").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que recibe (Para) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoexteriorpara").focus(); },3000); return false;
	}
	if($("#numero").val().length<4){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
	}	
	if($("#moneda").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#moneda").focus(); },3000); return false;
	}		
	if(($("#monto1").val()<=0)||($("#monto1").val()=='')){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
	}
	if($("#DPC_edit2").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
	}
	/*monto de Inscripcion*/
	if(moneda_actual>$("#monto1").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#monto1").focus();
		return false;
	}
}

else{
	
	var mispagos=$("#mispagos").val();
	var pmul=$("#pmul");

	if($("#bancoe").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que emite (De) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoe").focus(); },3000); return false;
	}

	if($("#bancod").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que recibe (Para) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancod").focus(); },3000); return false;
	}
	if($("#numero").val().length<6){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
	}	
	if($("#moneda").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#moneda").focus(); },3000); return false;
	}
			/*monto de Inscripcion*/
	if(($("#monto1").val()<=0)||($("#monto1").val()=='')){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
	}
	if($("#DPC_edit2").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
	}
	if(moneda_actual>$("#monto1").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#monto1").focus();
		return false;
	}
	
	
}

swal({
  title: "¿Esta segur@ que desea registrar la recarga?",
  text: msn,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
	
  if (isConfirm) {
  	var fdata = new FormData(document.getElementById("form_recargas"));
	var archivos = document.getElementById("file");
	var archivos1 = document.getElementById("file");
	var archivo = archivos.files; 
  	var form = $('#form_recargas').serialize();
	archivos.append('file',archivo[0]);
        fdata.append("opc", 1);
        fdata.append("tipot", tipot);
        fdata.append("codp", codp);
        fdata.append("bancod", bancod);
        fdata.append("fechad", fechad);
        fdata.append("numerod", numerod);
        fdata.append("montod", montod);
        fdata.append("montop", montop);
        fdata.append("moneda", md);
  	$.ajax({
		url: '../php/proceso_recargas.php',
		type: 'POST',
		// dataType: 'html',
		data:fdata,
		processData: false,
		contentType: false,
	})
	.done(function(data) {
		console.log("success");
		console.log(data);
		var resp = JSON.parse(data)
		if(resp.salida==1){

			if(resp.opcion==1){setTimeout(function(){
				swal("REGISTRO EXITOSO",resp.mensaje,"success");
					},1000);
					$("#codp").val("");
					$("#codp").attr({"readonly":false});
					$("#btn_recargar").attr({"disabled":true});
					if (parametro == 'recargar_pagina') {

						setTimeout(function(){
							window.location.reload();
						},1000);
					}
			}
			else{
				setTimeout(function(){
				swal("REGISTRO EXITOSO",resp.mensaje,"success");
				mensajeTextoRecarga();
				},1000);
				}	
		recarga_cofre1();
			if (parametro == 'recargar_pagina') {

				setTimeout(function(){
					window.location.reload();
				},1000);
			}
		}
		else
		{
		setTimeout(function(){
			swal("REGISTRO FALLIDO",resp.mensaje,"error");
			},1000);
		$("#mipie").show(); $("#ant").click();
		}
	})
	.fail(function(e) {
		console.log("error");
		console.log(e);
	})
	.always(function() {
		console.log("complete");
	});
	// $.post("../php/proceso_recargas.php",{opc:1,
	// tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,
	// numerod:numerod,montod:montod,montop:montop,moneda:md
	// }
	// ,function(resp){
		
	// 	if(resp.salida==1){

	// 		if(resp.opcion==1){setTimeout(function(){
	// 			swal("REGISTRO EXITOSO",resp.mensaje,"success");
	// 				},1000);
	// 				$("#codp").val("");
	// 				$("#codp").attr({"readonly":false});
	// 				$("#btn_recargar").attr({"disabled":true});
	// 		}
	// 		else{
	// 			setTimeout(function(){
	// 			swal("REGISTRO EXITOSO",resp.mensaje,"success");
	// 			mensajeTextoRecarga();
	// 			},1000);
	// 			}	
	// 	recarga_cofre1();
	// 	}
	// 	else
	// 	{
	// 	setTimeout(function(){
	// 		swal("REGISTRO FALLIDO",resp.mensaje,"error");
	// 		},1000);
	// 	$("#mipie").show(); $("#ant").click();
	// 	}
		
	// },"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
	$("#mipie").show();
	$("#ant").click();
  }
});

	


}




function recarga_cofre1(){
		$("#cofre").html('<img src="images/cargar6.gif" style="width:32px">');
	$.post("../php/planes_proceso_menu.php",{opc:2},function(resp){
/*bonos*/
		$("#cofre").html(resp.cofre);
/*notificaciones*/		
		$("#lnotifica").html(resp.notifica);	
		$("#nnotifica1").html("Tienes "+resp.nnotifica+" notificaciones");	
		$("#nnotifica").html(resp.nnotifica);	
		recarga_cofre();
	},"json");
	}

	



function opciones_recarga(opc){
		$("#mostrar_recarga").html('<center><br><br><br>Buscando...<br><img src="../images/cargar6.gif" style="width:32px"></center>');
	$("#form_recargas,.mostrar_recarga").hide(500,"linear");
	if(opc==1){
		$("#boton2").removeClass("btn-warning");
		$("#boton2").addClass("btn-default");
		$("#boton1").removeClass("btn-default");
		$("#boton1").addClass("btn-warning");
		$("#form_recargas").show(500,"linear");}
	if(opc==2){
		$("#boton1").removeClass("btn-warning");
		$("#boton1").addClass("btn-default");
		$("#boton2").removeClass("btn-default");
		$("#boton2").addClass("btn-warning");
		$(".mostrar_recarga").show(500,"linear");
		mostrar_recargas();}
}



	
function mostrar_recargas(){
	var divisa=$("#divisa1").val();
	$("#mostrar_recarga").html('<center><br><br><br>Buscando...<br><img src="../images/cargar6.gif" style="width:32px"></center>');
$.post("../php/proceso_recargas.php",{opc:2, divisa:divisa},function(resp){
$("#mostrar_recarga").html(resp.listado);	
},"json");	
}	
	
	
function mostrar_recargas_fecha(){
	var divisa=$("#divisa1").val();
	var fi=$("#DPC_edit0").val();
	var ff=$("#DPC_edit1").val();
	var esta=$("#esta").val();
	$("#mostrar_recarga").html('<center><br><br><br>Buscando...<br><img src="../images/cargar6.gif" style="width:32px"></center>');
$.post("../php/proceso_recargas.php",{opc:3, fi:fi, ff:ff, esta:esta, divisa:divisa},function(resp){
$("#mostrar_recarga").html(resp.listado);	
},"json");	
}	
	

	
function remover_registro(id){


swal({
  title: "¿Esta segur@ que desea registrar la recarga?",
  text: msn,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {

$.post("../php/proceso_recargas.php",{opc:4, id:id},function(resp){
if(resp.salida==1){swal("REGISTRO DE PAGO",resp.mensaje,"success");
mostrar_recargas();
}
else{swal("REGISTRO DE PAGO",resp.mensaje,"error");}
},"json");	
  
  }
	else{
	swal("REGISTRO DE PAGOS","PROCESO CANCELADO","error");	
		}  
});









}	
	
	
	
	

function CorreoE(opc){
	
	$.post("../php/correo.php",{},function(resp){
		
		
if(opc==1){
		if(resp.salida==1){swal("Envio de Correo","Se ha enviado el contrato de afiliacion a su direccion de correo electronico","success");
		mensajeTexto("Listo",1);}
		else{swal("Envio de Correo","No se pudo enviar el contrato de afiliacion a su direccion de correo electronico","error");}
		}
else{
		if(resp.salida==1){
		
		swal("Envio de Correo","Se ha enviado la informacion de pre-afiliacion a su direccion de correo electronico, y un mensaje de texto","success");
		mensajeTexto("Pre",1);
		}
		else{swal("Envio de Correo","No se pudo enviar la informacion de pre-afiliacion a su direccion de correo electronico.","error");}
		}
		
		//setTimeout(function(){ location.reload();},5000);
	},"json");
		
}


function mensajeTextoRecarga(){

$.post("../php/texto_recarga.php",{opc:1},function(resp){},"json");
	
}





var envios=0;
function mensajeTexto(opc,n){
$.post("../msn/proceso.php",{opc:n, texto:opc},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
		setTimeout(function(){
		if(envios==0){ envios=1; mensajeTexto("Admin",2); }
		else{setTimeout(function(){ location.reload();},2000);}
		},1000);
},"json");	
}

function  metodo_afiliacion(opc){

	if(opc.value=="PRECARGADO"){
	$("#ccodp").show(500,'linear');
	$(".metodo-dt").hide(500,'linear');
	$(".ocultacampo").hide();
	}else if(opc.value=="PAGO DESDE EL EXTERIOR"){
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$(".ocultacampo").show();
	$("#bancoe,  #bancod").hide();
	$("#bancoe,  #bancod").val(0);
	$("#bancoexterior, #bancoexteriorpara").show();
	}
	else if(opc.value=="PAGO MOVIL"){
	$("#btn_recargar").attr({"disabled":false});

	var pm = $("#selBNPM").html();	
	$("#bancod").html(pm);	
	$("#bancod").val('BDV');	
	//$("#bancod").attr({"disabled":true});
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$("#bancoexterior, #bancoexteriorpara").hide();
	$(".ocultacampo").show();
	}
	else{
	var spm = $("#selBN").html();
	$("#bancod").html(spm);
	$("#bancod").attr({"disabled":false});	
	$("#bancoe").hide();
	$("#bancoexterior, #bancoexteriorpara").show();
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$(".ocultacampo").show();
	$("#bancoe,  #bancod, #bancoexterior, #bancoexteriorpara").val('');
	$("#bancoexterior, #bancoexteriorpara").hide();
	}
}
/*validar codigo precargado*/
function valida_codigo(cod){

	if(cod.value.length<8){return false;}
	$("#codp").attr({"readOnly":true});
$.post("../php/valida_CP.php",{cod:cod.value},function(resp){
	if(resp.st==1){
	$("#btn_recargar").attr({"disabled":false});
	$("#codp").attr({"readOnly":true});
	$("#seguir").val(1);
	$("#montop1").val(resp.montop);
	$("#tmoneda").val(resp.moneda);
	alerta("Validación exitosa",resp.msn,3000,false);
	}
	else if(resp.st==2){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==3){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==0){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
},"json");
	
}



function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}




