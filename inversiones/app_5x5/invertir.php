<script>
function tipo_divisa(a){
	var periodo=$("#duracion");
	periodo.val("");
	if(a.value=="BsS"){
		$("#tablaInvBs").show();
		//periodo.options[periodo.selectedIndex].parentNode.label['Dolares'].css({"display":"none"});
		$("#duracion").children("optgroup[label='Bolivares']").show();
		$("#duracion").children("optgroup[label='Periodos']").hide();
	}
	else if(a.value!="BsS"){
		$("#tablaInvBs").hide();
		$("#duracion").children("optgroup[label='Bolivares']").hide();
		$("#duracion").children("optgroup[label='Periodos']").show();
	}


	$(".invMin").hide();
	if(a.value == 'BsS')
		$("#BsS").show();
	else
		$("#"+a.value ).show();

}


function inversion_divisa(){
	
	var monto=$("#monto").val();
	var periodo=$("#duracion").val();	
	var interes=0;
	if(periodo==60){interes=0.25;}
	if(periodo==120){interes=0.5;}
	if(periodo==180){interes=0.75;}
	if(periodo==240){interes=1;}
	if(periodo==300){interes=1.25;}
	if(periodo==360){interes=1.5;}
	

	var total=parseFloat(monto);
	total=total+eval(parseFloat(monto)*parseFloat(interes)/100);
	for(var i=2; i<=periodo; i++){
		total=total+eval(parseFloat(total)*parseFloat(interes)/100);
	}
	
	$("#total_dolares").val(parseFloat(total).toFixed(2));
	
	
}





</script>




<div class="panel panel-success">
<div class="panel-heading" style="font-size:20px">
  <center><i class="fa  fa-bar-chart-o"></i> <b>CREAR INVERSIÓN</b></center>
</div>
<div class="panel-body">


<input id="ni" value="0" type="hidden">
<input id="id_inv" value="0" type="hidden">
<div class="panel panel-success">
<div class="panel-heading">
	<div id="tablaInvBs" class="alert" style="background:#8DB514; color:white; font-weight:bold; display:none"><center><big>PROMOCIÓN DEL MES PARA INVERSIONES EN BOLIVARES.</big><br><i>Ver Tabla de Inversion en Bolivares.</i><br><b>Nota:</b> El PLAN PLUS solo aplica para clientes afiliados hasta el 15 de Diciembre de 2019.</center></div>
<span style="color:green">Monto minimo de inversión</span> <b><span id="inversionMinima">

	<?php
		include('../php/cnx.php');
		include('../php/funciones.php');
		include('../php/proceso_cofre.php');
		$cons = mysql_query("select * from monedas where estatus=1");    	
      	$lista_moneda = 0;
      	$lista_moneda1 = '<option value="">----</option>';
      	$lista_moneda2 = 0;
      	$lista_moneda_retiro ='';
		while($info = mysql_fetch_array($cons)){


			/*-*/
          	$moneda = $info['moneda']; 	
          	$indice = $info['indice']; 	
          	$cartera = $info['cartera']; 	
	        $lista_moneda .= '<option value="'.$moneda.'">'.utf8_decode($info['nombre']).'</option>';
	        $lista_moneda1 .= '<option value="'.$moneda.'" val_mimoneda="'.$info['nombre'].'">'.utf8_decode($info['nombre']).': '.masmenos(miscofre::cofre_divisas('mis_depositos'.$cartera,$moneda)).'</option>';

	        $lista_moneda2 .= '<option value="'.$indice.'">'.utf8_decode($info['nombre']).'</option>';

	        if($indice!='1'){
	        	$lista_moneda4 .= '<option value="'.$indice.'">'.utf8_decode($info['nombre']).'</option>';
	        }

			if($moneda=='BsS'){
				$lista_moneda3 .= '<option value="'.$indice.'" val_divisa="'.$moneda.'">'.utf8_decode($info['nombre']).': '.masmenos(miscofre::cofre_divisas('mis_depositos'.$cartera,$moneda)).'</option>';
			}else{
				$lista_moneda3 .= '<option value="'.$indice.'" val_divisa="'.$moneda.'">'.$info['nombre'].': '.masmenos(miscofre::cofre_disponible('mis_depositos'.$cartera,$moneda)).'</option>';
			}


			if($info['moneda']=='BsS'){
				echo '<span id="'.$info['moneda'].'"  class="invMin">'.masmenos($info['minimo_inversion']).' '.$info['moneda'].'</span>';
				echo '<input type="number" style="display:none" id="IM_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';
			}
			else{
				echo '<span id="'.$info['moneda'].'"  class="invMin" style="display:none">'.masmenos($info['minimo_inversion']).' '.$info['moneda'].'</span>';
				echo '<input type="number" style="display:none" id="IM_'.$info['moneda'].'" value="'.$info['minimo_inversion'].'">';
			}

			if($moneda=='BsS')
	        	$lista_moneda_retiro .= '<option value="'.$indice.'" val_divisa="'.$moneda.'">'.utf8_decode($info['nombre']).': '.masmenos(miscofre::cofre_divisas('mis_depositos'.$cartera,$moneda)).'</option>';
		}
        $_SESSION['lista_moneda'] = $lista_moneda;
        $_SESSION['lista_moneda1'] = $lista_moneda1;
        $_SESSION['lista_moneda2'] = $lista_moneda2;
        $_SESSION['lista_moneda3'] = $lista_moneda3;
        $_SESSION['lista_moneda4'] = $lista_moneda4;
       $_SESSION['lista_moneda_retiro'] = $lista_moneda3;
        // $_SESSION['lista_moneda_retiro'] = $lista_moneda_retiro;

	?>

</span></b>
<span class="pull-right" style="cursor:pointer" ><b>Tabla de Inversión</b> 
<i class="fa fa-question-circle fa-2x text-primary " data-toggle='tooltip' onclick="mostrar_tabla_divisa('bs')" title="Ver tabla de Inversión en Bolivares"></i>
<i class="fa fa-question-circle fa-2x text-success " data-toggle='tooltip'  onclick="mostrar_tabla_divisa('usd')" title="Ver tabla de Inversión en Dolares"></i>
<i class="fa fa-question-circle fa-2x text-warning " data-toggle='tooltip'  onclick="mostrar_tabla_divisa('euro')" title="Ver tabla de Inversión en Euros"></i>
<i class="fa fa-question-circle fa-2x text-danger " data-toggle='tooltip'  onclick="mostrar_tabla_divisa('pesoco')" title="Ver tabla de Inversión en Pesos Colombianos"></i>
</span>
</div>
</div>	   
     <div class="row">
      <div class="col-sm-4 col-lg-4">
        <div class="form-group">
		<label for="ape" class="col-md-12 control-label">Moneda</label>
          <div class="col-md-12">
           <select class="form-control" style="height:40px; font-size:20px" id="divisa" data-toggle="tooltip" onchange="return tipo_divisa(this);" title="Seleccione la divisa a invertir">

           	<?php 
	            echo $_SESSION['lista_moneda1'];
          	?>

		   <!--<option value="Bolivares" title="Bolivares Soberanos">BsS</option>
		   <option value="Dolares" title="Dolares EEUU">USD$</option>-->
		   </select>
			</div>
          </div>
       </div>	
	  <div class="col-sm-4 col-lg-4">
        <div class="form-group">
          <label for="ape" class="col-md-12 control-label">Monto a invertir</label>
          <div class="col-md-12">
            <input id="monto" style="height:40px; font-size:24px" onkeyup="return calculo_inv()" type="number" step="0.01" class="form-control"  maxlength="20" placeholder="Ej: 100,01" data-toggle="tooltip" title="Ingrese el monto a invertir">
			</div>
          </div>
       </div>  
   
      <div class="col-sm-4 col-lg-4">
        <div class="form-group">
		<label for="ape" class="col-md-12 control-label">Duración</label>
          <div class="col-md-12">
		  
		  
           <select class="form-control"  style="height:40px; font-size:20px" id="duracion" onchange="return calculo_inv()" data-toggle="tooltip" title="Seleccione el periodo de inversión">
		   <option value=""></option>
		   <optgroup label="Bolivares">
		   <option value="35">35 dias</option>
		   <?php
		   		if(comprobar_fecha2(cambiar_fecha($_SESSION["fecha_reg"]),'2019-12-15')>0){
		   	?>
		   <option value="70">70 dias</option>
		   <option value="105">105 dias</option>
		   <option value="140">140 dias</option>
		   <option value="175">175 dias</option>
		   <option value="210">210 dias</option>
		   
		   <option value="245">245 dias</option>
		   <option value="280">280 dias</option>
		   <option value="315">315 dias</option>
		   <option value="350">350 dias</option>
		   <option value="385">385 dias</option>
		   <option value="420">420 dias</option>
		   	<?php		
		   		} 
		   		else if(comprobar_fecha('2020-02-01 00:00:00')>0){ 

		   			$cons1=mysql_query("select * from inversiones where id_c='".$_SESSION['id_c']."' AND divisa='BsS'");
		   			if(mysql_num_rows($cons1)>0){
		   	?>
		   <option value="50">50 dias al 120%</option>
		    <?php }else{ ?>

		   <option value="70">70 dias</option>
		   <option value="105">105 dias</option>
		   <option value="140">140 dias</option>
		   <option value="175">175 dias</option>
		   <option value="210">210 dias</option>
		   
		   <option value="245">245 dias</option>
		   <option value="280">280 dias</option>
		   <option value="315">315 dias</option>
		   <option value="350">350 dias</option>
		   <option value="385">385 dias</option>
		   <option value="420">420 dias</option>
		    <?php }

			}else{ ?>
		   <option value="70">70 dias</option>
		   <option value="105">105 dias</option>
		   <option value="140">140 dias</option>
		   <option value="175">175 dias</option>
		   <option value="210">210 dias</option>
		   
		   <option value="245">245 dias</option>
		   <option value="280">280 dias</option>
		   <option value="315">315 dias</option>
		   <option value="350">350 dias</option>
		   <option value="385">385 dias</option>
		   <option value="420">420 dias</option>
		    <?php } ?>
		   </optgroup>
		   <!--periodo en dolares-->
		   <optgroup label="Periodos" style="display:none">
		   <option value="60">60 dias - 70%</option>
		   <option value="120">120 dias - 145%</option>
		   <option value="180">180 dias - 220%</option>
		   <option value="240">240 dias - 295%</option>
		   <option value="300">300 dias  - 370%</option>
		   <option value="360">360 dias - 445%</option>
		   </optgroup>
		   </select>
		   
           
			</div>
          </div>
       </div>
	</div> 
	
     <div class="row">     
      <div class="col-sm12 col-lg-12">
        <div class="form-group">
		<div class="col-md-12" align="center">
		   
		    <b>Nota:</b> <span style="color:red">Solo use <span id="decimal">la coma ( , )</span> para generar una cifra con decimales.</span> Ejemplo: <b>100,32</b>
		    <hr>
		<div id="cifra"><br></div>
		<hr>
		<b>Periodo de Inversión</b><bR><div  id="periodo">Desde __/__/____ Hasta __/__/____</div>
		<br><b>Ganancia</b><bR><div id="interes" style="color:blue; font-weight:bold; font-size:18px">0.00</div>
		<br>
		<div id="cifra2"><br></div>
		</div>
       </div>
       </div>
       
<center><div id="nliberacion">Lista de Liberaci&oacute;n...</div></center>
       
       
	</div> 
	<hr>
     <div class="row">     
      <div class="col-sm12 col-lg-12">
        <div class="form-group">
		<div class="col-md-12"  align="center">
		<button disabled class="btn btn-md btn-success" id="btn-invertir" onclick="return invertir()"><i class="fa  fa-plus-square"></i> Invertir</button>
		<button class="btn btn-md btn-danger" id="btn-cinvertir" onclick="$('#invertir,#cortina').hide(100,'linear'),	$('#btn-invertir').attr({'disabled':true});"><i class="fa  fa-remove"></i> Cancelar</button>
		</div>
       </div>
       </div>
	</div> 	
	
</div>	
</div>	


<script>parent.cerrar_carga();</script>
