<?php session_name("hng"); session_start();

if(isset($_SESSION["us"])){
  echo "<script>location.href='panel';</script>";
}

$n=rand(4,4);
//$n=rand(1,5);
 ?>
<!doctype html>
<html>
  <head>
  <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
  <meta http-equiv="Pragma" content="no-cache" />
  <meta http-equiv="Expires" content="0" />
  <meta http-equiv="Last-Modified" content="0">
  <meta name="description" content="Modal Window ">
    
    <script>
    var url=""+window.location+"";
    var url1, url2;
    url1=url.substring(0, 7); //http://
    url2=url.substring(7, 300); //www.fff.com....
    if(url1=='http://'){
          location.href='https://'+url2;
    } 
    </script>
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
   <link rel="icon" href="images/favicon.gif" type="image/gif">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
    <!-- FontAwesome 4.3.0 -->
     <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />    
    <link href="css/sweetalert.css" rel="stylesheet">
    <link href="css/style_inversion.css" rel="stylesheet">



    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  
<style>
#cortina{
    display:inline;
    background:#000;
    opacity:.6;
    top:0%;
    left:0%;
    width:100%;
    height:100%;
    position:fixed;
    z-index:1000;
}  
#anuncio{
    display:inline;
    background:#fff;
    top:10%;
    left:20%;
    width:60%;
    height:auto;
    position:absolute;
    z-index:1001;
    border-radius:6px;
}  

.parpadea {
  
  animation-name: parpadeo;
  animation-duration: 1s;
  animation-timing-function: linear;
  animation-iteration-count: infinite;

  -webkit-animation-name:parpadeo;
  -webkit-animation-duration: 2s;
  -webkit-animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
}
    
</style>
  </head>
  <body style="background-image:url(images/fondo<?php echo $n; ?>.jpg);  background-repeat:no-repeat; background-position:10% 0%;">
 

   <div id="cortina" class="hide"></div> 
   <div id="anuncio" class="hide">
       <div style="text-align:right;"><a href="#"  class="text-danger " onclick="$('#anuncio,#cortina').hide(500,'linear');">Cerrar <i class="fa fa-times"></i></a>
       <br></div>
       <center>
        <i class="fa fa-info-circle fa-4x"></i>
        <div align="justify" style="color:#000; font-size: 16px; padding:10px; background: #fff">Estimados clientes, en estos momentos estamos haciendo ajustes a la plataforma, por favor no realice operaciones de entrada y salida durante un periodo de 48 horas, pedimos disculpas por los inconvenientes generados, pero es de vital importancia realizar los ajustes para brindar mayor seguridad y confiabilidad a nuestros usuarios.</div>
      <!--<img src="images/anuncio.PNG"  class="img-responsive">-->
       </center>
       </div>
    
<!--ventana modal-->      
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="vmodal">
    
<div>

<div class="panel panel-warning">
<div class="panel-heading text-center"><big><big>¿OLVIDÓ SU CONTRASEÑA?</big></big></div>
<div class="panel-body">

<br><form onsubmit="return false;">
Correo Electrónico:

<div class="row">
<div class="col-md-12">
<input type="email" class="col-md-12 form-control" placeholder="Introduzca email, usuario@hng.com" style="height:48px; font-size:20px; width:100%;" id="correo">

<div class="col-md-12">
<br>
<center><button class="font btn btn-lg btn-success" id="recupera" style="margin:0px">Recuperar</button></center>
</div>
</div>
</div>



<br></form>
</div>
</div>
</div>
    </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
 
     

    
     <center>
     <div class="panel " align="" style="background:none; border:none;">
     
    <div class="panel-heading" style="height:86px;  background:#000" align="left">
      <a href="https://hombresdenegociosglobalca.com/principal" target="_blank" title="Ir al portal de HNG" data-toggle="tooltip">
        <img src="images/logo_oficial_new_acceso.png" style="width:250px;">
      </a>
    </div>
     <div class="panel-body" >
     
     <div class="panel panel-default"  style="width:280px">
     <div class="panel-heading">
     <big><big><b>ACCESO HNG-INVERSIONES</b></big></big>
     </div>
     <div class="panel-body">
     <b><center><span style="color:green; font-size:20px;" class="parpadea">Plataforma Operativa</span></center></b>
   <form class="form-horizontal" role="form" onsubmit="return false">   
   <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
     <label class="col-md-12 control-label" style="text-align:center">Correo Electronico</label>
          <div class="col-md-12">
    <input id="us" class="form-control" style="height:48px; font-size:18px">
      </div>
          </div>
       </div> 
    </div> 
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
     <label class="col-md-12 control-label" style="text-align:center">Contraseña</label>
          <div class="col-md-12">
    <input id="cv" type="password" class="form-control" style="height:48px; font-size:18px">
      </div>
          </div>
       </div> 
    </div> 
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
  
          <div class="col-md-12 pull-left">
    <input class="" style="height:24px; width:24px" id="recuerdame" type="checkbox" > <big>Recuerdame</big>
      </div>
      
          </div>
       </div> 
    </div> 
  
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
          <div class="col-md-12">
        <center><button class="btn btn-success btn-lg" style="border-radius:6px; font-family: 'Inconsolata';" id="btn-acceso"><i class="fa fa-sign-in"></i> Acceder</button></center>
      </div>
          </div>
       </div> 
    </div> 
  </form>
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group"><br><br>
     <label class="col-md-12 control-label"><button title="Pulse para recuperar contraseñas" class="btn-limpio text-success" data-toggle="modal" data-target="#myModal"  >¿Olvidó su Contraseña?</button></label>
          </div>
       </div> 
    </div>
    
     </div>
     </div>
     </div>
<a href="https://hombresdenegociosglobalca.com/principal" target="_blank" style="color:white; text-shadow:0px 0px 2px black; font-size: 14px;">
<b>www.hombresdenegociosglobalca.com</b>
</a>
     </div>
     </center>

      <footer class="main-footer" style="color:black; text-shadow:0px 0px 2px white">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.4.1
        </div>
        <strong>Copyright &copy; 2017.</strong> Todos los derechos reservados.
      </footer>

    <script src="js/jquery.js"></script>
    <script src="js/sweetalert.min.js"></script>
     <script src="js/md5-min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
    <script>
      $('[data-toggle="tooltip"]').tooltip(); 
    </script>
    <script src="js/proceso_acceso.js"></script>
  </body>
</html>