<?php 

/*esta funcion define la zona horaria, debes colocarla siempre al inicio o antes de usar la fecha u hora*/
date_default_timezone_set('America/Caracas');

/*ejemplo*/
$fecha=date('d-m-Y H:i:s a');


require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        // Logo

        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // AQUI SE EDITA LA CABECERA DE LA PAGINA (HEADER)
        $this->Cell(0, 15, 'aqui colocas el header', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // AQUI SE EDITA EL PIE DE PAGINA (FOOTER)
        $this->Cell(0, 10, 'aqui puedes colocar el footer', 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('EJEMPLO');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// ---------------------------------------------------------
// set font
$pdf->SetFont('helvetica', 'BI', 12);
// add a page
$pdf->AddPage();






// AQUI SE MUESTRA LA INFORMACION EN EL CUERPO DEL DOCUMENTO
$txt = <<<EOD
esto es un ejemplo de la fecha $fecha
EOD;







// print a block of text using Write()
$pdf->Write(0, $txt, '', 0, 'C', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
//SOLO VISUALIZA
$pdf->Output('mi_pdf.pdf', 'I'); 
//SOLO GENERA EL ARCHIVO SIN VISUALIZAR
//$pdf->Output('mi_pdf.pdf', 'F'); 
// GENERA EL ARCHIVO Y LO VISUALIZA
//$pdf->Output('mi_pdf.pdf', 'FI'); 






//============================================================+
// END OF FILE
//============================================================+


?>