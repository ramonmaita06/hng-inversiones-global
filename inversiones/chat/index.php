<?php  unset($_SESSION['nmsn']); $_SESSION['nmsn']=0; ?>

<script src="chat/js/procesos.js"></script>
<input id="nmens" type="hidden" value="<?php echo $_SESSION['nmsn']; ?>">
<audio src="js/sonido/tono1.mp3" style="display:none" controls id="tono"></audio>
              <!-- DIRECT CHAT -->
              <div id="myDirectChat" class="box box-warning direct-chat direct-chat-warning" style="border-bottom:none; box-shadow:0px 0px 0px white;" onclick="$('#mimsn').focus();">
                <div class="box-header with-border">
                  <h6 class="box-title" style="font-size:12px; font-weight:bold;" onclick="return listar_msn()"><i class="fa fa-comments-o"></i> Chat - <span data-toggle="tooltip" id="chat_user" ><?php echo @$_SESSION['con_quien']; ?></span></h6>
                  <div class="box-tools pull-right">
				  
				  <!--numeros de nuevos usuarios conectados-->
                    <span data-toggle="tooltip" title="" class='badge bg-yellow'><!--10--></span>
					
                    <button class="btn btn-box-tool"  id="michat1" onclick="return chat(0);"><i class="fa fa-minus text-red"></i></button>
					
                    <button title="Contactos" class="btn btn-box-tool" data-toggle="tooltip" data-widget="chat-pane-toggle" onclick="return listar_contactos()" id="lcontactos"><i class="fa fa-users"></i></button>
                    <button data-toggle="tooltip" style="display:none" class="btn btn-box-tool" id="bccerrar"  onclick="return chatear(0,'HNG - INVERSIONES');" title="Pulse cerrar la conversación"><i class="fa fa-times"></i></button>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body" >
				
                  <!-- Conversations are loaded here -->
                  <div class="direct-chat-messages" id="mensajes"  style="height:350px">
                  </div><!--/.direct-chat-messages-->


                  <!-- Contacts are loaded here -->
                  <div class="direct-chat-contacts"  style="height:350px">
                    <ul class='contacts-list' id="miscontactos">
                     					  
                    </ul><!-- /.contatcts-list -->
                  </div><!-- /.direct-chat-pane -->
                </div><!-- /.box-body -->
				
				
                <div class="box-footer">
                  <form action="#" method="post" onsubmit="return enviar_mens()">
                    <div class="input-group">
                      <input type="text" name="message" id="mimsn" placeholder="Escribir mensaje..." maxlength="250" class="form-control" disabled />
                      <span class="input-group-btn">
                        <button type="button" disabled id="benviar" class="btn btn-primary btn-flat" onclick="return enviar_mens();">Enviar</button>
                      </span>
                    </div>
                  </form>
                </div><!-- /.box-footer-->
              </div><!--/.direct-chat -->

			<input id="id_el" value="<?php echo @$_SESSION['id_el']; ?>" type="hidden">  
			 <script>listar_msn(); chat(0);</script>
			  