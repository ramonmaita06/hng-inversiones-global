<?php session_name("hng"); session_start();
include('../../php/cnx.php');
include('../../php/funciones.php');
date_default_timezone_set('America/Caracas');
class cajero{


	public function buscarCliente(){
		$resp = 0;
		$cons = mysql_query("select * from cliente where 
			cod_id LIKE '%".$_POST['buscar']."%' OR
			correo LIKE '%".$_POST['buscar']."%' OR
			nombre LIKE '%".$_POST['buscar']."%' OR
			apellido LIKE '%".$_POST['buscar']."%' OR
			tcasa LIKE '%".$_POST['buscar']."%'
			");
		$num = mysql_num_rows($cons);
		if($num>0){
			$lista = '<table id="table1" class=" table table-condensed table-striped table-hover table-fluid">
			<thead>
			<tr>
			<th>N&deg;</th>
			<th>Cod-id</th>
			<th>Nombres y Apellidos</th>
			<th>Telefono</th>
			<th>Correo</th>
			<th>Acción</th>
			</tr>
			</thead>
			<tbody>
			';
			$n=1;
			$resp = 1;
			while($info = mysql_fetch_array($cons)){

				$telf = "'".$info["tcasa"]."'";
				$correo = "'".$info["correo"]."'";
				$cliente = "'".$info["cod_id"]." - ".$info["nombre"]." ".$info["apellido"]."'";

					$select = '<button class="btn btn-sm btn-info" onclick="enviarUsuario('.$telf.','.$correo.','.$cliente.')" style="font-size:10px"><i class="fa fa-thumbs-o-up" style="font-size:16px"></i> Asignar</button>';
				

				$lista .= '<tr style="font-size:12px">
					<td>'.($n++).'</td>
					<td> <b>'.$info['cod_id'].'</b></td>
					<td>'.$info['nombre'].' '.$info['apellido'].'</td>
					<td>'.$info['tcasa'].'</td>
					<td>'.$info['correo'].'</td>
					<td>'.$select.'</td>
				</tr>';
			}
			$lista .= '</tbody></table>';
		}else{
			$lista = '
				<br><div class="alert alert-danger">No hubo coincidencias en la busqueda.</div><br><br>
				<center><i class="fa fa-group" style="font-size:128px; opacity:0.06;"></i></center>
				';
		}


		
		echo json_encode(array("salida"=>$resp,"listado"=>$lista));
	}


	public function verCajero(){
		$resp = 1;
		
		echo json_encode(array("salida"=>$resp,"saldo"=>$this->saldoCajero(),"estatus"=>$this->estatusCajero()));
	}

	public function saldoCajero(){
		$cons = mysql_query("select * from monedas where estatus = 1");
		if(mysql_num_rows($cons)>0){
			$lista = '';
			while($info = mysql_fetch_array($cons)){
				$cons1 = mysql_query("select SUM(monto) AS recarga from historico_cajero where id_c = '".$_SESSION['id_c']."' AND divisa = '".$info['moneda']."' AND estatus = '1'");
				$reg1 = mysql_fetch_array($cons1);

				$cons2 = mysql_query("select SUM(monto) AS retiro from historico_cajero where id_c = '".$_SESSION['id_c']."' AND divisa = '".$info['moneda']."' AND estatus = '2'");
				$reg2 = mysql_fetch_array($cons2);
				$total = $reg1['recarga'] - $reg2['retiro'];
				$lista .= '<option value="'.$total.'" val_moneda="'.$info['moneda'].'">'.$info['moneda'].' '.masmenos($total).'</option>';

			}
			$resp = 1;
			return $lista;
		}else{
			$resp = 0;
			return '<option style="color:red; font-size:10px">No posee portafolios</option>';
		}
		
	}




	public function saldoCajeroInterno(){

		$cons1 = mysql_query("select SUM(monto) AS recarga from historico_cajero where id_c = '".$_SESSION['id_c']."' AND divisa = '".$_POST['divisa']."' AND estatus = '1'");
		$reg1 = mysql_fetch_array($cons1);

		$cons2 = mysql_query("select SUM(monto) AS retiro from historico_cajero where id_c = '".$_SESSION['id_c']."' AND divisa = '".$_POST['divisa']."' AND estatus = '2'");
		$reg2 = mysql_fetch_array($cons2);
		$total = $reg1['recarga'] - $reg2['retiro'];
		
		if($total==''){$total=0;}
		$resp = 1;
		return $total;		
	}


	public function estatusCajero(){
		$cons = mysql_query("select * from cajero where id_c = '".$_SESSION['id_c']."'");
		if(mysql_num_rows($cons)>0){
			$info = mysql_fetch_array($cons);
			if($info['estatus'] == 1){ $bloquear = '<button  onclick="bloquearCajero(2)" class="btn btn-info" title="Bloquear cajero" data-toggle="tooltip"><i class="fa fa-unlock" style="font-size:20px"></i> <span class="ocultame2">Bloquear Cajero</span></button>';
			}
			else{ $bloquear = '<button  onclick="bloquearCajero(1)" class="btn btn-danger" title="Desbloquear cajero" data-toggle="tooltip"><i class="fa fa-lock" style="font-size:20px"></i> <span class="ocultame2">Desbloquear Cajero</span></button>';}
		}else{
			$bloquear = 2;
		}
		return $bloquear;
	}

	public function generarCodigo(){

	$fecha = date("dmYHis");
	$nue1 = '';
	$nue2 = '';

	if($this->saldoCajeroInterno()<floatval($_POST['monto']) || floatval($_POST['monto']) <= 0){

		echo json_encode(array("salida"=>0,"msn"=>"El monto del codigo estatero es superior al monto del portafolio, debe verificar el monto. "));
		return false;
	}




	if(isset($_FILES['billete']['name'])){

	
	
		$uploaddir="soporteCodigo/".$_SESSION['cod_id']."/";
		if(!is_dir($uploaddir)){
			mkdir($uploaddir);
		}

			

		if(isset($_FILES['tasa']) && $_FILES['tasa']!==''){

			$uploadfile = $uploaddir . basename($_FILES['tasa']['name']);
			if (move_uploaded_file($_FILES['tasa']['tmp_name'], $uploadfile)) {

			$extension1 = pathinfo($_FILES['tasa']['name'], PATHINFO_EXTENSION);
			$nue1=$uploaddir.$fecha."_tasa.".$extension1;
			rename($uploadfile,$nue1);


			}else{
				echo json_encode(array("salida"=>0,"msn"=>"Debe subir el soporte de la tasa del dia."));
				return false;

			}
		}


		$uploadfile1 = $uploaddir . basename($_FILES['billete']['name']);
		if (move_uploaded_file($_FILES['billete']['tmp_name'], $uploadfile1)) {

		$extension2 = pathinfo($_FILES['billete']['name'], PATHINFO_EXTENSION);
		$nue2=$uploaddir.$fecha."_billete.".$extension2;
		rename($uploadfile1,$nue2);


		}else{
			echo json_encode(array("salida"=>0,"msn"=>"Debe subir el soporte de los billetes o la transferencia."));
			return false;

		}

		
	}
	/*Guardar informacion del codigo estatero*/



		$infocliente = 'Generado por '.$_SESSION['cliente'];

		if(isset($_POST['telefono'])){
			$envia = $_POST['telefono'].','.$_POST['correo'];
		}else{
			$envia = '';
		}

		$cons = mysql_query("select * from monedas where moneda = '".$_POST['divisa']."'");
		$reg = mysql_fetch_array($cons);
		$divisa = $reg['indice'];

		include("../../php/cnxCentral.php");
		$codigoEst = $this->new_code();

		$infocod = "Su codigo estatero es: ".$codigoEst." por un monto de ".masmenos($_POST['monto'])." ".$_POST['divisa']." \nNota: ".substr($_POST['descripcion'],0,80)." \n".date('d/m/Y H:i:s a');

		$infocodCorreo = '<center><div style="font-size:16px; border-radius:6px; padding:10px; background: #000; width:400px; border:1px solid grey; font-weight:bold; color:white;"><div style="text-align: left;"><img src="https://hombresdenegociosglobalca.com/inversiones/images/logo_oficial_new_acceso.png" style="width:200px"></div><br>Su código estatero es: <span style="font-size:20px; background:white; color:green">'.$codigoEst.'</span> por un monto de '.masmenos($_POST['monto']).' '.$_POST['divisa'].' <br>Nota: '.substr($_POST['descripcion'],0,80).' <br>'.date('d/m/Y H:i:s a').'<br><br><span style="font-size:12px"><i>'.$infocliente.'</i></span></div></center><hr><br>';


		mysql_query("insert into precargados values(NULL,'".$_SESSION['cod_id']."','".$codigoEst."','".$_POST['monto']."','".$infocliente."',NOW(),NOW(),'Activo','".$envia."','".$divisa."','Estatero: ".$_POST['descripcion']."','".$_POST['estatus']."',NOW())");

		$cons2 = mysql_query("select * from precargados order by id_cod desc");
		$info = mysql_fetch_array($cons2);


		mysql_query("insert into soporte_codigos values(NULL,'".$info['id_cod']."',NOW(),'".$nue1."','".$nue2."')");



		include("../../php/cnx.php");
		mysql_query("insert into historico_cajero values(NULL,'".$_SESSION['id_c']."',NOW(),'HNG-ESTATERO',NOW(),'".$this->referencia()."','".$_POST['monto']."',2,'PRECARGADO','".$_POST['divisa']."','".$codigoEst."')");



		if(isset($_POST['telefono']) || isset($_POST['correo'])){
			$this->enviar_a($infocod,$infocodCorreo);
		}

		   echo json_encode(array("salida"=>1, "msn"=>"El codigo estatero fue generado correctamente.\n\n".$infocod));

		
	}

	public function new_code(){

		$n=rand(10000000,99999999);
		$cons=mysql_query("select * from precargados where cod='".$n."'");
		if(mysql_num_rows($cons)>0){
			$this->new_code();
		}else{return $n;}
		
	}

	public function referencia(){
			$ref = rand(10000000, 99999999);
			$cons = mysql_query("select * from historico_cajero where referencia = '".$ref."'");
			if(mysql_num_rows($cons)>0){
				$this->referencia();
			}else{
				return $ref;
			}
		}




	public function enviar_a($texto,$micorreo){
		

		$telefonos = '58'.substr($_POST['telefono'], 1,20);
		$correo="<".$_POST["correo"].">";
		 /*Correos*/
		require_once('../../php/AttachMailer.php'); 
		$mailer = new AttachMailer("HNG C.A. <avisos.hng@gmail.com>", $correo, "CODIGO ESTATERO HNG", $micorreo);
		$mailer->send();



		$usuario="hombresdenegociosglobal@gmail.com";
 		$clave="deydeco_1709";
		$parametros="usuario=$usuario&clave=$clave&texto=$texto&telefonos=".$telefonos;
		$url = "http://www.sistema.massivamovil.com/webservices/SendSms";
		$handler = curl_init();
		curl_setopt($handler, CURLOPT_URL, $url);
		curl_setopt($handler, CURLOPT_POST,true);
		curl_setopt($handler, CURLOPT_POSTFIELDS, $parametros);
		curl_setopt($handler, CURLOPT_TIMEOUT, 12); 
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, TRUE);
		$response = curl_exec ($handler);
		curl_close($handler);

		return true;
	}




	public function ListarCodigo(){

		$cons = mysql_query("select * from monedas where estatus = 1");
		if(mysql_num_rows($cons)>0){
			
			while($reg = mysql_fetch_array($cons)){
				$misMonedas[$reg['indice']] = $reg['moneda'];
			}
		}

		$condicion['Pendiente'] = '<span style="color:orange; font-weight:bold;"><i class="fa fa-clock-o"></i> Pendiente</span>'; 
		$condicion['Pagado'] = '<span style="color:green; font-weight:bold;"><i class="fa fa-check"></i> Pagado</span>'; 

		$estatus['Usado'] = '<span style="color:red; font-weight:bold;"><i class="fa fa-frown-o"></i> Usado</span>'; 
		$estatus['Activo'] = '<span style="color:green; font-weight:bold;"><i class="fa fa-smile-o"></i> Activo</span>'; 
		$estatus['Cancelado'] = '<span style="color:red; font-weight:bold;"><i class="fa fa-smile-o"></i> Cancelado</span>';

		include("../../php/cnxCentral.php");
		$resp = 0;
		$cons = mysql_query("select * from precargados where cod_id = '".$_SESSION['cod_id']."' AND descripcion LIKE '%Estatero%' limit 1000");
		$num = mysql_num_rows($cons);
		if($num>0){
			$lista = '
			<style>
			.rasparCod{
				color:grey;
				background: grey;
				width: 120px;
				height: 10px;
				border-radius: 4px;
				border:1px #aaa solid;
				padding:4px;
				cursor: pointer;
			}
			.rasparCod:hover{
				color:#000;
				background: white;
			}
			</style>
			<table id="table2" class=" table table-condensed table-striped table-hover table-fluid table-bordered">
			<thead>
			<tr style="font-size:12px">
			<th>N&deg;</th>
			<th>Cod-id</th>
			<th>Cod. Estatero</th>
			<th>Monto</th>
			<th>Moneda</th>
			<th>Estado</th>
			<th>Activación</th>
			<th>Fecha</th>
			<th>Descripción</th>
			<th>Correo</th>
			<th>Condición</th>
			<th>Soporte</th>
			</tr>
			</thead>
			<tbody>
			';
			$n=1;
			$resp = 1;
			while($info = mysql_fetch_array($cons)){

				$cons2 = mysql_query("select * from soporte_codigos where id_cod = '".$info['id_cod']."'");
				$reg = mysql_fetch_array($cons2);
				$soporte = '<center><table width="80">';

				if($reg['tasadeldia']!=''){
					$soporte.= '<td width="40" align="center"><a href="php/'.$reg['tasadeldia'].'" data-toggle="tooltip" title="Pulse para ver el soporte de la tasa del dia" target="'.$reg['tasadeldia'].'"><i class="fa fa-newspaper-o fa-2x text-danger"></i></td>';
				}
				if($reg['billetes']!=''){
					$soporte.= '<td width="40" align="center"><a href="php/'.$reg['billetes'].'" data-toggle="tooltip" title="Pulse para ver el soporte de los billetes o transferencia" target="'.$reg['billetes'].'"><i class="fa fa-money fa-2x text-success"></i></td>';
				}
				$soporte.='</table></center>';

				$enviara = explode(",",$info['correo']);

				if($info['estatus']=='Activo'){$mostrarCod = '<span class="rasparCod">'.$info['cod'].'</span>';}
				else{
					$mostrarCod = $info['cod'];
				}


				$lista .= '<tr style="font-size:11px">
					<td>'.($n++).'</td>
					<td> <b>'.$info['cod_id'].'</b></td>
					<td style="color:blue; font-weight:bold;">'.$mostrarCod.'</td>
					<td style="text-align:right">'.masmenos($info['monto']).'</td>
					<td>'.$misMonedas[$info['divisa']].'</td>
					<td>'.$estatus[$info['estatus']].'</td>
					<td>'.$info['infocliente'].'</td>
					<td>'.cambiar_fecha_es($info['fecha_creacion']).'</td>
					<td>'.$info['descripcion'].'</td>
					<td>'.$enviara[0].' '.$enviara[1].'</td>
					<td>'.$condicion[$info['estado']].'</td>
					<td>'.$soporte.'</td>
				</tr>';
			}
			$lista .= '</tbody></table>';
		}else{
			$lista = '
				<br><div class="alert alert-danger">No hubo coincidencias en la busqueda.</div><br><br>
				<center><i class="fa fa-group" style="font-size:128px; opacity:0.06;"></i></center>
				';
		}


		
		echo json_encode(array("salida"=>$resp,"listado"=>$lista));
	}


	public function infoEstatero($cod){
		include("../../php/cnxCentral.php");
		$cons = mysql_query("select * from precargados where cod = '".$cod."'");
		$info = mysql_fetch_object($cons);
		include("../../php/cnx.php");
		return $info;
	}

//	id_bono	id_c	fecha_compra	banco	fecha	referencia	monto	estatus	tipot	divisa estatero
	public function movimientos(){
		$resp = 0;
		$total = Array();
		$entrada = Array();
		$salida = Array();
		$cons = mysql_query("select * from historico_cajero where id_c = '".$_SESSION['id_c']."' order by fecha desc limit 1000");
		if(mysql_num_rows($cons)){
			$lista = '
			<style>
			.rasparCod{
				color:grey;
				background: grey;
				width: 120px;
				height: 10px;
				border-radius: 4px;
				border:1px #aaa solid;
				padding:4px;
				cursor: pointer;
			}
			.rasparCod:hover{
				color:#000;
				background: white;
			}
			</style>
			<table id="table1" class="table table-condensed table-hover table-striped table-fluid">
			<thead>
				<tr style="font-size:12px">
				<th>Fecha</th>
				<th>Descripción</th>
				<th>Referencia</th>
				<th>Estatero</th>
				<th>Monto</th>
				<th>Moneda</th>
				<th>Control</th>
				</tr>
			</thead>
			<tbody>';
			while ($info = mysql_fetch_array($cons)) {
				if($info['estatus']==1){
					$control = '<div style="color:green; font-size:18px; font-weight:bold;" title="Ingreso de Estateros" data-toggle="tooltic">+</div>';
				}else{
					$control = '<div style="color:red; font-size:18px; font-weight:bold;" title="Egreso de Estateros" data-toggle="tooltic">-</div>';
				}


				if($this->infoEstatero($info['estatero'])->estatus=='Activo'){$mostrarCod = '<span class="rasparCod">'.$info['estatero'].'</span>';}
				else{
					$mostrarCod = $info['estatero'];
				}


				$lista .= '
				<tr style="font-size:12px">
				<td>'.cambiar_fecha_es_hora($info['fecha']).'</td>
				<td>'.$info['banco'].'</td>
				<td>'.$info['referencia'].'</td>
				<td style="color:blue; font-weight:bold;">'.$mostrarCod.'</td>
				<td><b>'.masmenos($info['monto']).'</b></td>
				<td><b>'.$info['divisa'].'</b></td>
				<td align="center">'.$control.'</td>
				</tr>
				';
				if($info['estatus'] == 1){
					$entrada[$info['divisa']] = $entrada[$info['divisa']] + $info['monto'];
				}

				if($info['estatus'] == 2){
					$salida[$info['divisa']] = $salida[$info['divisa']] + $info['monto'];
				}

				$total[$info['divisa']] = $entrada[$info['divisa']] - $salida[$info['divisa']];
			}
			$lista .= '</tbody></table>';


			$lista .= '
			<table class="table alert alert-warning">
			<tr>';
			$cons2 = mysql_query("select * from monedas where estatus = 1");
			while($info2 = mysql_fetch_array($cons2)){
				if(isset($total[$info2['moneda']])){
					$lista .='<td style="border-radius:6px; border:1px #ccc solid; font-size:12px; font-weight:bold" data-toggle="tooltip" title="Posee: '.masmenos($total[$info2['moneda']]).' '.$info2['moneda'].'"><img src="../images/monedas_paises/'.$info2['pais'].'.gif" width="32" height="20"> '.masmenos($total[$info2['moneda']]).' <span style="font-size:10px">'.$info2['moneda'].'</span></td>';
				}
			}
			$lista .='</tr>
			</table>';


			$resp = 1;
		}else{
			$lista = '<center><br><br><br><i class="fa fa-info-circle fa-3x text-warning"></i> Sin movimientos...</center>';
		}
		echo json_encode(array("salida"=>$resp,"listado"=>$lista));
	}


}$obj = new cajero;

if($_POST['opc']==1){ $obj->buscarCliente(); }
if($_POST['opc']==2){ $obj->verCajero(); }
if($_POST['opc']==3){ $obj->generarCodigo(); }
if($_POST['opc']==4){ $obj->ListarCodigo(); }
if($_POST['opc']==5){ $obj->movimientos(); }




?>



