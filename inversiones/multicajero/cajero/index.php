<?php session_name("hng"); session_start(); 
date_default_timezone_set('America/Caracas');
?>


	<style>
	.adaptame .icon{font-size:24px;}
	.ocultame2{display:inline;}
	.adaptame{	width:86px; }
	.mimenu{width:100%;}
	 @media only screen and (max-width: 600px){
	.mimenu{position:absolute;}
	.adaptame{width:auto;}
	.adaptame .icon{font-size:20px;}

	.ocultame2{display:none;}
	}

	#cuerpoCajero{
		width:100%;
		height:auto;
	}
	.noAplica{
		display:;
	}

input[type="file"] {
    padding: 5px 10px;
    background: #D4EDE0;
    color:#888;
    border:0px solid #fff;
}
 
input[type="file"]:hover{
    color:#fff;
    background: #70C399;
}

.validar{
	border: 1px solid red;
	border-left:26px solid #AA000A;
}
	</style>


   <form enctype="multipart/form-data"  class="form-horizontal" role="form" id="formuploadajax" method="post">
<div class="panel panel-success">
	<div class="panel-heading"><i class="fa fa-barcode icon fa-2x"></i> &nbsp;&nbsp;&nbsp;<big><b>CODIGO ESTATERO</b></big>
		<a href="#" class="pull-right btn btn-warning btn-md" onclick="verCodigos()"><i class="fa fa-barcode"></i> <i class="fa fa-search"></i> Ver Codigos</a>
				
	</div>
	<div class="panel-body">

		<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<label class="col-md-4">Saldo Estateros</label>
					<div class="col-md-8"><select class="form-control" id="miSaldo" name="miSaldo" onchange="condicionSoporte()"></select>
						<input type="hidden" name="divisa" id="divisa">
					</div>
				</div>
				<div class="col-md-6">
					<label class="col-md-4">Monto a precargar</label>
					<div class="col-md-8"><input class="form-control" min="0" step="0.01" type="number" id="monto" name="monto" placeholder="Monto estatero"></div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<label class="col-md-4">Descripción</label>
					<div class="col-md-8">
						<textarea class="form-control" id="descripcion" maxlength="60" name="descripcion"style="width:100%; height:64px;" placeholder="Descripcion opcional"></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<label class="col-md-4">Estatus</label>
					<div class="col-md-8">
						<select class="form-control" id="estatus" name="estatus">
							<option value="Pagado">Pagado</option>
							<option value="Pendiente">Pendiente</option>
						</select>
					</div>
				</div>

			</div>
		</div>


		<div class="row noAplica" >
			<div class="form-group">
				<div class="col-md-6 oculto">
					<label class="col-md-4">Tasa del Día</label>
					<div class="col-md-8">
						<!---->

						<input  type="file" id="tasa" class="filestyle form-control" name="tasa"/>
					</div>
				</div>
				<div class="col-md-6">
					<label class="col-md-4">Billetes o Transferencia (Electronicos)</label>
					<div class="col-md-8">
						<!---->
						<input  type="file" id="billete" class="form-control" name="billete"/>
					</div>
				</div>

			</div>
		</div>
<hr>



		<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<label class="col-md-4">Enviar a</label>
					<div class="col-md-8">&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" style="transform: scale(3);" id="enviara" onchange="enviar_a(this);"> &nbsp;&nbsp;<span class="text-success" style="font-weight:bold;" id="cliente"></span><br><br></div>
				</div>
				<div class="col-md-6">

					<label class="col-md-4">Buscar Usuario</label>
				 	<div class="col-md-8">
				 	<div class="input-group">
				      <input class="form-control envia" disabled type="text" id="busuario" placeholder="Buscar por: nombre, apellido, correo, telefono" title="Buscar por: nombre, apellido, correo, cedula, telefono" data-toggle="tooltip" >
				      <span class="input-group-btn">
				        <a href="#" onclick="buscarUsuario()" disabled class="btn btn-success btn-md envia"><i class="fa fa-search"></i> Buscar</a>
				      </span>
				    </div>
				    </div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-md-6">
					<label class="col-md-4">Telefono Celular</label>
					<div class="col-md-8">
						<input class="form-control envia" disabled id="telefono" maxlength="11" name="telefono" title="Colocar telefono celular del receptor del codigo" data-toggle="tooltip" placeholder="Ej: 04141234567">
					</div>
				</div>
				<div class="col-md-6">
					<label class="col-md-4">Correo Electronico</label>
					<div class="col-md-8">
						<input class="form-control envia" disabled id="correo" name="correo" placeholder="Ej: pedroperez123@gmail.com (Opcional)">
					</div>
				</div>

			</div>
		</div>

		<div class="row">
			<div class="form-group">
				<div class="col-md-12">
					
					<div class="col-md-12">
						<br><br>
						<center><button type="submit"  class="btn btn-md btn-info"  id="generar" ><i class="fa fa-barcode"></i> Generar Estatero</button></center>
					</div>
				</div>
			</div>
		</div>

	</div>


    </form>
 


<script type="text/javascript">

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}

$(function(){

	$("input").click(function(e){
		$(this).removeClass("validar");
	});

	$('input[type="file"]').click(function(e){
		$(this).css({"border": "1px solid #ccc","border-left":"1px solid #ccc"});
	});


    $("#formuploadajax").on("submit", function(e){
        e.preventDefault();


	if($("#monto").val()<=0){
		swal("","Debe colocar un monto superior a 0","error");
		$("#monto").addClass("validar");
		return false;
	}
	var monto = $("#monto").val();
	var divisa=$("#miSaldo option:selected").attr('val_moneda');
	if(divisa=='BsS'){
		if($("#tasa").val()==''){
			swal("","Debe cargar la imagen de la tasa del dia","error");
			$("#tasa").css({"border": "1px solid red","border-left":"26px solid #AA000A"});
			return false;
		}

		if($("#billete").val()==''){
			swal("","Debe cargar la imagen de soporte de los billetes o transferencia bancaria","error");
			$("#billete").css({"border": "1px solid red","border-left":"26px solid #AA000A"});
			$("#billete").addClass("validar");
			return false;
		}
	}


	if(divisa!='BsS'){
		/*if($("#tasa").val()==''){
			swal("","Debe cargar la imagen de la tasa del dia","error");
			$("#tasa").css({"border": "1px solid red","border-left":"26px solid #AA000A"});
			return false;
		}*/

		if($("#billete").val()==''){
			swal("","Debe cargar la imagen de soporte de los billetes o transferencia bancaria","error");
			$("#billete").css({"border": "1px solid red","border-left":"26px solid #AA000A"});
			$("#billete").addClass("validar");
			return false;
		}
	}

	if($("#enviara").prop("checked")==true){
		if($("#telefono").val().length<10){
			$("#telefono").addClass("validar");
			swal("","Debe agregar el número telefónico donde llegará el código estatero","error");
			return false;
		}
		if($("#correo").val().length>0){
			var validarEmail=validateEmail($("#correo").val());
			if(validarEmail==false){
				swal("","Debe agregar correctamente la dirección de correo donde llegará el código estatero","error");
				$("#correo").addClass("validar"); 
				return false;	
			}
			if($("#correo").val().length < 6){
				swal("","Debe agregar correctamente la dirección de correo donde llegará el código estatero","error");
				$("#correo").addClass("validar");
				return false;
			}
		}

	}

		swal({
		  title: "¿Esta segur@ que desea generar un codigo Estatero?",
		  text: "Esta a punto de generar un codigo estatero de "+number_format(monto,2,",",".")+" "+divisa,
		  type: "info",
		  showCancelButton: true,
		  closeOnConfirm: false,
		  showLoaderOnConfirm: true,
		  confirmButtonText: "Si, Acepto",
		  cancelButtonText: "No, Cancelar"
		},
		function(isConfirm){
			
		  if (isConfirm) {


        var f = $(this);
        var formData = new FormData(document.getElementById("formuploadajax"));
        formData.append("opc", 3);
        //formData.append(f.attr("name"), $(this)[0].files[0]);
        $.ajax({
            url: "php/procesoCajero.php",
            type: "post",
            dataType: "html",
            data: formData,
            cache: false,
            contentType: false,
     		processData: false,
     		success:function(datos){
				var jsonData = JSON.parse(datos);
            	if(jsonData.salida==1){
            		$("#tasa, #billete").val("");
            		swal("",jsonData.msn,"success");
                	saldoCajero();
            	}else{
            		swal("",jsonData.msn,"error");            		
            	}

            }
        });

		}
		});
		e.stopPropagation();
    });


	$('[data-toggle="tooltip"]').tooltip(); 
});
	saldoCajero();

</script>
