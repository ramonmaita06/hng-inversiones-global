

  <div class="modal fade" id="modalCajeroAsignado" role="dialog" style="z-index:1060">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-user fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">Cajero</span>
          <span id="idCliente1" class="hide"></span>
          <span id="codCliente1" class="hide"></span>
        </div>
        <div class="modal-body">
         <span id="btnBloquear"></span>
          <a href="#" id="accesoCuenta"   target="1" >
          <button class="btn btn-warning" title="Ver oficina del cajero" data-toggle="tooltip"><i class="fa fa-sign-in" style="font-size:20px"></i> <span  class="ocultame2">Ver Cuenta</span></button></a>
          <span class="pull-right" style="font-weight: bold; font-size: 18px" title="Saldo actual del cajero" data-toggle="tooltip">
            <table><td><i class="fa fa-briefcase" style="font-size:20px" ></i> &nbsp;</td>
           <td><select id="mi_saldo" class="form-control" style=""></select></td></table>
         &nbsp;&nbsp;&nbsp;</span>

          <div class="panel panel-success">
            <div class="panel-heading">
              <b>RECARGA DE ESTATERO</b>
            </div>
            <div class="panel-body">
          <form class="form-horizontal" role="form" onsubmit="return false;">
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-4">Estatero</label>
                  <div class="col-md-8"><select id="mi_saldoEstatero2" class="form-control"></select></div>
                </div>

                <div class="col-md-6">
                  <label class="col-md-4">Monto</label>
                  <div class="col-md-8"><input type="number" step="0.01" min="0" placeholder="Monto a recargar"  id="monto_recarga" class="form-control"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-4">Descripción</label>
                  <div class="col-md-8"><textarea id="descripcion" class="form-control" placeholder="Opcional"></textarea></div>
                </div>
                <label class="col-md-4"></label>
                <div class="col-md-6">
                  <div class="col-md-12"><center>
                    <button onclick="agregarEstatero()" class="btn btn-success" title="Agregar Estatero" data-toggle="tooltip"><i class="fa fa-money" style="font-size:20px"></i> <span class="">Agregar Estatero</span></button>
                  </center></div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
          




          <div class="panel panel-success">
            <div class="panel-heading">
            <table width="100%">
              <td width="86%"><b>MIS MOVIMIENTOS</b></td>
              <td align="right"><button class="btn btn-default fa fa-file-pdf-o"></button></td>
              <td align="right"><button class="btn btn-default fa fa-print"></button></td>
            </table>
            </div>
          </div>
          <div id="movimientos1"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>









  <div class="modal fade" id="modalMultiCajero" role="dialog">
    <div class="modal-dialog modal-lg">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-users fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">MultiCajero Estatero HNG</span>

        </div>
        <div class="modal-body">
       
          <div class="panel panel-success">
            <div class="panel-heading">
            <table width="100%">
              <td width="86%"><b>USUARIOS REGISTRADOS</b></td>
              <td align="right"><button class="btn btn-default fa fa-file-pdf-o"></button></td>
              <td align="right"><button class="btn btn-default fa fa-print"></button></td>
            </table>
            </div>
          </div>
          <div id="listadoCajeros"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>



<div class="modal fullscreen-modal fade" id="modalCodigos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-lg" style="width:96%;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-barcode fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">CODIGOS ESTATEROS</span>

        </div>
        <div class="modal-body">

          <div id="listadoCodigos"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>



<div class="modal fullscreen-modal fade" id="modalCuentas" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-list-alt fa-2x text-danger"></i> <span class="text-danger" style="font-weight: bold;" id="modaltitle1">CUENTAS AUTORIZADAS</span>

        </div>
        <div class="modal-body">
          <p>
            <pre style="background: white">
            <a href="https://www.mercantilbanco.com" data-toggle="tooltip" title="Ir al banco Mercantil" target="https://www.mercantilbanco.com"><img src="../images/mercantil.jpg" width="120"></a>
            <b>0105-0086-97-1086086791</b>
            Cta. Corriente
            Titular: Jose Gregorio Paredes Andrades
            C.I.V: 13.131.611
            Correo: joseparedes_empresario@hotmail.com
            Teléfono: 0414-8590115 <span style="color:blue">Pago Móvil</span>
          </pre>
          </p>
          <p>
            <pre style="background: white">
            <a href="https://www.provincial.com/" data-toggle="tooltip" title="Ir al banco Provincial" target="https://www.provincial.com/"><img src="../images/provincial.jpg" width="120"></a>
            <b>0108-0165-64-0100032501</b>
            Cta. Corriente
            Titular: Jose Gregorio Paredes Andrades
            C.I.V: 13.131.611
            Correo: joseparedes_empresario@hotmail.com
            Teléfono: 0414-8590115 <span style="color:blue">Pago Móvil</span>
          </pre>
          </p>


        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>