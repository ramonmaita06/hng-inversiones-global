function colorCajero() {
var i=Math.floor(Math.random() * (17 - 1)) + 1;
  
var color=["warning","success","info","danger","default","primary","success","danger","warning","primary","danger","success","default","primary","success","danger","warning"];  
return color[i];
}




var afil="";
function agregar_multicajero(){
var cajero=$("#multicajeros").val();
var cajero2=$("#multicajeros option:selected").text();



if(document.getElementById("enviar_a").innerHTML.search(cajero)>0){
	swal("MULTICAJERO","El multicajero ya fue agregado, intente con otro.","error");
}
else{
	var otro = '"'+cajero+', "';
afil="<button onclick='return quitar_multicajero(this,"+otro+")' title='Pulse para quitar el: "+cajero+"' class='btn btn-"+colorCajero()+"  fa fa-envelope-o'> "+cajero2+"</button> ";
$("#enviar_a").append(afil);
$("#multicajeros").val("");
$("#enviar_aH").append(cajero+", ");
	//swal("MULTICAJERO","Agregando multicajero","success");	
}


}


function quitar_multicajero(el,codid){
var ancla = el;
var padre = ancla.parentNode;
var hijoRemovido = padre.removeChild(ancla);
var cod=document.getElementById("enviar_aH").innerHTML;
cod=cod.replace(codid, ' ');
document.getElementById("enviar_aH").innerHTML=cod;
	//swal("MULTICAJERO","Quitando multicajero","success");

}





function enviar_mensaje(){
	
	
	var cajero=$("#enviar_aH").html();
	var mtexto=$("#mtexto").val();
	var codH = $("#enviar_aH").html();
	document.getElementById("envioMensaje").disabled=true;
	document.getElementById("envioMensaje").innerHTML='<i class="fa fa-spinner imgr"></i> Espere...';
	
	codH = $("#enviar_aH").text().replace(' ', '');
	if(codH==""){
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar al menos un cajero para poder enviar el mensaje", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#multicajeros").focus(); 
		document.getElementById("envioMensaje").disabled=false;
		document.getElementById("envioMensaje").innerHTML='<i class="fa fa-send"></i> Enviar';

		},3000); return false;
	}


	if($("#mtexto").val().length<10){
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar al menos 10 caracteres para enviar el mensaje", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#mtexto").focus(); 
		document.getElementById("envioMensaje").disabled=false;
		document.getElementById("envioMensaje").innerHTML='<i class="fa fa-send"></i> Enviar';
		},3000); return false;
	}

	

swal({
  title: "¿Esta segur@ que desea enviar el mensaje?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
	
  if (isConfirm) {
	$.post("php/procesoMensaje.php",{opc:2, lista:cajero, mtexto:mtexto},function(resp){
		if(resp.salida){
			swal("ENVIO DE MENSAJE","El mensaje se envio exitosamente","success");
		}else{
			swal("ENVIO DE MENSAJE","No fue posible enviar el mensaje a los multicajeros asignados","error");		
		}
		document.getElementById("envioMensaje").disabled=false;
		document.getElementById("envioMensaje").innerHTML='<i class="fa fa-send"></i> Enviar';
	},"json");
  }else{
  	document.getElementById("envioMensaje").disabled=false;
	document.getElementById("envioMensaje").innerHTML='<i class="fa fa-send"></i> Enviar';
  }
  
});


	
}



function cargarCajeros(codid){

	$.post("php/procesoMensaje.php",{opc:1,codid:codid},function(resp){
		if(resp.salida==1){
			$('#modalMMMultiCajero').modal('show');
			$('#enviar_a').html(resp.listado);
			$('#enviar_aH').html(resp.listadoH);
			$('#multicajeros').html(resp.listadoSel);
		}else{
			swal("","Error al cargar los datos de los cajeros, intente de nuevo.","error");
		}
	},"json");
}