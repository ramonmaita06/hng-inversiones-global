function moduloCajero(opc){
	$("#cuerpoCajero").load(opc);
	$(document).ready();	
}

function data_tables(){
	setTimeout(function(){
 		
	    $('#table1').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });

		$("#table1").DataTable();
		$("#table1_wrapper").css({"width":"96%"});
		$('[data-toggle="tooltip"]').tooltip();
	},500);
}


function data_tables1(){
	setTimeout(function(){
 		
	    $('#table5').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });

		$("#table5").DataTable();
		$("#table5_wrapper").css({"width":"96%"});
		$('[data-toggle="tooltip"]').tooltip();
	},500);
}


function data_tables2(){
	setTimeout(function(){


	    $('#table2').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });
		$("#table2").DataTable();
		$("#table2_wrapper").css({"width":"96%"});
		$('[data-toggle="tooltip"]').tooltip();
	},500);
}



function data_tables3(){
	setTimeout(function(){


	    $('#table3').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });
		$("#table3").DataTable();
		$("#table3_wrapper").css({"width":"96%"});
		$('[data-toggle="tooltip"]').tooltip();
	},500);
}

function buscarCliente(){
	var buscar = $("#cliente").val();
	if(buscar.length < 3){ $("#cliente").focus(); return false;}
	$.post("php/proceso.php",{opc:1, buscar:buscar},function(resp){
		$("#infoCajero").html(resp.listado);
		data_tables();
	},"json");

}



function asignarCajero(id,cliente,cod){
	$("#modalCajero").modal("show");
	$("#modaltitle").html(cliente);
	$("#idCliente").html(id);
	$("#codCliente").html(cod);
	$(".oculto").show();
	$("#monto").val("");
	$("#movimientos").html("");
}

function crear_cajero(){
	var moneda = $("#mi_saldoEstatero1").val();
	var divisa=$("#mi_saldoEstatero1 option:selected").attr('val_moneda');
	var monto = $("#monto").val();
	var fecha = $("#fecha_recarga").val();
	var id = $("#idCliente").html();
	var cod = $("#codCliente").html();

	if(monto<=0){
		$("#monto").focus();
		return false;
	}


swal({
  title: "¿Esta segur@ que desea asignar al usuario como cajero HNG?",
  text: "Esta a punto de asignar "+number_format(monto,2,",",".")+" "+divisa,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
	
  if (isConfirm) {
	$(".oculto").hide();

	$.post("php/proceso.php",{opc:2, moneda:moneda, divisa:divisa, monto:monto, fecha:fecha, id_c:id, cod:cod},function(resp){
		
		if(resp.salida == 1){
			swal("",resp.msn,"success");
			saldoEstatero();
			buscarCliente();
		}else{
			$(".oculto").show();
			swal("",resp.msn,"error");
		}
		
		$("#movimientos").html(resp.movimientos);


	},"json");	
  }
  
});
}



function verCajero(id,cliente,cod,correo){
	$("#modalCajeroAsignado").modal("show");
	$("#modaltitle1").html(cliente);
	$("#idCliente1").html(id);
	$("#codCliente1").html(cod);
	$("#accesoCuenta").attr({"href":"https://hombresdenegociosglobalca.com/inversiones/php/proceso_admin_cambiar.php?us="+correo});
	$("#accesoCuenta").attr({"target": correo});

	$.post("php/proceso.php",{opc:3, id_c:id, cod:cod},function(resp){
		
		if(resp.salida == 1){
			data_tables2();
			$("#movimientos1").html(resp.movimientos);
			$("#mi_saldo").html(resp.saldo);
			$("#mi_saldoEstatero1").html(resp.saldoEstatero);
			$("#mi_saldoEstatero2").html(resp.saldoEstatero);
			$("#btnBloquear").html(resp.estatus);
		}
		
	},"json");	
}


function verCodigos(){
	var cod_id = $("#codCliente1").html();
	$.post("php/proceso.php",{opc:10,cod_id:cod_id},function(resp){
		$("#modalCodigos").modal("show");
		$("#listadoCodigos").html(resp.listado);
		data_tables1();
	},"json");	

}

function saldoEstatero(){
	$.post("php/proceso.php",{opc:7},function(resp){
	
		$("#mi_saldoEstatero1, #mi_saldoEstatero2").html(resp.saldoEstatero);
		
	},"json");	

}saldoEstatero();


function bloquearCajero(n){
	
	var id = $("#idCliente1").html();
	


	swal({
	  title: "¿Esta segur@ que desea realizar la operacion?",
	  text: "",
	  type: "info",
	  showCancelButton: true,
	  closeOnConfirm: false,
	  showLoaderOnConfirm: true,
	  confirmButtonText: "Si, Acepto",
	  cancelButtonText: "No, Cancelar"
	},
	function(isConfirm){
		
	  if (isConfirm) {
	$.post("php/proceso.php",{opc:5, id_c:id, n:n},function(resp){
		
		if(resp.salida == 1){
			$("#btnBloquear").html(resp.estatus);
			swal("",resp.msn,"success");
		}else{
			swal("",resp.msn,"error");
		}
		
	},"json");	

	}
	});
}


function agregarEstatero(){
	var moneda = $("#mi_saldoEstatero2").val();
	var divisa=$("#mi_saldoEstatero2 option:selected").attr('val_moneda');
	var monto = $("#monto_recarga").val();
	var descripcion = $("#descripcion").val();
	var id = $("#idCliente1").html();
	var cod = $("#codCliente1").html();

	if(monto<=0){
		$("#monto_recarga").focus();
		return false;
	}


swal({
  title: "¿Esta segur@ que desea agregar estateros cajero seleccionado?",
  text: "Esta a punto de asignar "+number_format(monto,2,",",".")+" "+divisa,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
	
  if (isConfirm) {

	$.post("php/proceso.php",{opc:8,  moneda:moneda, divisa:divisa, monto:monto, descripcion:descripcion, id_c:id, cod:cod},function(resp){
		
		if(resp.salida == 1){
			$("#movimientos1").html(resp.movimientos);
			$("#mi_saldo").html(resp.saldo);
			swal("",resp.msn,"success");
			saldoEstatero();
			setTimeout(function(){
				data_tables2();
			});
		}else{
			swal("",resp.msn,"error");
		}
		
	},"json");	
	}
	});
}



function multiCajeros(){
	$("#modalMultiCajero").modal("show");
	$.post("php/proceso.php",{opc:9},function(resp){
		
		if(resp.salida == 1){
		$("#listadoCajeros").html(resp.listado);
			
		data_tables3();
		
		}else{
			$("#listadoCajeros").html(resp.listado); 
		}
		
	},"json");	
}