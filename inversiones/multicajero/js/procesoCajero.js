function moduloCajero(opc){
	$("#cuerpoCajero").load(opc);
	$(document).ready();	
}

function data_tables(){
	setTimeout(function(){
 		
	    $('#table1').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });

		$("#table1").DataTable();
		$("#table1_wrapper").css({"width":"96%"});
		
	},500);
}


function data_tables1(){
	setTimeout(function(){
 		
	    $('#table2').DataTable({
	    	pageLength: 5,
	    	"order": [[ 5, "desc" ]],
	        language: {
	          "decimal": "",
	          "emptyTable": "No hay información",
	          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
	          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
	          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
	          "infoPostFix": "",
	          "thousands": ",",
	          "lengthMenu": "Mostrar _MENU_ Entradas",
	          "loadingRecords": "Cargando...",
	          "processing": "Procesando...",
	          "search": "Buscar:",
	          "zeroRecords": "Sin resultados encontrados",
	            "paginate": {
	                "first": "Primero",
	                "last": "Ultimo",
	                "next": "Siguiente",
	                "previous": "Anterior"
	            }
	        }
	    });

		$("#table2").DataTable();
		$("#table2_wrapper").css({"width":"96%"});
		
	},500);
}


function enviar_a(opc){
	if($(opc).prop("checked")==true)
		$(".envia").attr({"disabled":false});
	else{
		$(".envia").attr({"disabled":true});
		$("#telefono").val("");
		$("#correo").val("");
		$("#cliente").html("");
	}
}



function buscarUsuario(){

	var buscar = $("#busuario").val();

	if(buscar.length<3){
		$("#busuario").focus();
		return false;
	}
	$.post("php/procesoCajero.php",{opc:1, buscar:buscar},function(resp){

		$("#modalMultiCajero").modal("show");
		$("#listadoCajeros").html(resp.listado);
		data_tables();

	},"json");

}


function enviarUsuario(telf,correo,cliente){
	$("#telefono").val(telf);
	$("#correo").val(correo);
	$("#cliente").html(cliente);
	$("#modalMultiCajero").modal("hide");
}


function saldoCajero(){
	$.post("php/procesoCajero.php",{opc:2},function(resp){
	
		$("#miSaldo").html(resp.saldo);
		condicionSoporte();
	},"json");	

}

function condicionSoporte(){
	var divisa=$("#miSaldo option:selected").attr('val_moneda');
	$("#divisa").val(divisa);
	if(divisa == 'BsS'){
		$("#tasa, #billete").attr({"disabled":false});
		$(".noAplica, .oculto").show();
	}else{
		$("#billete").attr({"disabled":false});
		$("#tasa").attr({"disabled":true});
		$(".oculto").hide();
	}
}


function verCodigos(){
	$.post("php/procesoCajero.php",{opc:4},function(resp){
		$("#modalCodigos").modal("show");
		$("#listadoCodigos").html(resp.listado);
		data_tables1();
	},"json");	

}



function misMovimientos(){
	$.post("php/procesoCajero.php",{opc:5},function(resp){
		$("#listadoMov").html(resp.listado);
		data_tables();
	},"json");	

}