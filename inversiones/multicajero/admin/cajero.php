<!DOCTYPE html>
<html>
<head>
	<title>Gestion Cajero</title>
</head>
<body>
<div class="alert bg-success" style="color:green; font-size:16px; font-weight: bold; font-style:italic;">Gestión Cajero</div>

	<table class="table table-condensed" style=" width:100%">
		<tr>
	  		<td style="text-align:right; font-weight: bold; padding-top: 15px">Cliente</td>
	  		<td><input type="search" id="cliente"  class="form-control" title="Buscar clientes por: Cod-id, cedula, nombres, apellidos, correo, telefono" data-toggle="tooltip" placeholder="Cod-id, cedula, nombres, apellidos, correo, telefono"></td>
	  		<td><button onclick="buscarCliente()" class="btn btn-md btn-default" title="Pulse para buscar clientes" data-toggle="tooltip" ><i class="fa fa-search"></i> Buscar</button></td>
	  		<td><button onclick="multiCajeros()" class="btn btn-md btn-success" title="Mostrar cajeros agregados" data-toggle="tooltip" ><i class="fa fa-list"></i> Cajeros</button></td>
  		</tr>
 	</table>


	<div id="infoCajero" style="overflow:auto;">
		<br><br><br>
		<center><i class="fa fa-group" style="font-size:128px; opacity:0.06;"></i></center>
	</div>



<script type="text/javascript">
	$(function(){ $('[data-toggle="tooltip"]').tooltip(); });
	$('#cliente').focus();
</script>
</body>
</html>