  <div class="modal fade" id="modalCajero" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-user fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle">Nuevo Cajero</span>
          <span id="idCliente" class="hide"></span>
          <span id="codCliente" class="hide"></span>
        </div>
        <div class="modal-body" id="modalInfo">
          <div class="container-fluid">
           <form class="form-horizontal" role="form" onsubmit="return false;">

            <div class="row oculto">
              <div class="form-group">

                <div class="col-md-12">
                  <label class="col-md-4">Moneda</label>
                  <div class="col-md-8">
                   <select id="mi_saldoEstatero1" class="form-control"></select>
                  </div>
                </div>
              </div>
            </div>

            <div class="row oculto">
              <div class="form-group">
                <div class="col-md-12">
                  <label class="col-md-4">Monto</label>
                  <div class="col-md-8"><input type="number" placeholder="Estatero" step="0.01" min="0" id="monto" class="form-control"></div>
                </div>
              </div>
            </div>

            <div class="row oculto">
              <div class="form-group">
                <div class="col-md-12">
                  <label class="col-md-4">Fecha</label>
                  <div class="col-md-8"><input type="text" placeholder="Fecha de recarga"  id="fecha_recarga" class="form-control" readonly value="<?php echo date('d/m/Y H:i:s a'); ?>"></div>
                </div>
              </div>
            </div>


            <div class="row oculto">
              <div class="form-group">
                <div class="col-md-12">
                  <div class="col-md-12"><center><button onclick="crear_cajero()" class="btn btn-sm btn-success btn_crear"><i class="fa fa-check-circle"></i> Asignar Cajero</button></center></div>
                </div>
              </div>
            </div>
        </form>
        <hr>
          <div class="panel panel-success">
            <div class="panel-heading">
              Mis Movimientos
            </div>
          </div>
        <div id="movimientos"  style="overflow:auto;"><span style=" color:red; font-weight: bold; font-size:16px; text-align:center">Aun sin recargas.</span></div>
      </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>






  <div class="modal fade" id="modalCajeroAsignado" role="dialog" style="z-index:1060">
    <div class="modal-dialog modal-lg" style="width:90%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-user fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">Cajero</span>
          <span id="idCliente1" class="hide"></span>
          <span id="codCliente1" class="hide"></span>
        </div>
        <div class="modal-body">
         <span id="btnBloquear"></span>

       
          <a href="#" id="accesoCuenta"   target="1" >
          <button class="btn btn-warning" title="Ver oficina del cajero" data-toggle="tooltip"><i class="fa fa-sign-in" style="font-size:20px"></i> <span  class="ocultame2">Ver Cuenta</span></button></a>
        
          
          <button class="btn btn-success" onclick="verCodigos()" title="Ver codigos generados del cajero" data-toggle="tooltip"><i class="fa fa-barcode" style="font-size:20px"></i> <span  class="ocultame2">Codigos Estateros Generados</span></button>


          <span class="pull-right" style="font-weight: bold; font-size: 18px" title="Saldo actual del cajero" data-toggle="tooltip">
            <table><td><i class="fa fa-briefcase" style="font-size:20px" ></i> &nbsp;</td>
           <td><select id="mi_saldo" class="form-control" style=""></select></td></table>
         &nbsp;&nbsp;&nbsp;</span>

          <div class="panel panel-success">
            <div class="panel-heading">
              <b>RECARGA DE ESTATERO</b>
            </div>
            <div class="panel-body">
          <form class="form-horizontal" role="form" onsubmit="return false;">
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-4">Estatero</label>
                  <div class="col-md-8"><select id="mi_saldoEstatero2" class="form-control"></select></div>
                </div>

                <div class="col-md-6">
                  <label class="col-md-4">Monto</label>
                  <div class="col-md-8"><input type="number" step="0.01" min="0" placeholder="Monto a recargar"  id="monto_recarga" class="form-control"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="form-group">
                <div class="col-md-6">
                  <label class="col-md-4">Descripción</label>
                  <div class="col-md-8"><textarea id="descripcion" class="form-control" placeholder="Opcional"></textarea></div>
                </div>
                <label class="col-md-4"></label>
                <div class="col-md-6">
                  <div class="col-md-12"><center>
                    <button onclick="agregarEstatero()" class="btn btn-success" title="Agregar Estatero" data-toggle="tooltip"><i class="fa fa-money" style="font-size:20px"></i> <span class="">Agregar Estatero</span></button>
                  </center></div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
          




          <div class="panel panel-success">
            <div class="panel-heading">
            <table width="100%">
              <td width="86%"><b>MIS MOVIMIENTOS</b></td>
              <td align="right"><button class="btn btn-default fa fa-file-pdf-o"></button></td>
              <td align="right"><button class="btn btn-default fa fa-print"></button></td>
            </table>
            </div>
          </div>
          <div id="movimientos1"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>







<div class="modal fullscreen-modal fade" id="modalCodigos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:20000;">

    <div class="modal-dialog modal-lg" style="width:96%;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-barcode fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">CODIGOS ESTATEROS</span>

        </div>
        <div class="modal-body">

          <div id="listadoCodigos"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>


  <div class="modal fade" id="modalMultiCajero" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-users fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">MultiCajero Estatero HNG</span>

        </div>
        <div class="modal-body">
       
          <div class="panel panel-success">
            <div class="panel-heading">
            <table width="100%">
              <td width="86%"><b>CAJEROS</b></td>
              <td align="right"><button class="btn btn-default fa fa-file-pdf-o" disabled></button></td>
              <td align="right"><button onclick="cargarCajeros('*')" class="btn btn-info btn-sm" title="pulse para enviar mensaje a todos los multicajeros" data-toggle="tooltip"><i class=" fa fa-envelope-o"></i> Envio Masivo</button></td>
            </table>
            </div>
          </div>
          <div id="listadoCajeros"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>




  <!--ENVIO DE MENSAJES A LOS MULTICAJEROS-->





  <div class="modal fade" id="modalMMMultiCajero" role="dialog">
    <div class="modal-dialog modal-lg" style="width:90%; z-index:10000;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <i class="fa fa-envelope fa-2x text-success"></i> <i class="fa fa-users fa-2x text-success"></i> <span class="text-success" style="font-weight: bold;" id="modaltitle1">Envío de Mensajes a los MultiCajero</span>

        </div>
        <div class="modal-body">
       
          <div class="panel panel-success">
            <div class="panel-heading">
            
              <table class="table">
                <tr><th>Enviar a: 
                  <select class="form-control" id="multicajeros" onchange="return agregar_multicajero();">
                  </select> 
                  </th></tr>
                <tr><td>
                  <div id="enviar_a" class="jumbotron" style="overflow-y:auto; overflow-x:auto; height:180px; background: #fff; "></div>
                  <div id="enviar_aH" style="display: none" class="jumbotron"></div>
                </td></tr>
                <tr><th>Mensaje de Texto</th></tr>
                <tr><td><textarea id="mtexto" class="form-control" maxlength="470" placeholder="Escriba el mensaje a enviar (Max 470 caracteres)"></textarea></td></tr>
                <tr><td>
                  <center>
                    <button class="btn btn-sm btn-success" id="envioMensaje" onclick="enviar_mensaje()"><i class="fa fa-send"></i> Enviar</button>
                  </center></td></tr>
              </table>



            </div>
          </div>
          <div id="listadoMensajes"  style="overflow:auto;"></div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>