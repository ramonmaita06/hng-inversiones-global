<?php session_name("hng"); session_start(); 
date_default_timezone_set('America/Caracas');
?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../bootstrap/datatables/datatables/datatables.min.css">
	<title>MultiCajero Estatero HNG</title>

	<style>
	.adaptame .icon{font-size:24px;}
	.ocultame2{display:inline;}
	.adaptame{	width:86px; }
	.mimenu{width:100%;}
	 @media only screen and (max-width: 600px){
	.mimenu{position:absolute;}
	.adaptame{width:auto;}
	.adaptame .icon{font-size:20px;}

	.ocultame2{display:none;}
	}

	#cuerpoCajero{
		width:100%;
		height:auto;
	}

	</style>
</head>
<body>
<?php include("cajero/modalCajero.php"); ?>
<div class="panel panel-success">
	<div class="panel-heading"><img src="../images/estatero_mini.png" style="width:86px"> &nbsp;&nbsp;&nbsp;<big><b>MULTICAJERO ESTATERO HNG </b></big><br>
				
				<center>
				<button onclick="moduloCajero('cajero/index.php')" class="btn btn-success adaptame" title="Generar codigos estateros" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-barcode icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Codigo <br>Estatero</span></button>

		  		<button disabled onclick="moduloCajero('cajero/estatero.php')" class="btn btn-info adaptame" title="Acreditar Estateros al cajero" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-bank icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Recargar <br> Cajero</span></button>

		  		<button  onclick="moduloCajero('cajero/movimiento.php')" class="btn btn-warning adaptame" title="Ver movimientos del cajero" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-line-chart icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Movimiento <br> Cajero</span></button>

		  		<button  onclick="$('#modalCuentas').modal('show')" class="btn btn-danger adaptame" title="Ver cuentas autorizadas" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-list-alt icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Cuentas <br> Autorizadas</span></button>
		  	</center>
	</div>
	<div class="panel-body">
		<div id="cuerpoCajero"></div>
	</div>











<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/sweetalert.min.js"></script>
<script type="text/javascript" src="../js/number_format.js"></script>
<script type="text/javascript" src="../bootstrap/datatables/datatables/datatables.min.js" ></script>
<script type="text/javascript" src="js/procesoCajero.js"></script>
<script type="text/javascript">
	$(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
		moduloCajero('cajero/index.php');
	});
</script>
</body>
</html>