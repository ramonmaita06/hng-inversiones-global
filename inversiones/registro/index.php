<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta name="description" content="Modal Window ">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--Archivos de calendario-->
	<script src="../js/js_hng/src/js/jscal2.js"></script>
    <script src="../js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
    <link rel="stylesheet" href="../css/font-awesome.min.css" media="screen">
  <link rel="stylesheet" href="../js/bootstrap/css/bootstrap.min.css" media="screen">
  <link rel="stylesheet" href="../css/css_hng/estilo_dey.css" type="text/css">
  <link rel="stylesheet" href="../css/css_hng/estilo_input.css">
  <link rel="stylesheet" href="../js/alerta/dist/sweetalert.css">
  <link rel="stylesheet" href="../css/style_dorado.css">
  <link href="../bootstrap/fileinput/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
  <script src="../js/alerta/dist/sweetalert-dev.js"></script>
   <script src="../js/wow.min.js"></script>
  <script src="../js/jq.min.js"></script>
  <script src="../js/bootstrap/js/bootstrap.min.js"></script>
  <script src="../js/number_format.js"></script>

  
<script src="../js/procesos_registro.js"></script>
<script src="../js/teclado.js"></script>
<script src="../js/ubicacion.js"></script>
<script src="js/metodo_inscripcion.js"></script>
<script src="../bootstrap/fileinput/js/fileinput.min.js"></script>
<script>
      $(function() {
        
        $("#file").fileinput({
          showPreview:false,
          showCaption: true,
          showUpload: false,
          showRemove: false,
          browseClass: "btn btn-primary btn-lg",
          fileType: "any",
          browseLabel: 'Elegir &hellip;',
              browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;',
              browseClass: 'btn btn-primary',
              removeLabel: 'Quitar Imagen',
              removeIcon: '<i class="glyphicon glyphicon-ban-circle"></i> ',
              removeClass: 'btn btn-default',
              uploadLabel: 'Subir Imagen',
              uploadIcon: '<i class="glyphicon glyphicon-upload"></i> ',
              uploadClass: 'btn btn-default',
              msgLoading: 'Cargando &hellip;',
              msgProgress: 'Cargado {percent}% of {file}',
              msgSelected: '{n} Imagenes Seleccionadas',
              previewFileType: 'image',
              wrapTextLength: 250,
          msgFileTypes: {'image': 'Solo se permiten archivos de tipo imagen [PNG, JPG, JPEG, BMP, GIF]'}
        });
      });
  </script>
<style>
.ocultame{display:inline;}
 @media only screen and (max-width: 600px){
.ocultame{display:none;}
}
.nacio{
	display:inline;
}
</style>
<script>
//location.href="https://hombresdenegociosglobalca.com/";
</script>  
</head>
<body style="font-size:12px;" onload="return bandera(document.getElementById('pais').value),$('#mensaje_carga,#cortina').show(500,'linear');">


 
<div class="ttitulom1 tleft pfixed" style="z-index:1000001; background:#000;">
<div class="tcenter">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>

<td width="20%" align="left"><a href="https://hombresdenegociosglobalca.com/principal"><img title="Ir al portal de Hombres de Negocios Global C.A." src="../images/logo_oficial_new_acceso.png" width="160"></a></td>
<td align="center">
<span class="ftm tcenter ocultame" style="font-size:17px;  color:#D2C74A;  font-weight:bold;">PLANILLA DE REGISTRO - INVERSIONES<span id="n"></span>
</span>
</td>
<td width="20%" align="right">
<a href="https://hombresdenegociosglobalca.com/inversiones/"><i class="fa fa-sign-in " title="Acceder a mi cuenta" style="font-size:32px; color:#D2C74A;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<img  src="../images/inversionista1.png" width="48" class="ocultame"></td>
</tr>
<tr>
<td colspan="3" class="ftp" align="left" style="background:#D2C74A;">
<div style="height:2px; background:#D2C74A; margin-top:-1px"></div>
</td>
</tr>
</table>
</div>
</div>

						<!--FORMULARIOS-->
<p><br><br><br></p>
<div class="container-fluid cuerpoP">



<div id="plan1" style="display:inline"><?php include("planilla.php"); ?></div>
<div id="plan2" style="display:none"><?php include("vista.php"); ?></div>
<div id="plan3" style="display:none"><?php include("condiciones.php"); ?></div>

<br><br>
<br>
<br>
</div>
<!--fin-->		
</div>
</form>



<!--pie de pagina-->
<div class="ttitulom1" id="mipie" style="position:fixed; z-index:1000001; left:0%; bottom:0%; background:#222;">
<div class="tcenter">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td colspan="2" class="ftp" align="left" style="background:#fff;">
<div style="height:2px; background:#C3B844"></div>
</td>
</tr>
<tr align="center">
<td width="100%">
<div  class="btn-group">
<!--<button id="can" class="btn btn-danger btn-xs"> Cancelar <span class="glyphicon glyphicon-remove"></span></button>-->
<button id="ant" class="btn btn-danger btn-md" disabled><span class="glyphicon glyphicon-arrow-left"></span> Anterior </button>
<button id="sig" class="btn btn-success btn-md"> Siguiente <span class="glyphicon glyphicon-arrow-right"></span></button>
</div>
</td>
</tr>

</table>
</div>

</div>

</body>
</html>
