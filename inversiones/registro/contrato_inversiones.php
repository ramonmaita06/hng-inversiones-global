<?php session_name("contador"); session_start();
date_default_timezone_set('America/Caracas');


require("../php/cifra_letras1.php");
require("../php/cifra_letras2.php");
require("../php/cifra_letras3.php");

$cmeses = Array("00"=>"","1"=>"ENERO","2"=>"FEBRERO","3"=>"MARZO","4"=>"ABRIL","5"=>"MAYO","6"=>"JUNIO","7"=>"JULIO","8"=>"AGOSTO","9"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$meses = Array("00"=>"","01"=>"ENERO","02"=>"FEBRERO","03"=>"MARZO","04"=>"ABRIL","05"=>"MAYO","06"=>"JUNIO","07"=>"JULIO","08"=>"AGOSTO","09"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$nmeses = Array("00"=>"","ENERO"=>"01","FEBRERO"=>"02","MARZO"=>"03","ABRIL"=>"04","MAYO"=>"05","JUNIO"=>"06","JULIO"=>"07","AGOSTO"=>"08","SEPTIEMBRE"=>"09","OCTUBRE"=>"10","NOVIEMBRE"=>"11","DICIEMBRE"=>"12");

function cero_fecha_mes($f){
if($f=="01"){$f=1;} if($f=="02"){$f=2;} if($f=="03"){$f=3;} if($f=="04"){$f=4;}
if($f=="05"){$f=5;} if($f=="06"){$f=6;} if($f=="07"){$f=7;} if($f=="08"){$f=8;} if($f=="09"){$f=9;}
return $f;
}


function cambiar_fecha($fecha){
$d=substr($fecha,0,2);
$m=substr($fecha,3,2);
$a=substr($fecha,6,4);
$fecha=$a.'-'.$m.'-'.$d;
return $fecha;
	}
	
function cambiar_fecha_es($fecha){
$d=substr($fecha,8,2);
$m=substr($fecha,5,2);
$a=substr($fecha,0,4);
$fecha=$d.'-'.$m.'-'.$a;
return $fecha;
	}
$cons=mysql_query("select * from cliente where id_c='".$_SESSION['id_c']."'");
$info=mysql_fetch_array($cons);

$cons1=mysql_query("select * from debe d, afiliacion a, posee p where d.id_c='".$_SESSION['id_c']."' AND d.id_af=a.id_af AND a.id_af=p.id_af");
$info1=mysql_fetch_array($cons1);

$nacio=substr($info["rif"],0,2);
if($info["sexo"]=='Femenino'){$genero="a"; $ella="la";}else{$genero="o"; $ella="el";}
if($info["nombrect"]!=''){$titular=$info["nombrect"]." con cedula de identidad ".number_format($info["cedulact"],0,",",".");}
else{
$titular=$info["nombre"]." ".$info["apellido"]." con cedula de identidad ".number_format($info["cedula"],0,",",".");	
}


$_SESSION["cod_idp"]=$info1['cod_id'];

$_SESSION["contrato"]='

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<style type="text/css">
<!--
.Estilo1 {color: #FF0000}
-->
</style>
</head>

<body>
<table width="100%"><tr>
<td width="20%" align="left"></td>
<td width="60%"></td>
<td width="20%" align="right"><img src="../../images/logo.png" width="120"></td>
</tr></table>

<p align="center"><b>&nbsp;CONTRATO DE  AFILIACI&Oacute;N&nbsp; PARA LA COMPRA-VENTA DE  MATERIALES APTOS PARA&nbsp; RECICLAJE</b>
<br />
<br />
    <b>DISPOSICIONES  GENERALES</b></p>
<p align="justify">Entre, la Compa&ntilde;&iacute;a &ldquo;Hombres de Negocios Global&rdquo; C.A. Sociedad de  Comercio domiciliada en Ciudad Bol&iacute;var, Municipio Aut&oacute;nomo Heres del Estado  Bol&iacute;var, debidamente inscrita ante el Registro &nbsp;Mercantil Segundo del Estado Bol&iacute;var, en fecha  seis (06) de agosto del a&ntilde;o 2014, quedando inserta en el Tomo: 32-A REGMESEGBO  304, N&uacute;mero: 36, siendo su &uacute;ltima modificaci&oacute;n en fecha, once (11) de enero del  a&ntilde;o 2016, quedando registrada en el Tomo: 1-A REGMESEGBO 304, N&uacute;mero: 47, con Registro de Informaci&oacute;n Fiscal (R.I.F.) N&deg; J404527362, venezolano, mayor de edad, titular de la c&eacute;dula de identidad N&deg; V-11.724.557, procediendo con el car&aacute;cter de  Presidente de la Sociedad de Comercio &ldquo;Hombres de Negocios Global&rdquo;  C.A. conforme se desprende de las Art&iacute;culo 14&deg; y Art&iacute;culo 24&deg; del  Acta Constitutiva y Estatutos, quien en lo sucesivo y para los efectos del presente documento se denominar&aacute; <span style="color:#000033">LA EMPRESA</span>, por una parte y  por la otra&nbsp; '.$ella.' ciudadan'.$genero.' <u>'.strtoupper($info["nombre"].' '.$info["apellido"]).'</u>, C.I <u>'.$nacio.''.number_format($info["cedula"],0,",",".").'</u> persona natural quien en lo adelante se denominar&aacute; <span style="color:#000033">EL VENDEDOR</span>, a quien <span style="color:#000033">LA EMPRESA </span>&nbsp;garantiza los servicios plasmados en  el presente Contrato a <span style="color:#000033">EL &nbsp;VENDEDOR</span>, el presente Contrato se regir&aacute; por  las siguientes clausulas: </p>
<p align="justify"><span style="color:#000033">&nbsp;</span></p>
<p align="justify"><span style="color:#000033">CLAUSULA 1</span>.  El Marco Legal de <span style="color:#000033">HOMBRES DE NEGOCIOS  GLOBAL, C.A.</span>: Para efectos del presente contrato <span style="color:#000033">HOMBRES DE NEGOCIOS GLOBAL, C.A.</span> ser&aacute; denominado <span style="color:#000033">LA EMPRESA,</span> la cual manifiesta ser una  Sociedad Mercantil constituida bajo las leyes de la Rep&uacute;blica Bolivariana De  Venezuela, legal y debidamente inscrita en el Registro Mercantil Segundo de la  Circunscripci&oacute;n Judicial del estado Bol&iacute;var bajo el Numero 36 Tomo 32-A de  fecha 06 de Agosto de 2014, Inscrita en el Instituto Venezolano de los Seguros  Sociales (I.V.S.S) bajo el Numero O61545173. Inscripci&oacute;n en el R.N.A, Aportante No 1060484212  del Instituto Nacional de Capacitaci&oacute;n y Educaci&oacute;n Socialista (I.N.C.E.S.),  Inscrita ante el Registro Nacional de Empresas y Establecimientos Bajo el  N&uacute;mero de Identificaci&oacute;n Laboral (NIL) EN TRAMITES, Inscrito en el Servicio Municipal de  Administraci&oacute;n tributaria de la Alcald&iacute;a del Municipio Heres bajo licencia  n&uacute;mero (EN TRAMITE), Registro de Informaci&oacute;n Fiscal (RIF) . J-40452736-2,  inscrita en el registro &uacute;nico de personas que desarrollan actividades  econ&oacute;micas (RUPDAE) y por otra parte, el o la solicitante ser&aacute; denominado <span style="color:#000033">EL VENDEDOR</span>. &nbsp;</p>
<p align="justify"><span style="color:#000033">CLAUSULA 2</span>: <span style="color:#000033">Sistema de compras Mensuales:</span> <span style="color:#000033">LA EMPRESA</span> mediante el presente  contrato ofrece a<span style="color:#000033"> EL VENDEDOR</span>, la  oportunidad de comprarle sus desechos con la finalidad de ser reciclados. <span style="color:#000033">EL VENDEDOR</span> clasificar&aacute; los desechos  para la venta destinada a Reciclaje de materiales, a <span style="color:#000033">LA EMPRESA</span> por tipo de material, seleccionando y separando seg&uacute;n  sea el tipo de material tomando en cuenta las caracter&iacute;sticas siguientes: <u>Materiales  Reciclables Ferrosos (chatarra) tales como</u>: COBRE, BRONCE, ACERO AL  CARBONO, ACERO INOXIDABLE, LAT&Oacute;N,&nbsp; &nbsp;y <u>Materiales Reciclables No Ferrosos tales  como</u>: CART&Oacute;N, PAPEL ALUMINIO, &nbsp;VIDRIO,  PL&Aacute;STICO, PLOMO, HUESO, ACEITE Y TODOS LOS ELEMENTOS CUYAS CARACTER&Iacute;STICAS  PERMITAN SER OBJETO DE RECICLAJE nuevo o usado para ser transformado en  productos &uacute;tiles &nbsp;a adoptar el Sistema Clasificado  en su regl&oacute;n alternativo y apegado en forma planificada la prestaci&oacute;n del  Servicio a &nbsp;<span style="color:#000033">EL VENDEDOR</span> &nbsp;que haya cumplido  con lo estipulado en las Cl&aacute;usulas y condiciones del presente contrato.<br />
  &nbsp;<br />
  <span style="color:#000033">CLAUSULA 3</span>: <span style="color:#000033">Aceptaci&oacute;n de EL VENDEDOR:</span> <span style="color:#000033">EL VENDEDOR</span> manifiesta su aceptaci&oacute;n en  incorporarse al Sistema de <span style="color:#000033">LA EMPRESA</span> de compra Mensual para la obtenci&oacute;n de los Materiales para Reciclaje ofrecidos  por <span style="color:#000033">EL VENDEDOR</span> &nbsp;destinados a los Servicios que promueve <span style="color:#000033">LA EMPRESA,</span> que se regir&aacute; por un  cronograma de Ruta establecido, de acuerdo al reporte que hagan los <span style="color:#000033">Vendedores (Grupo),</span> ello con la  finalidad de tener la garant&iacute;a de recolectar cargas completas (camiones) &nbsp;en la Ruta del Sector&nbsp; o Parroquia.&nbsp;&nbsp;&nbsp; <br />
  <span style="color:#000033">MATERIALES:</span> <span style="color:#000033">EL VENDEDOR</span> se compromete a  clasificar los desechos en cada regl&oacute;n, la compra m&iacute;nima de kilos mensuales de  Materiales a Reciclar previamente clasificados, ser&aacute;&nbsp; la siguiente:<br />
</p>
<table cellspacing="0" cellpadding="0">
  <col width="80" span="4" />
  <tr height="20">
    <td height="20" width="290"><ul class="Estilo1">
      <li>100 Kgs.    HIERRO PESADO</li>
    </ul></td>
    <td width="374"><ul class="Estilo2">
      <li>50 Kgs. ALUMINIO    LIVIANO</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo3">
      <li>100 Kgs.    HIERRO LIVIANO</li>
    </ul></td>
    <td><ul class="Estilo4">
      <li>20 Kgs. COBRE</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo5">
      <li>100 Kgs.    CARTON</li>
    </ul></td>
    <td><ul class="Estilo6">
      <li>20 Kgs. BRONCE</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo7">
      <li>100 Kgs. PAPEL</li>
    </ul></td>
    <td><ul class="Estilo8">
      <li>50 Kgs. ANTIMONIO</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo9">
      <li>100 Kgs.    VIDRIO LIMPIO</li>
    </ul></td>
    <td><ul class="Estilo10">
      <li>50 Kgs. HIERO COLADO</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo11">
      <li>100    Kgs&nbsp;VIDRIO COLOR</li>
    </ul></td>
    <td><ul class="Estilo12">
      <li>50 Kgs. ACERO    INOXIDABLE</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo13">
      <li>100 Kgs.    PLASTICO DURO</li>
    </ul></td>
    <td><ul class="Estilo14">
      <li>Und. BATERIA</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo15">
      <li>50 Kgs.    PACKAGING o PLASTICO SOPLADO</li>
    </ul></td>
    <td><ul class="Estilo16">
      <li>50 Kgs. PLOMO</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo17">
      <li>50 Kgs.    PLASTICO LIVIANO</li>
    </ul></td>
    <td><ul class="Estilo18">
      <li>100 Kgs. HUESO</li>
    </ul></td>
  </tr>
  <tr height="20">
    <td height="20"><ul class="Estilo19">
      <li>50 Kgs.    ALUMINIO DURO</li>
    </ul></td>
    <td><ul class="Estilo20">
      <li>200 Ltr.&nbsp;ACEITE</li>
    </ul></td>
  </tr>
</table>
<p align="justify">&nbsp;</p>
<p align="justify"><br />
En el supuesto de ser menor los kilos de los materiales,&nbsp; <span style="color:#000033">EL VENDEDOR</span>&nbsp; tiene la opci&oacute;n de venderlos en el Sitio de Acopio  para activar el plan de compensaci&oacute;n ofrecido por <span style="color:#000033">LA EMPRESA. </span>Los precios, para la compra de los Materiales a Reciclar  estar&aacute; publicado por <span style="color:#000033">LA EMPRESA</span> en  cada Portal (Oficina Virtual) de su Afiliado.<span style="color:#000033">&nbsp;&nbsp;</span></p>
  
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 4.</span> <span style="color:#000033">La integraci&oacute;n del Grupo:</span> <span style="color:#000033">EL VENDEDOR</span> formar&aacute; parte de un grupo DE VENDEDORES de sus Productos de Desechos para Reciclar, conformado por integrantes ilimitados en n&uacute;meros de personas, por otra parte, deber&aacute; realizar el pago para activar el contrato de Inscripci&oacute;n y ventas de los Materiales plasmado en la Cl&aacute;usula 3 y as&iacute; poder participar en el evento de asignaci&oacute;n del plan de cobranza por sus recomendaciones y tendr&aacute; toda la informaci&oacute;n de su grupo y las cuentas por pagar, cuenta por cobrar, en su Oficina Virtual&nbsp; por el portal <span style="color:#000033">www.hombresdenegociosglobalca.com</span> <span style="color:#000033">asignado con su c&oacute;digo de CONTRATO DE AFILIACI&Oacute;N PARA LA COMPRA-VENTA DE MATERIALES APTOS PARA RECICLAJE.</span></p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 5.</span> <span style="color:#000033">Constituci&oacute;n del Grupo:</span> Un grupo se  considera constituido cuando hayan sido aceptados por <span style="color:#000033">LA EMPRESA</span>. <span style="color:#000033">EL VENDEDOR</span> participar&aacute;  en el evento de asignaci&oacute;n, como integrante del grupo, siempre que hayan  aportado su cuota mensual y est&eacute;n al d&iacute;a en sus pagos. </p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 6. Precio de los Servicios a Adquirir:</span> Luego de la Contrataci&oacute;n de los Servicios a Adquirir, <span style="color:#000033">LA EMPRESA</span> abonar&aacute; el saldo a favor a &nbsp;<span style="color:#000033">EL VENDEDOR</span>  15 d&iacute;as despu&eacute;s del cierre de mes (los 30 d&iacute;as de cada mes a excepci&oacute;n del mes de febrero), el precio de los materiales para Reciclar ser&aacute; publicado en  su Oficina Virtual&nbsp; por el portal&nbsp; <span style="color:#000033">www.hombresdenegociosglobalca.com  </span>asignado con su c&oacute;digo, <span style="color:#000033">EL VENDEDOR</span>,  &nbsp;la diferencia y ajustes a que hubiere  lugar para su venta, ser&aacute; igualmente publicado. <span style="color:#000033">EL VENDEDOR</span> beneficiario o beneficiaria deber&aacute; cumplir con los  requisitos exigidos,<span style="color:#000033"> EL VENDEDOR</span> acepta que <span style="color:#000033">LA EMPRESA</span>, haga  publicidad sobre el o los Servicios solicitados con los cuales ha sido  beneficiado sin recibir por ello retribuci&oacute;n alguna&hellip;&hellip;</p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 7. EL VENDEDOR</span> se obliga a cumplir sus compromisos y lograr sus objetivos en el Sistema de Ventas  mensuales, aportando los siguientes conceptos que integran las ventas mensuales: <span style="color:#000033">7.1</span>. Aporte Neto: que representa la  cantidad que <span style="color:#000033">EL VENDEDOR</span> aportar&aacute;  como MONTO CONTRATADO para obtener los servicios, el monto ser&aacute; de Tres Mil  Bol&iacute;vares (Bs. 3.000,oo) por punto. <span style="color:#000033">7.2</span>.  Costos Operativos: Es la cantidad que paga <span style="color:#000033">EL  VENDEDOR</span> en funci&oacute;n de cubrir los gastos que resultan de las actividades  del Servicio que presta <span style="color:#000033">LA EMPRESA.</span> El monto ser&aacute; de Tres Mil Bol&iacute;vares (Bs. 3.000,oo) por punto el cual se  ajustar&aacute; cada 90 d&iacute;as, tomando como referencia la Inflaci&oacute;n en nuestro Pa&iacute;s. <span style="color:#000033">7.3</span> Penalidad por atraso en el&nbsp; pago de activaci&oacute;n de los Servicios: <span style="color:#000033">EL VENDEDOR</span> no cobrar&aacute; el monto que le  corresponde por concepto de regal&iacute;as, derivadas de sus recomendaciones de  Afiliados. <span style="color:#000033">7.4</span>. <span style="color:#000033">EL VENDEDOR</span> pagar&aacute; la inscripci&oacute;n y los pagos mensuales de 1 punto  por la cantidad de Tres Mil Bol&iacute;vares (Bs. 3.000,oo). <span style="color:#000033">7.5. EL VENDEDOR </span>se compromete a clasificar los desechos en cada  regl&oacute;n.&nbsp; <span style="color:#000033">7.6. </span>Penalidad (suspensi&oacute;n): Se suspender&aacute; el Servicio por no  cumplir con la cantidad estipulada en el presente Contrato para la venta de Un  Mil Doscientos (1.200) kilos al a&ntilde;o de Materiales apto para Reciclaje entre no  metales y metales ferrosos y no ferrosos, ser&aacute; suspendido el contrato (c&oacute;digo  ID N&ordm; <u><b>'.$_SESSION["cod_idp"].'</u></b>)&nbsp;&nbsp; de manera eliminatoria del sistema de <span style="color:#000033">LA EMPRESA.</span></p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 8</span>. <span style="color:#000033">Duraci&oacute;n del Plan:</span> Tendr&aacute; una  duraci&oacute;n de 12 meses y <span style="color:#000033">EL VENDEDOR</span> deber&aacute;  aportar la cantidad de 12 cuotas mensuales y consecutivas, <span style="color:#000033">EL VENDEDOR</span> podr&aacute; realizar el pago total de las cuotas mensuales, de  los gastos administrativos si estuviesen pendientes y hacer el finiquito del  contrato.<span style="color:#000033"> LA EMPRESA </span>&nbsp;realizar&aacute; la  renovaci&oacute;n del contrato de manera autom&aacute;tica para el siguiente per&iacute;odo. Para  cancelar el convenio, <span style="color:#000033">EL VENDEDOR</span> deber&aacute; notificar efectivamente y por escrito con noventa  (90) d&iacute;as de anticipaci&oacute;n a la fecha de vencimiento del convenio, a trav&eacute;s de  una carta privada que deber&aacute; ser firmada en indicaci&oacute;n de recibida, donde  indique los motivos o razones que la originaron. En caso contrario se procede a  lo anteriormente expuesto&nbsp; la renovaci&oacute;n<span style="color:#000033"> CONTRATO DE AFILIACI&Oacute;N&nbsp; PARA SUMINISTRALES&nbsp; PRODUCTOS RECICLAJES&nbsp; </span>e igual<span style="color:#000033"> LA EMPRESA.</span></p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 9.</span> <span style="color:#000033">Secuencia de pago:</span> Ser&aacute; de una (1)  cuota mensual m&iacute;nima ininterrumpida durante todo el Plan, permiti&eacute;ndose abonos  adelantados para reducir el monto pendiente y la fecha de vencimiento de los  pagos ser&aacute; especificada en la solicitud de inscripci&oacute;n, no se aceptar&aacute;n pagos  fraccionados de la cuota mensual. En caso que la fecha de vencimiento de pagos coincida  con un d&iacute;a festivo o no laborable, podr&aacute; efectuar su pago al siguiente d&iacute;a  h&aacute;bil. </p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA</span> <span style="color:#000033">10</span>. <span style="color:#000033">Opciones de pago de EL VENDEDOR PARA LA EMPRESA:</span> Los pagos de  cuotas deber&aacute;n efectuarse con tarjeta de d&eacute;bito o cr&eacute;dito, puntos de ventas,  transferencia electr&oacute;nica, cheques endosados a nombre de <span style="color:#000033">HOMBRES DE NEGOCIOS GLOBAL C.A.</span> o depositadas en las cuentas  bancarias girada contra el Banco <span style="color:#000033">Mercantil. Cuenta  Corriente&nbsp; Nro.0105 0134 21 1134145497</span>.  Girada contra el Banco <span style="color:#000033">Venezuela Cuenta Corriente Nro.0102  0414 38 0000441096</span>. Girada contra el Banco <span style="color:#000033">Venezolano de cr&eacute;dito Cuenta Corriente Nro. 0104 0036 83  0360119874. </span> Cuyo titular es <span style="color:#000033">LA EMPRESA</span>, o en las Cuentas Bancarias  Autorizadas por <span style="color:#000033">LA EMPRESA</span> atendiendo la ubicaci&oacute;n geogr&aacute;fica de <span style="color:#000033">EL  VENDEDOR</span>, las cuales estar&aacute;n a disposici&oacute;n en la cartelera informativa, o en  su p&aacute;gina web <span style="color:#000033">www.hombresdenegociosglobalca.com</span> o la gu&iacute;a del cliente y en la tarjeta de  pago que <span style="color:#000033">EL VENDEDOR</span> manifiesta  recibir al momento de firmar el presente contrato. El ingreso de estas  cantidades se amparar&aacute; mediante facturas y recibos de pago. </p>
<p align="justify"><span style="color:#000033">CLAUSULA 11.</span> <span style="color:#000033">Opciones de pago de LA EMPRESA PARA EL VENDEDOR:</span> <span style="color:#000033">EL VENDEDOR</span> debe presentar la  factura para el cobro de&nbsp; las ventas  (cuentas por cobrar) de materiales de reciclaje, <span style="color:#000033">LA EMPRESA </span>&nbsp;efectuara el  pago&nbsp; en la &nbsp;tarjeta de d&eacute;bito o cr&eacute;dito que le otorgar <span style="color:#000033">LA EMPRESA</span>, transferencia electr&oacute;nica,  cheques endosados a nombre del <span style="color:#000033">EL VENDEDOR</span> o depositadas en las cuentas bancarias girada contra el Banco <u><b>'.$info["banco"].'</u></b><span style="color:#000033">.</span><span style="color:#000033"> Cuenta Corriente&nbsp;  Nro.<u><b>'.$info["ncuenta"].'</u></b>. </span>Cuyo titular es  <u><b>'.$titular.'</u></b>, o en las  Cuentas Bancarias Autorizadas por <span style="color:#000033">EL VENDEDOR</span> identificado con el ID c&oacute;digo<span style="color:#000033"> <u><b>'.$_SESSION["cod_idp"].'</u></b></span>.<br />
<br />
<span style="color:#000033">CLAUSULA 12.</span> <span style="color:#000033">Evento de Asignaci&oacute;n:</span> Mensualmente <span style="color:#000033">LA EMPRESA</span> organizar&aacute; una reuni&oacute;n que  contar&aacute; con la presencia opcional de cada <span style="color:#000033">VENDEDOR</span>.  En dicho evento &nbsp;s&oacute;lo podr&aacute;n participar  aquellos <span style="color:#000033">VENDEDORES</span> que est&eacute;n  solventes y se encuentre al d&iacute;a con sus pagos mensuales correspondientes. <span style="color:#000033">EL VENDEDOR</span> deber&aacute; consignar el soporte  de sus pago a la oficina principal de <span style="color:#000033">HOMBRES  DE NEGOCIOS GLOBAL C.A</span> &nbsp;o enviar un&nbsp; e-mail: <span style="color:#000033">pagos.hng@gmail.com </span>&nbsp;dejando una copia en su poder como soporte. &nbsp;Los&nbsp;  gastos&nbsp; de tr&aacute;mites  administrativos y profesionales para la materializaci&oacute;n del presente negocio de  Servicios corren por cuenta de <span style="color:#000033">EL  VENDEDOR</span>.</p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 13.</span> <span style="color:#000033"> </span>El presente Contrato quedar&aacute;  rescindido de pleno derecho&nbsp; si se<span style="color:#000033"> </span>producen las  circunstancias descritas a continuaci&oacute;n: <span style="color:#000033">a</span>)  Terremoto, maremoto, erupciones volc&aacute;nicas o cualquier otra cat&aacute;strofe natural; <span style="color:#000033">b</span>) Fisi&oacute;n o fusi&oacute;n nuclear,  radiaciones ionizantes y contaminaci&oacute;n&nbsp;  radioactivas; <span style="color:#000033">c</span>) Aplicaci&oacute;n  de la carta Democr&aacute;tica; <span style="color:#000033">d</span>) &nbsp;Guerra declarada o no, invasi&oacute;n, guerra civil,  revoluci&oacute;n o insurrecci&oacute;n o cualquier acci&oacute;n tomada por el Gobierno tendiente a  combatir o defenderse de tales eventualidades, ante tales hechos fortuitos y de  fuerza mayor quedar&aacute; rescindido el presente Contrato, sin ninguna obligaci&oacute;n  pendiente para las partes.</p>
<p align="justify">&nbsp;</p>
<p align="justify"><span style="color:#000033">CLAUSULA 14. Atraso de pagos de EL VENDEDOR:</span> Se considerar&aacute; en atraso, las cuotas totales pagadas con posterioridad a la  fecha l&iacute;mite de pagos estipulada en la solicitud de inscripci&oacute;n, las abonadas  con valor inferior al establecido en el presente contrato y las abonadas con  cheques, o transferencias que el banco emisor no pague por cualquier motivo. <span style="color:#000033">EL VENDEDOR</span> acepta que el atraso en los  pagos mensuales correspondientes, ocasionar&aacute; por consiguiente la consecuencia  de p&eacute;rdida definitiva de incentivo y bonos en el mes que ocurra el atraso. </p>
<p align="justify"><span style="color:#000033">CLAUSULA 15. Atraso  de pagos del EL VENDEDOR: </span>En el supuesto de que <span style="color:#000033">EL VENDEDOR </span>dejase de abonar tres  (3) cuotas de forma consecutiva, previo an&aacute;lisis del caso <span style="color:#000033">LA EMPRESA</span> podr&aacute; dejar sin efecto el nexo contractual sin previo  aviso y excluir del grupo &nbsp;<span style="color:#000033">EL VENDEDOR</span>. </p>
<p align="justify"><br />
<span style="color:#000033">CLAUSULA 16: NOTIFICACIONES. &ldquo;LAS PARTES&rdquo;</span> convienen en que las notificaciones y comunicaciones deben  remitirse por escrito con acuse de recibo, a las siguientes direcciones:<span style="color:#000033"> LA EMPRESA:</span> cruce del paseo Meneses Av. Bol&iacute;var, en el Centro Comercial  Meneses, Planta Baja Local Numero 4, Parroquia Catedral, Municipio Heres  ,Ciudad Bol&iacute;var, Estado Bol&iacute;var. Tel&eacute;fonos: (0285) 415.03.25 - (0414) 853.79.99 e-mail: avisos.hng@gmail.com.<br />
<span style="color:#000033">EL VENDEDOR,</span> Parroquia: <u><b>'.$info["parroquia"].'</b></u>, Municipio: <u><b>'.$info["municipio"].'</b></u>, 
 Ciudad: <u><b>'.$info["ciudad"].'</b></u>, Estado: <u><b>'.$info["estado"].'</b></u>. Direcci&oacute;n: <u><b>'.$info["direccion"].'</b></u> Tel&eacute;fonos: <u><b>'.$info["tcasa"].'</b></u> E-MAIL: <u><b>'.$info["correo"].'</b></u>.</p>
	
	
<p align="justify"><br />
<span style="color:#000033">CLAUSULA</span> <span style="color:#000033">17</span>: <span style="color:#000033">Aceptaci&oacute;n de explicaci&oacute;n del Sistema: EL VENDEDOR</span> declara haber  recibido a satisfacci&oacute;n la asesor&iacute;a adecuada, oportuna y veraz por el ejecutivo  de negocios referente al Sistema <u>de</u> cuentas por pagar mensuales y  cuentas por cobrar&nbsp; Mensuales y los  precios de pago para la compra de los materiales reciclaje&nbsp; que promueve <span style="color:#000033">LA EMPRESA </span> y le&iacute;do cada una de las cl&aacute;usulas del presente contrato  aceptando las mismas, manifest&aacute;ndolo expresamente con su firma, (electr&oacute;nica en  la p&aacute;gina Web) <span style="color:#000033"> www.hombresdenegociosglobalca.com </span> as&iacute; mismo acepta que recibi&oacute; copia de la Informaci&oacute;n sobre el Servicios, (<span style="color:#000033">Electr&oacute;nica en la p&aacute;gina Web</span>), <span style="color:#000033">www.hombresdenegociosglobalca.com </span><span style="color:#000033"> Constancia de Explicaci&oacute;n del sistema de  pagos, Declaraci&oacute;n de lectura de contrato, declaraci&oacute;n jurada, Tarjeta de  control de pagos, Gu&iacute;a del Cliente y una copia impresa del contrato de adhesi&oacute;n  para el conocimiento de las condiciones y cl&aacute;usulas del mismo antes de su  suscripci&oacute;n</span><span style="color:#000033">.</span></p>
<p align="justify"><br />
<b>Nota:</b> <span style="color: blue">Los pagos realizados a la empresa seran liberados entre 6 y 72 horas habiles bancarios asi cono tambien los pagos realizados al afiliado.</span>

<p align="justify"><br />
<span style="color:#000033">CLAUSULA 18</span>.<span style="color:#000033"> &nbsp;JURISDICCI&Oacute;N  PARA DIRIMIR LOS CONFLICTOS: </span> Ambas partes se someten a la Jurisdicci&oacute;n de  los Tribunales de Ciudad Bol&iacute;var, Municipio Heres del Estado Bol&iacute;var. <span style="color:#000033">AL FIRMAR, </span><span style="color:#000033">El Solicitante estar&aacute; asumiendo  que ley&oacute; y acepto las condiciones descritas en las Cl&aacute;usulas del presente  contrato.</span> </p>



 <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr>
      <td width="250" colspan="2"  height="130"  valign="top"></td>
    </tr>
    <tr>
      <td width="250"></td>
      <td width="250" align="center" valign="top">
	   <img src="../../images/firma.png" width="80">
	   </td>
    </tr>
    <tr>
      <td width="250" valign="top" align="center">__________________________</td>
      <td width="250" valign="top" align="center">__________________________</td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center"><b><span id="ccliente2">'.$info['nombre'].' '.$info['apellido'].'</span><br />C.I: <b><span id="cci1">'.$nacio.number_format($info['cedula'],0,",",".").'</span></b></b> </div></td>
      <td width="250" valign="top"><div align="center"><b>Virma Josefina Flores Poyo</b><br /><b>C.I: V-11.724.557</b> </div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center">COD-ID: <b>'.$_SESSION["cod_idSV"].'</b></div>
          <div align="center"><b>HUELLA </b></div></td>
      <td width="250" valign="top"><div align="center"><b>Hombres de Negocios Global C.A</b><br />
        <b>Rif: j-40452736-2</b></div></td>
    </tr>
    <tr>
      <td width="250" rowspan="2"  align="center">
	  <center>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <table width="75" height="87" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
          <tr>
            <td height="81">&nbsp;</td>
          </tr>
        </table>      
		</center>
		</td>
      <td width="250" valign="top"><div align="center">&nbsp;</div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center">&nbsp;</div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center"><b>EL VENDEDOR</b></div>      </td>
      <td width="250" valign="top"><div align="center"><b>LA EMPRESA</b></div>      </td>
    </tr>
  </table>	
	
</body>
</html>
';
?>