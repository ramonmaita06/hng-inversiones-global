<center><div class="panel panel-warning" style=" width:100%;">
<div class="panel-heading">
<div style=" width:100%;" align="center"> 
<span class="ftg1 titulo">METODO DE PAGO</span>
</div>
</div>
<div class="panel-body cuerpo" style="text-align:left">

      <div class="row">
	 <br>
	 <div class="col-lg-12" align="center"><img src="../images/bancos.png"class="img-responsive">
	 <hr></div>
	 </div>
	 
<div class="container"> 	 
	<div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Tipo de Transacción</label>
          <div class="col-md-8">
<select name="tipot" id="tipot" class="form-control" style="width:100%;" onchange="return metodo_afiliacion(this)">
<option value="">Seleccione el tipo de pago
<option value="PRECARGADO">CÓDIGO PRECARGADO
<option value="DEPOSITO">DEPOSITO
<option value="TRANSFERENCIA">TRANSFERENCIA
<option value="PAGO MOVIL">PAGO MOVIL
</select>			
			</div>
          </div>
       </div>
	   	  <div class="col-sm-6 col-lg-6" id="ccodp" style="display:none">
        <div class="form-group">
          <label for="codp" class="col-md-4 control-label">Serial del Código Precargado</label>
          <div class="col-md-8">
            <input id="codp" class="form-control" placeholder="Inserte código precargado" maxlength="8" onkeyup="return valida_codigo(this, 1);">
			<i title="Pulse para limpiar el codigo" class="glyphicon glyphicon-qrcode" style="font-size:24px; position:absolute; right:5%; top:15%;cursor:pointer" onclick="$('#codp').attr({'readOnly':false}); $('#codp').val('')"></i>
			<input id="seguir" type="hidden" value="0">
			<input id="montop" type="hidden" value="0">
			</div>
          </div>
       </div>
	</div>   
	 <br>

     <div class="row metodo-dt">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="bancoe" class="col-md-4 control-label">Banco Emisor <b>(De)</b></label>
          <div class="col-md-8">
<select name="bancoe" id="bancoe" class="form-control" style="width:100%;">
<option value="">Seleccione el Banco
<option value="BBM">BANCAMIGA BANCO MICROFINANCIERO, C.A.
<option value="BANCO ACTIVO">BANCO ACTIVO
<option value="BANCO AGRICOLA">BANCO AGRICOLA
<option value="BANCO BICENTENARIO">BANCO BICENTENARIO
<option value="BANCO CARONI">BANCO CARONI
<option value="BCV">BANCO CENTRAL DE VENEZUELA
<option value="BANFANB">BANCO DE LAS FUERZAS ARMADAS(BANFANB)
<option value="BANCO DE VENEZUELA">BANCO DE VENEZUELA
<option value="BANCO DEL CARIBE">BANCO DEL CARIBE
<option value="BANCO DEL PUEBLO">BANCO DEL PUEBLO SOBERANO, C.A.
<option value="BANCO DEL SOL">BANCO DEL SOL
<option value="BANCO DEL TESORO">BANCO DEL TESORO
<option value="BES">BANCO ESPIRITO SANTO, S.A.
<option value="BANCO EXTERIOR">BANCO EXTERIOR
<option value="BIV">BANCO INDUSTRIAL DE VENEZUELA
<option value="BID">BANCO INTERNACIONAL DE DESARROLLO
<option value="BANCO MERCANTIL">BANCO MERCANTIL
<option value="BNC">BANCO NACIONAL DE CREDITO
<option value="BOD">BANCO OCCIDENTAL DE DESCUENTO CA.
<option value="BANCO PLAZA">BANCO PLAZA
<option value="BANCO PROVINCIAL">BANCO PROVINCIAL
<option value="BANCO SOFITASA">BANCO SOFITASA
<option value="BVC">BANCO VENEZOLANO DE CREDITO
<option value="BANCRECER">BANCRECER
<option value="BANESCO">BANESCO
<option value="BANGENTE">BANGENTE
<option value="BANPLUS">BANPLUS, BANCO COMERCIAL
<option value="CITIBANK">CITIBANK
<option value="CORP BANCA">CORP BANCA
<option value="EAP DEL SUR">EAP DEL SUR
<option value="FONDO COMUN">FONDO COMUN BANCO UNIVERSAL
<option value="IMC">INSTITUTO MUNICIPAL DE CREDITO P.
<option value="IBC">INVERUNION BANCO COMERCIAL
<option value="MBD">MIBANCO BANCO DE DESARROLLO
<option value="100% BANCO">100% BANCO
</select>
			</div>
          </div>
       </div>
       
       	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Banco Receptor <b>(Para)</b></label>
          <div class="col-md-8">
<select name="bancod" id="bancod" class="form-control" style="width:100%;">
<option value="">Seleccione el Banco
<option value="BANCO BICENTENARIO">BANCO BICENTENARIO
<option value="BANCO CARONI">BANCO CARONI
<option value="BANCO DE VENEZUELA">BANCO DE VENEZUELA
<option value="BANCO MERCANTIL">BANCO MERCANTIL
<option value="BNC">BANCO NACIONAL DE CREDITO
<option value="BVC">BANCO VENEZOLANO DE CREDITO
<option value="BANESCO">BANESCO
<option value="100% BANCO">100% BANCO
<option value="BANCO PROVINCIAL">BANCO PROVINCIAL
</select>
			</div>
          </div>
       </div>
	</div>   
	
	<br>
     <div class="row metodo-dt">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Numero de Transacción</label>
          <div class="col-md-8">
            <input id="numero" class="form-control" type="number" maxlength="30">
			</div>
          </div>
       </div>
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Monto (Bs.)</label>
          <div class="col-md-8">
		  <input id="monto" class="form-control" type="number" step="0.01"   maxlength="30"></div>
          </div>
       </div> 
	</div>   
 <div class="row metodo-dt">     
   	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha de la Transacción</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit2" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit2",
        trigger    : "DPC_edit2",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div>  
       </div>    
      
      
</div> 
 
 
  <hr>
  <center><div  class="ftg"><b>INFORMACIÓN DEL PATROCINADOR</b></div></center><br><br>
      <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-6 control-label">Codigo de Referencia</label>
          <div class="col-md-6" id="cod_rf">
          <?php echo $_GET['cod']; ?>
			</div>
          </div>
       </div>      
	  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-6 control-label">Patrocinador</label>
          <div class="col-md-6" id="patroc">
			</div>
          </div>
       </div>
	</div> 
       <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-6 control-label">Telefonos de Contacto</label>
          <div class="col-md-6" id="patro_tel">
         
			</div>
          </div>
       </div>      
	  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-6 control-label">Correo Elecronico</label>
          <div class="col-md-6" id="patro_correo">
			</div>
          </div>
       </div>
	</div>  
  <hr>
  
    <div class="row">
 	   <center><div  class="ftg"><b>INFORMACIÓN DEL AFILIADO</b></div></center> <br><br>     
	  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-6 control-label">Apellidos</label>
          <div class="col-md-6" id="vape">
            
			</div>
          </div>
       </div>
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-6 control-label">Nombres</label>
          <div class="col-md-6" id="vnom">
		            </div>
       </div> 
	</div>   
	</div>   
<!--fila 1-->

	   <div class="row">  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-6 control-label">Cedula de Identidad</label>
          <div class="col-md-6" id="vci">
       </div> 	
	</div>
	</div>
	
	   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-6 control-label">R.I.F</label>
          <div class="col-md-6" id="vrif" >
		  </div>
       </div> 	
	</div>	
	</div>
	
	
	  <div class="row">
	<div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="sexo" class="col-md-6 control-label">Sexo</label>
          <div class="col-md-6" id="vsexo" >
		  </div>
          </div>
       </div> 
	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="fechan" class="col-md-6 control-label">Fecha de Nacimiento</label>
          <div class="col-md-6" id="vfechan"> </div>
		  </div>
       </div> 	   
       </div> 	   
<!--fila 2-->	
	
	  	
   
<!--fila 3-->		
	
 <div class="col-sm-12 col-lg-12"><hr> </div> 	
	
	  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nacionalidad" class="col-md-6 control-label">Nacionalidad o Pais de Origen</label>
          <div class="col-md-6" id="vnacionalidad">
		  </div>
          </div>
       </div> 	
	
		  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="pais" class="col-md-6 control-label">Pais de Residencia &nbsp; &nbsp;</label>
          <div class="col-md-6" id="vpais">
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 4-->		
	
	  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="estado" class="col-md-6 control-label">Estado de Residencia</label>
          <div class="col-md-6" id="vestado">
		  
		  </div>
          </div>
       </div> 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ciudad" class="col-md-6 control-label">Ciudad de Residencia</label>
          <div class="col-md-6" id="vciudad">
		  
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 5-->	
	
	
	  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ci" class="col-md-6 control-label">Municipio de Residencia</label>
          <div class="col-md-6" id="vmunicipio">
		 
		  </div>
          </div>
       </div> 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ci" class="col-md-6 control-label">Parroquia de Residencia</label>
          <div class="col-md-6" id="vparroquia">
		
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 6-->	
	


	
	  <div class="row">
	  
	  	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="comunidad" class="col-md-6 control-label">Tipo de Comunidad</label>
          <div class="col-md-6" id="vcomunidad">
          </div>
          </div>
       </div>
	  
	  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="infra" class="col-md-6 control-label">Infraestructura</label>
          <div class="col-md-6" id="vinfra">
		  </div>
          </div>
       </div> 	
	   
       </div> 	   
<!--fila 9-->

 <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="piso" class="col-md-6 control-label">Piso</label>
          <div class="col-md-6" id="vpiso">
		  
		  </div>
          </div>
       </div> 	
 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nivel" class="col-md-6 control-label">Nivel</label>
          <div class="col-md-6" id="vnivel">
		  
		  </div>
          </div>
       </div> 	   
	
       </div> 	
	
	
	  <div class="row">

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="tcasa" class="col-md-6 control-label">N&deg; de Teléfono (Movil o Casa)</label>
          <div class="col-md-6" id="vtcasa">
		 
		  </div>
          </div>
       </div> 	
  	
	

	<div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ttrabajo" class="col-md-6 control-label">N&deg; de Teléfono del Trabajo (Opcional)</label>
          <div class="col-md-6" id="vttrabajo">
		  
		  </div>
          </div>
       </div> 
	   
       </div> 	   
<!--fila 12-->


  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="correo" class="col-md-6 control-label">Correo Electrónico</label>
          <div class="col-md-6" id="vcorreo">
		 
		   </div>
          </div>
       </div> 


	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-6 control-label">Repita el Correo Electrónico</label>
          <div class="col-md-6" id="vccorreo">
		  
		  </div>
          </div>
       </div> 	
	   
       </div> 	

	
	     
</div> 	     
 </div> 	</center>