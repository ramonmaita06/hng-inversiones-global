$(function(){
	// cerrar_preloader();
	$(document).on('click', '#btn-renovar', function(event) {
      event.preventDefault();
      $('#div-contrato').show('slow/400/fast');
    });
    $(document).on('click', '#btn-recargar', function(event) {
      event.preventDefault();
      $('#div-recargar').show('slow/400/fast');
    });

    $('#aceptar').change(function() {
        
      if( $('#aceptar').prop('checked') ) {
        $('#btn-renovar-c').removeAttr('disabled')
      }else{
        $('#btn-renovar-c').attr('disabled','disabled')
      }
    });
	$('[data-toggle="tooltip"]').tooltip();
});

function preloader() {
	$('.cortina_kyc').show('slow/400/fast');
}
function cerrar_preloader() {
	setTimeout(function(){
		$('.cortina_kyc').hide('slow/400/fast');
	},1500);
}