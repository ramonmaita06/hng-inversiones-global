function cargar_vista(){
	
	$.post("php/proceso_usuarios.php",{opc:1},function(resp){

	
	if(resp.cod_pais!='VE'){
		$(".nacio").hide();
		setTimeout(function(){
			$("#estado").html("<option value='26'>None</option>");
			$("#ciudad").html("<option value='523'>None</option>");
			$("#municipio").html("<option value='475'>None</option>");
			$("#parroquia").html("<option value='1151'>None</option>");
		},3000);
		
	}else{
		var local=resp.ubicacion.split(",",3);
		setTimeout(function(){
			document.getElementById('estado').value=local[0];
			ciudades(document.getElementById('estado'));
			setTimeout(function(){	
			document.getElementById("ciudad").value=local[1];
			document.getElementById("municipio").value=local[2];
			parroquias(document.getElementById("municipio"));
				setTimeout(function(){	
			document.getElementById("parroquia").value=resp.parroquia;
				},1500);
			},1500);
		},1500);
	}



	var trif='';
	var tci='';
	var tiporif = {'V-':'RIF-','E-':'RIF-','J-':'RIF-','G-':'RIF-','P-':'RIF-'};
	var tipoci = {'V-':'CI-','E-':'CI-','J-':'RIF-','G-':'RIF-','P-':'RIF-'};

	if(tiporif[resp.trif]){
		trif = tiporif[resp.trif];
	}else{
		trif = resp.trif;
	}
	if(tipoci[resp.tci]){
		tci = tipoci[resp.tci];
	}else{
		tci = resp.tci;
	}

		
	$("#ape").val(resp.ape);
	$("#nom").val(resp.nom);
	$("#ci").val(tci);
	$("#nci").val(resp.ci);
	$("#rif").val(trif);
	$("#nrif").val(resp.rif);
	$("#sexo").val(resp.sexo);
	$("#fechan").val(resp.fechan);
	$("#tcomunidad").val(resp.tdireccion);
	$("#comunidad").val(resp.direccion);
	$("#infra").val(resp.infra);
	$("#piso").val(resp.piso);
	$("#nivel").val(resp.nivel);
	$("#tcasa").val(resp.tcasa);
	$("#ttrabajo").val(resp.ttrabajo);
	$("#correo").val(resp.correo);
	$("#ccorreo").val(resp.correo);	
	$("#nacionalidad").val(resp.cod_pais_n);
	$("#pais").val(resp.cod_pais);
		
		setTimeout(function(){
			parent.bandera(resp.cod_pais);
		},2000);
	},"json");
	

	
}cargar_vista();



/*guardar  los datos personales*/
function guardar(){



if($("#ape").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus apellidos", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ape").focus(); },3000); return false;
}



if($("#nom").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus nombres", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nom").focus(); },3000); return false;	
}





if($("#ci").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ci").focus();},3000);  return false;
	}




if($("#nci").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe ingresar el numero de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nci").focus(); },3000); return false;
}


if($("#rif").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#rif").focus(); },3000); return false;
}



if($("#nrif").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su numero de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nrif").focus(); },3000); return false;
}



if($("#sexo").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el sexo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#sexo").focus(); },3000); return false;
}



if($("#fechan").val().length<10){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar o agregar su fecha de nacimiento", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#fechan").focus(); },3000); return false;
}



if($("#nacionalidad").val().length<2){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar su nacionalidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nacionalidad").focus(); },3000); return false;}


if($("#pais").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el pais de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#pais").focus(); },3000); return false;
}



if($("#estado").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el estado de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#estado").focus(); },3000); return false;
}




if($("#ciudad").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la ciudad de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ciudad").focus(); },3000); return false;
}



if($("#municipio").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el municipio de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#municipio").focus(); },3000); return false;
}



if($("#parroquia").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la parroquia de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#parroquia").focus(); },3000); return false;
}


if($("#tcomunidad").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de comunidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcomunidad").focus(); },3000); return false;
}


if($("#comunidad").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre de la comunidad donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#comunidad").focus(); },3000); return false;
}


if($("#infra").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de infraestructura donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#infra").focus(); },3000); return false;
}


if($("#piso").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de piso o numero de la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#piso").focus(); },3000); return false;
}


if($("#nivel").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar los niveles que posee la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nivel").focus(); },3000); return false;
}

if(($("#tcasa").val().length<8)||($("#tcasa").val().length>16)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcasa").focus(); },3000); return false;
}


if($("#ttrabajo").val().length>0){
if(($("#ttrabajo").val().length<8)||($("#ttrabajo").val().length>16)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de su trabajo, (Opcional)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ttrabajo").focus(); },3000); return false;
}}



if($("#correo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}



if($("#correo").val().length>0){
	var validarEmail=validateEmail($("#correo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;	
	}
	
	
if($("#correo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}}


if($("#ccorreo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar nuevamente su correo electronico para confirmar", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}

if($("#ccorreo").val().length>0){
	var validarEmail=validateEmail($("#ccorreo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;	
	}
if($("#ccorreo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}
}
if($("#correo").val()!=$("#ccorreo").val()){
swal({title: "COMPLETAR INFORMACION", text: "Los correos electronicos no coinciden, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}




var c1=$("#ape").val();
var c2=$("#nom").val();
var c3=$("#ci").val()+$("#nci").val();
var c4=$("#rif").val()+$("#nrif").val();
var c5=$("#fechan").val();
var c6=$("#sexo").val();

var c7=$("#pais").val();
var c71=$("#pais option:selected").text();
var c8=$("#nacionalidad").val();
var c81=$("#nacionalidad option:selected").text();
var c9=$("#estado  option:selected").text();
var c10=$("#ciudad option:selected").text();
var c11=$("#municipio option:selected").text();
var c12=$("#parroquia").val();
var c13=$("#tcomunidad").val()+": "+$("#comunidad").val();
var ubica=$("#estado").val()+","+$("#ciudad").val()+","+$("#municipio").val();
var c14=$("#infra").val();
var c15=$("#piso").val();
var c16=$("#nivel").val();
var c17=$("#tcasa").val();
var c18=$("#ttrabajo").val();
var c19=$("#correo").val();




swal({
  title: "¿Esta segur@ que desea actualizar los datos?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
$.post("php/proceso_usuarios.php",{opc:2,
ubica:ubica,c71:c71,c81:c81,
c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,c8:c8,
c9:c9,c10:c10,c11:c11,c12:c12,c13:c13,c14:c14,
c15:c15,c16:c16,c17:c17,c18:c18,c19:c19
}
,function(resp){
	
	if(resp.salida==1){
	swal("ACTUALIZACION EXITOSA",resp.mensaje,"success");
	}
	else{
		swal("ACTUALIZACION FALLIDA",resp.mensaje,"error"); 
		}
},"json");
  } else {
    swal("ACTUALIZACION CANCELADA", "", "error");
  }
});

	
}






function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}


