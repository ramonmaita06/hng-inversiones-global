$(document).ready(function() {
/*mostrar titles*/
    $('[data-toggle="tooltip"]').tooltip();  
});





function letra_cifra(monto){
var minimo=$("#retiro_minimo").html();
    if(parseFloat(monto)<parseFloat(minimo)){
       // $("#bretiro").attr({"disabled":true});
        $("#cifrar").html("");
        return false;
    }
    
 $("#bretiro").attr({"disabled":false});
   $.post("../php/proceso.php",{opc:11,monto:monto}
,function(resp){ 
$("#cifrar").html("<i><b>Usted est&aacute; retirando: <span class=' text-success'><i class='fa fa-money'></i> "+resp.cifra+"</span></b></i>");
},"json");
}



function retirar_fondo_new(i){
var monto = $("#monto"+i).val();
var divisa = $("#moneda"+i).val();

monto=parseFloat(monto);

if($("#monto"+i).val()<=0){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe ingresar el monto a retirar", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto"+i).focus(); },3000); return false;
}



swal({
  title: "¿Esta segur@ que desea retirar el monto ingresado?",
  text: "Monto a retirar: "+number_format(monto,2,',','.')+" "+divisa,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
  
  
$.post("../php/proceso_retiro.php",{opc:4,monto:monto, divisa:divisa}
,function(resp){
  
  if(resp.salida==1){
    setTimeout(function(){swal("REGISTRO EXITOSO",resp.mensaje,"success");},1500);
        $("#retiroModal").modal('hide');
        listar_retiros(); 
        reloadDatatable();
        //lista_divisa_monto_retiro();
      } 
    else{
      setTimeout(function(){swal("REGISTRO FALLIDO",resp.mensaje,"error");},1500);  
    }
  
},"json");
  
  
  } else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});



}




function retirar_fondo(){
var cifra=$("#cifrar").text();
var monto=$("#monto_retiro").val();
var minimo=$("#retiro_minimo").html();
var divisa=$("#midivisa").val();
monto=parseFloat(monto);

if($("#monto_retiro").val()<parseFloat(minimo)){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe ingresar el monto minimo a retirar", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto_retiro").focus(); },3000); return false;
}



if($("#pgeneral").val()<monto){ 
swal({title: "COMPLETAR INFORMACION", text: "El monto ingresado es superior al monto disponible de retiro, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto_retiro").focus(); },3000); return false;
}

 
swal({
  title: "¿Esta segur@ que desea retirar el monto ingresado?",
  text: "Monto a retirar: "+number_format(monto,2,',','.')+" "+divisa+". \n\n"+cifra+"\n",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
	
	
$.post("../php/proceso_retiro.php",{opc:4,monto:monto, divisa:divisa}
,function(resp){
	
	if(resp.salida==1){
		setTimeout(function(){swal("REGISTRO EXITOSO",resp.mensaje,"success");},1500);
		listar_retiros();	
		// $("#bretiro").attr({"disabled":true});
            $.post("../php/proceso_retiro.php",{opc:3},function(resp1){},"json");
            reloadDatatable();
            //lista_divisa_monto_retiro();

			}	
		else{
			setTimeout(function(){swal("REGISTRO FALLIDO",resp.mensaje,"error");},1500);	
		}
	
},"json");
  
  
  } else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

	}



function listar_retiros(){
// $("#bretiro").attr({"disabled":true});
$.post("../php/proceso_retiro.php",{opc:2}
,function(resp){
lista_cofres_retiro();	
$("#midivisa").html(resp.lista_moneda_retiro);
parent.recarga_cofreBTN();
$("#listado_retiros").html(resp.listado);
$("#listado_retiros1").html(resp.total);
	
},"json");
 
	}
	



function lista_cofres_retiro(){
	$.post("../php/proceso_cofre.php",{opc:'cofre'},function(resp){
	$("#pgeneral1").html("Saldo Disponible: <i class='fa fa-money'></i> "+number_format(resp.monto1,2,",","."));
	$("#pgeneral").val(resp.monto1);
  $("#bretiro").attr({"disabled":false});
	$("#mret").html(resp.listado);
	},"json");	
}	
	
function lista_divisa_monto_retiro(){
  $.post("../php/proceso.php",{opc:15},function(resp){
    $("#midivisa").html(resp.lista_moneda_retiro);
  },"json");
}


function validar_minimo(){
  var divisa = $("#midivisa").val();
  var minR = $("#mR"+divisa).val();
  var monto = $("#monto_retiro").val();

  if(parseFloat(minR)>parseFloat(monto) || monto==''){
    swal("","El monto de retiro es menor al monto minimo de retiro, por favor aumente el monto.","info");
    $("#monto_retiro").focus();
    return false;

  }
  retirar_fondo();

}






function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}




