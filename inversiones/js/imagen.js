$(document).on('ready', function() {
	/*mostrar titles*/
  $('[data-toggle="tooltip"]').tooltip(); 
  
	
$("#imagen , .file-input").fileinput({
		showPreview:false,
		showCaption: true,
		showUpload: false,
		showRemove: false,
		browseClass: "btn btn-primary btn-lg",
		fileType: "any",
		browseLabel: 'Elegir &hellip;',
        browseIcon: '<i class="glyphicon glyphicon-folder-open"></i> &nbsp;',
        browseClass: 'btn btn-primary',
        removeLabel: 'Quitar Imagen',
        removeIcon: '<i class="glyphicon glyphicon-ban-circle"></i> ',
        removeClass: 'btn btn-default',
        uploadLabel: 'Subir Imagen',
        uploadIcon: '<i class="glyphicon glyphicon-upload"></i> ',
        uploadClass: 'btn btn-default',
        msgLoading: 'Cargando &hellip;',
        msgProgress: 'Cargado {percent}% of {file}',
        msgSelected: '{n} Imagenes Seleccionadas',
        previewFileType: 'image',
        wrapTextLength: 250,
		msgFileTypes: {'image': 'Solo se permiten archivos de tipo imagen [PNG, JPG, JPEG, BMP, GIF]'}
	});
});

  // function fileinput() {
  		
	
	
  // }





function cambiar_imagen() {
	
$("#bsubir").attr({"disabled":true});
	
	
		var archivos = document.getElementById("imagen");
		var archivos1 = document.getElementById("imagen");
		var archivo = archivos.files; 
		var archivos = new FormData();
 var extension = (archivos1.value.substring(archivos1.value.lastIndexOf("."))).toLowerCase();
if((extension!='.png')&&(extension!='.gif')&&(extension!='.jpg')&&(extension!='.jpeg')&&(extension!='.bmp')){
swal({title: "IMAGEN DEL PERFIL", text:"El archivo no es valido, verifique y elija un archivo tipo imagen. (JPG, JPEG, PNG, GIF, BMP)" , timer: 4000,  showConfirmButton: true});setTimeout(function(){$("#imagen").focus(); 	$("#bsubir").attr({"disabled":false}); },5000);
	return false;
}
		archivos.append('imagen',archivo[0]);
		/*Ejecutamos la función ajax de jQuery*/		
		$.ajax({
			url:'php/subir.php', 
			type:'POST',
			contentType:false,
			data:archivos, 
			processData:false, 
			cache:false 
		}).done(function(msn){
		$("#bsubir").attr({"disabled":false});
			document.getElementById("imagenp1").src=msn;
			document.getElementById("imagenp2").src=msn;
			document.getElementById("imagenp3").src=msn;
			document.getElementById("miimagen").src=msn;
			setTimeout(function(){location.reload(); },6000);
			swal({title: "IMAGEN DEL PERFIL", text:"Su imagen se ha cargado exitosamente." , timer: 4000,  showConfirmButton: true});setTimeout(function(){
				mis_carteras("php/imagen.php"); 
				},5000); return false;
		});

}


function CamaraMovil() {
	
$("#bsubir").attr({"disabled":true});
	
	
		var archivos = document.getElementById("imagen");
		var archivos1 = document.getElementById("imagen");
		var archivo = archivos.files; 
		var archivos = new FormData();
 var extension = (archivos1.value.substring(archivos1.value.lastIndexOf("."))).toLowerCase();
if((extension!='.png')&&(extension!='.gif')&&(extension!='.jpg')&&(extension!='.jpeg')&&(extension!='.bmp')){
swal({title: "ALERTA", text:"El archivo no es valido, verifique y elija un archivo tipo imagen. (JPG, JPEG, PNG, GIF, BMP)" , timer: 4000,  showConfirmButton: true});setTimeout(function(){$("#imagen").focus(); 	$("#bsubir").attr({"disabled":false}); },5000);
	return false;
}
		archivos.append('imagen',archivo[0]);
		archivos.append('opc',2);
		/*Ejecutamos la función ajax de jQuery*/		
		$.ajax({
			url:'php/guardar_foto.php', 
			type:'POST',
			contentType:false,
			data:archivos, 
			processData:false, 
			cache:false 
		}).done(function(msn){
		$("#bsubir").attr({"disabled":false});
			document.getElementById("miimagen1").src=msn;
			setTimeout(function(){location.reload(); },2000);
			swal({title: "EXITO!", text:"Su foto se ha cargado con exito, nuesto equipo esta trabajando para verificar su identidad.." , timer: 4000,  showConfirmButton: true});setTimeout(function(){
				// mis_carteras("kyc/index.php"); 
				// alert(msn)
				},5000); return false;
		});

}


