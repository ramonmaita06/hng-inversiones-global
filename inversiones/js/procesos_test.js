


function cofre(){
	
$("#ganancias").html('<center><img src="../images/cargar6.gif" style="width:18px"> Espere un momento...</center>');
	$.post("../php/proceso_test.php",{opc:0},function(resp){
		
	$("#ganancias").html('Portafolio de Inversiones &nbsp;<i class="fa fa-money"></i> '+resp.monto);
	//$("#ganancias1").val(resp.monto1);
	$("#ganancias1").val(resp.monto2);
	parent.mostrar_notas();
	},"json");

/*//cofre interno viejo	
$("#ganancias").html('<center><img src="../images/cargar.gif" style="width:18px"> Espere un momento...</center>');

	$.post("../php/planes_proceso_menu.php",{opc:2},function(resp){
		$("#ganancias").html('<i class="fa fa-money"></i> '+resp.cofre);
		$("#ganancias1").val(resp.cofre1);
	},"json");
*/
	
	
}


function cargar_bonos_bloqueo(){
	$.post("php/proceso_bonos.php",{opc:1},function(resp){
	$("#listado_bonos").html(resp.listado);
	$("#monto_bloqueo").html(resp.monto);
	},"json");	
}


function cargar_bonos_libera(){
	$.post("php/proceso_bonos.php",{opc:2},function(resp){
	$("#listado_bonos").html(resp.listado);
	$("#monto_libera").html(resp.monto);
	},"json");	
}

function lista_cofres(){

	$.post("../php/proceso_test.php",{opc:4},function(resp){
	$("#portafolio1").html(resp.listado);
	$("#portafolio2").html(resp.listado);
	$("#montoportafolio1").val(resp.monto1);
	$("#montoportafolio2").val(resp.monto2);
	},"json");	
}


function preparar_tranferencia(){
	var ptf=document.getElementById("portafolio1").value;
	if(ptf==1){document.getElementById("portafolio2").value=2;}
	else if(ptf==2){document.getElementById("portafolio2").value=1;}
}


function transferir(){
	var monto=$("#montoinv").val();
	var ptf=$("#portafolio1").val();
	var montoptf1=$("#montoportafolio1").val();
	var montoptf2=$("#montoportafolio2").val();
	
	if((monto=="")||(monto<=0)){
		alerta("Notificación","Debe introducir un monto valido",5000,"success"); return false;
	}
	
	if(ptf==1){
	if(parseFloat(monto)>parseFloat(montoptf1)){
		alerta("Notificación","El monto introducido no debe ser mayor al monto de origen...",5000,"success"); return false;
	}}
	else if(ptf==2){
	if(parseFloat(monto)>parseFloat(montoptf2)){
		alerta("Notificación","El monto introducido no debe ser mayor al monto de origen.",5000,"success"); return false;
	}}	
	
	
swal({
  title: "¿Esta segur@ que desea realizar la transferencia?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
	
  if (isConfirm) {	
	$.post("../php/proceso_test.php",{opc:5,ptf:ptf,monto:monto},function(resp){
	$("#montoinv").val("");
	//alerta("Notificación",resp.msn,5000,"success");
	swal("Notificación",resp.msn,"success");
	parent.menu_planes2();
	cofre();
	lista_cofres();
	setTimeout(function(){preparar_tranferencia();},1000);
	},"json");	
	
  }else {
    swal("PROCESO CANCELADO", "", "error");
  }
  
});	
	
}


var modulo_inv=1;
function mis_inversiones(){
	modulo_inv=1;
cofre();
parent.recarga_cofreBTN();
$("#titulomodulo").html("<code>MIS INVERSIONES</code>");
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar6.gif"><br>Espere un momento...</center>');
	$.post("../php/proceso_test.php",{opc:1},function(resp){
/*bonos*/
		$("#cuerpoinfo").html(resp.listado);
	},"json");

}mis_inversiones();


function opcionINV(opc){
	$("#cortina,#recargar,#invertir,#retirar,#bonos").hide(100,"linear");
	if(opc==1){
	$("#ni").val(0);
	$("#monto").attr({"readOnly":false});
	$("#duracion").val("");	
	$("#interes").html("0,00");	
	$("#monto").val("");
	$("#nliberacion").html("<br>");
	$("#cortina,#invertir").show(100,"linear");}
	if(opc==2){$("#ni").val(1); $("#monto").attr({"readOnly":true}); $("#cortina,#invertir").show(100,"linear");}
	if(opc==3){historial_inversiones();}
	if(opc==4){mis_inversiones();}
	if(opc==6){
		lista_cofres();
		$("#btnt").html("Espere...");
		$("#btntranferir").attr({"disabled":true});
		$("#cortina,#recargar").show(100,"linear");
		setTimeout(function(){
			preparar_tranferencia(); 
		$("#btntranferir").attr({"disabled":false});
		$("#btnt").html("Transferir");
		},1000); 
		
		}
	if(opc==7){$("#cortina,#retirar").show(100,"linear");}	
	if(opc==8){$("#cortina,#bonos").show(100,"linear");}	
	
}

function reinvertir(monto,id_inv){
$("#duracion").val();	
$("#monto").val(monto);	
$("#id_inv").val(id_inv);	
}


var hasta, actual, interes=0;
function calculo_inv(){
	var monto=$("#monto").val();
	var dura=$("#duracion").val();
	var porce=0;
	if(dura==""){    $("#nliberacion").hide(500,"linear"); return false;}
	
	//interes=eval((monto*dura)/100);
	if(dura==35){porce=0.50;}
	if(dura==70){porce=1;}
	if(dura==105){porce=1.50;}
	if(dura==140){porce=2;}
	if(dura==175){porce=2.50;}
	if(dura==210){porce=3;}
	if(dura==245){porce=3.50;}
	if(dura==280){porce=4;}
	if(dura==315){porce=4.50;}
	if(dura==350){porce=5;}
	if(dura==385){porce=5.50;}
	if(dura==420){porce=6;}
	interes=eval(monto*porce);
	
//	$("#interes").html(number_format(interes,2,",","."));
	periodo_inv(interes);

}


var LMSN;

function periodo_inv(interes){
var dura=$("#duracion").val();	
clearTimeout(LMSN);

 milisegundos=parseInt(dura*24*60*60*1000);
 fecha=new Date();
 dia=fecha.getDate();
 mes=fecha.getMonth()+1;
 anio=fecha.getFullYear();
 
if(dia<10){
    dia='0'+dia;
} 
if(mes<10){
    mes='0'+mes;
}
actual=dia+"/"+mes+"/"+anio;


  
 tiempo=fecha.getTime();
 total=fecha.setTime(parseInt(tiempo+milisegundos));
 dia=fecha.getDate();
 mes=fecha.getMonth()+1;
 anio=fecha.getFullYear();
if(dia<10){
    dia='0'+dia;
} 
if(mes<10){
    mes='0'+mes;
}
//hasta=dia+"/"+mes+"/"+anio;


LMSN=setTimeout(function(){
fin_semana(actual,interes);
},2000);
}




function fin_semana(fecha,interes){
	$("#periodo").html("Calculando fechas de liberacion, espere...");
	var ndias=$("#duracion").val();
	var monto=$("#monto").val();
	$("#nliberacion").html("<br>Generando posibles fechas de liberaciones de la inversi&oacute;n...");

$.post("../php/proceso_test.php",{opc:8,fecha:fecha,ndias:ndias,monto:monto,interes:interes},function(resp){
    $("#nliberacion").hide(500,"linear");
	$("#cifra").html("<i><b>Usted está invirtiendo: <span class=' text-success'><i class='fa fa-money'></i> "+resp.cifra+"</span></b></i>");
	$("#cifra2").html("<i><b>La ganancia será de: <span class=' text-success'><i class='fa fa-money'></i> "+resp.cifra2+"</span></b></i>");	
	
	$("#periodo").html("Desde <b>"+resp.fecha_actual+"</b> Hasta <b>"+resp.fecha_final+"</b>");
	$("#interes").html(resp.interes);
	$("#nliberacion").show(500,"linear");
	$("#nliberacion").html(resp.lista);
	$("#btn-invertir").attr({"disabled":false});
	
	
	actual=resp.fecha_actual;
	hasta=resp.fecha_final;
},"json");
}





function mostrar_detalles_inv(n,opc){
$(".detalles").hide(100,"linear");
if(opc==1){

$("#detalles"+n).attr({"onclick":"return mostrar_detalles_inv("+n+",0)"});
$("#detalles"+n).show(250,"linear");
}
else{
$("#detalles"+n).attr({"onclick":"return mostrar_detalles_inv("+n+",1)"});
$("#detalles"+n).hide(250,"linear");
}
}





function invertir(){
	var ni=$("#ni").val();
	var id_inv=$("#id_inv").val();
	var monto=$("#monto").val();
	var dura=$("#duracion").val();
	var cifra=$("#cifra").text();

if($("#monto").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto a invertir", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}
if($("#duracion").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar los dias a inversión", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#duracion").focus(); },3000); return false;
}

if(ni==0){
if(parseFloat($("#monto").val())>parseFloat($("#ganancias1").val())){
swal({title: "COMPLETAR INFORMACION", text: "La inversión no puede ser mayor al cofre de inversiones", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}}


$("#btn-invertir,#btn-cinvertir").attr({"disabled":true});


swal({
  title: "¿Desea registrar la inversion?",
  text: "Monto a invertir: "+number_format(monto,2,',','.')+"\n\n"+cifra+"\n",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: true,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {	
	$.post("../php/proceso_test.php",{opc:2,ni:ni,id_inv:id_inv,
	monto:monto, dura:dura, interes:interes, hasta:hasta, actual:actual
	},function(resp){
		setTimeout(function(){
		if(resp.salida==1){
		$("#invertir,#cortina").hide(100,"linear");
		$("#btn-invertir").attr({"disabled":true});
		mis_inversiones();
		
		swal("REGISTRO EXITOSO",resp.msn,"success");
		//envio_mensaje();	
		$("#btn-invertir,#btn-cinvertir").attr({"disabled":false});	
		}else{
		swal("REGISTRO FALLIDO",resp.msn,"error");
		$("#btn-invertir,#btn-cinvertir").attr({"disabled":false});
		}
		},2000);
		
	},"json");
} else {
	$("#btn-invertir,#btn-cinvertir").attr({"disabled":false});
    swal("REGISTRO CANCELADO", "", "error");
  }
});
	
}



function envio_mensaje(){
$.post("../msn/procesoI.php",{opc:1},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
},"json");	
}




function extraer_inv(id_inv){

swal({
  title: "¿Desea extraer toda la inversion?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {	
	$.post("../php/proceso_test.php",{opc:3,id_inv:id_inv},function(resp){
	if(resp.salida==1){
	swal("REGISTRO EXITOSO",resp.msn,"success");
	mis_inversiones();
	}
	else{swal("REGISTRO FALLIDO",resp.msn,"error");}
	},"json");
} else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

}






function historial_inversiones(){
	modulo_inv=2;
$("#titulomodulo").html("<code>HISTORIAL DE INVERSIONES</code>");
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar6.gif" ><br>Espere un momento...</center>');
$.post("../php/proceso_test.php",{opc:6},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}


function buscar_ref(ref){

	if(ref.value.length<5){return false;}
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar6.gif" ><br>Espere un momento...</center>');
$.post("../php/proceso_test.php",{opc:7,ref:ref.value,mod:	modulo_inv},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}


function buscar_fecha_ref(){
var fi=$("#fci").val();
var ff=$("#fcf").val();

$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar6.gif"><br>Espere un momento...</center>');
$.post("../php/proceso_test.php",{opc:7,ref:0,fi:fi,ff:ff,mod:modulo_inv},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}



function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}