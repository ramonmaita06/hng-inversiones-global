var n=1;
$(document).ready(function(){

$("#pmul").change(function(){
	
	if(this.checked){
		$("#btn-agregarp").show();
	}else{
		$("#btn-agregarp").hide();
	}
	
});



$("#sig").click(function(){
	if(cargar_vista()==true){
	n++;
	$("#plan1,#plan2,#plan3").hide(500,"linear");
	$("#plan"+n).show(500,"linear");

	if(n==3){
		$("#sig").attr({"class":"btn btn-warning btn-md"});
		$("#sig").attr({"onclick":"return registrar()"});
		document.getElementById("sig").innerHTML='Registrar <span class="glyphicon glyphicon-edit"></span>';
		vista_contrato();
	}
	else{ $("#sig,#ant").attr({"disabled":false});}
	}

});
	
$("#ant").click(function(){
	n--;
	$("#plan1,#plan2,#plan3").hide(500,"linear");
	$("#sig").attr({"class":"btn btn-success btn-md"});
	$("#plan"+n).show(500,"linear");
	if(n==1){$("#ant").attr({"disabled":true}); }
	else if(n<3){ document.getElementById("sig").innerHTML='Siguiente <span class="glyphicon glyphicon-arrow-right"></span>';}
	else if(n==3){
		$("#sig").attr({"class":"btn btn-warning btn-md"});
		$("#sig").attr({"onclick":"return registrar()"});
		document.getElementById("sig").innerHTML='Registrar <span class="glyphicon glyphicon-edit"></span>';
	}
	else{$("#ant,#sig").attr({"disabled":false});}

});	
		
});









/*vista previa de los datos personales*/
function cargar_vista(){
	

	$("#vape").html($("#ape").val());
	$("#vnom").html($("#nom").val());
	$("#vci").html($("#ci").val()+number_format($("#nci").val(),0,",","."));
	$("#vrif").html($("#rif").val()+$("#nrif").val());
	$("#vsexo").html($("#sexo").val());
	$("#vfechan").html($("#fechan").val());
	$("#vnacionalidad").html($("#nacionalidad option:selected").text());
	$("#vpais").html($("#pais option:selected").text());
	
	if($("#pais").val()=='VE'){
		$(".nacio").show();
	}else{
		$(".nacio").hide();
	}
	$("#vestado").html($("#estado option:selected").text());
	$("#vciudad").html($("#ciudad option:selected").text());
	$("#vmunicipio").html($("#municipio option:selected").text());
	$("#vparroquia").html($("#parroquia").val());
	
	$("#vcomunidad").html($("#tcomunidad").val()+": "+$("#comunidad").val());
	$("#vinfra").html($("#infra").val());
	$("#vpiso").html($("#piso").val());
	$("#vnivel").html($("#nivel").val());
	$("#vtcasa").html($("#codPrincipal").val()+"-"+$("#tcasa").val().substring(1,12));
	$("#vttrabajo").html($("#codSecundario").val()+"-"+$("#ttrabajo").val().substring(1,12));
	$("#vcorreo").html($("#correo").val());
	$("#vccorreo").html($("#ccorreo").val());
	$("#patroc").html($("#patrocina").val());
	$("#patro_tel").html($("#patrotel").val());
	$("#patro_correo").html($("#patrocorreo").val());
	
	
if($("#cod").val().length<6){ swal("COMPLETAR INFORMACION","Debe poseer la referencia del patrocinador","error"); $("#cod").focus(); return false;}


if($("#ape").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus apellidos", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ape").focus(); },3000); return false;
}



if($("#nom").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus nombres", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nom").focus(); },3000); return false;	
}





if($("#ci").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ci").focus();},3000);  return false;
	}




if(($("#nci").val().length<5)||($("#nci").val().length>12)){
swal({title: "COMPLETAR INFORMACION", text: "Debe ingresar el numero de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nci").focus(); },3000); return false;
}


if($("#rif").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#rif").focus(); },3000); return false;
}



if(($("#nrif").val().length<6)||($("#nrif").val().length>13)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su numero de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nrif").focus(); },3000); return false;
}



if($("#sexo").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el sexo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#sexo").focus(); },3000); return false;
}



if($("#fechan").val().length<10){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar o agregar su fecha de nacimiento", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#fechan").focus(); },3000); return false;
}



if($("#nacionalidad").val().length<2){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar su nacionalidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nacionalidad").focus(); },3000); return false;}


if($("#pais").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el pais de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#pais").focus(); },3000); return false;
}



if($("#estado").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el estado de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#estado").focus(); },3000); return false;
}




if($("#ciudad").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la ciudad de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ciudad").focus(); },3000); return false;
}



if($("#municipio").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el municipio de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#municipio").focus(); },3000); return false;
}



if($("#parroquia").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la parroquia de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#parroquia").focus(); },3000); return false;
}


if($("#tcomunidad").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de comunidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcomunidad").focus(); },3000); return false;
}


if($("#comunidad").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre de la comunidad donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#comunidad").focus(); },3000); return false;
}


if($("#infra").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de infraestructura donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#infra").focus(); },3000); return false;
}


if($("#piso").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de piso o numero de la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#piso").focus(); },3000); return false;
}


if($("#nivel").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar los niveles que posee la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nivel").focus(); },3000); return false;
}


if(($("#tcasa").val().length<8)||($("#tcasa").val().length>14)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcasa").focus(); },3000); return false;
}


if(($("#ttrabajo").val().length<8)||($("#ttrabajo").val().length>14)){
swal({title: "COMPLETAR INFORMACION", text: "Debe repetir el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ttrabajo").focus(); },3000); return false;
}



if($("#tcasa").val() != $("#ttrabajo").val()){
swal({title: "COMPLETAR INFORMACION", text: "La informacion del numero de telefono de casa o personal no coincide, verifique.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ttrabajo").focus(); },3000); return false;
}




if($("#correo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}



if($("#correo").val().length>0){
	var validarEmail=validateEmail($("#correo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;	
	}
	
	
if($("#correo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}}


if($("#ccorreo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar nuevamente su correo electronico para confirmar", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}

if($("#ccorreo").val().length>0){
	var validarEmail=validateEmail($("#ccorreo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;	
	}
if($("#ccorreo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}
}
if($("#correo").val()!=$("#ccorreo").val()){
swal({title: "COMPLETAR INFORMACION", text: "Los correos electronicos no coinciden, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}




 if(n<2){ $("#sig").removeAttr("onclick");}

else if(n==2){

	
	
var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancoe=$("#bancoe").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();
var md = $("#moneda").val();
var mds = $("#moneda option:selected").text();
var moneda_actual = $("#moneda_"+md).val();
moneda_actual = parseFloat(moneda_actual);

if($("#tipot").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de pago", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipot").focus(); },3000); return false;
}




if($("#tipot").val()=="PRECARGADO"){
	if($("#codp").val().length<8){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el codigo precargado", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
	}	

	/*monto de Inscripcion*/
	if(moneda_actual>$("#montop").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#codp").focus();
		return false;
	}
			
	if($("#seguir").val()==0){
	swal({title: "COMPLETAR INFORMACION", text: "El codigo precargado no es valido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
	}

}

else if($("#tipot").val()=="PAGO DESDE EL EXTERIOR"){
	if($("#bancoexterior").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que emite (De) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoexterior").focus(); },3000); return false;
	}
	if($("#bancoexteriorpara").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que recibe (Para) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoexteriorpara").focus(); },3000); return false;
	}
	if($("#numero").val().length<4){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
	}	
	if($("#moneda").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#moneda").focus(); },3000); return false;
	}		
	if(($("#monto").val()<=0)||($("#monto").val()=='')){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
	}
	if($("#DPC_edit2").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
	}
	/*monto de Inscripcion*/
	if(moneda_actual>$("#monto").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#monto").focus();
		return false;
	}
}

else{
	
	var mispagos=$("#mispagos").val();
	var pmul=$("#pmul");

	if($("#bancoe").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que emite (De) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancoe").focus(); },3000); return false;
	}

	if($("#bancod").val()==""){ 
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco que recibe (Para) la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancod").focus(); },3000); return false;
	}
	if($("#numero").val().length<6){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
	}	
	if($("#moneda").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#moneda").focus(); },3000); return false;
	}
			/*monto de Inscripcion*/
	if(($("#monto").val()<=0)||($("#monto").val()=='')){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
	}
	if($("#DPC_edit2").val()==""){ 
		swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
	}
	if(moneda_actual>$("#monto").val()){
		swal("","El monto sumistrado no cubre el monto de inscripcion en "+mds,"info");
		$("#monto").focus();
		return false;
	}
	
	
}

}






return true;

}

function vista_contrato(){
	
	
if($("#avanzar").val() < 1){ 
	$("#ant").click();
	swal("COMPLETAR INFORMACION","El o los montos agregados no cubren el monto de inscripcion, por favor use la opcion de pagos multiples para completar el monto.","info");
	return false;
}
	
	
var c0=$("#cod").val();
var c1=$("#vape").html();
var c2=$("#vnom").html();
var c3=$("#vci").html();
var c4=$("#vrif").html();
var c5=$("#vfechan").html();
var c6=$("#vsexo").html();
var c7=$("#vpais").html();
var c71=$("#pais").val();
var c8=$("#vnacionalidad").html();
var c81=$("#nacionalidad").val();
var c9=$("#vestado").html();
var c10=$("#vciudad").html();
var c11=$("#vmunicipio").html();
var c12=$("#vparroquia").html();
var c13=$("#vcomunidad").html();
var ubica=$("#estado").val()+","+$("#ciudad").val()+","+$("#municipio").val();
var c14=$("#vinfra").html();
var c15=$("#vpiso").html();
var c16=$("#vnivel").html();
var c17=$("#vtcasa").html();
var codPrincipal=$("#codPrincipal").val();
var c18=$("#vttrabajo").html();
var c19=$("#vcorreo").html();
var c20=$("#vccorreo").html();


var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancoe=$("#bancoe").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();
var tmoneda=$("#moneda").val();
var bancoext=$("#bancoexterior").val();
var bancoextrec=$("#bancoexteriorpara").val();
if(bancod==0){
	bancod='DE '+bancoext+' A '+bancoextrec;
	}
else{
	bancod='DE '+bancoe+' A '+bancod;
}

if(tipot!=='PRECARGADO'){
var infoBanco ="<div align='justify'><b>Tipo de Transacción:</b> "+tipot+
				"<br><b><span style='color:blue'>"+bancod+"</span></b>"+
				"<br><b>Numero de Transacción: </b><span style='color:green'>"+numerod+"</span>"+
				"<br><b>Monto: </b><span style='color:green'>"+number_format(montod,2,",",".")+" "+tmoneda+" </span>"+
				"<br><b>Fecha de la Transacción: </b>"+fechad+"</div>";

swal({
  title: "<b><big>Información de Pago</big></b>",
  text: infoBanco,
  html: true
});
}


$.post("php/registro.php",{opc:2,
c0:c0,ubica:ubica,c71:c71,c81:c81,
c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,c8:c8,
c9:c9,c10:c10,c11:c11,c12:c12,c13:c13,c14:c14,
c15:c15,c16:c16,c17:c17,c18:c18,c19:c19,c20:c20,
tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,numerod:numerod,montod:montod,codPrincipal:codPrincipal
},function(resp){
	if(resp.salida==1){
setTimeout(function(){document.getElementById("micontrato").src="../pdf/vista_contrato_general_inversiones.php";},2000);
	}
},"json");
}





function registrar(){
	
if($("#avanzar").val() < 1){ 
	$("#ant").click();
	swal("COMPLETAR INFORMACION","El o los montos agregados no cubren el monto de inscripcion, por favor use la opcion de pagos multiples para completar el monto.","info");
	return false;
}


if(!$('#aceptar').is(':checked')){   $("#ant").click();  
	swal({title: "COMPLETAR INFORMACION", text: "Debe aceptar los terminos y condiciones antes de registrarse.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#aceptar").focus(); },3000); return false;
}



var c0=$("#cod").val();
var c1=$("#vape").html();
var c2=$("#vnom").html();
var c3=$("#vci").html();
var c4=$("#vrif").html();
var c5=$("#vfechan").html();
var c6=$("#vsexo").html();
var c7=$("#vpais").html();
var c71=$("#pais").val();
var c8=$("#vnacionalidad").html();
var c81=$("#nacionalidad").val();
var c9=$("#vestado").html();
var c10=$("#vciudad").html();
var c11=$("#vmunicipio").html();
var c12=$("#vparroquia").html();
var c13=$("#vcomunidad").html();
var ubica=$("#estado").val()+","+$("#ciudad").val()+","+$("#municipio").val();
var c14=$("#vinfra").html();
var c15=$("#vpiso").html();
var c16=$("#vnivel").html();
var c17=$("#tcasa").val();
var c18=$("#ttrabajo").val();
var c19=$("#vcorreo").html();
var c20=$("#vccorreo").html();

var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancoe=$("#bancoe").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();
var montop=$("#montop").val();
var tmoneda=$("#moneda").val();
var codPrincipal=$("#codPrincipal").val();
var bancoext=$("#bancoexterior").val();
var bancoextrec=$("#bancoexteriorpara").val();
if(tipot=='PAGO DESDE EL EXTERIOR'){
	bancod='DE '+bancoext+' A '+bancoextrec;
	}
else{
	bancod='DE '+bancoe+' A '+bancod;
}



$("#mipie").hide();
swal({
  title: "",
  text: "¿Esta segur@ que desea registrarse?",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
  	var fdata = new FormData(document.getElementById("form-inscripcion"));
	var archivos = document.getElementById("file");
	var archivos1 = document.getElementById("file");
	var archivo = archivos.files; 
  	var form = $('#form_recargas1').serialize();
	fdata.append('file',archivo[0]);
        fdata.append("opc", 1);
        fdata.append("c0", c0);
        fdata.append("ubica", ubica);
        fdata.append("c71", c71);
        fdata.append("c81", c81);
        fdata.append("c1", c1);
        fdata.append("c2", c2);
        fdata.append("c3", c3);
        fdata.append("c4", c4);
        fdata.append("c5", c5);
        fdata.append("c6", c6);
        fdata.append("c7", c7);
        fdata.append("c8", c8);
        fdata.append("c9", c9);
        fdata.append("c10", c10);
        fdata.append("c11", c11);
        fdata.append("c12", c12);
        fdata.append("c13", c13);
        fdata.append("c14", c14);
        fdata.append("c15", c15);
        fdata.append("c16", c16);
        fdata.append("c17", c17);
        fdata.append("c18", c18);
        fdata.append("c19", c19);
        fdata.append("c20", c20);
        fdata.append("tipot", tipot);
        fdata.append("codp", codp);
        fdata.append("bancod", bancod);
        fdata.append("fechad", fechad);
        fdata.append("numerod", numerod);
        fdata.append("montod", montod);
        fdata.append("montop", montop);
        fdata.append("moneda", tmoneda);
        fdata.append("codPrincipal", codPrincipal);

        $.ajax({
			url: 'php/registro.php',
			type: 'POST',
			// dataType: 'html',
			data:fdata,
			processData: false,
			contentType: false,
		})
		.done(function(data) {
			console.log("success");
			console.log(data);
			var resp = JSON.parse(data)
			var mensaje = resp.mensaje;
			if(resp.salida==1){
				if(resp.opcion==1){
					//document.getElementById("micontrato").src="../pdf/vista_contrato_general_inversiones.php";	
					swal("AFILIACION",mensaje,"success");			
				}
				else{
					mensaje = mensaje+"\n Debe esperar de 24 a 72 horas habiles para confirmar su pago.";
					swal("AFILIACION",mensaje,"success");
				}
				//setTimeout(function(){CorreoE(resp.opcion,mensaje);},3000);
				setTimeout(function(){ location.reload();},5000);		
			}
			else{
				setTimeout(function(){
					swal("REGISTRO FALLIDO",mensaje,"error"); 	
					$("#mipie").show(); 
					$("#ant").click();
				},1000);
				}
		})
		.fail(function(e) {
			console.log("error");
			console.log(e);
		})
		.always(function() {
			console.log("complete");
		});

        // $.post("php/registro.php",{fdata}
// $.post("php/registro.php",{opc:1,
// c0:c0,ubica:ubica,c71:c71,c81:c81,
// c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,c8:c8,
// c9:c9,c10:c10,c11:c11,c12:c12,c13:c13,c14:c14,
// c15:c15,c16:c16,c17:c17,c18:c18,c19:c19,c20:c20,
// tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,
// numerod:numerod,montod:montod,montop:montop,moneda:tmoneda,codPrincipal:codPrincipal
// }
// ,function(resp){
// 	var mensaje = resp.mensaje;
// 	if(resp.salida==1){
// 		if(resp.opcion==1){
// 			//document.getElementById("micontrato").src="../pdf/vista_contrato_general_inversiones.php";	
// 			swal("AFILIACION",mensaje,"success");			
// 		}
// 		else{
// 			mensaje = mensaje+"\n Debe esperar de 24 a 72 horas habiles para confirmar su pago.";
// 			swal("AFILIACION",mensaje,"success");
// 		}
// 		//setTimeout(function(){CorreoE(resp.opcion,mensaje);},3000);
// 		setTimeout(function(){ location.reload();},5000);		
// 	}
// 	else{
// 		setTimeout(function(){
// 			swal("REGISTRO FALLIDO",mensaje,"error"); 	
// 			$("#mipie").show(); 
// 			$("#ant").click();
// 		},1000);
// 		}
	
// },"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
	$("#mipie").show();
	$("#ant").click();
  }
});

	
}

function portafolio_auto(){
$.post("php/procesos_planes.php",{opc:1,id_plan:1,metodo:"true"}
,function(resp){ },"json");
}



function CorreoE(opc,mensaje){
	
	$.post("php/correo.php",{},function(resp){
				
		if(opc==1){
		    mensajeTexto("Listo",1,mensaje);

		}else{
		    mensajeTexto("Pre",1,mensaje);

		}
		setTimeout(function(){ location.reload();},5000);

	},"json");
			
}



var envios=0;
function mensajeTexto(opc,n,mensaje){
$.post("../msn/proceso.php",{opc:n, texto:opc},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
if(opc=="Listo"){$.post("../msn/apiP.php",{},function(resp1){},"json");}
		setTimeout(function(){
			setTimeout(function(){
				swal("REGISTRO EXITOSO",mensaje,"success");
			},1000);
		if(envios==0){ envios=1; mensajeTexto("Admin",2); }
		else{setTimeout(function(){ location.reload();},3000);}
		},1000);
},"json");	
}

function  metodo_afiliacion(opc){

	if(opc.value=="PRECARGADO"){
	$("#ccodp").show(500,'linear');
	$(".metodo-dt").hide(500,'linear');
	$(".ocultacampo").hide();
	}else if(opc.value=="PAGO DESDE EL EXTERIOR"){
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$(".ocultacampo").show();
	$("#bancoe,  #bancod").hide();
	$("#bancoe,  #bancod").val(0);
	$("#bancoexterior, #bancoexteriorpara").show();
	}
	else if(opc.value=="PAGO MOVIL"){
	$("#btn_recargar").attr({"disabled":false});

	var pm = $("#selBNPM").html();	
	$("#bancod").html(pm);	
	$("#bancod").val('BDV');	
	//$("#bancod").attr({"disabled":true});
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$("#bancoexterior, #bancoexteriorpara").hide();
	$(".ocultacampo").show();
	}
	else{
	var spm = $("#selBN").html();
	$("#bancod").html(spm);
	$("#bancod").attr({"disabled":false});	
	$("#bancoe").hide();
	$("#bancoexterior, #bancoexteriorpara").show();
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	$(".ocultacampo").show();
	$("#bancoe,  #bancod, #bancoexterior, #bancoexteriorpara").val('');
	$("#bancoexterior, #bancoexteriorpara").hide();
	}
}

/*validar codigo precargado*/
function valida_codigo(cod){

	if(cod.value.length<8){return false;}
	$("#codp").attr({"readOnly":true});
$.post("../php/valida_CPR.php",{cod:cod.value},function(resp){
	if(resp.st==1){
	$("#codp").attr({"readOnly":true});
	$("#montop").val(resp.montop);
	var div = "";
	if(resp.divisa==0){div = "USD";}
	if(resp.divisa==1){div = "BsS";}
	if(resp.divisa==2){div = "Euro";}
	if(resp.divisa==3){div = "PesoCo";}
	document.getElementById("moneda").value=div;
	$("#seguir").val(1);
	
	alerta("Validacion exitosa",resp.msn,3000,false);
	}
	else if(resp.st==2){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validacion fallida",resp.msn,3000,false);
	}
	else if(resp.st==3){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validacion fallida",resp.msn,3000,false);
	}
	else if(resp.st==0){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validacion fallida",resp.msn,3000,false);
	}
},"json");
	
}




function agregar_pagos(n){
	var tipot=$("#tipot").val();
	var codp=$("#codp").val();
	var bancoe=$("#bancoe").val();
	var bancor=$("#bancod").val();
	var fechad=$("#DPC_edit2").val();
	var numerod=$("#numero").val();
	var montod=$("#monto").val();
	var montop=$("#montop").val();
	bancod='DE '+bancoe+' A '+bancor;
	$.post("php/proceso_pago_temp.php",{opc:n, tipot:tipot,codp:codp,bancod:bancod,bancor:bancor,bancoe:bancoe,fechad:fechad,
	numerod:numerod,montod:montod,montop:montop},function(resp){
		$("#lista_pago").html(resp.listado);

		if(resp.salida == 1){
			$("#mispagos").val(resp.npagos);
			$("#avanzar").val(resp.avanzar);
			swal("",resp.mensaje,"success");
		}else{
			swal("",resp.mensaje,"error");
		}
	},"json");
	$('[data-toggle="tooltip"]').tooltip(); 
}


function quitar_pagos(i,n){
	
	$.post("php/proceso_pago_temp.php",{opc:n, i:i},function(resp){
		$("#lista_pago").html(resp.listado);

		if(resp.salida == 1){
			$("#mispagos").val(resp.npagos);
			$("#avanzar").val(resp.avanzar);
			swal("",resp.mensaje,"success");
		}else{
			swal("",resp.mensaje,"error");
		}
	},"json");
	$('[data-toggle="tooltip"]').tooltip(); 
}


function listar_pagos(opcion){
	var n='';
	if(opcion!=''){
		if(opcion=='PRECARGADO'){n = 6;}
		else{ n= 5; }
		$.post("php/proceso_pago_temp.php",{opc:n},function(resp){
			$("#lista_pago").html(resp.listado);
			$("#mispagos").val(resp.npagos);
			$("#avanzar").val(resp.avanzar);
		},"json");
	}

}


function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}








function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}


