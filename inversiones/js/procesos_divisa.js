
function mis_cofres(){

	$.post("php/proceso_divisa.php",{opc:4},function(resp){	
		$("#div_dolar").html("<b>USD$ "+resp.cofre_dolar+"</b>");
		$("#div_bolivar").html("<b>BsS "+resp.cofre+"</b>");
		$("#tasa_div_compra").html("<b>"+resp.pctasa_compra+"</b>");
		$("#tasa_div_venta").html("<b>"+resp.pctasa_venta+"</b>");
		$("#div_fecha").html("<b class='text-success'>Ultima Actualizaci&oacute;n "+resp.fecha_conf+"</b>");
		/*tasa monitor*/
		$("#tasa_monitor").html(resp.tasa_monitor);
		$("#fecha_tasa_monitor").html(resp.fecha_tasa_monitor);
	},"json");
}


function mis_cofres_gestion(){
	$.post("proceso_divisa.php",{opc:4},function(resp){	
		$("#div_dolar").html("<b>USD$ "+resp.cofre_dolar+"</b>");
		$("#div_bolivar").html("<b>BsS "+resp.cofre+"</b>");
		var pct_compra = resp.pctasa_compra.replace("BsS","BsS <br>");
		var pct_venta = resp.pctasa_venta.replace("BsS","BsS <br>");
		$("#tasa_div_compra").html("<b>"+pct_compra+"</b>");
		$("#tasa_div_venta").html("<b>"+pct_venta+"</b>");
		$("#div_fecha").html("<b class='text-success'>Fecha De Ultima Actualizaci&oacute;n "+resp.fecha_conf+"</b>");
		$("#cporc_tasa_compra").html("<b>"+resp.cporc_tasa_compra+"</b>");
		$("#cporc_tasa_venta").html("<b>"+resp.cporc_tasa_venta+"</b>");
		$("#tasa_actual").html("<b>"+number_format(resp.ptasa_compra,2,",",".")+" BsS.</b>");
		
		/*para modificacion*/
		$("#tasa_porc_venta").val(resp.cporc_tasa_venta1);
		$("#tasa_porc_compra").val(resp.cporc_tasa_compra1);
		$("#tasa_compra").val(resp.ptasa_compra);
		$("#tasa_venta").val(resp.ptasa_venta);
		$("#fechadivisa").val(resp.fecha_conf1);
		
		
	},"json");
}


function limpiar_divisa(){
	mis_cofres();
	$("#resultado_divisa").html("0,00");	
	$("#miresultado,#btn_comprar").hide();	
	$("#monto_divisa").val("");	
}
	
function cambio_divisa(){	

	var tipo = $("#tipo").val();
	var monto = $("#monto_divisa").val();
	var dolar = $("#div_dolar").val();
	var bolivar = $("#div_bolivar").val();
	
	$.post("php/proceso_divisa.php",{tipo:tipo, monto:monto, dolar:dolar, bolivar:bolivar},function(resp){
		if(resp.salida == 1){
			$("#resultado_divisa").html(resp.monto);	
			$("#miresultado,#btn_comprar").show(500,'linear');		
			$("#compra").html("Usted esta realizando una compra de "+resp.montoletra+"  ("+resp.monto+")");	
		}else{
			$("#miresultado,#btn_comprar").hide(500,'linear');
			swal("Notificación",resp.mensaje,"error");
		}
	},"json");	
	
}

function comprar_divisas(){	
var mensaje = $("#compra").html();
swal({
  title: "¿Esta segur@ que desea realizar la compra?",
  text: mensaje,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
	$.post("php/proceso_divisa.php",{tipo:3},function(resp){
		
		if(resp.salida == 1){
			setTimeout(function(){
				swal("Notificación",resp.resultado,"success");
			},1500);
						
			limpiar_divisa();
		}else{
		    $("#miresultado,#btn_comprar").hide(500,'linear');
			setTimeout(function(){
			    swal("Notificación",resp.resultado,"error");
			},1500);
		}	
		
	},"json");	
  } 
});
	
}





function modificar_divisa(){
	
var pcompra = $("#tasa_porc_compra").val();
var pventa = $("#tasa_porc_venta").val();
var fecha = $("#fechadivisa").val();
var tcompra = $("#tasa_compra").val();
var tventa = $("#tasa_venta").val();
	
	
swal({
  title: "",
  text: "¿Esta segur@ que desea realizar los cambios en la divisa?",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
	
	$.post("proceso_divisa.php",{opc:5, pcompra:pcompra, pventa:pventa, fecha:fecha, tcompra:tcompra, tventa:tventa},function(resp){
		if(resp.salida == 1){
			mis_cofres_gestion();
			setTimeout(function(){swal("",resp.mensaje,"success");},1000);
			$("#formulario").hide();
		}else{
			ssetTimeout(function(){swal("",resp.mensaje,"success");},1000);
		}	
	},"json");
  }
});
	
}