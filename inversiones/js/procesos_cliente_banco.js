function datostitular(opc){
	if(opc.value=="Si"){
		//$("#titular,#cititular,#correotitular,#teltitular").attr({"readOnly":true});
		$("#titular,#cititular,#correotitular").attr({"readOnly":true});
		$("#titular").val($("#nom").val()+" "+$("#ape").val());
		$("#cititular").val($("#ci").val()+$("#nci").val());
		$("#correotitular").val($("#correo").val());
		$("#teltitular").val($("#tcasa").val());
	}
	else{
		//$("#titular,#cititular,#correotitular,#teltitular").attr({"readOnly":false});
		$("#titular,#cititular,#correotitular").attr({"readOnly":false});
		$("#titular").val("");
		$("#cititular").val("");
		$("#correotitular").val("");
		$("#teltitular").val("");		
	}
}




function cargar_vista(){
	
	$.post("php/proceso_usuarios.php",{opc:1},function(resp){
	
	$("#ape").val(resp.ape);
	$("#nom").val(resp.nom);
	$("#ci").val(resp.tci);
	$("#nci").val(resp.ci);
	$("#rif").val(resp.trif);
	$("#nrif").val(resp.rif);
	$("#tcasa").val(resp.tcasa);
	$("#correo").val(resp.correo);
	},"json");

}cargar_vista();


function cargar_datos_bancarios(){
	
	$.post("php/proceso_usuarios.php",{opc:4},function(resp){
	if(resp.salida==1){

$("#posee").val(resp.posee);
$("#titular").val(resp.titular);
$("#cititular").val(resp.cedulat);
$("#correotitular").val(resp.correot);
$("#teltitular").val(resp.telefonot);
$("#banco").val(resp.banco);
$("#tipoc").val(resp.tipo);
$("#ncuenta").val(resp.ncuenta);

if(resp.posee=="Si"){
	//$("#titular,#cititular,#correotitular,#teltitular").attr({"readOnly":true});
	$("#titular,#cititular,#correotitular").attr({"readOnly":true});
}

	$("#Rbancaria,#Cbancaria").hide();
	$("#Cbancaria").show();
	}else{
	$("#Rbancaria,#Cbancaria").hide();
	$("#Rbancaria").show();	
	}
	
	
	},"json");
}


function lista_cuentas_bancarias(){
	$.post("php/proceso_usuarios.php",{opc:5},function(resp){
		$("#listaCuentas").html(resp.listado);
		if(resp.num>0)
			reloadDatatableRetiro();
		limpiar_cuenta();
	},"json");
}


function limpiar_cuenta(){
	$("#Rbancaria,#Cbancaria,#Canbancaria,#Rbancaria1,#Cbancaria1,#Canbancaria1").hide();
	$("#Rbancaria,#Rbancaria1").show();
	$("#titular,#cititular,#correotitular,#banco,#tipoc,#ncuenta,#posee").val('');
	$("#pagomovil").prop('checked',false);
	$("#titular1,#cititular1,#correotitular1,#teltitular1,#banco1,#tipoc1,#ncuenta1,#divisa1,#nruta1,#npropia1").val('');
}

function addcount(tipo){

$(".addC").hide();
if(tipo == 'ven')
	$("#addVen").show(500,'linear');
else
	$("#addExt").show(500,'linear');
}

/*guardar  los datos personales*/
function guardar_bancario(n){

if($("#posee").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar si posee cuenta", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#posee").focus(); },3000); return false;
}

if($("#titular").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir los nombres y apellidos del titular", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#titular").focus(); },3000); return false;
}





if($("#cititular").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir la cedula  de identidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#cititular").focus();},3000);  return false;
	}


if($("#correotitular").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular").focus(); },3000); return false;
}
if($("#correotitular").val().length>0){
	var validarEmail=validateEmail($("#correotitular").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular").focus(); },3000); return false;	
	}
	
	
if($("#correotitular").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular").focus(); },3000); return false;
}}

if(($("#teltitular").val().length<11)||($("#teltitular").val().length>11)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#teltitular").focus(); },3000); return false;
}

if($("#banco").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la entidad bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#banco").focus(); },3000); return false;
}


if($("#tipoc").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar tipo de cuenta bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipoc").focus(); },3000); return false;
}


if(($("#ncuenta").val().length<20)||($("#ncuenta").val().length>20)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar los 20 digitos de su cuenta bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ncuenta").focus(); },3000); return false;
}


var c0=$("#posee").val();
var c1=$("#titular").val();
var c2=$("#cititular").val();
var c3=$("#correotitular").val();
var c4=$("#teltitular").val();
var c5=$("#banco").val();
var c6=$("#tipoc").val();
var c7=$("#ncuenta").val();
var c8='1';
var c9=$("#pagomovil").val();
var idban=$("#idban").val();



swal({
  title: "¿Esta segur@ que desea registrar esta informacion bancaria?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
$.post("php/proceso_usuarios.php",{opc:3,n:n,
c0:c0,c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,divisa:c8,pagomovil:c9,c10:'',id:idban
}
,function(resp){
	
	if(resp.salida==1){
	swal("REGISTRO EXITOSO",resp.msn,"success");
	lista_cuentas_bancarias();
	}
	else{
		swal("REGISTRO FALLIDO",resp.msn,"error"); 
		}
},"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

	
}




/*guardar  los datos personales*/
function guardar_bancario_ext(n){

if($("#titular1").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir los nombres y apellidos del titular", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#titular1").focus(); },3000); return false;
}


if($("#cititular1").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir la cedula  de identidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#cititular1").focus();},3000);  return false;
	}


if($("#correotitular1").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular1").focus(); },3000); return false;
}
if($("#correotitular1").val().length>0){
	var validarEmail=validateEmail($("#correotitular1").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular1").focus(); },3000); return false;	
	}
	
	
if($("#correotitular1").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correotitular1").focus(); },3000); return false;
}}

if($("#teltitular1").val().length>0){
	if(($("#teltitular1").val().length<8)||($("#teltitular1").val().length>16)){
		swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#teltitular1").focus(); },3000); return false;
	}
}

if($("#banco1").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la entidad bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#banco1").focus(); },3000); return false;
}


if($("#divisa1").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la moneda a recibir la cuenta bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa1").focus(); },3000); return false;
}

if($("#tipoc1").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar tipo de cuenta bancaria", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipoc1").focus(); },3000); return false;
}


if(($("#nruta1").val().length<6)||($("#nruta1").val().length>32)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar Código del Banco / Numero de Ruta ABA/ACH", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nruta1").focus(); },3000); return false;
}



if(($("#ncuenta1").val().length<6)||($("#ncuenta1").val().length>32)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar N° de Cuenta / IBAN", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ncuenta1").focus(); },3000); return false;
}



var c0=$("#npropia1").val();
var c1=$("#titular1").val();
var c2=$("#cititular1").val();
var c3=$("#correotitular1").val();
var c4=$("#teltitular1").val();
var c5=$("#banco1").val();
var c6=$("#tipoc1").val();
var c7=$("#ncuenta1").val();
var c10=$("#nruta1").val();
var c8=$("#divisa1").val();
var c9='No'; //pago movil
var idban=$("#idban").val();


swal({
  title: "¿Esta segur@ que desea registrar esta informacion bancaria?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
$.post("php/proceso_usuarios.php",{opc:3,n:n,
c0:c0,c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,divisa:c8,pagomovil:c9,c10:c10,id:idban
}
,function(resp){
	
	if(resp.salida==1){
		swal("REGISTRO EXITOSO",resp.msn,"success");
		lista_cuentas_bancarias();
	}
	else{
		swal("REGISTRO FALLIDO",resp.msn,"error"); 
		}
},"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

	
}


function editar_cuenta(id){
	$.post("php/proceso_usuarios.php",{opc:7, id:id},function(resp){
		$("#Rbancaria,#Cbancaria,#Canbancaria,#Rbancaria1,#Cbancaria1,#Canbancaria1").hide();

		$("#idban").val(id);
		if(resp.divisa==1){
			$("#titular").val(resp.titular);
			$("#cititular").val(resp.cedulat);
			$("#correotitular").val(resp.correot);
			$("#teltitular").val(resp.telefonot);
			$("#banco").val(resp.banco);
			$("#tipoc").val(resp.tipo);
			$("#ncuenta").val(resp.ncuenta);
			$("#divisa").val(resp.divisa);

			if(resp.pagomovil=='Si'){
				$("#pagomovil").prop('checked',true);
			}

			$("#posee").val(resp.posee);
			$("#Cbancaria,#Canbancaria,#Rbancaria1").show();
			addcount('ven');
		}else{
			
			$("#titular1").val(resp.titular);
			$("#cititular1").val(resp.cedulat);
			$("#correotitular1").val(resp.correot);
			$("#teltitular1").val(resp.telefonot);
			$("#banco1").val(resp.banco);
			$("#tipoc1").val(resp.tipo);
			$("#ncuenta1").val(resp.ncuenta);
			$("#nruta1").val(resp.ruta);
			$("#divisa1").val(resp.divisa);
			$("#npropia1").val(resp.posee);
			$("#Cbancaria1,#Canbancaria1,#Rbancaria").show();
			addcount('ext');
		}

		


		

	},"json");
}


function cancelar(n){
	if(n==1){
		$("#Cbancaria,#Canbancaria").hide();
		$("#Rbancaria").show();
	}
	if(n==2){
		$("#Cbancaria1,#Canbancaria1").hide();
		$("#addExt").hide(500,'linear');
	}
	limpiar_cuenta();
}

function borra_cuenta(id){
	swal({
		title: "¿Esta segur@ que desea eliminar esta informacion bancaria?",
		text: "",
		type: "info",
		showCancelButton: true,
		closeOnConfirm: false,
		showLoaderOnConfirm: true,
		confirmButtonText: "Si, Acepto",
		cancelButtonText: "No, Cancelar"
	},
	function(isConfirm){
	  	if (isConfirm) {
			$.post("php/proceso_usuarios.php",{opc:6, id:id},function(resp){
				if(resp.salida==1){
					swal("",resp.msn,"success");
					lista_cuentas_bancarias();
				}else{
					swal("",resp.msn,"error");
				}
			},"json");
		} 
	});
}



function cargar_entidad(entidad){
	$("#banco1").val(entidad);
	$("#detallesBanco").hide();
	}
function mostrar_bancos(ele,opc){
	if(opc==1){
		$("#detallesBanco").show();
		$(ele).attr({"onclick":"mostrar_bancos(this,0)"});
	}
	else{
		$("#detallesBanco").hide();
		$(ele).attr({"onclick":"mostrar_bancos(this,1)"});
	}
}

function noAplica(opc){
	if(opc.value=='No Aplica'){
		$("#ncuenta1, #nruta1").attr({"readOnly":true}).val('10000000000000000000');
		$("#ncuenta1, #nruta1").css({"color":"white","background-color":"white"});
	}
	else{
		$("#ncuenta1, #nruta1").attr({"readOnly":false}).val('');
		$("#ncuenta1, #nruta1").css({"color":"black","background-color":"white"});
	}

}



function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}


