  function asistir(){
	
		
	var boleto = $("#boletos").val();
	var plural='';
	if(boleto > 1){ plural='s'; }
	swal({
	title: "¿Estas seguro de reservar "+boleto+" Boleto"+plural+"?",
	text: "La reserva de boletos es temporal, por lo que se recomienda realizar la compra lo antes posible.",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar"
	 },

		function(isConfirm){
			if (isConfirm) {
				
				$.post("php/proceso_evento.php",{opcion:1, nboleto:boleto},function(resp){
					
				if(resp.salida == 1){
					setTimeout(function(){	
					swal("¡Hecho!",resp.mensaje,"success");
					},1000);
					
				}else{
					setTimeout(function(){	
					swal("¡Oop!",resp.mensaje,"error");
					},1000);	
				}	
				consultaboleto();
				},"json");

			} 
	});
}


function consultaboleto(){
	
	$.post("php/proceso_evento.php",{opcion:2},function(resp){
		/*mostrar opcion de precompra*/

		if(resp.boletoE == resp.venta || resp.cupos == 0 || resp.cupos == resp.disponible){
			$(".asistir").css({"display":"none"});
		}else{
			$(".asistir").css({"display":"inline"});
		}

		/*mostrar boletos comprados*/
		if(resp.boleto>0){
			$("#mostrarboletos").css({"display":"inline"});
		}else{
			$("#mostrarboletos").css({"display":"none"});
		}
		/*mostrar opcion de compra*/
		if(resp.disponible == 0){
			$("#portafolioCompra").css({"display":"none"});
			$("#paracomprar").html('').css({"display":"none"});
		}else{
			var plural='';
			if(resp.disponible>1){plural = 's';}
			portafolio();
			$("#portafolioCompra").css({"display":"inline"});
			$("#paracomprar").html('Usted tiene <label class="label label-success" id="reservado">'+resp.disponible+'</label> boleto'+plural+' reservados.  <b>¿Desea comprarlos?</b><br> <a href="#" onclick="return comprar()" class="btn btn-sm btn-success"   title="Comprar boletos"><i class="fa fa-shopping-cart"></i> Comprar</a> <a href="#" onclick="return cancelar_comprar()" class="btn btn-sm btn-danger"   title="Cancelar compra de boletos"><i class="fa fa-ban"></i> Cancelar</a>').css({"display":"inline"});
		}

		var plural2 ='';
		if(resp.boleto>1){plural2='s';}
		$("#cupos").html(resp.cupos);
		$("#nbol").html(resp.boleto);
		$("#nbol").html('Usted posee la cantidad de <span class="label label-primary" style="font-size:12px;">'+resp.boleto+'</span> boleto'+plural2+'  comprado'+plural2+'.');
		$("#precio").html(number_format(resp.precio,2,",","."));
		$("#precio1").html(number_format(resp.precioUSD,2,",","."));
		$("#pre1").html(resp.precio);
		$("#pre2").html(resp.precioUSD);
		

	},"json");
	
}
function comprar(){
	var divisa = $("#portafolios").val();
	var boleto = $("#reservado").html();
	var precio = $("#precio1").html();
	var plural='';
	var precioBol=0;

	if(divisa=='BsS'){ precioBol = $("#pre1").html();}
	if(divisa=='USD'){ precioBol = $("#pre2").html();}

	if(boleto > 1){ plural='s'; }
	swal({
	title: "¿Estas seguro de comprar "+boleto+" Boleto"+plural+"?",
	text: "La compra del cada boleto se le deducirá de su portafolio de inversiones.\n El monto a pagar es de "+number_format(eval(parseFloat(precioBol)*parseFloat(boleto)),2,",",".")+" "+divisa,
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar" 
},

	function(isConfirm){
		if (isConfirm) {
				
			$.post("php/proceso_evento.php",{opcion:3,divisa:divisa},function(resp){ 
				if(resp.salida == 1){
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"success"); },1000);
					consultaboleto();
					
				}else{
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"error"); },1000);
					
				}
			},"json");
		}
	});
}

function cancelar_comprar(){
	var boleto = $("#reservado").html();
	var plural='';
	if(boleto > 1){ plural='s'; }
	swal({
	title: "",
	text: "¿Estas seguro de cancelar la compra de "+boleto+" Boleto"+plural+"?",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar"
   },

	function(isConfirm){
		if (isConfirm) {
				
			$.post("php/proceso_evento.php",{opcion:4,nboleto:boleto},function(resp){ 
				if(resp.salida == 1){
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"success"); },1000);
					
				}else{
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"error"); },1000);
					
				}
				consultaboleto();
			},"json");
		}
	});
}


function portafolio(){
	$.post("php/proceso_evento.php",{opc:'boleto'},function(resp){
		$("#portafolioCompra").html(resp.lista_moneda);
	},"json");
}