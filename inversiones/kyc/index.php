<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html lang="en">
	<meta charset="utf8">
	<head>
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
		<script src="js/jq.min.js"></script>
		<link href="../css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
		<script src="js/AjaxUpload.2.0.min.js"></script>
		<script src="bootstrap/fileinput/js/fileinput.min.js"></script>
		<link href="css/sweetalert.css" rel="stylesheet">
		<script src="js/imagen.js"></script>
		<style>
			.imgr{
			    -webkit-animation: 3s rotate linear infinite;
			    animation: 3s rotate linear infinite;
			    -webkit-transform-origin: 50% 50%;
			    transform-origin: 50% 50%;
			}
			.img-circle {
			  background:#fff;
			  width:100%;
			  border-radius:45px;
			  height:100%;
			}


		</style>


	</head>
	<body>

		<div class="panel panel-success" style="background-image:url(images/fondo_red.png)">
			<div class="panel-heading text-center"><big><big>VERIFICAR IDENTIDAD</big></big></div>
				<div class="panel-body">

					<br>
					<div style="background:#fff;">
							<br>


						

						<br>
						<div class="row">
							<div class="col-md-12 jumbotrom"  style="background-image:url('images/cuadros.png'); ">
								<div class="text-center" id="pc">
									<div>
										<div class="col-md-4">
											<select name="listaDeDispositivos" id="listaDeDispositivos" class="form-control">
												<option value="">NO HAY CAMARAS CONECTADAS</option>
											</select>
										</div>
										<div class="col-md-4">
											<button id="boton" class="btn btn-info"> <i class="fa fa-camera"></i>Tomar foto</button>
											<p id="estado"></p>
											<button id="boton-c" class="btn btn-info"> <i class="fa fa-camera"></i>Activar Camara</button>
											<button id="boton-a" class="btn btn-info"> <i class="fa fa-camera"></i>Apagar Camara</button>
											<p id="estado"></p>
										</div>
									</div>
									<br>
									<video muted="muted" id="video"></video>
									<canvas id="canvas" style="display: none; width: 100% !important; height: 100px !important:" width="100%" style="border:10px solid #000000;"></canvas>
								</div>
								<div id="tel">
									<img src="" alt="" id="miimagen1">
									<input id="imagen" name="imagen" data-toggle="tooltip" title="Debe seleccionar un archivo de imagen para al perfil"  type="file" class="file-loading"  accept="image/*" capture>
									<a href="#"  data-toggle="tooltip" id="bsubir"  class="btn btn-md btn-success" onclick="return  CamaraMovil()" title="Pulse para subir la imagen"><i class="fa fa-picture-o"></i> Subir Imagen <i class="fa fa-upload"></i></a>
								</div>
							</div>
						</div>
						<!-- <div class="jumbotron" style="background-image:url('images/cuadros.png'); ">
							<center>
								<div class="" style="width:50%; height:50%;">
									<img id="miimagen" src="<?php echo $_SESSION["imagend"]; ?>"  class="img-responsive img-circle" />
								</div>
							</center>
						</div>
 -->
					</div>
				</div>
			</div>
		</div>
		<script>parent.cerrar_carga();</script>
	    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script> 
	    <script src="js/sweetalert.min.js"></script>
	    <script src="js/sweetalert.dev.js"></script>
	    <script src="js/wow.min.js"></script>
	    <script src="js/script.js"></script>
		<script>
			$(function() {
				
				if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
				   
				    $('#pc').hide();
				    $('#tel').show();

				}
				else{
					$('#tel').hide();
				    $('#pc').show();
				    // camara();
				}

				$(document).on('click', '#boton-c', function(event) {
					event.preventDefault();
					/* Act on the event */
					camara();
				});

				$(document).on('click', '#boton-a', function(event) {
					event.preventDefault();
					/* Act on the event */
					vidOff();
					// $('#video').attr('src', '#');
					
				});
			});

			function vidOff() { 
			    //clearInterval(theDrawLoop); 
			    //ExtensionData.vidStatus = 'off'; 
			    video.pause(); 
			    video.src = ""; 
			    localstream.getTracks()[0].stop(); 
			    console.log("Vid off"); 
			} 
		</script>
	</body>
</html>