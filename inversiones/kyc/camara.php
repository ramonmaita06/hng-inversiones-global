<div class="row">
	<div class="col-md-12 jumbotrom"  style="background-image:url('images/cuadros.png'); ">
		<div class="text-center" id="pc">
			<div>
				<div class="col-md-4">
					<select name="listaDeDispositivos" id="listaDeDispositivos" class="form-control">
						<option value="">NO HAY CAMARAS CONECTADAS</option>
					</select>
				</div>
				<div class="col-md-4">
					<button id="boton" class="btn btn-info"> <i class="fa fa-camera"></i>Tomar foto</button>
					<p id="estado"></p>
					<button id="boton-c" class="btn btn-info"> <i class="fa fa-camera"></i>Activar Camara</button>
					<button id="boton-a" class="btn btn-info"> <i class="fa fa-camera"></i>Apagar Camara</button>
					<p id="estado"></p>
				</div>
			</div>
			<br>
			<video muted="muted" id="video"></video>
			<canvas id="canvas" style="display: none; width: 100% !important; height: 100px !important:" width="100%" style="border:10px solid #000000;"></canvas>
		</div>
		<div id="tel">
			<img src="" alt="" id="miimagen1">
			<input id="imagen" name="imagen" data-toggle="tooltip" title="Debe seleccionar un archivo de imagen para al perfil"  type="file" class="file-loading"  accept="image/*" capture>
			<a href="#"  data-toggle="tooltip" id="bsubir"  class="btn btn-md btn-success" onclick="return  CamaraMovil()" title="Pulse para subir la imagen"><i class="fa fa-picture-o"></i> Subir Imagen <i class="fa fa-upload"></i></a>
		</div>
	</div>
</div>