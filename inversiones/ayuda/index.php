<!doctype html>
<html>
<meta charset="UTF8">
<head><title>Limpiar cache</title></head>
<body>
<div class="panel panel-warning" style="width:100%">
	<div class="panel-heading" style="font-size: 18px;">
		<div class="pull-right"><button class="btn btn-sm btn-default" style="border:none; background: none;" onclick="$('.ayuda, .cortina_ayuda').hide(500,'linear');" title="Pulse para cerra la ventana de ayuda especifica" data-toggle="tooltip"data-placement="bottom"><i class=" fa fa-times"></i> Cerrar</button></div>
		<br><br>
		   <center><div class="alert alert-warning" style="font-size: 24px;"><b>Ayudanos a mejorar los procesos de las App de HNG.</b>
		   	<p>Estimad@ cliente, constantemente el departamento de sistema de información realiza mejoras a la aplicacion, es por ello que le recomendamos realizar algunos pasos para que estos procesos nuevos puedan ejecutarse y no generen inconvenientes en los procesos que usted realiza.</p></div><br>
		 <img src="ayuda/images/navegadores.png" class="img-responsive"><br>
		 <img src="ayuda/images/ctrolf5.jpg">
		</center><br>

<b>HNG recomienda limpia la caché del navegador web 2 o 3 veces por semana.</b><br>
  ¿Como lo Hago?<br>
  <p>&nbsp;&nbsp;&nbsp;&nbsp; Presiona la combinación de teclas <b>Ctrl + F5</b>
  o bien: Presiona la tecla <b>Ctrl y haz clic en el botón "Actualizar"</b> de la barra de herramientas.</p><br>
  
  ¿Como Limpio la caché del navegador web de mi telefono inteligente (SmartPhone)?<br>
  <p>
  	<b>Eliminar los datos de navegación</b><br>
1.- En tu teléfono o tablet Android, abre la aplicación Chrome.<br>
2.- Toca Más Ajustes.<br>
3.- En la sección "Configuración avanzada", toca Privacidad Borrar datos de navegación.<br>
4.- Elige un periodo (por ejemplo, Última hora o Desde siempre).<br>
5.- Selecciona el tipo de información que quieras eliminar.
</p>
<br>
Mas Opciones, 	<b><a href="#masopciones" onclick="$('.verMasAyuda').show(500,'swing');">Pulsa aquí</a></b>
	</div>
	<div class="panel-body verMasAyuda" style="display: none">
		<a name="masopciones"></a>


		<b>Limpia la caché y elimina los archivos temporales de Internet para solucionar problemas habituales con los sitios web.</b>
		La caché de Firefox almacena temporalmente imágenes, scripts y otras partes de los sitios web que visitas para mejorar tu experiencia de navegación. Este artículo describe cómo limpiar la caché.<br>
		•	Para limpiar el historial (es decir, cookies, historial de navegación, caché, etc.) todo de una vez, puedes ver el artículo Limpiar el historial de navegación, búsquedas y descargas.<br>
		<b>Tabla de contenidos</b><br>
		•	Limpiar la caché<br>
		•	Limpiar la caché de forma automática<br>
		<b>Limpiar la caché</b><br>
		1.	Haz clic en el botón Menú   y elige Opciones.<br>
		2.	Selecciona el panel Privacidad y seguridad.<br>
		3.	En la sección Cookies y datos del sitio, haz clic en Limpiar datos….<br>
		 <img src="ayuda/images/cache1.png" class="img-responsive"><br>
		4.	Desmarca la casilla que se encuentra frente a la opción Cookies y datos del sitio.<br>
		o	Para obtener más información sobre cómo gestionar los datos del sitio, consulta el artículo API de almacenamiento web - algunas páginas quieren agregar archivos a tu equipo.<br>
		5.	Cuando hayas marcado la casilla Contenido web en caché, haz clic en Limpiar.<br>
		6.	Cierra la página about:preferences. Cualquier cambio que hayas hecho se guardará automáticamente.
		Consejo: <b>Otra forma de limpiar la caché de Firefox:</b><br>
		Haz clic en el botón de Catálogo <img src="ayuda/images/cache2.png" class="img-responsive">    , en Historial y, a continuación, en Limpiar el historial reciente…. Junto a la opción Rango temporal a limpiar, elige Todo, selecciona Caché, asegúrate de que todo lo que no quieres limpiar no esté seleccionado y haz clic en Limpiar ahora. Para obtener más información.<br>
		<b>Limpiar la caché de forma automática</b><br>
		Puedes configurar Firefox para limpiar la caché de forma automática cuando Firefox se cierre:<br>
		1.	Haz clic en el botón Menú   y elige Opciones.<br>
		2.	Selecciona el panel Privacidad y seguridad y ve a la sección Historial.<br>
		3.	En el menú desplegable, junto a Firefox:, elige la opción Utilizar una configuración personalizada para el historial.<br>
		 <img src="ayuda/images/cache3.png" class="img-responsive"><br>
		4.	Marca la casilla de verificación Limpiar el historial cuando Firefox se cierre.<br>
		5.	Junto a Limpiar el historial cuando Firefox se cierre, haz clic sobre el botón Configuración…. Se abrirá la ventana de Configuración para Limpiar historial.<br>
		6.	En la ventana de Configuración para Limpiar historial, marca la casilla de verificación llamada Caché.
		<br>
		  <img src="ayuda/images/cache4.png" class="img-responsive">
		  <br>
		o	Para más información sobre otras opciones, puedes ver el artículo Limpiar el historial de navegación, búsquedas y descargas.
		<br>
		7.	Haz clic en Aceptar para cerrar la ventana de Configuración para Limpiar historial.
		<br>
		8.	Cierra la página about:preferences. Cualquier cambio que hayas hecho se guardará automáticamente.
		<br><br>

		<b>Información extraída de:</b> <a href="https://support.mozilla.org/es/kb/limpia-la-cache-y-elimina-los-archivos-temporales-#w_limpiar-la-cachae-de-forma-automaatica" target="1">
			https://support.mozilla.org/es/kb/limpia-la-cache-y-elimina-los-archivos-temporales-#w_limpiar-la-cachae-de-forma-automaatica
		</a>
	</div>
</div>

</body>
</html>
