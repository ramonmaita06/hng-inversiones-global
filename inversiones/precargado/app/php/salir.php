<?php session_name("hng"); session_start(); ?>

<!doctype html>
<html>

<head>
<link rel="icon" href="../../images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../../images/favicon.ico" type="image/x-icon" />
<title><?php echo $_SESSION["version"]; ?></title>
<meta charset="utf8">
<link rel="icon" href="../../images/favicon.gif" type="image/gif">
<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
</head>
<body>


<?php 
if(isset($_SESSION['us'])){
session_unset();
session_destroy();


echo "
<div class='panel panel-success'>
<div class='panel-heading'>
<center><i class='fa fa-sign-out' style='font-size:48px'></i></center>
</div>
<div class='panel-body'>
<br><br><center><h1><img src='../../images/cargar6.gif' width='48'><br><span style='color:green'>Saliendo del sistema...</span></h1></center>
</div></div>
<script>setTimeout(function(){location.href='../.././';},2000);</script>
";
}else{
echo "
<br><br><br><br><center><div class='panel panel-danger' style='width:50%'>
<div class='panel-heading'>
<i class='fa fa-ban' style='font-size:48px'></i>

</div>
<div class='panel-body'>
<br><br><center><h3><span style='color:red'>Acceso Denegado</span></h3></center>

</div>
</div></center>
<script>location.href='../.././';</script>
";

}
?>
</body>
</html>