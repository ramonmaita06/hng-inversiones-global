<?php 


function abecedario($v){
	$l=array("0"=>"A","1"=>"B","2"=>"C","3"=>"D","4"=>"E","5"=>"F","6"=>"G","7"=>"H","8"=>"I","9"=>"J","10"=>"K","11"=>"L","12"=>"M","13"=>"N","14"=>"Ñ","15"=>"O","16"=>"P","17"=>"Q","18"=>"R","19"=>"S","20"=>"T","21"=>"U","22"=>"V","23"=>"W","24"=>"X","25"=>"Y","26"=>"Z");
	return $l[$v];
}



function separar_cuenta($nc){
	
	$ncuenta=substr($nc,0,4).'-'.substr($nc,4,4).'-'.substr($nc,8,2).'-'.substr($nc,10,10);
	return $ncuenta;
}


function gclave(){
	$codigo=array("0","1","2","3","4","5","6","7","8","9","A","B","C","D","E","F");
	for($i=0; $i<8; $i++){
		$clave=$clave.$codigo[rand(0,15)];
	}
	return $clave;
}

	
function sumar_hora($hora,$nm){
$nuevahora = strtotime ( '+'.$nm.' minute' , strtotime ( $hora ) ) ;
$nuevahora = date ( 'H:i' , $nuevahora ); 
return $nuevahora;	
	}
	
function sumar_hora2($hora,$nm){
$nuevahora = strtotime ( '+'.$nm.' minute' , strtotime ( $hora ) ) ;
$nuevahora = date ( 'H:i:s' , $nuevahora ); 
return $nuevahora;	
	}	
	
function color($n){

	$colores=array(
	"0"=>"success",	"1"=>"danger",	"2"=>"primary",	"3"=>"info",
	"4"=>"default",	"5"=>"warning",	"6"=>"primary",	"7"=>"danger",
	"8"=>"default",	"9"=>"success",	"10"=>"info",	"11"=>"warning",
	"12"=>"danger",	"13"=>"primary","14"=>"default","15"=>"info","16"=>"default","17"=>"danger","18"=>"primary","19"=>"success",
	"20"=>"warning","21"=>"primary","22"=>"info","23"=>"default",
	"24"=>"success","25"=>"warning","26"=>"info","27"=>"danger"
	);
	return $colores[$n];
}	

function estatus_transferencia($valor){
	$repite=array(
	"0"=>"<i class='fa fa-clock-o' style='color:orange' title='En espera'></i>",
	"1"=>"<i class='fa fa-check' style='color:green' title='Realizada'></i>",
	"2"=>"<i class='fa fa-times-circle' style='color:red' title='Cancelada'></i>",
	"3"=>"<i class='fa fa-save' style='color:blue' title='Guardada'></i>"
	);
	return $repite[$valor];
}


function estatus_inv($valor){
	$repite=array(
	"0"=>"<span style='color:orange'>En espera</span>",
	"1"=>"<span style='color:green'>En proceso</span>",
	"2"=>"<span style='color:orange'>Finalizada</span>",
	"3"=>"<span style='color:red'>Cancelada</span>",
	"4"=>"<span style='color:blue'>Procesada</span>",
	"5"=>"<span style='color:black; font-weight:bold'>Guardada</span>"
	);
	return $repite[$valor];
}



function acentos($string){
	$conA=array("à","á","â","ã","ä","ç","è","é","ê","ë","ì","í","î","ï","ñ","ò","ó","ô","õ","ö","ù","ú","û","ü","ý","ÿ","À","Á","Â","Ã","Ä","Ç","È","É","Ê","Ë","Ì","Í","Î","Ï","Ñ","Ò","Ó","Ô","Õ","Ö","Ù","Ú","Û","Ü","Ý");
	$sinA=array("a","a","a","a","a","c","e","e","e","e","i","i","i","i","n","o","o","o","o","o","u","u","u","u","y","y","A","A","A","A","A","C","E","E","E","E","I","I","I","I","N","O","O","O","O","O","U","U","U","U","Y");
	
	return str_replace($conA,$sinA,$string);
}



function fecha_afilia(){
$da=date("d");
$mimes=date("m")+1;
$mimes1=date("m");
$mihora=date("H:i:s");	

if(($da<=25)&&($mimes1<=12)){return "NOW()";}
else if(($da>25)&&($mimes1==12)){
	$miano=date("Y")+1;
	$mifecha=$miano."-01-01 ".$mihora;
	$mifecha='"'.$mifecha.'"';
	return $mifecha;
	}
else if(($da>25)&&($mimes1<12)){
	$miano=date("Y");
	$mifecha=$miano."-".con_cero_fecha_mes($mimes)."-01 ".$mihora;
	$mifecha='"'.$mifecha.'"';
	return $mifecha;
	}
	
}


function dias($fecha_f)
{	$fecha_i=date("Y-m-d H:i:s");
	/*verificar la fecha mayor*/
	$fecha_inicio = new DateTime($fecha_i);
	$fecha_fin    = new DateTime($fecha_f);

	/*calcula el numero de dias*/
	$dias	= (strtotime($fecha_i)-strtotime($fecha_f))/86400;
	$dias 	= abs($dias); $dias = floor($dias);		
		if($fecha_inicio < $fecha_fin)
			return $dias;
		else{
			return $dias;
		}
}




function comprobar_fecha($fecha){

$fecha_inicio = new DateTime($fecha); //2017-12-28 09:00:00
$fecha_fin    = new DateTime(date("Y-m-d H:i:s")); //2017-12-28 09:03:00
if($fecha_fin < $fecha_inicio)
	return 1;
else
	return 0;
}




function comprobar_fecha2($fecha,$n){
$hora=substr($fecha,11,8);
$fecha=substr($fecha,0,10);
$fecha_inicio = new DateTime($fecha." ".sumar_hora2($hora,$n)); //2017-12-28 09:00:00
$fecha_fin    = new DateTime(date("Y-m-d H:i:s")); //2017-12-28 09:03:00
if($fecha_fin < $fecha_inicio)
	return 1;
else
	return 0;
}

function iniciar($valor){
	$inicia=array("0"=>"","1"=>"En la fecha","2"=>"1 Hora antes",
	"3"=>"3 Hora antes","4"=>"6 Hora antes","5"=>"12 Hora antes","6"=>"1 Dia antes",
	"7"=>"2 Dia antes","8"=>"3 Dia antes","9"=>"1 Semana antes");
	return $inicia[$valor];
}

function iniciarT($valor){/*los valores estan por dia*/
	$inicia=array("0"=>"","1"=>"0","2"=>"
	",	"3"=>"3","4"=>"6","5"=>"12",
	"6"=>"1",
	"7"=>"2","8"=>"3","9"=>"7");
	return $inicia[$valor];
}


function repetir($valor){
	$repite=array("0"=>"","1"=>"Fecha Especifica","2"=>"Cada dia",
	"3"=>"Dia Especifico","4"=>"Semana y Dia Especifico","5"=>"Cada Mes","6"=>"Cada A&ntilde;o");
	return $repite[$valor];
}

function repetirT($valor){/*los valores estan por horas*/
	$repite=array("0"=>"","1"=>"0.5","2"=>"1",
	"3"=>"6","4"=>"12","5"=>"24","6"=>"0");
	return $repite[$valor];
}

function estatus($valor){
	$repite=array("0"=>"<span style='color:black'>Desactivado</span>",
	"1"=>"<span style='color:green'>Activado</span>",
	"2"=>"<span style='color:orange'>Finalizada</span>",
	"3"=>"<span style='color:red'>Cancelada</span>");
	return $repite[$valor];
}

function masmenos($valor){
	if($valor<0){return "<span style='color:red'>".number_format($valor,2,",",".")."</span>";}
	else if($valor==0){return "<span style='color:brown'>".number_format($valor,2,",",".")."</span>";}
	else{return "<span style='color:green'>".number_format($valor,2,",",".")."</span>";}
	//return number_format($valor,2,",",".");
}



function espacio($cadena){
	
	$cadena=str_replace("_"," ",$cadena);
	return $cadena;
}

function restar_hora($fecha){

$nuevafecha = strtotime ( '+5 hour' , strtotime ( $fecha ) ) ;
//$nuevafecha = strtotime ( '-30 minute' , strtotime ( $fecha ) ) ;
//$nuevafecha = strtotime ( '+30 second' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'h:i:s' , $nuevafecha );
$nuevafecha = strtotime ( '-30 minute' , strtotime ( $nuevafecha ) ) ; 
$nuevafecha = date ( 'h:i:s' , $nuevafecha );

return $nuevafecha;
	
}



function MAXDEY($n,$m){
	if($n>$m){return $n;}
	else if($m>$n){return $m;}
	else {return $n;}
}


function eval_dey($valor){
	
	if(substr($valor,0,1)=="-"){$valor='<span style="font-weight:bold; color:red; text-shadow: 1px 2px 6px black;">'.$valor.'</span>';}
	
	return $valor;
}

function cambiar_hora_12($hora){
//0 1 2 3 4 5 6 7
//h h : m m : s s	

$h12= array("00"=>"12","01"=>"01","02"=>"02","03"=>"03","04"=>"04","05"=>"05","06"=>"06","07"=>"07","08"=>"08","09"=>"09","10"=>"10","11"=>"11","12"=>"12","13"=>"01","14"=>"02","15"=>"03","16"=>"04","17"=>"05","18"=>"06","19"=>"07","20"=>"08","21"=>"09","22"=>"10","23"=>"11");


$h=substr($hora,0,2);
$m=substr($hora,3,2);
$s=substr($hora,6,2);

if(($h=="12")||($h=="13")||($h=="14")||($h=="15")||($h=="16")||($h=="17")||($h=="18")||($h=="19")||($h=="20")||($h=="21")||($h=="22")||($h=="23")){
	$a="PM";
}else{$a="AM";}

$h=$h12[$h];
$hora=$h.":".$m.":".$s." ".$a;
return $hora;	
}

function cambiar_hora_24($hora){
//0 1 2 3 4 5 6 7
//h h : m m : s s	

$h24= array("12"=>"00","01"=>"13","02"=>"14","03"=>"15","04"=>"16","05"=>"17","06"=>"18","07"=>"19","08"=>"20","09"=>"21","10"=>"22","11"=>"23");


$h=substr($hora,0,2);
$m=substr($hora,3,2);
$s=substr($hora,6,2);

if(($h=="12")||($h=="13")||($h=="14")||($h=="15")||($h=="16")||($h=="17")||($h=="18")||($h=="19")||($h=="20")||($h=="21")||($h=="22")||($h=="23")){
	$a="PM";
}else{$a="AM";}

$h=$h24[$h];
$hora=$h.":".$m.":".$s." ".$a;
return $hora;	
}

function cambiar_fecha($fecha){

$d=substr($fecha,0,2);
$m=substr($fecha,3,2);
$a=substr($fecha,6,4);
$fecha=$a.'-'.$m.'-'.$d;

return $fecha;
	}
	
function cambiar_fecha_es($fecha){
$d=substr($fecha,8,2);
$m=substr($fecha,5,2);
$a=substr($fecha,0,4);
$fecha=$d.'/'.$m.'/'.$a;
return $fecha;
	}
	
function cambiar_fecha_es1($fecha){
$d=substr($fecha,8,2);
$m=substr($fecha,5,2);
$a=substr($fecha,0,4);
$fecha=$d.'-'.$m.'-'.$a;
return $fecha;
	}
	
function cambiar_fecha_es_hora($fecha){
$d=substr($fecha,8,2);
$m=substr($fecha,5,2);
$a=substr($fecha,0,4);
$hora=substr($fecha,11,8);


$fecha=$d.'/'.$m.'/'.$a;
return $fecha." ".$hora;
	}

function sumar_fecha($fecha,$nd){
$nuevafecha = strtotime ( '+'.$nd.' day' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-d' , $nuevafecha ); 
return $nuevafecha;	
	}


function restar_fecha($fecha,$nd){
$nuevafecha = strtotime ( '-'.$nd.' day' , strtotime ( $fecha ) ) ;
$nuevafecha = date ( 'Y-m-d' , $nuevafecha ); 
return $nuevafecha;	
	}	
	
	
function con_cero_fecha_mes($f){
$fc= Array("00"=>"",1=>"01",2=>"02",3=>"03",4=>"04",5=>"05",6=>"06",7=>"07",8=>"08",9=>"09",10=>"10",11=>"11",12=>"12");
return $fc[$f];
}
function sin_cero_fecha_mes($f){
$fc= Array("00"=>"","01"=>1,"02"=>2,"03"=>3,"04"=>4,"05"=>5,"06"=>6,"07"=>7,"08"=>8,"09"=>9,"10"=>10,"11"=>11,"12"=>12);
return $fc[$f];
}

function cambio_mes($mes,$n){
$cmeses = Array("00"=>"","1"=>"ENERO","2"=>"FEBRERO","3"=>"MARZO","4"=>"ABRIL","5"=>"MAYO","6"=>"JUNIO","7"=>"JULIO","8"=>"AGOSTO","9"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$meses = Array("00"=>"","01"=>"ENERO","02"=>"FEBRERO","03"=>"MARZO","04"=>"ABRIL","05"=>"MAYO","06"=>"JUNIO","07"=>"JULIO","08"=>"AGOSTO","09"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$nmeses = Array("00"=>"","ENERO"=>"01","FEBRERO"=>"02","MARZO"=>"03","ABRIL"=>"04","MAYO"=>"05","JUNIO"=>"06","JULIO"=>"07","AGOSTO"=>"08","SEPTIEMBRE"=>"09","OCTUBRE"=>"10","NOVIEMBRE"=>"11","DICIEMBRE"=>"12");
$mes=strtoupper($mes);

if($n==1){if($cmeses[$mes]!=""){return $cmeses[$mes];}else{return $mes;}}
if($n==2){if($meses[$mes]!=""){return $meses[$mes];}else{return $mes;}}
if($n==3){if($nmeses[$mes]!=""){return $nmeses[$mes];}else{return $mes;}}
else{return $mes;}
}


function periodo($fecha){
	$fecha=strtoupper($fecha);
	$miperiodo=Array(
	"ENERO"=>"'01' AND '01'","FEBRERO"=>"'02' AND '02'","MARZO"=>"'03' AND '03'",
	"ABRIL"=>"'04' AND '04'","MAYO"=>"'05' AND '05'","JUNIO"=>"'06' AND '06'",
	"JULIO"=>"'07' AND '07'","AGOSTO"=>"'08' AND '08'","SEPTIEMBRE"=>"'09' AND '09'",
	"OCTUBRE"=>"'10' AND '10'","NOVIEMBRE"=>"'11' AND '11'","DICIEMBRE"=>"'12' AND '12'",
		
	"TRIMESTRE 1"=>"'01' AND '03'",
	"TRIMESTRE 2"=>"'04' AND '06'",
	"TRIMESTRE 3"=>"'07' AND '09'",
	"TRIMESTRE 4"=>"'10' AND '12'",
	"SEMESTRE 1"=>"'01' AND '06'",
	"SEMESTRE 2"=>"'07' AND '12'",
	"ANUAL"=>"'01' AND '12'"
	);
	if($miperiodo[$fecha]!=""){return "BETWEEN ".$miperiodo[$fecha];}
	else{return $fecha;}
}



function periodo2($fecha){
	$fecha=strtoupper($fecha);
	$miperiodo=Array(
	"ENERO"=>"01","FEBRERO"=>"02","MARZO"=>"03",
	"ABRIL"=>"04","MAYO"=>"05","JUNIO"=>"06",
	"JULIO"=>"07","AGOSTO"=>"08","SEPTIEMBRE"=>"09",
	"OCTUBRE"=>"10","NOVIEMBRE"=>"11","DICIEMBRE"=>"12",
	"TRIMESTRE 1"=>"01-03","TRIMESTRE 2"=>"04-06","TRIMESTRE 3"=>"07-09","TRIMESTRE 4"=>"10-12",
	"SEMESTRE 1"=>"01-06","SEMESTRE 2"=>"07-12","ANUAL"=>"01-12");
	if($miperiodo[$fecha]!=""){return $miperiodo[$fecha];}
	else{return $fecha;}
}

function cero_fecha_mes($f){
if($f=="01"){$f=1;} if($f=="02"){$f=2;} if($f=="03"){$f=3;} if($f=="04"){$f=4;}
if($f=="05"){$f=5;} if($f=="06"){$f=6;} if($f=="07"){$f=7;} if($f=="08"){$f=8;} if($f=="09"){$f=9;}
return $f;
}	
	
	
	
function dia($dia){
	
	$dia = date('N',strtotime($dia));
	        $d= array(
       1=> 'lunes',
       2=> 'martes',
       3=> 'miercoles',
       4=> 'jueves',
       5=> 'viernes',
       6=> 'sabado',
       7=> 'domingo'
        );
	
	return $d[$dia];
}	
	
	
	
function semana($dia){
	
	$dia = date('N',strtotime($dia));
	        $d= array(
       1=> 'lunes',
       2=> 'martes',
       3=> 'miercoles',
       4=> 'jueves',
       5=> 'viernes',
       6=> 'sabado',
       7=> 'domingo'
        );
	
	return $d[$dia];
}	
	
	
	
	
function codigo($nc){
if(strlen($nc)==1)
$nc="00000".$nc;
else if(strlen($nc)==2)
$nc="0000".$nc;
else if(strlen($nc)==3)
$nc="000".$nc;
else if(strlen($nc)==4)
$nc="00".$nc;
else if(strlen($nc)==5)
$nc="0".$nc;
else if(strlen($nc)==6)
$nc=$nc;
return $nc;
}
	
function codigo_mil($nc){
if(strlen($nc)==1)
$nc="000".$nc;
else if(strlen($nc)==2)
$nc="00".$nc;
else if(strlen($nc)==3)
$nc="0".$nc;
else if(strlen($nc)==4)
$nc=$nc;
return $nc;
}	
	
	

function count_dias($fecha = "")
    {
    
    # Reseteo la matriz
        $d = array(
        'lunes'     =>  0,
        'martes'    =>  0,
        'miercoles' =>  0,
        'jueves'    =>  0,
        'viernes'   =>  0,
        'sabado'    =>  0,
        'domingo'   =>  0,
        );
        
        $dias_mes  = date('t',strtotime($fecha));
                            
                # Descompongo la fecha para poder tener manejo.     
                $f = explode("-",$fecha);
                    
                #Realizo un for hasta el ultimo dia de tu fecha para ir contando cada dia   
                for ($i=1; $i<=$f[2]; $i++){
                
$day = date('N',strtotime($f[0]."-".$f[1]."-".$i));
                    switch($day){
                
                        case 1:
                            
                            $d['lunes']++;
                        
                        break;
                        
                        case 2:
                            
                            $d['martes']++;
                        
                        break;
                        
                        case 3:
                            
                            $d['miercoles']++;
                        
                        break;      
                                    
                        case 4:
                            
                            $d['jueves']++;
                        
                        break;          
                                
                        case 5:
                            
                            $d['viernes']++;
                        
                        break;                  
 
                        case 6:
                            
                            $d['sabado']++;
                        
                        break;  
                        
                        case 7:
                            
                            $d['domingo']++;
                        
                        break;  
                        
                        default:
                                exit();
                        break;
                        
                    }           
                                                
                        }
                
                return $d;          
    }
 
$resultado = count_dias(date("Y")."-".date("m")."-".date("t"));
 
extract($resultado);

?>