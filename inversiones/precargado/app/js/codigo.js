/**/


function mostrar_codigos(opc){

if(opc==1){
	$("#btn_mostrar").html('<span onclick="return mostrar_codigos(0)" class="btn">Quiero solo uno <i class="fa fa-minus text-danger"></i></span>');
	$(".masmontos").show(500,"linear");
}

if(opc==0){
	$("#btn_mostrar").html('<span onclick="return mostrar_codigos(1)" class="btn">Agregar más códigos <i class="fa fa-plus text-success"></i></span>');
	$(".masmontos").hide(500,"linear");
	$("#monto2,#monto3,#monto4,#monto5").val("");
	$("#divisa2,#divisa3,#divisa4,#divisa5").val("");
}	
}


function pagar_codigo(id,ele){

var descripcion = $("#descripcion"+id).val();
swal({
  title: "¿Esta segur@ que desea cambiar la condición?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  	if(isConfirm) {
		$.post("precargado/app/php/proceso_codigo.php",{opc:6,id:id,estado:ele.innerHTML,descripcion:descripcion},function(resp){
			listar_codigos();	
			swal("","La condicion fue cambiada exitosamente.","success");
		},"json");
	}
	});

}



function buscar_contacto(nro){
if(nro.value.length<11){$("#correo").val(""); return false;}
	$.post("precargado/app/php/proceso_codigo.php",{opc:5,nro:nro.value},function(resp){	
	$("#correo").val(resp.listado);
	},"json");

}


function ver_puntos(){

	$.post("php/proceso.php",{opc:0},function(resp){
		
	$("#puntos").html('Portafolio de Inversiones &nbsp;<i class="fa fa-money"></i> '+resp.monto3);
	},"json");

}ver_puntos();


function enviar_cliente(opc){
	
	if(opc.checked==true){
	$("#telefono,#correo").attr({"disabled":false});
	}else{
	$("#telefono,#correo").attr({"disabled":true});
	$("#telefono,#correo").val("");
	}
	$("#telefono").focus();
}

function generar_codigo(){
//var divisa=$("#divisa").val();
var valdivisa1=$("#divisa1 option:selected").attr('val_divisa');
var valdivisa2=$("#divisa2 option:selected").attr('val_divisa');
var valdivisa3=$("#divisa3 option:selected").attr('val_divisa');
var valdivisa4=$("#divisa4 option:selected").attr('val_divisa');
var valdivisa5=$("#divisa5 option:selected").attr('val_divisa');

var monto1=$("#monto1").val();
var monto2=$("#monto2").val();
var monto3=$("#monto3").val();
var monto4=$("#monto4").val();
var monto5=$("#monto5").val();

var divisa1=$("#divisa1").val();
var divisa2=$("#divisa2").val();
var divisa3=$("#divisa3").val();
var divisa4=$("#divisa4").val();
var divisa5=$("#divisa5").val();

var descripcion1=$("#descripcion1").val();
var descripcion2=$("#descripcion2").val();
var descripcion3=$("#descripcion3").val();
var descripcion4=$("#descripcion4").val();
var descripcion5=$("#descripcion5").val();

var estatus1=$("#estatus1").val();
var estatus2=$("#estatus2").val();
var estatus3=$("#estatus3").val();
var estatus4=$("#estatus4").val();
var estatus5=$("#estatus5").val();


var tel=$("#telefono").val();
var correo=$("#correo").val();
var enviar=$("#enviar:checked").val();
var resumen='';

if(divisa1==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda para generar el codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa1").focus(); },3000); return false;
}

if(monto1<=0){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto del codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto1").focus(); },3000); return false;
}
resumen+='Monto: '+number_format(monto1,2,",",".")+" "+valdivisa1+"\n";

if(monto2.length>0){
	if(divisa2==""){
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda para generar el codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa2").focus(); },3000); return false;
	}
	if(monto2<=0){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto del codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto2").focus(); },3000); return false;
	}
	resumen+='Monto: '+number_format(monto2,2,",",".")+" "+valdivisa2+"\n";
}else{ monto2=0; }


if(monto3.length>0){
	if(divisa3==""){
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda para generar el codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa3").focus(); },3000); return false;
	}
	if(monto3<=0){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto del codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto3").focus(); },3000); return false;
	}
	resumen+='Monto: '+number_format(monto3,2,",",".")+" "+valdivisa3+"\n";
}else{ monto3=0; }



if(monto4.length>0){
	if(divisa4==""){
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda para generar el codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa4").focus(); },3000); return false;
	}
	if(monto4<=0){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto del codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto4").focus(); },3000); return false;
	}
	resumen+='Monto: '+number_format(monto4,2,",",".")+" "+valdivisa4+"\n";
}else{ monto4=0; }



if(monto5.length>0){
	if(divisa5==""){
	swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de moneda para generar el codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#divisa5").focus(); },3000); return false;
	}
	if(monto5<=0){
	swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto del codigo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto5").focus(); },3000); return false;
	}
	resumen+='Monto: '+number_format(monto5,2,",",".")+" "+valdivisa5+"\n";
}else{ monto5=0; }





if(enviar=="on"){
	
if(tel.length<11){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero telefonico del cliente", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#telefono").focus(); },3000); return false;
}


if(correo.length>0){
	var validarEmail=validateEmail(correo);
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(pedroperez@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus();  },3000); return false;	
	}
	
	
if(correo.length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}}
resumen+='Enviado a '+tel+'  '+correo;
}



var mensaje = 'RESUMEN:\n'+resumen;

swal({
  title: "",
  text: "¿Esta segur@ que desea generar el codigo precargado?\n"+mensaje,
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: true,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {
	  $("#btn_mostrar").hide();
		$("#bgenerar").html('<i class="fa fa-refresh imgr"></i> Generando el codigo...');
	$.post("precargado/app/php/proceso_codigo.php",{opc:2, 
		monto1:monto1, monto2:monto2, monto3:monto3, monto4:monto4, monto5:monto5, 
		descripcion1:descripcion1, descripcion2:descripcion2, descripcion3:descripcion3, descripcion4:descripcion4, descripcion5:descripcion5, 
		valdivisa1:valdivisa1, valdivisa2:valdivisa2, valdivisa3:valdivisa3, valdivisa4:valdivisa4, valdivisa5:valdivisa5, 
		divisa1:divisa1, divisa2:divisa2, divisa3:divisa3, divisa4:divisa4, divisa5:divisa5, 
		estatus1:estatus1, estatus2:estatus2, estatus3:estatus3, estatus4:estatus4, estatus5:estatus5, 
		tel:tel, correo:correo},function(resp){
			if(resp.salida==1){	
			
setTimeout(function(){
			swal("",resp.mensaje,"success");
			$("#btn_mostrar").show();
			$("#bgenerar").attr({"disabled":false});
			$("#bgenerar").html('<i class="fa fa-refresh"></i> Generar Codigo');
	},1000);		
			ver_puntos();
			lista_divisa_monto();
			parent.recarga_cofreBTN();
	if(enviar=="on"){
	$.post("precargado/app/php/proceso_codigo.php",{opc:3},function(resp1){},"json");	
	}		
			
			}else{
			setTimeout(function(){
			swal("",resp.mensaje,"error"); 
			$("#btn_mostrar").show();
			$("#bgenerar").attr({"disabled":false});
			$("#bgenerar").html('<i class="fa fa-refresh"></i> Generar Codigo');			
			},1000);

			}
			
	},"json");
}else{	$("#bgenerar").attr({"disabled":false});}
});
}

function lista_divisa_monto(){
	$.post("php/proceso.php",{opc:14},function(resp){
		$("#divisa1,#divisa2,#divisa3,#divisa4,#divisa5").html(resp.lista_moneda3);
	},"json");
}


function listar_codigos(){
	
	$.post("precargado/app/php/proceso_codigo.php",{opc:4},function(resp){
	if(resp.salida==1){
		$("#listado_cod").html(resp.listado);
		}
	else{
		$("#listado_cod").html(resp.listado);
	}
	},"json");	

}