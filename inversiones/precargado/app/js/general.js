$(document).ready(function(){
$("#salir").click(function(){

swal({
  title: "¿Esta segur@ que desea salir del sistema?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {	
	
	location.href="php/salir.php";	
	
  }
  
});	


});
});

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}



function delete_calc(){
	var num=$("#calc").val();
	var lg=num.length;
    var salida=num.substring(0,lg-1);
	$("#calc").val(salida);
}

var nav4 = window.Event ? true : false;
function micalc(n,evt){

	if(n.innerHTML=='C'){
		$("#calc").val("");
	}
	else if(n.innerHTML=='='){
		$("#calc").val(eval($("#calc").val()));
	} 
	
	else{
	$("#calc").val($("#calc").val()+n.innerHTML);
	}

}




function micalc_enter(evt){
	var key = nav4 ? evt.which : evt.keyCode;	
	if (evt.keyCode == 13)
    {
	$("#calc").val(eval($("#calc").val()));	

	}

}




function ocultar(opc){
	if(opc==0){
		$("#ocultar_panel").attr({"onclick":"return ocultar(1);"});
		$("#ocultar_panel").attr({"title":"Visualizar panel superior"});
		$("#ocultar_panel").html("<i class='fa fa-angle-double-down' style='font-size:16px'></i>");
		
		$("#cabecera").hide(1000,"linear");
	}else{
		$("#ocultar_panel").attr({"onclick":"return ocultar(0);"});
		$("#ocultar_panel").attr({"title":"Ocultar panel superior"});
		$("#ocultar_panel").html("<i class='fa fa-angle-double-up' style='font-size:16px'></i>");
		$("#cabecera").show(1000,"linear");
	}
}



/*RELOJ*/
function cambiar_hora(h,m,s){
	var hr = Array("12","01","02","03","04","05","06","07","08","09","10","11","12","01","02","03","04","05","06","07","08","09","10","11","12");
	var turno;
	if(parseInt(h)>12){turno="Pm";}else{turno="Am";}
	if(parseInt(m)<10){
		var ms = Array("00","01","02","03","04","05","06","07","08","09");
		m=ms[m];
	}
	if(parseInt(s)<10){
		var ms = Array("00","01","02","03","04","05","06","07","08","09");
		s=ms[s];
	}
	
	var hora=hr[h]+":"+m+":"+s+" "+turno;
	return hora;
}
function reloj(){
    var fecha = new Date();
    var segundos = fecha.getSeconds();
    var minutos = fecha.getMinutes();
    var horas = fecha.getHours();
	
	var hora=cambiar_hora(horas,minutos,segundos);
	$("#reloj").val(hora);
	
	
}setInterval(reloj,1000);







/*BOTONES DE GESTION ()*/

function modulo(url,titulo,mv){
	$("#cuerpo").html('<img src="images/cargar6.gif" width="48">');
	$("#atras").show();
	
	$("#tcuerpo").html(titulo);
	$("#cuerpo").load(url);

	if((url=="pag/usuarios.php")&&(mv==1)){
	setTimeout(function(){modulo_usuario('pag/registrados.php');},1500);
	}
	else if((url=="pag/usuarios.php")&&(mv==2)){
	setTimeout(function(){modulo_usuario('pag/editar.php');},1500);
	}

}





/*MODULO USUARIO*/

function modulo_usuario(opc){
	$("#formu").load(opc);
	if(opc=="pag/editar.php"){
	setTimeout(function(){listar_usuario();},1500);
	}
	else if(opc=="pag/registrados.php"){
	setTimeout(function(){registrados();},1500);
	}
}

/*CAMBIAR CLAVE*/
function cambiar_clave(){
	var cv=$("#clave");
	var ccv=$("#cclave");
	
if(cv.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "La contraseña debe estar entre 6 y 16 caracteres", timer: 2000,  showConfirmButton: false});setTimeout(function(){cv.focus(); },3000); return false;
}
	
	
if(ccv.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "La confirmacion de la contraseña debe estar entre 6 y 16 caracteres", timer: 2000,  showConfirmButton: false});setTimeout(function(){ccv.focus(); },3000); return false;
}
	
if(cv.val()!=ccv.val()){
swal({title: "COMPLETAR INFORMACION", text: "Las contraseñas no coinciden, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){ccv.focus(); },3000); return false;
}

swal({
  title: "¿Esta segur@ que desea cambiar la contraseña?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {
	$.post("php/proceso_usuario.php",{opc:1, cv:cv.val()},function(resp){
		if(resp.salida==1){
			swal("NOTIFICACIÓN",resp.mensaje,"success");
		}else{
			swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");
}
});

}



/*visualizar datos del usuario*/
function listar_usuario(){

	$.post("php/proceso_usuario.php",{opc:3},function(resp){
		if(resp.salida==1){
			
			$("#nombreu").val(resp.datos[1]);
			$("#correo").val(resp.datos[7]);
			$("#user").val(resp.datos[2]);
			$("#pregs").val(resp.datos[5]);
			$("#resps").val(resp.datos[6]);
			$("#cedula").val(resp.datos[9]);
			$("#telcel").val(resp.datos[10]);
			document.getElementById("priv").value=resp.datos[4];
			
		}else{
			swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");

}



function gestion_registrados(id,n,b){

swal({
  title: "Clave de Seguridad",
  text: "Debe autenticarse antes de realizar la operación",
  type: "input",
  inputType: "password",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Introduzca la clave de seguridad"
},
function(inputValue){
  if (inputValue === false) return false;
  
  if (inputValue === "") {
    swal.showInputError("Debe agregar la clave de seguridad antes de modificar los datos.");
    return false
  }
  
  
	$.post("php/proceso_usuario.php",{opc:6,id:id,n:n,b:b,clave:inputValue},function(resp){
		if(resp.salida==1){
			if(resp.rg==1){registrados();}
			swal("NOTIFICACIÓN",resp.mensaje,"success");
		}else{
			swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");
  
});
}




/*visualizar datos del usuario registrados*/
function registrados(){

	$.post("php/proceso_usuario.php",{opc:5},function(resp){
		if(resp.salida==1){
			
			$("#listado_usuario").html(resp.listado);

		}else{
			$("#listado_usuario").html(resp.listado);
			//swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");

}


/*CAMBIAR usuario*/
function editar_usuario(){
		var nom=$("#nombreu");
		var ced=$("#cedula");
		var correo=$("#correo");
		var priv=$("#priv");
		var us=$("#user");
		var preg=$("#pregs");
		var resp=$("#resps");
		var telcel=$("#telcel");
	
if(nom.val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre y apellido del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){nom.focus(); },3000); return false;
}

if(ced.val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la cedula de identidad del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){ced.focus(); },3000); return false;
}

if(telcel.val().length<11){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){telcel.focus(); },3000); return false;
}


if(correo.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;
}



if(correo.val().length>0){
	var validarEmail=validateEmail(correo.val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(lotto@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;	
	}
	
	
if(correo.val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;
}}

	
if(us.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre de usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){us.focus(); },3000); return false;
}	

	
if(priv.val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el privilegio del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){priv.focus(); },3000); return false;
}	
	
if(preg.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la pregunta de seguridad", timer: 2000,  showConfirmButton: false});setTimeout(function(){preg.focus(); },3000); return false;
}
	
if(resp.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la respuesta de seguridad", timer: 2000,  showConfirmButton: false});setTimeout(function(){resp.focus(); },3000); return false;
}

swal({
  title: "¿Esta segur@ que desea editar los datos?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	$.post("php/proceso_usuario.php",{opc:4, nom:nom.val(), correo:correo.val(), priv:priv.val(), us:us.val(), preg:preg.val(), resp:resp.val(), cedula:ced.val(), telcel:telcel.val()},function(resp){
		if(resp.salida==1){
			swal("NOTIFICACIÓN",resp.mensaje,"success");
		}else{
			swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");
}
});

}



/*agregar usuarios*/
function agregar_usuario(){
		var nom=$("#nombreu");
		var ced=$("#cedula");
		var correo=$("#correo");
		var us=$("#user");
		var cv=$("#clave");
		var ccv=$("#cclave");
		var preg=$("#pregs");
		var resp=$("#resps");
		var priv=$("#priv");
		var telcel=$("#telcel");
	
if(nom.val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre y apellido del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){nom.focus(); },3000); return false;
}
	
if(ced.val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la cedula de identidad del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){ced.focus(); },3000); return false;
}
	
if(telcel.val().length<11){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero telefonico del usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){telcel.focus(); },3000); return false;
}

if(correo.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;
}



if(correo.val().length>0){
	var validarEmail=validateEmail(correo.val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(lotto@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;	
	}
	
	
if(correo.val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){correo.focus(); },3000); return false;
}}

	
	
if(us.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre de usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){us.focus(); },3000); return false;
}	

if(cv.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "La contraseña debe estar entre 6 y 16 caracteres", timer: 2000,  showConfirmButton: false});setTimeout(function(){cv.focus(); },3000); return false;
}
	
	
if(ccv.val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "La confirmacion de la contraseña debe estar entre 6 y 16 caracteres", timer: 2000,  showConfirmButton: false});setTimeout(function(){ccv.focus(); },3000); return false;
}
	
if(cv.val()!=ccv.val()){
swal({title: "COMPLETAR INFORMACION", text: "Las contraseñas no coinciden, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){ccv.focus(); },3000); return false;
}


	
if(preg.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la pregunta de seguridad", timer: 2000,  showConfirmButton: false});setTimeout(function(){preg.focus(); },3000); return false;
}
	
if(resp.val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar la respuesta de seguridad", timer: 2000,  showConfirmButton: false});setTimeout(function(){resp.focus(); },3000); return false;
}

swal({
  title: "¿Esta segur@ que desea agregar un nuevo usuario para administrar el sistema?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	$.post("php/proceso_usuario.php",{opc:2, cv:cv.val(), priv:priv.val(), nom:nom.val(), correo:correo.val(), us:us.val(), preg:preg.val(), resp:resp.val(), cedula:ced.val(), telcel:telcel.val()},function(resp){
		if(resp.salida==1){
			document.getElementById("formulario").reset();
			swal("NOTIFICACIÓN",resp.mensaje,"success");
		}else{
			swal("NOTIFICACIÓN",resp.mensaje,"error");
		}
	},"json");
}
});

}



