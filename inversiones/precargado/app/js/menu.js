function imprimir_ventana(){
	$("#principal, .btn, hr").hide();
	$("#cortina_blanca").show();
	dimensionar2();
	print();
	
		$("#cortina_blanca").hide();
		$("#principal, .btn, hr").show();
		dimensionar();
	
}


function gestion(url){
	$("#cuerpo_codigo").html('<img src="../images/cargar6.gif" width="48">');
	$("#cuerpo_codigo").load(url);
	$("#atras").show();
	setTimeout(function(){$("#buscar").focus();},1000);
	
}

function atras(url){
	$("#atras").hide();
	$("#cuerpo").load(url);
}


$(window).resize(function() {
if(window.innerWidth<=768){$("#ventana_modal").css({"width":"94%","left":"1%"});}
else{$("#ventana_modal").css({"width":"50%","left":"25%"});}	
});

function dimensionar(){
if(window.innerWidth<=768){$("#ventana_modal").css({"width":"94%","left":"1%"});}
else{$("#ventana_modal").css({"width":"50%","left":"25%"});}
}

function dimensionar1(){
$("#ventana_modal").css({"width":"80%","left":"10%"});
}

function dimensionar2(){
$("#ventana_modal").css({"width":"98%","left":"0%"});
}
function modulo_modal(url){
	dimensionar();
	$("#cuerpo_modal").load(url);
}
function modulo_modal_print(url){
	
	$("#cuerpo_modal").load(url);
setTimeout(function(){
	imprimir_ventana(); 
$("#myModal").hide();
},2000);

	
}

/*MODULO CLIENTES*/


function buscar_clientes(){
	var buscar=$("#buscar").val();
	
	if(valcam("#buscar",4,"Información Incompleta","Debe agregar al menos 4 digitos",3000)==false){return false;}
	
	
	$.post("php/proceso_cliente.php",{opc:1, cd:buscar},function(resp){
		if(resp.salida==1){
			//swal(resp.mensaje);
			$("#form-cuenta-cliente").show();
			$("#form-cuenta-cliente").html(resp.listado);
			$("#form-cliente").hide();
			}
		else{
		document.getElementById("formulario").reset();
		swal(resp.mensaje);
		$("#cedula").val(buscar);
		$("#form-cliente").show();
		$("#form-cuenta-cliente").hide();
		}
	},"json");
	
}


function cargar_clientes(cd){
	$("#buscar").val(cd);
	$("#myModal").hide();
	buscar_clientes();
}



function directorio_cliente(){
	
	$.post("php/proceso_cliente.php",{opc:0},function(resp){
		if(resp.salida==1){
			$("#lista_cliente").html(resp.listado);
			}
		else{
			$("#lista_cliente").html(resp.listado);
		}
	},"json");
	
}



function letras_directorio(v){
$("#lista_cliente").html("<br><br><br><center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");

	$.post("php/proceso_cliente.php",{opc:0, letra:v},function(resp){
		if(resp.salida==1){
			$("#lista_cliente").html(resp.listado);
			}
		else{
			$("#lista_cliente").html(resp.listado);
		}
	},"json");

	
}


function agregar_cliente(){
	var c1=$("#nombre").val();
	var c2=$("#apellido").val();
	var c3=$("#cedula").val();
	var c4=$("#sexo").val();
	var c5=$("#telefono").val();
	var c6=$("#direccion").val();

	if(valcam("#nombre",3,"Información Incompleta","Debe agregar el nombre del cliente, minimo 3 caracteres",3000)==false){return false;}
	
	if(valcam("#apellido",3,"Información Incompleta","Debe agregar el apellido del cliente, minimo 3 caracteres",3000)==false){return false;}
	
	if(valcam("#cedula",3,"Información Incompleta","Debe agregar el numero de cedula del cliente, minimo 5 caracteres",3000)==false){return false;}
	
	if(valcam("#sexo",0,"Información Incompleta","Debe seleccionar el sexo del cliente",3000)==false){return false;}
	
	if(valcam("#telefono",11,"Información Incompleta","Debe agregar los 11 digitos de telefono del cliente",3000)==false){return false;}
	
	if(c6.length>0){
	if(valcam("#direccion",5,"Información Incompleta","Debe agregar la direccion de habitacion del cliente, minimo 5 caracteres",3000)==false){return false;}
	}

	
	$.post("php/proceso_cliente.php",{opc:2, c1:c1, c2:c2, c3:c3, c4:c4, c5:c5,c6:c6},function(resp){
		if(resp.salida==1){swal("",resp.mensaje,"success");
		$("#buscar").val(c3);
		buscar_clientes();
		}
		else{swal("",resp.mensaje,"error");}
	},"json");
	
}


function agregar_cuenta(){
	var c1=$("#banco").val();
	var c2=$("#tipoc").val();
	var c3=$("#ncuenta").val();
	var c4=$("#posee").val();
	var c5=$("#cititular").val();
	var c6=$("#titular").val();
	var c7=$("#telefonoc").val();
	var c8=$("#correotitular").val();
	var cd=$("#ct3").text();
	cd=cd.replace(".","");
	cd=cd.replace(".","");
	
	
	if(valcam("#banco",0,"Información Incompleta","Debe seleccionar la entidad bancaria",3000)==false){return false;}	
	
	if(valcam("#tipoc",0,"Información Incompleta","Debe seleccionar el tipo de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#ncuenta",20,"Información Incompleta","Debe agregar el numero de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#posee",0,"Información Incompleta","Debe seleccionar si posee cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#cititular",5,"Información Incompleta","Debe agregar la cedula del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#titular",6,"Información Incompleta","Debe agregar el nombre y apellido del titular de la cuenta bancaria",3000)==false){return false;}	
	

	if(valcam("#telefonoc",11,"Información Incompleta","Debe agregar el numero de telefono del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#correotitular","@","Información Incompleta","Debe agregar el correo electronico del titular, ej: pedroperez@hotmail.com",2000)==false){return false;}	
	
	
	swal({
  title: "¿Esta segur@ que desea agregar la cuenta?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	
	$.post("php/proceso_cliente.php",{opc:3, c1:c1, c2:c2, c3:c3, c4:c4, c5:c5 ,c6:c6 ,c7:c7 ,c8:c8},function(resp){
		if(resp.salida==1){swal("",resp.mensaje,"success");
		$("#buscar").val(cd);
		buscar_clientes();
		$("#myModal").hide();
		}
		else{swal("",resp.mensaje,"error");}
	},"json");
	
}});
}




function mimonto(){
$("#monto").focus();	
}



function vista_transferencia(){
var info=$("#cuentas").val();
var monto=$("#monto").val();
var motivo=$("#motivo").val();


	if(valcam("#cuentas",0,"Información Incompleta","Debe seleccionar la cuenta bancaria a transferir",3000)==false){return false;}	

	if(valcam("#monto",3,"Información Incompleta","Debe agregar el monto a transferir",3000)==false){return false;}	
	
	if(motivo.length>0){
	if(valcam("#motivo",3,"Información Incompleta","Debe agregar el motivo de la transferencia, minimo 3 caracteres",3000)==false){return false;}	
	}

$("#btn-oculto-modal").click();
info=info.split("/");
$("#cuerpo_modal").html("<center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");
var ncuenta=info[3].substring(0,4)+'-'+info[3].substring(4,8)+'-'+info[3].substring(8,10)+'-'+info[3].substring(10,20);



var vista='<table width="100%" border="0" class="table table-striped table-info"><tr><td colspan="2"><center><h3><b>VISTA DE CONFIRMACIÓN</b></h3></center></td></tr><tr><td><b>Titular:</b> </td><td>'+info[4]+'</td></tr>'+
'<tr><td><b>Cedula:</b> </td><td>'+info[5]+'</td></tr>'+
'<tr><td><b>Banco:</b></td><td> '+info[1]+'</td></tr>'+
'<tr><td><b>N&deg; Cuenta:</b></td><td> '+ncuenta+'</td></tr>'+
'<tr><td><b>Tipo:</b> </td><td>'+info[2]+'</td></tr>'+
'<tr><td><b>Monto:</b> </td><td><span style="color:green"><b>'+number_format(monto,2,",",".")+'</b></span> bs.</td></tr>'+
'<tr><td><b>Motivo:</b> </td><td>'+motivo+'</td></tr>'+
'<tr><td colspan="2"><center><br><code>¿Esta segur@ que desea realizar la transferencia?</code><hr><button id="confir" onclick="return enviar_transferencia()" class="btn btn-md btn-success"><i class="fa fa-send"></i> Confirmar envio</button></center></td></tr></table>';
	
dimensionar();

setTimeout(function(){
$("#cuerpo_modal").html(vista);
},1000);
}



function enviar_transferencia(){
	
	
	$("#confir").attr({"disabled":true});
	$("#confir").html('<i class="fa fa-spinner imgr"></i> Espere');
	
	var c1=$("#cuentas").val();
	var c2=$("#monto").val();
	var c3=$("#motivo").val();
	var cd=$("#ct3").text();
	cd=cd.replace(".","");
	cd=cd.replace(".","");

	
swal({
  title: "¿Esta segur@ que desea enviar la transferencia?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {	
	
	$.post("php/proceso_cliente.php",{opc:4, c1:c1, c2:c2, c3:c3},function(resp){
		if(resp.salida==1){
		//swal("",resp.mensaje,"success");
	swal({
  title: "ENVIO DE DATOS DE TRANSFERENCIA",
  text: resp.mensaje+" <a style='color:#fff' class='btn btn-success' href='#' onclick=gestion('pag/transferencias/index.php'),swal.close();>Procesar transferencia<a><hr>",
  html: true,
  showConfirmButton: false,
  cancelButtonText:"No, realizar otro envio",
  showCancelButton: true
});
	
		$("#buscar").val(cd);
		buscar_clientes();
		$("#myModal").hide();
		$("#confir").attr({"disabled":false});
		$("#confir").html('<i class="fa fa-send"></i> Confirmar envio');
		}
		else{swal("",resp.mensaje,"error");
		$("#confir").attr({"disabled":false});
		$("#confir").html('<i class="fa fa-send"></i> Confirmar envio');
		}
	},"json");

	
  }else{
	$("#confir").attr({"disabled":false});
	$("#confir").html('<i class="fa fa-send"></i> Confirmar envio');
  }
  
});	
	
	

	
}




function datostitular(opc){
	
	if(opc.value=="Si"){
	var cd=$("#ct3").text();
	cd=cd.replace(".","");
	cd=cd.replace(".","");
		
	$("#titular, #cititular").attr({"readOnly":true});
	$("#titular").val($("#ct1").text()+" "+$("#ct2").text());
	$("#cititular").val(cd);
	$("#telefonoc").val($("#ct5").text());
	}else{
	$("#titular, #cititular").attr({"readOnly":false});
	$("#titular").val("");
	$("#cititular").val("");
	$("#telefonoc").val("");
	}
	
	
}




/*MODULO TRANSACCIONES*/

function transferencias_sin_procesar(){
	
	$.post("php/proceso_transferencia.php",{opc:1},function(resp){
		if(resp.salida==1){

			$("#notifica").html(resp.cantidad);
			$("#notifica").attr({"title":"Nro. de transferencias sin ejecutar"});
			$("#notifica").show();
			
			$("#mtm").html(resp.mtm);
			$("#mtd").html(resp.mtd);
			$("#met").html(resp.met);
			$("#cet").html(resp.cantidad);
			$("#cem").html(resp.cantidadm);
			$("#ced").html(resp.cantidadd);
			
	if(resp.cantidad==0){
		$("#notifica").hide();
	}
			
			}
		else{
		$("#notifica").hide();
		}
	},"json");
	
}

function enviar_texto(){

	$.post("php/proceso_transferencia.php",{opc:8},function(resp){	},"json");
	
}


function listar_ultimas_trans(){

	$.post("php/proceso_transferencia.php",{opc:2},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html(resp.listado);
			}
		else{
		$("#listado_transferencias").html(resp.listado);
		}
	},"json");
	
}




function listar_ultimas_trans_cliente(){

	$.post("php/proceso_transferencia.php",{opc:6},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html(resp.listado);
			}
		else{
		$("#listado_transferencias").html(resp.listado);
		}
	},"json");
	
}


function buscar_trans(){

var fi=$("#fechai").val();
var ff=$("#fechaf").val();
var estatus=$("#estatus").val();

	$.post("php/proceso_transferencia.php",{opc:2, fi:fi, ff:ff, estatus:estatus},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html(resp.listado);
			}
		else{
		$("#listado_transferencias").html(resp.listado);
		}
	},"json");
	
}




function mostrar_cliente_opera(){

$("#cuerpo_modal").html("<center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");


var vista='<form onsubmit="return false"><table width="100%" border="0" class="table table-striped table-info"><tr><td colspan="2"><center><h3><b>BUSCAR TRANSFERENCIA</b></h3></center></td></tr>'+
'<tr><td><b>Buscar:</b> </td><td><input placeholder="Buscar por: Cedula, Nº operacion, Nº transferencia" title="Buscar por: Cedula, Nº operacion, Nº transferencia" type="text" class="form-control" id="nopera"></td></tr>'+
'<tr><td colspan="2"><center><button id="" onclick="return buscar_cliente_opera()" class="btn btn-md btn-success"><i class="fa fa-search"></i> Buscar</button></center></td></tr></table></form>';
	
dimensionar();

setTimeout(function(){
$("#cuerpo_modal").html(vista);
$("#nopera").focus();
},1000);

}


function buscar_cliente_opera(){
var buscar=$("#nopera").val();
	

	$.post("php/proceso_transferencia.php",{opc:6, buscar:buscar},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html("<br><br><center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");
			setTimeout(function(){
		$("#listado_transferencias").html(resp.listado);
		},1000);
		$("#myModal").hide();
			}
		else{
		swal("","Información no encontrada, intente otra opcion de busqueda.","error");
		}
	},"json");

 }




function mostrar_gestion_trans(id){
$("#cuerpo_modal").html("<center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");	
dimensionar();

	$.post("php/proceso_transferencia.php",{opc:3, id:id},function(resp){
		if(resp.salida==1){
		$("#cuerpo_modal").html(resp.listado);
			}
		else{
		$("#cuerpo_modal").html(resp.listado);
		}
	},"json");

 }



function mostrar_cliente(id){
$("#cuerpo_modal").html("<center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");	
dimensionar();

	$.post("php/proceso_transferencia.php",{opc:7, id:id},function(resp){
		if(resp.salida==1){
		$("#cuerpo_modal").html(resp.listado);
			}
		else{
		$("#cuerpo_modal").html(resp.listado);
		}
	},"json");

 }



function confirmar_transferencia(id){

var fecha=$("#fecha_realizo").val();
var referencia=$("#nreferencia").val();
var id_cta=$("#quebanco").val();
var monto=$("#montot").val();

	

	if(valcam("#quebanco",0,"Información Incompleta","Debe seleccionar el banco donde realizo la transferencia",3000)==false){return false;}	
	
	if(valcam("#fecha_realizo",10,"Información Incompleta","Debe agregar la fecha de realizacion de la transferencia en el banco seleccionado, minimo 3 caracteres",3000)==false){return false;}
	
	if(valcam("#nreferencia",4,"Información Incompleta","Debe agregar la referencia que arrojo la transferencia bancaria",3000)==false){return false;}	
	

$("#btn-oculto-modal").click();


id_cta=id_cta.split("/");

if(parseFloat(monto)>parseFloat(id_cta[1])){

}



var archivos = document.getElementById("soporte");

if(archivos.value!=""){
		var archivos1 = document.getElementById("soporte1");
		var archivo = archivos.files; 
		var archivos = new FormData();
 var extension = (archivos1.value.substring(archivos1.value.lastIndexOf("."))).toLowerCase();	
	if((extension!='.png')&&(extension!='.gif')&&(extension!='.jpg')&&(extension!='.jpeg')&&(extension!='.bmp')){
swal({title: "IMAGEN DEL SOPORTE", text:"El archivo no es valido, verifique y elija un archivo tipo imagen. (JPG, JPEG, PNG, GIF, BMP)" , timer: 4000,  showConfirmButton: true});
			/*$("#bguardar").attr({"disabled":false});
			$("#bguardar").html('<i class="fa fa-save"></i> Reportar');*/
	return false;
}

}



swal({
  title: "¿Esta segur@ que desea realizar la transferencia?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: true,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {	
	
  $("#confir").attr({"disabled":true});
	$("#confir").html("<i class='fa fa-spinner imgr'></i> Por favor espere...");
	
	
	$.post("php/proceso_transferencia.php",{opc:4, id:id, id_cta:id_cta[0], fecha:fecha, referencia:referencia},function(resp){
		if(resp.salida==1){
			//swal("",resp.mensaje,"success");
			
			
			if($("#soporte").val()!=""){
				subir_soporte(resp.mensaje);
				}
			else{swal("",resp.mensaje,"success");
			buscar_trans();
			transferencias_sin_procesar();
			$("#myModal").hide();
			}
			}
		else{
			swal("",resp.mensaje,"error");
		}
	},"json");
	
  }
  
});	


 }


function subir_soporte(mensaje) {
	


	
		var archivos = document.getElementById("soporte");
		
		var archivo = archivos.files; 
		var archivos = new FormData();

		archivos.append('soporte',archivo[0]);

		/*Ejecutamos la función ajax de jQuery*/		
		$.ajax({
			url:'php/subir_soporte.php', 
			type:'POST',
			contentType:false,
			data:archivos, 
			processData:false, 
			cache:false 
		}).done(function(msn){
			swal("",mensaje,"success");
			buscar_trans();
			transferencias_sin_procesar();
			$("#myModal").hide();
			enviar_texto();
		});

		
		
		
		
}
 
 
 
function borrar_transferencia(id){


swal({
  title: "¿Esta segur@ que desea borrar la transferencia?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){
	
  if (isConfirm) {	
	
	$.post("php/proceso_transferencia.php",{opc:5, id:id},function(resp){
		if(resp.salida==1){
			swal("",resp.mensaje,"success");
			$("#myModal").hide();
			buscar_trans();
			}
		else{
			swal("",resp.mensaje,"error");
		}
	},"json");
	
  }
  
});	



 }
 

 
function listar_cuentas_admin(){

$("#listado_cuentas").html('<br><br><br><i class="fa fa-home" style="font-size:80px; color:grey"></i><br><i class="fa fa-spinner imgr"></i> Buscando información de cuentas bancarias');


	$.post("php/proceso_cuentas.php",{opc:1},function(resp){
		if(resp.salida==1){
		$("#listado_cuentas").html(resp.listado);
			}
		else{
		$("#listado_cuentas").html(resp.listado);
		}
	},"json");

	
}
 
 

function mostrar_modal(pag){
$("#cuerpo_modal").html("<center><img src='../images/cargar6.gif'><br>Espere por favor.</center>");

dimensionar();

setTimeout(function(){
$("#cuerpo_modal").load(pag);
$("#nopera").focus();
},1000);

}


function agregar_quitar(n){

var saldo=$("#saldo").val();
var nsaldo=$("#nsaldo").text();
var id=$("#id_cta").val();
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(".","");
nsaldo=nsaldo.replace(",",".");



if(eval(saldo)==false){swal("","Debe agregar solo numeros.","error");
$("#saldo").focus();
return false;
}

if((parseFloat(saldo)==0)||(saldo.length<1)){swal("","Debe agregar el monto antes de agregar o descontar al saldo disponible de la cuenta.","error");
$("#saldo").focus();
return false;
}

if(n==0){
if(parseFloat(nsaldo)<parseFloat(saldo)){swal("","No es posible descontar el monto al saldo de la cuenta. \nLa cuenta no puede quedar negativa, verifique el monto e intente de nuevo","error");
$("#saldo").focus();
return false;
}
}



	$.post("php/proceso_cuentas.php",{opc:2,n:n, saldo:saldo, id_cta:id},function(resp){
		if(resp.salida==1){
		$("#nsaldo").html(resp.saldo);
		swal("",resp.mensaje,"success");
		listar_cuentas_admin();
		$("#saldo").val("");
			}
		else{
	swal("",resp.mensaje,"error");
		}
	},"json");	
	
}



 
function borrar_cuenta(id){


swal({
  title: "¿Esta segur@ que desea borrar la cuenta bancaria?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	  
	 swal({
  title: "Clave de Seguridad",
  text: "Debe autenticarse antes de realizar la operación",
  type: "input",
  inputType: "password",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Introduzca la clave de seguridad"
},
function(inputValue){
  if (inputValue === false) return false;
  
  if (inputValue === "") {
    swal.showInputError("Debe agregar la clave de seguridad antes de modificar los datos.");
    return false
  }
   
	  
	  
	$.post("php/proceso_cuentas.php",{opc:3,id_cta:id,cv:inputValue},function(resp){
		if(resp.salida==1){
			swal("",resp.mensaje,"success");
			listar_cuentas_admin();
			}
		else{
		swal("",resp.mensaje,"error");
		}
	},"json");
});
}});
}




function agregar_cuenta_admin(){
	var c1=$("#banco").val();
	var c2=$("#tipoc").val();
	var c3=$("#ncuenta").val();
	var c4=$("#posee").val();
	var c5=$("#cititular").val();
	var c6=$("#titular").val();
	var c7=$("#telefonoc").val();
	var c8=$("#correotitular").val();
	var cd=$("#ct3").text();
	cd=cd.replace(".","");
	cd=cd.replace(".","");
	
	
	if(valcam("#banco",0,"Información Incompleta","Debe seleccionar la entidad bancaria",3000)==false){return false;}	
	
	if(valcam("#tipoc",0,"Información Incompleta","Debe seleccionar el tipo de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#ncuenta",20,"Información Incompleta","Debe agregar el numero de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#posee",0,"Información Incompleta","Debe seleccionar si posee cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#cititular",5,"Información Incompleta","Debe agregar la cedula del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#titular",6,"Información Incompleta","Debe agregar el nombre y apellido del titular de la cuenta bancaria",3000)==false){return false;}	
	

	if(valcam("#telefonoc",11,"Información Incompleta","Debe agregar el numero de telefono del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#correotitular","@","Información Incompleta","Debe agregar el correo electronico del titular, ej: pedroperez@hotmail.com",2000)==false){return false;}	
	
	
	swal({
  title: "¿Esta segur@ que desea agregar la cuenta bancaria?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	
	$.post("php/proceso_cuentas.php",{opc:4, c1:c1, c2:c2, c3:c3, c4:c4, c5:c5 ,c6:c6 ,c7:c7 ,c8:c8},function(resp){
		if(resp.salida==1){swal("",resp.mensaje,"success");
		$("#buscar").val(cd);
		listar_cuentas_admin();
		$("#myModal").hide();
		}
		else{swal("",resp.mensaje,"error");}
	},"json");
	
}});
}



function editar_cuenta_admin(){
	var id_cta=$("#id_cta").val();
	var c1=$("#banco").val();
	var c2=$("#tipoc").val();
	var c3=$("#ncuenta").val();
	var c4=$("#posee").val();
	var c5=$("#cititular").val();
	var c6=$("#titular").val();
	var c7=$("#telefonoc").val();
	var c8=$("#correotitular").val();
	var cd=$("#ct3").text();
	cd=cd.replace(".","");
	cd=cd.replace(".","");
	
	
	if(valcam("#banco",0,"Información Incompleta","Debe seleccionar la entidad bancaria",3000)==false){return false;}	
	
	if(valcam("#tipoc",0,"Información Incompleta","Debe seleccionar el tipo de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#ncuenta",20,"Información Incompleta","Debe agregar el numero de cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#posee",0,"Información Incompleta","Debe seleccionar si posee cuenta bancaria",3000)==false){return false;}
	
	if(valcam("#cititular",5,"Información Incompleta","Debe agregar la cedula del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#titular",6,"Información Incompleta","Debe agregar el nombre y apellido del titular de la cuenta bancaria",3000)==false){return false;}	
	

	if(valcam("#telefonoc",11,"Información Incompleta","Debe agregar el numero de telefono del titular de la cuenta bancaria",3000)==false){return false;}	
	
	if(valcam("#correotitular","@","Información Incompleta","Debe agregar el correo electronico del titular, ej: pedroperez@hotmail.com",2000)==false){return false;}	
	
	
	swal({
  title: "¿Esta segur@ que desea editar la cuenta bancaria?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: true
},
function(isConfirm){

  if (isConfirm) {
	
	
	 swal({
  title: "Clave de Seguridad",
  text: "Debe autenticarse antes de realizar la operación",
  type: "input",
  inputType: "password",
  showCancelButton: true,
  closeOnConfirm: false,
  animation: "slide-from-top",
  inputPlaceholder: "Introduzca la clave de seguridad"
},
function(inputValue){
  if (inputValue === false) return false;
  
  if (inputValue === "") {
    swal.showInputError("Debe agregar la clave de seguridad antes de modificar los datos.");
    return false
  }	
	
	
	$.post("php/proceso_cuentas.php",{opc:6, c1:c1, c2:c2, c3:c3, c4:c4, c5:c5 ,c6:c6 ,c7:c7 ,c8:c8, id:id_cta, cv:inputValue},function(resp){
		if(resp.salida==1){swal("",resp.mensaje,"success");
		$("#buscar").val(cd);
		listar_cuentas_admin();
		$("#myModal").hide();
		}
		else{swal("",resp.mensaje,"error");}
	},"json");
});
}});
}





function datostitular_admin(opc){
	
	if(opc.value=="Si"){
	
	$("#titular,#cititular").attr({"readOnly":true});
	$("#titular").val($("#nombre_ti").val());
	$("#correotitular").val($("#correo_adm").val());
	$("#cititular").val($("#cedula_adm").val());
	}else{
	$("#titular,#cititular").attr({"readOnly":false});
	$("#titular").val("");
	$("#correotitular").val("");
	$("#cititular").val("");
	}
	
	
}



function listar_cuenta_admin(){
	var id_cta=$("#id_cta").val();

	$.post("php/proceso_cuentas.php",{opc:5,id:id_cta},function(resp){
		
		if(resp.salida==1){

		$("#banco").val(resp.datos[1]);
		$("#tipoc").val(resp.datos[2]);
		$("#ncuenta").val(resp.datos[3]);
		$("#posee").val(resp.datos[9]);
		$("#cititular").val(resp.datos[5]);
		$("#titular").val(resp.datos[4]);
		$("#telefonoc").val(resp.datos[6]);
		$("#correotitular").val(resp.datos[7]);
		}else{
			
		}
		
	},"json");
	
}






function listar_ultimas_trans_historial(){

	$.post("php/proceso_historial.php",{opc:2},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html(resp.listado);
			}
		else{
		$("#listado_transferencias").html(resp.listado);
		}
	},"json");
	
}


function buscar_trans_historial(){

var fi=$("#fechai").val();
var ff=$("#fechaf").val();
var estatus=$("#estatus").val();
var ir;
if((estatus==0)||(estatus==1)){
	ir=2;
}else{ir=1;}
	$.post("php/proceso_historial.php",{opc:ir, fi:fi, ff:ff, estatus:estatus},function(resp){
		if(resp.salida==1){
		$("#listado_transferencias").html(resp.listado);
			}
		else{
		$("#listado_transferencias").html(resp.listado);
		}
	},"json");
	
}






















function valcam(id,nc,tt,msn,tm){
	//id/clase campo (id)
	//numero caracter (nc)
	//titulo (tt)
	//Mensaje de alerta (msn)
	//tiempo (tm)

if(nc>0){	
	if($(id).val().length< nc){
swal({title: tt, text: msn, timer: tm,  showConfirmButton: false});setTimeout(function(){$(id).focus(); },tm); return false;
}
}else if(nc==0){
	if($(id).val()==""){
swal({title: tt, text: msn, timer: tm,  showConfirmButton: false});setTimeout(function(){$(id).focus(); },tm); return false;
}
}else if(nc=="@"){
	var correo=$(id);
	if(correo.val().length<6){
swal({title: tt, text: msn, timer: tm,  showConfirmButton: false});setTimeout(function(){correo.focus(); },eval(tm+1000)); return false;
}

if(correo.val().length>0){
	var validarEmail=validateEmail(correo.val());
	if(validarEmail==false){
swal({title: tt, text: msn, timer: tm,  showConfirmButton: false});setTimeout(function(){correo.focus(); },eval(tm+1000)); return false;	
	}
		
if(correo.val().length < 6){
swal({title: tt, text: msn, timer: tm,  showConfirmButton: false});setTimeout(function(){correo.focus(); },eval(tm+1000)); return false;
}}
}

}

