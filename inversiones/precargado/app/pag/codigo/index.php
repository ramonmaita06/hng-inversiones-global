<?php session_name("hng"); session_start();  date_default_timezone_set("America/Caracas"); 

include('../../php/funciones.php');
?>
<html>
<meta charset="utf8">
<body>
<center><table><th style="font-size:18px;">GENERAR CODIGO PRECARGADO</th></table></center>

<hr>
<center>


<div class="box box-solid bg-yellow" align="center">
<i class="fa fa-info-circle parpadea" style="font-size:32px"></i><br>
Estimad@ usuario, usted puede generar hasta 5 codigos precargados y enviarlos a traves de un mensaje de texto o correo electronico al contacto de su preferencia, o simplemente generar el o los codigos y tenerlos almacenados para usarlos o compartilos en cualquier momento.<br>&nbsp;
</div>

<div align="right">
<a href="https://www.instagram.com/monitordolarvzla__/?hl=es-la" target="https://www.instagram.com/monitordolarvzla__/?hl=es-la"><img src="images/monitorDolar.png" style="width:32px"></a> Tasa Monitor Dolar <b style="color:green; font-size:16px;"><?php echo masmenos($_SESSION['tasa_compra']); ?> BsS.</b>
Ultima actualización: 
	<b style="font-size:16px; color:black"><?php echo cambiar_fecha_es($_SESSION['cfecha_conf']); ?></b>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<a href="#" class="btn btn-success" onclick="return gestion('precargado/app/pag/codigo/lista.php');"><i class="fa fa-list"></i> Ver códigos</a></div>
<br>

<form id="formulario1" onsubmit="return false;" class="form-horizontal" role="form">
<div class="row">	
	<div class="col-ms-12 col-lg-12">
		<div class="form-group">

			<table class="table table-condensed table-fluid table-striped table-hover">
				<thead>
					<tr>
						<th>Moneda</th>
						<th>Monto</th>
						<th>Descripción</th>
						<th>Estatus</th>
					</tr>
				</thead>
				<tbody>

					<tr>
						<td>
							<select id="divisa1" class="form-control">
								<option value=""></option>
								<?php echo $_SESSION['lista_moneda3']; ?>
							</select>
						</td>
						<td><input class="form-control" maxlength="20" placeholder="ej: 10,23" id="monto1" type="number" min="0" step="0,01" style="width:100%;"></td>
						<td><input id="descripcion1"  class="form-control" maxlength="60" placeholder="ej: para pedro perez (Opcional)"></td>
						<td>
							<select id="estatus1" class="form-control">
								<option value="Pagado">Pagado</option>
								<option value="Pendiente">Pendiente</option>
							</select></td>
					</tr>

					<tr>
						<td colspan="4">
							<div align="center" id="btn_mostrar"><span onclick="return mostrar_codigos(1)" class="btn">Agregar más códigos <i class="fa fa-plus text-success"></i></span></div>
							
						</td>
					</tr>
					<?php for($i=2; $i<=5; $i++){ 
					echo '<tr class="masmontos" style="display:none">
						<td>
							<select id="divisa'.$i.'" class="form-control">
								<option value=""></option>
								'.$_SESSION['lista_moneda3'].'
							</select>
						</td>
						<td><input class="form-control" maxlength="20" placeholder="ej: 10,23" id="monto'.$i.'" type="number" min="0" step="0,01" style="width:100%;"></td>
						<td><input id="descripcion'.$i.'"  class="form-control" maxlength="100" placeholder="ej: para pedro perez (Opcional)"></td>
						<td>
							<select id="estatus'.$i.'" class="form-control">
								<option value="Pagado">Pagado</option>
								<option value="Pendiente">Pendiente</option>
							</select></td>
					</tr>';
					} ?>

				</tbody>
			</table>
			

		</div>
	</div>



	
	<div class="col-ms-12 col-lg-12">
	<div class="form-group">
		<label class="col-md-4 control-label">Enviar a Cliente</label>
		<div class="col-md-2" align="left">
&nbsp;&nbsp;&nbsp;<input style="transform: scale(3)" type="checkbox" onchange="return enviar_cliente(this)" id="enviar">
		</div>
		</div>
	</div>	

		
	<div class="col-ms-12 col-lg-12">
	<div class="form-group">
		<label class="col-md-4 control-label">Teléfono Celular</label>
		<div class="col-md-8">
<input class="form-control" type="text" id="telefono" maxlength="11" disabled placeholder="Ej: 04141234567" onclick="return buscar_contacto(this)" onkeyup="return buscar_contacto(this)"  onKeyPress='return acceptNum(event)'>
		</div>
		</div>
	</div>
		
	<div class="col-ms-12 col-lg-12">
	<div class="form-group">
		<label class="col-md-4 control-label">Correo Electronico</label>
		<div class="col-md-8">
<input id="correo" disabled type="email" class="form-control" placeholder="Ej: pedroperez@hotmail.com (Opcional)" maxlength="80" >
		</div>
		</div>
	</div>
		
		
		
	<div class="col-ms-12 col-lg-12">
	<div class="form-group">
		<div class="col-md-12">
		<center>
<button onclick="return generar_codigo()" id="bgenerar" class="btn btn-md btn-info"><i class="fa fa-refresh"></i> Generar Codigo</button>
</center>
		</div>
		</div>
	</div>
		

	


</div>


</form>


</center>

</body>
</html>