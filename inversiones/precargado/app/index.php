<?php session_name("hng"); session_start();
if(isset($_SESSION['us'])){
date_default_timezone_set("America/Caracas");
include("php/funciones.php");

?>

<!doctype html>
<html>
<meta charset="utf8">
<head>
<title><?php echo $_SESSION["version"]; ?></title>
<link rel="icon" href="../images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="precargado/app/images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="precargado/app/images/favicon.gif" type="image/gif">




<link href="precargado/css/sweetalert.css" rel="stylesheet">
<link href="precargado/css/general.css" rel="stylesheet" type="text/css">
<script src="precargado/js/sweetalert.min.js"></script>
<script src="precargado/app/js/general.js"></script>
<script src="precargado/app/js/teclado.js"></script>
<script src="precargado/app/js/menu.js"></script>
<script src="precargado/app/js/codigo.js"></script>
</head>
<body style="background:#fff;">



<center>

<div id="cortina_blanca"></div>

<div class="panel panel-success" id="principal">
<div class="panel-heading">
<big><big><b>GESTIÓN DE CÓDIGO PRECARGADO</b></big></big>
<table  width="100%">
<tr>
<td><i class="fa fa-barcode fa-3x"></i></td>
<td width="80%" align="justify">&nbsp;<b><i class="fa fa-user"></i></b> <?php echo $_SESSION['cliente']; ?> 
<br>
<div id="puntos" class="text-success hidden" style="text-align:center; font-size:16px; font-weight:bold;" onclick="return ver_puntos();">Puntos: 0.00</div> 
</td>
<td width="10%"><!--<a class="btn btn-danger btn-sm" href="#" id="salir"><i class="fa fa-power-off"></i> Salir</a>--></td>
</tr>
</table>


</div>
<div class="panel-body">
<!--opcion reloj-->
<div  class="menu-ajustes" style="float:right; color:#66994D">
<a class="btn btn-success btn-sm" href="#" onclick="return app_inversiones('app_5x5/index.php')" id="misInversiones"><i class="fa fa-bar-chart-o"></i> Mis Inversiones</a>
<a class="btn btn-warning btn-sm" href="#" onclick="return atras('precargado/app/index.php')" id="atras" style="display:none"><i class="fa fa-arrow-left"></i> Atras</a>
</div>
<br>
<br>
<br>

<div class=""  id="cuerpo_codigo">
<center>

<div class="row">
	<div class="col-md-12">
		<div class="col-md-6">
		<br><button class="btn btn-lg btn-info" style="width:90%" onclick="return gestion('precargado/app/pag/codigo/index.php')"><i class="fa fa-barcode" style="font-size:32px; height:44px; font-weight:bold;"></i><br>GENERAR CODIGO</button>
		</div>
		<br>
		<div class="col-md-6">
		<button  class="btn btn-lg btn-success" style="width:90%" onclick="return gestion('precargado/app/pag/codigo/lista.php')"><i class="fa fa-list" style="font-size:32px; height:44px; font-weight:bold;"></i><br>LISTA DE CODIGOS GENERADOS
		</button>
		</div>
		
	</div>
</div>


</center>
</div>


<br>
<br>
<br>


</div>
</div>

</center>


<!--ventana modal-->			
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade ui-widget-content" data-backdrop="static" id="myModal" role="dialog" style="border:none; ">
    <div class="modal-dialog"  id="ventana_modal" style="position:absolute;">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header" align="right">
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
		   <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><i class="fa fa-times"></i></button>
         <div id="tcuerpo" align="center" style="color:darkblue; font-weight:bold; font-size:20px;"></div>
		 </div>
        <div class="modal-body" id="cuerpo_modal"><center><i class="fa fa-spinner imgr"></i> Cargando...</center></div>
        <div class="modal-footer" id="modpie">
          <!--<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cerrar</button>-->
        </div>
      </div>
      
    </div>
  </div>
  <script>
  $( function() {
   // $( "#ventana_modal" ).draggable();
  } );
  </script>
<script>parent.cerrar_carga();</script>


</body>
</html>
<?php } else{echo "<script>location.href='php/salir.php';</script>";} ?>