<?php session_name("hng"); session_start();
if(isset($_SESSION['us'])){
	echo "<script>location.href='app/';</script>";
}
?>

<!doctype html>
<html>

<head>

<link rel="icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="icon" href="images/favicon.gif" type="image/gif">
<title><?php echo $_SESSION["version"]; ?></title>
<meta charset="utf8">

<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />  
<link href="css/fileinput.css" rel="stylesheet" type="text/css" />
<link href="../css/jquery-ui.css" rel="stylesheet">
<link href="css/sweetalert.css" rel="stylesheet">
<link href="css/general.css" rel="stylesheet" type="text/css">
<script src="js/jq.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/sweetalert.dev.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/general.js"></script>
<script src="js/acceso.js"></script>
</head>
<body>
<form onsubmit="return false;">
<br>
<center>
<div class="panel panel-success acceso">
<div class="panel-heading ajuste">
<img src="images/acceso.jpg" class="img-responsive">
</div>
<div class="panel-body" align="left">
<center>

<img src="images/codigo.gif" style="width:60%"></center><br>
<label for="us" class="alineal">Usuario</label>
<input class="form-control input-md" type="text" id="us" placeholder="Introduzca el usuario">


<label for="us" class="alineal">Contraseña</label>
<input class="form-control input-md" type="password" id="cv" placeholder="Introduzca la contraseña">


<br>
<center><button class="form-control btn btn-primary" id="entrar" style="width:auto"><i class="fa fa-lock"></i> Entrar</button></center>

<br>
<!--<label class="olvido"><a href="#" id="rclave" onclick="return agrega_us()" >¿Olvidó su Contraseña?</a></label>-->
<br><br>
<cite>Derechos Reservados  - HNG-Software &reg; 2018.<br><center><code><?php echo $_SESSION["version"]; ?></code></center></cite>

</div>
</div>
</center>

</form>




<!--ventana modal-->			
  <!-- Trigger the modal with a button -->
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="vmodal">
		
<div>

<div class="panel panel-success">
<div class="panel-heading text-center"><big><big>¿OLVIDÓ SU CONTRASEÑA?</big></big></div>
<div class="panel-body">

<br><form onsubmit="return false;">
Correo Electrónico:

<div class="row">
<div class="col-md-12">
<input type="email" class="col-md-12 form-control" placeholder="Introduzca correo, usuario@gmail.com" style="height:48px; font-size:20px; width:100%;" id="correo">

<div class="col-md-12">
<br>
<center><button class="font btn btn-md btn-success" id="recupera" style="margin:0px"><i class="fa fa-unlock-alt"></i> Recuperar</button></center>
</div>
</div>

<div class="col-md-12">
<!--<a href="#" class="btn">Mas Opciones</a>-->
</div>
</div>



<br></form>
</div>
</div>
</div>
		</div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>
 




</body>
</html>
