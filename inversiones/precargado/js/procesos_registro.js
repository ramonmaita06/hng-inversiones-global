var n=1;
$(document).ready(function(){


$("#sig").click(function(){
	if(cargar_vista()==true){
	n++;
	$("#plan1,#plan2,#plan3").hide(500,"linear");
	$("#plan"+n).show(500,"linear");

	if(n==3){
		$("#sig").attr({"class":"btn btn-warning btn-md"});
		$("#sig").attr({"onclick":"return registrar()"});
		document.getElementById("sig").innerHTML='Registrar <span class="glyphicon glyphicon-edit"></span>';
		vista_contrato();
	}
	else{ $("#sig,#ant").attr({"disabled":false});}
	}

});
	
$("#ant").click(function(){
	n--;
	$("#plan1,#plan2,#plan3").hide(500,"linear");
	$("#sig").attr({"class":"btn btn-success btn-md"});
	$("#plan"+n).show(500,"linear");
	if(n==1){$("#ant").attr({"disabled":true}); }
	else if(n<3){ document.getElementById("sig").innerHTML='Siguiente <span class="glyphicon glyphicon-arrow-right"></span>';}
	else if(n==3){
		$("#sig").attr({"class":"btn btn-warning btn-md"});
		$("#sig").attr({"onclick":"return registrar()"});
		document.getElementById("sig").innerHTML='Registrar <span class="glyphicon glyphicon-edit"></span>';
	}
	else{$("#ant,#sig").attr({"disabled":false});}

});	
		
});









/*vista previa de los datos personales*/
function cargar_vista(){
	

	$("#vape").html($("#ape").val());
	$("#vnom").html($("#nom").val());
	$("#vci").html($("#ci").val()+number_format($("#nci").val(),0,",","."));
	$("#vrif").html($("#rif").val()+$("#nrif").val());
	$("#vsexo").html($("#sexo").val());
	$("#vfechan").html($("#fechan").val());
	$("#vnacionalidad").html($("#nacionalidad option:selected").text());
	$("#vpais").html($("#pais option:selected").text());
	$("#vestado").html($("#estado option:selected").text());
	$("#vciudad").html($("#ciudad option:selected").text());
	$("#vmunicipio").html($("#municipio option:selected").text());
	$("#vparroquia").html($("#parroquia").val());
	$("#vcomunidad").html($("#tcomunidad").val()+": "+$("#comunidad").val());
	$("#vinfra").html($("#infra").val());
	$("#vpiso").html($("#piso").val());
	$("#vnivel").html($("#nivel").val());
	$("#vtcasa").html($("#tcasa").val());
	$("#vttrabajo").html($("#ttrabajo").val());
	$("#vcorreo").html($("#correo").val());
	$("#vccorreo").html($("#ccorreo").val());
	
	

	
if($("#cod").val().length<6){ swal("COMPLETAR INFORMACION","Debe poseer la referencia del patrocinador","error"); $("#cod").focus(); return false;}


if($("#ape").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus apellidos", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ape").focus(); },3000); return false;
}



if($("#nom").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar sus nombres", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nom").focus(); },3000); return false;	
}





if($("#ci").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ci").focus();},3000);  return false;
	}




if(($("#nci").val().length<5)||($("#nci").val().length>12)){
swal({title: "COMPLETAR INFORMACION", text: "Debe ingresar el numero de identificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nci").focus(); },3000); return false;
}


if($("#rif").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#rif").focus(); },3000); return false;
}



if(($("#nrif").val().length<6)||($("#nrif").val().length>13)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su numero de rif", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nrif").focus(); },3000); return false;
}



if($("#sexo").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el sexo", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#sexo").focus(); },3000); return false;
}



if($("#fechan").val().length<10){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar o agregar su fecha de nacimiento", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#fechan").focus(); },3000); return false;
}



if($("#nacionalidad").val().length<2){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar su nacionalidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nacionalidad").focus(); },3000); return false;}


if($("#pais").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el pais de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#pais").focus(); },3000); return false;
}



if($("#estado").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el estado de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#estado").focus(); },3000); return false;
}




if($("#ciudad").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la ciudad de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ciudad").focus(); },3000); return false;
}



if($("#municipio").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el municipio de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#municipio").focus(); },3000); return false;
}



if($("#parroquia").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la parroquia de residencia", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#parroquia").focus(); },3000); return false;
}


if($("#tcomunidad").val().length<5){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de comunidad", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcomunidad").focus(); },3000); return false;
}


if($("#comunidad").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el nombre de la comunidad donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#comunidad").focus(); },3000); return false;
}


if($("#infra").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de infraestructura donde habita", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#infra").focus(); },3000); return false;
}


if($("#piso").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de piso o numero de la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#piso").focus(); },3000); return false;
}


if($("#nivel").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar los niveles que posee la vivienda", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nivel").focus(); },3000); return false;
}


if(($("#tcasa").val().length<11)||($("#tcasa").val().length>11)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de casa o personal", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tcasa").focus(); },3000); return false;
}


if($("#ttrabajo").val().length>0){
if(($("#ttrabajo").val().length<11)||($("#ttrabajo").val().length>11)){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el numero de telefono de su trabajo, (Opcional)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ttrabajo").focus(); },3000); return false;
}}




if($("#correo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}



if($("#correo").val().length>0){
	var validarEmail=validateEmail($("#correo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;	
	}
	
	
if($("#correo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}}


if($("#ccorreo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar nuevamente su correo electronico para confirmar", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}

if($("#ccorreo").val().length>0){
	var validarEmail=validateEmail($("#ccorreo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;	
	}
if($("#ccorreo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(hng@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}
}
if($("#correo").val()!=$("#ccorreo").val()){
swal({title: "COMPLETAR INFORMACION", text: "Los correos electronicos no coinciden, verifique e intente de nuevo.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#ccorreo").focus(); },3000); return false;
}



 if(n<2){ $("#sig").removeAttr("onclick");}

else if(n==2){

	
	
var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();

if($("#tipot").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de pago", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipot").focus(); },3000); return false;
}

if($("#tipot").val()=="PRECARGADO"){
if($("#codp").val().length<8){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el codigo precargado", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
}	

if($("#seguir").val()==0){
swal({title: "COMPLETAR INFORMACION", text: "El codigo precargado no es valido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
}

}else{
	
if($("#bancod").val()==""){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco donde realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancod").focus(); },3000); return false;
}
if($("#DPC_edit2").val()==""){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
}	
if($("#numero").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
}	


if($("#monto").val().length<4){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}	
	
}

}

return true;

}

function vista_contrato(){
var c0=$("#cod").val();
var c1=$("#vape").html();
var c2=$("#vnom").html();
var c3=$("#vci").html();
var c4=$("#vrif").html();
var c5=$("#vfechan").html();
var c6=$("#vsexo").html();
var c7=$("#vpais").html();
var c71=$("#pais").val();
var c8=$("#vnacionalidad").html();
var c81=$("#nacionalidad").val();
var c9=$("#vestado").html();
var c10=$("#vciudad").html();
var c11=$("#vmunicipio").html();
var c12=$("#vparroquia").html();
var c13=$("#vcomunidad").html();
var ubica=$("#estado").val()+","+$("#ciudad").val()+","+$("#municipio").val();
var c14=$("#vinfra").html();
var c15=$("#vpiso").html();
var c16=$("#vnivel").html();
var c17=$("#vtcasa").html();
var c18=$("#vttrabajo").html();
var c19=$("#vcorreo").html();
var c20=$("#vccorreo").html();

var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();

$.post("php/registro.php",{opc:2,
c0:c0,ubica:ubica,c71:c71,c81:c81,
c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,c8:c8,
c9:c9,c10:c10,c11:c11,c12:c12,c13:c13,c14:c14,
c15:c15,c16:c16,c17:c17,c18:c18,c19:c19,c20:c20,
tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,numerod:numerod,montod:montod
},function(resp){
	if(resp.salida==1){
setTimeout(function(){document.getElementById("micontrato").src="../pdf/vista_contrato_general.php";},2000);
	}
},"json");
}





function registrar(){

if(!$('#aceptar').is(':checked')){   $("#ant").click();  
swal({title: "COMPLETAR INFORMACION", text: "Debe aceptar los terminos y condiciones antes de registrarse.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#aceptar").focus(); },3000); return false;
}


var c0=$("#cod").val();
var c1=$("#vape").html();
var c2=$("#vnom").html();
var c3=$("#vci").html();
var c4=$("#vrif").html();
var c5=$("#vfechan").html();
var c6=$("#vsexo").html();
var c7=$("#vpais").html();
var c71=$("#pais").val();
var c8=$("#vnacionalidad").html();
var c81=$("#nacionalidad").val();
var c9=$("#vestado").html();
var c10=$("#vciudad").html();
var c11=$("#vmunicipio").html();
var c12=$("#vparroquia").html();
var c13=$("#vcomunidad").html();
var ubica=$("#estado").val()+","+$("#ciudad").val()+","+$("#municipio").val();
var c14=$("#vinfra").html();
var c15=$("#vpiso").html();
var c16=$("#vnivel").html();
var c17=$("#vtcasa").html();
var c18=$("#vttrabajo").html();
var c19=$("#vcorreo").html();
var c20=$("#vccorreo").html();

var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();
var montop=$("#montop").val();




$("#mipie").hide();
swal({
  title: "¿Esta segur@ que desea registrarse?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
$.post("php/registro.php",{opc:1,
c0:c0,ubica:ubica,c71:c71,c81:c81,
c1:c1,c2:c2,c3:c3,c4:c4,c5:c5,c6:c6,c7:c7,c8:c8,
c9:c9,c10:c10,c11:c11,c12:c12,c13:c13,c14:c14,
c15:c15,c16:c16,c17:c17,c18:c18,c19:c19,c20:c20,
tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,
numerod:numerod,montod:montod,montop:montop
}
,function(resp){
	
	if(resp.salida==1){
		if(resp.opcion==1){
			swal("REGISTRO EXITOSO",resp.mensaje,"success");
			//portafolio_auto();
		}
else{swal("REGISTRO EXITOSO",resp.mensaje+"\n Debe esperar de 24 a 48 horas para confirmar su pago.","success");}

document.getElementById("micontrato").src="../pdf/vista_contrato_general.php?accion=guardar";			
setTimeout(function(){CorreoE(resp.opcion);},5000);			

	}
	else{swal("REGISTRO FALLIDO",resp.mensaje,"error"); 	$("#mipie").show(); $("#ant").click();}
	
},"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
	$("#mipie").show();
	$("#ant").click();
  }
});

	
}

function portafolio_auto(){
$.post("php/procesos_planes.php",{opc:1,id_plan:1,metodo:"true"}
,function(resp){ },"json");
}



function CorreoE(opc){
	
	$.post("php/correo.php",{},function(resp){
		
		
if(opc==1){
		if(resp.salida==1){swal("Envio de Correo","Se ha enviado el contrato de afiliacion a su direccion de correo electronico","success");
		mensajeTexto("Listo",1);}
		else{swal("Envio de Correo","No se pudo enviar el contrato de afiliacion a su direccion de correo electronico","error");}
		}
else{
		if(resp.salida==1){
		
		swal("Envio de Correo","Se ha enviado la informacion de pre-afiliacion a su direccion de correo electronico, y un mensaje de texto","success");
		mensajeTexto("Pre",1);
		//setTimeout(function(){location.reload();},15000);
		}
		else{swal("Envio de Correo","No se pudo enviar la informacion de pre-afiliacion a su direccion de correo electronico.","error");
		setTimeout(function(){ location.reload();},3000);
		}
		}
		
		//setTimeout(function(){ location.reload();},5000);
	},"json");
		
}



var envios=0;
function mensajeTexto(opc,n){
$.post("../msn/proceso.php",{opc:n, texto:opc},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
		setTimeout(function(){
		if(envios==0){ envios=1; mensajeTexto("Admin",2); }
		else{setTimeout(function(){ location.reload();},2000);}
		},1000);
},"json");	
}



function  metodo_afiliacion(opc){
	if(opc.value=="PRECARGADO"){
	$("#ccodp").show(500,'linear');
	$(".metodo-dt").hide(500,'linear');
	}else{
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	}
}

/*validar codigo precargado*/
function valida_codigo(cod){

	if(cod.value.length<8){return false;}
	$("#codp").attr({"readOnly":true});
$.post("php/valida_CP.php",{cod:cod.value},function(resp){
	if(resp.st==1){
	$("#codp").attr({"readOnly":true});
	$("#seguir").val(1);
	$("#montop").val(resp.montop);
	alerta("Validación exitosa",resp.msn,3000,false);
	}
	else if(resp.st==2){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==3){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==0){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
},"json");
	
}





function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}








function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}


