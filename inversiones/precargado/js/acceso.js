$(document).ready(function(){


/*ACCESO*/
$("#entrar").click(function(){

var us=$("#us").val();
var cv=$("#cv").val();



if($("#us").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su nombre de usuario", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#us").focus(); },3000); return false;
}




if($("#cv").val().length<1){
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir la contraseña", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#cv").focus(); },3000); return false;
}


$.post("app/php/acceso.php",{opc:1, us:us, cv:cv},function(resp){

if(resp.salida==1){
swal("INFORMACIÓN VALIDADA EXITOSAMENTE",resp.msn,"success");
setTimeout(function(){location.href='app/';},2000);
}else{
swal("NOTIFICACIÓN",resp.msn,"error");
}
	
},"json");

});




/*RECUPERACION CLAVE*/	
$("#recupera").click(function(){

var correo=$("#correo").val();




if($("#correo").val().length<6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}



if($("#correo").val().length>0){
	var validarEmail=validateEmail($("#correo").val());
	if(validarEmail==false){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico, ej:(usuario@gmail.com)", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;	
	}
	
	
if($("#correo").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar su correo electronico", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#correo").focus(); },3000); return false;
}}

var us=$("#us").val();

$("#recupera").html("<i class='fa fa-spinner imgr'></i> Enviando");
$("#correo, #recupera").attr({"disabled":true});

$.post("app/php/acceso.php",{opc:2, usuario:us, correo:correo},function(resp){

if(resp.salida==1){
setTimeout(function(){
$("#recupera").html("<i class='fa fa-unlock-alt'></i> Recuperar");
$("#correo, #recupera").attr({"disabled":false});
swal("NOTIFICACIÓN",resp.msn,"success");
$("#myModal").modal("hide");
},3000);
}else{
$("#recupera").html("<i class='fa fa-unlock-alt'></i> Recuperar");
$("#correo, #recupera").attr({"disabled":false});
swal("NOTIFICACIÓN",resp.msn,"error");
}
	
},"json");



});
		
});



function agrega_us(){
	
if($("#us").val().length < 6){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el usuario antes de recuperar la contraseña", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#us").focus(); },3000); return false;

$("#myModal").modal("hide");
$("#rclave").attr({"data-toggle":"","data-target":""});

	}else{
$("#rclave").attr({"data-toggle":"modal","data-target":"#myModal"});
$("#rclave").click();
	}
}





function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
 return re.test(email);
}


