<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--Archivos de calendario-->
	<script src="js/js_hng/src/js/jscal2.js"></script>
    <script src="js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  
  <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css" media="screen">

  <link rel="stylesheet" href="js/alerta/dist/sweetalert.css">
  <script src="js/alerta/dist/sweetalert-dev.js"></script>
  <script src="js/jq.min.js"></script>
  <script src="js/bootstrap/js/bootstrap.min.js"></script>
  <script src="js/number_format.js"></script>

  
<script src="js/procesos_cliente.js"></script>
<script src="js/teclado.js"></script>
<script src="js/ubicacion2.js"></script>

<style>
	.nacio{display:inline;}
</style>
  
</head>
<body style="font-size:12px;">

<div class="panel panel-success" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading" align="center">
<big><big>INFORMACIÓN PERSONAL</big></big>
</div>
<div class="panel-body">
<br>
<div style="background:#fff;"><br>
  <form class="form-horizontal" role="form" onsubmit="return false;">

  
    <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Apellidos</label>
          <div class="col-md-8">
            <input id="ape" class="form-control" readonly maxlength="30" onKeyPress="return acceptText(event), stopRKey(event)">
			</div>
          </div>
       </div>
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Nombres</label>
          <div class="col-md-8">
		  <input id="nom" class="form-control" readonly  maxlength="30" onKeyPress="return acceptText(event), stopRKey(event)"></div>
          </div>
       </div> 
	</div>   
<!--fila 1-->

	   <div class="row">  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-4 control-label">Identificación (CI / DNI / PAS)</label>
          <div class="col-md-8">
		  <div class="input-group" style="width:100%">
		  <span class="input-group-addon">
		  <select id="ci" class="sel-add" disabled>
		  <option value=""></option>
		  <option value="CI-">CI</option>
		  <option value="RIF-">RIF</option>
		  <option value="PAS-">PAS</option>
		  <option value="DNI-">DNI</option>
		  <option value="RUT-">RUT</option>
		  <option value="NIT-">NIT</option>
		  <option value="SIN-">SIN</option>
		  <option value="CPF-">CPF</option>
		  <option value="NITE-">NITE</option>
		  <option value="RNC-">RNC</option>
		  <option value="TIN-">TIN</option>
		  <option value="RFC-">RFC</option>
		  <option value="RUC-">RUC</option>
		  <option value="RTU-">RTU</option>
		  <option value="RTN-">RTN</option>
		  <option value="VAT-">VAT</option>
		  <option value="CIF-">CIF</option>
		  </select></span>
		  <span><input id="nci"  class="form-control inp-add" type="text" disabled  maxlength="16" onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)"  placeholder="Ej: 123456780" style="width:100%; border-top-right-radius:5px; border-bottom-right-radius:5px;"></span>
          </div>
		  </div>
       </div> 	
	</div>

	   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-4 control-label">Documento Fiscal Tributario (RIF / CUIL / PAS)</label>
          <div class="col-md-8">
		  <div class="input-group">
		  <span class="input-group-addon">
		  <select id="rif"  class="sel-add"  >
			<option value=""></option>
		  	<!--<option value="No">NP</option>-->
		  	<option value="RIF-">RIF</option>
		  	<option value="PAS-">PAS</option>
		  	<option value="DNI-">DNI</option>
		  	<option value="CUIL-">CUIL</option>
		  	<option value="RUT-">RUT</option>
		  	<option value="NAS-">NAS</option>
		  	<option value="NIT-">NIT</option>
		  	<option value="CUIT-">CUIT</option>
		  	<option value="NITE-">NITE</option>
		  	<option value="RNC-">RNC</option>
		  	<option value="TIN-">TIN</option>
		  	<option value="RFC-">RFC</option>
		  	<option value="RUC-">RUC</option>
		  	<option value="RTU-">RTU</option>
		  	<option value="RTN-">RTN</option>
		  	<option value="NIF-">NIF</option>
		  	<option value="CIF-">CIF</option>
		  </select></span>
		  <span><input id="nrif" class="form-control" type="text"  maxlength="16" onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)"  placeholder="Ej: 123456780" style="width:100%; border-top-right-radius:5px; border-bottom-right-radius:5px;"></span>
          </div>
		  </div>
       </div> 	
	</div>	
	</div>
	
	
	  <div class="row">
	<div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="sexo" class="col-md-4 control-label">Sexo</label>
          <div class="col-md-8">
		  <select id="sexo" class="form-control" ><option value="">Seleccione</option><option value="Femenino">Femenino</option><option value="Masculino">Masculino</option></select>
		  </div>
          </div>
       </div> 
	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="fechan" class="col-md-4 control-label">Fecha de Nacimiento</label>
          <div class="col-md-8">
		  <div class="input-group">
		  <span><input onselect="return validar_fechan(this.value)" class="form-control" onblur="return validar_fechan(this.value)" onkeyup="return validar_fechan(this.value)" onclick="return validar_fechan(this.value)" id="fechan" placeholder="Ej.: dd-mm-aaaa" ></span>
		  <span id="bfecha" class=" input-group-addon glyphicon glyphicon-calendar"></span>
		</div>
		 <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fechan",
        trigger    : "bfecha",
        onSelect   : function() { this.hide()
					validar_fechan($("#fechan").val());
		},
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
		  });//]]>
		  
		  
		  function validar_fechan(fecha){

var f = new Date();
var hoy=f.getDate() + "-" + (f.getMonth() +1) + "-" + f.getFullYear();
		if(validafecha1(fecha,hoy)==true){
			  if(fecha.length==10){
			  $("#miedad").html(calcularEdad(fecha)+" A&ntilde;o(s) de Edad");
			  }else{ $("#miedad").html("");}
		}else{
			if(fecha.length==10){
			msnKo("Formato incorrecto, por favor guiese por el siguiente formato, Ej: dd-mm-aaaa");
			//$("#fechan").val("");
			$("#miedad").html("");
			}
		}
			  
		  }
		  
		  
		  
		  function validafecha1 (fecha,menorquehoy) {  
      var valida = true;  
      //valida que el formato sea dd/dd/dddd (donde d son numeros)  
      var matches = /^(\d{2})[-](\d{2})[-](\d{4})$/.exec(fecha);  
   if (matches == null){  
         valida = false;  
   }else{  
        var d = matches[1];  
        var m = matches[2]-1;  
        var a = matches[3];  
        var f = new Date(a, m, d);  
        //valida si los digitos de entrada dieron de resultado al crear el objeto date  
        // de la misma fecha  
        if(f.getDate() != d || f.getMonth() != m ||f.getFullYear() != a){  
             valida = false;  
        }else if (menorquehoy){  
             var hoy = new Date();  
             //valida que la fecha sea menor a hoy  
             if(f>hoy)  
                  valida=false;  
        }  
    }  
      return valida;  
 }
		  
</script>
		  
		  </div>
		  </div>
       </div> 	   
       </div> 	   
<!--fila 2-->	
	
	  	
   
<!--fila 3-->		
	
 <div class="col-sm-12 col-lg-12"><hr> </div> 	
	
	  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nacionalidad" class="col-md-4 control-label">Nacionalidad o Pais de Origen</label>
          <div class="col-md-8">
		  		  <div class="input-group" style="width:100%">
		  <select id="nacionalidad" class="form-control" onchange="return bandera_nacion(this.value);"><option value=""></option><?php include("../registro/pais.html"); ?></select>
		  <span id="bandera_nacion" class="input-group-addon" style="width:auto; height:20px">...</span>
		  
					</div>
		  </div>
          </div>
       </div> 	
	
		  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="pais" class="col-md-4 control-label">Pais de Residencia &nbsp; &nbsp;</label>
          <div class="col-md-8">
		  		  <div class="input-group" style="width:100%">
		 <select id="pais" class="form-control" disabled onchange="return bandera(this.value);">
		<?php include("../registro/pais.html"); ?>
			</select>

		  <span id="bandera" class="input-group-addon" style="width:auto; height:20px">...</span>
		  
					</div>
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 4-->		
	
	  <div class="row nacio">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="estado" class="col-md-4 control-label">Estado de Residencia</label>
          <div class="col-md-8">
		  <select id="estado" class="form-control" onchange="return ciudades(this);"><option value="">Seleccione el estado</option></select>
		  </div>
          </div>
       </div> 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ciudad" class="col-md-4 control-label">Ciudad de Residencia</label>
          <div class="col-md-8">
		  <select id="ciudad" class="form-control"><option value="">Seleccione la ciudad</option></select>
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 5-->	
	
	
	  <div class="row nacio">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ci" class="col-md-4 control-label">Municipio de Residencia</label>
          <div class="col-md-8">
		  <select id="municipio" class="form-control" onchange="return parroquias(this);"><option value="">Seleccione el municipio</option></select>
		  </div>
          </div>
       </div> 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ci" class="col-md-4 control-label">Parroquia de Residencia</label>
          <div class="col-md-8">
		  <select id="parroquia" class="form-control"><option value="">Seleccione la parroquia</option></select>
		  </div>
          </div>
       </div> 	   
       </div> 	   
<!--fila 6-->	
	


	
	  <div class="row">
	  
	  	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="comunidad" class="col-md-4 control-label">Tipo de Comunidad</label>
          <div class="col-md-8">
		  <div class="input-group">
		 <span class="input-group-addon">
		 <select id="tcomunidad"  class="sel-add" >
		  <option value=""></option>
		  <option value="Urbanizacion">Urbanización</option>
		  <option value="Barrio">Barrio</option>
		  <option value="Sector">Sector</option>
		  <option value="Caserio">Caserio</option>
		  <option value="Pueblo">Pueblo</option>
		   </select>  </span>
		  <span><input id="comunidad" class="form-control"  type="text"  maxlength="60"  placeholder="Ej: El manzanal" style="width:100%; border-top-right-radius:5px; border-bottom-right-radius:5px;"></span>
		  
		  </div>
          </div>
          </div>
       </div>
	  
	  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="infra" class="col-md-4 control-label">Infraestructura</label>
          <div class="col-md-8">
		  <select id="infra" class="form-control"  placeholder="Ej: Avenida, Calle, Carretera, Carrera o Vereda">
		  <option value=""></option>
		  <option value="Casa">Casa</option>
		  <option value="Edificio">Edificio</option>
		  <option value="Apartamento">Apartamento</option>
		  <option value="Local">Local</option>
		  <option value="Centro Empresarial">Centro Empresarial</option>
		  <option value="Centro Comercial">Centro Comercial</option>
		  </select>
		  </div>
          </div>
       </div> 	
	   
       </div> 	   
<!--fila 9-->

 <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="piso" class="col-md-4 control-label">N&deg; o Nombre de Casa </label>
          <div class="col-md-8">
		  <input id="piso" class="form-control"  type="text"  maxlength="20"  placeholder="Ej: 1, 2, 3, Mi hogar....">
		  </div>
          </div>
       </div> 	
 	
	
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nivel" class="col-md-4 control-label">Nivel o Piso</label>
          <div class="col-md-8">
		  <input id="nivel" class="form-control"  type="text"  maxlength="20"  placeholder="Ej: Nivel 1, 1 o planta baja, etc">
		  </div>
          </div>
       </div> 	   
	
       </div> 	
	
	
	  <div class="row">

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="tcasa" class="col-md-4 control-label">N&deg; de Teléfono (Movil o Casa)</label>
          <div class="col-md-8">
		  <input id="tcasa" class="form-control" maxlength="16" type="text" onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)" placeholder="Ej: 04121234567">
		  </div>
          </div>
       </div> 	
  	
	

	<div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ttrabajo" class="col-md-4 control-label">N&deg; de Teléfono del Trabajo (Opcional)</label>
          <div class="col-md-8">
		  <input id="ttrabajo" class="form-control"  maxlength="16" type="text"  onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)"  placeholder="Opcional">
		  </div>
          </div>
       </div> 
	   
       </div> 	   
<!--fila 12-->


  <div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="correo" class="col-md-4 control-label">Correo Electrónico</label>
          <div class="col-md-8">
		  <input id="correo" class="form-control" placeholder="Ej: hng@gmail.com" maxlength="50" onKeyPress="return stopRKey(event)">
		   </div>
          </div>
       </div> 


	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label">Repita el Correo Electrónico</label>
          <div class="col-md-8">
		  <input id="ccorreo" class="form-control" placeholder="Ej: hng@gmail.com" maxlength="50" onKeyPress="return stopRKey(event)">
		  </div>
          </div>
       </div> 	
	   
       </div> 	
	
  <div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group">
		<hr>
          <div class="col-md-12">
<center><button class="btn btn-lg btn-success" onclick="return guardar()"><i class="fa fa-save"></i> Guardar</button></center>
		  </div>
          </div>
       </div> 	
  </div> 	
  
</form>	
</div>
</div>
</div>
<script>parent.cerrar_carga();</script>

</body>
</html>
