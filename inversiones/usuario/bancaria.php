<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Last-Modified" content="0">
  <meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <script src="js/procesos_cliente_banco.js"></script>

  <style>
  .ocultar{display:none}
  #detallesBanco{
    cursor: pointer;    
    height: 300px;
    position:absolute;
    z-index:1000; 
    overflow-y:auto;
    overflow-x: hidden;
    width:260px;
    display: none;
  }

  #detallesBanco div{
    margin-left: 32px;
    background: white;
    padding:4px;

  }
  #detallesBanco div:hover{
    background: white;
    font-weight: bold;
    cursor: pointer;
    background: #efefef;
  }
  </style>
</head>
<body style="font-size:12px;">

<div class="panel panel-success" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading" align="center">
<big><big>INFORMACIÓN BANCARIA</big></big>
</div>
<div class="panel-body">
<br>
<div style="background:#fff">
  <form class="form-horizontal" role="form" onsubmit="return false;">
  <input type="hidden" id="idban">  

  
    <div class="row" style="display:none">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Apellidos</label>
          <div class="col-md-8">
            <input  id="ape" class="form-control"  maxlength="30" onKeyPress="return acceptText(event), stopRKey(event)">
			</div>
          </div>
       </div>
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Nombres</label>
          <div class="col-md-8">
		  <input  id="nom" class="form-control"  maxlength="30" onKeyPress="return acceptText(event), stopRKey(event)"></div>
          </div>
       </div> 
	</div>   
<!--fila 1-->

	   <div class="row" style="display:none">  
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-4 control-label">Cedula de Identidad</label>
          <div class="col-md-8">
		  <div class="input-group" style="width:100%">
		  <span class="input-group-addon">
		  <input id="ci"  class="sel-add" ></span>
		  <span><input id="nci" class="form-control inp-add" type="text"  maxlength="20"  style="width:100%; border-top-right-radius:5px; border-bottom-right-radius:5px;"></span>
          </div>
		  </div>
       </div> 	
	</div>
	
	   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="rif" class="col-md-4 control-label">R.I.F</label>
          <div class="col-md-8">
		  <div class="input-group">
		  <span class="input-group-addon"><select disabled id="rif"  class="sel-add"  ><option value=""></option><!--<option value="No">NP</option>--><option value="J-">J-</option><option value="G-">G-</option><option value="V-">V-</option><option value="E-">E-</option></select></span>
		  <span><input id="nrif" disabled class="form-control" type="number"  maxlength="16" onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)"  placeholder="Ej: 123456780" style="width:100%; border-top-right-radius:5px; border-bottom-right-radius:5px;"></span>
          </div>
		  </div>
       </div> 	
	</div>	
	</div>
	
	

	
	  	
   
	
	  <div class="row" style="display:none">

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="tcasa" class="col-md-4 control-label">N&deg; de Teléfono (Movil o Casa)</label>
          <div class="col-md-8">
		  <input id="tcasa" disabled class="form-control" maxlength="11" type="number" onKeyPress="return acceptNum(event), stopRKey(event), cuenta_numero(this)" placeholder="Ej: 04121234567">
		  </div>
          </div>
       </div> 	
  	
		

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="correo" class="col-md-4 control-label">Correo Electrónico</label>
          <div class="col-md-8">
		  <input id="correo" disabled class="form-control" placeholder="Ej: hng@gmail.com" maxlength="50" onKeyPress="return stopRKey(event)">
		   </div>
          </div>
       </div> 

       </div> 	 



<div class="row">
  <div class="col-md-12">
    <div class="col-md-6 text-center"><button class="btn btn-danger btn-md ven" onclick="addcount('ven')"><i class="fa fa-plus fa-1x"></i> Cuenta Bancaria para Venezuela</button></div>
    <div class="col-md-6 text-center"><button class="btn btn-info btn-md ext" onclick="addcount('ext')"><i class="fa fa-plus fa-1x"></i> Cuentas Extranjeras</button></div>
  </div>
</div>



	   
	<!--DATOS BANCARIOS-->   
	 <div class="row"><hr></div>	


  <div id="addVen" class="addC" style="display:none">

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group alert alert-danger">
          <b>Añadir Cuenta Venezolana</b>
        </div>
    </div>
</div>
<div class="row">

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label text-danger" style="font-size:11px">Posee Cuenta Bancaria Propia</label>
          <div class="col-md-8">

<select id="posee" onchange="return datostitular(this)" class="form-control"  style="width:100%;">
<option value=""></option>
<option value="Si">Si</option>
<option value="No">No, Cuenta de Terceros</option>
</select>
      </div>
          </div>
       </div>
     
  </div>
  
  
    
   <div class="row">

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Nombre del Titular</label>
          <div class="col-md-8">
<input class="form-control" id="titular" style="width:100%;">
      </div>
          </div>
       </div>

    

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Cedula de Identidad / RIF</label>
          <div class="col-md-8">
<input class="form-control" id="cititular" placeholder="Ej: V123454566" style="width:100%;">
      </div>
          </div>
       </div> 
     
     
       </div> 

     
     
  <div class="row">   
    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Correo Electronico</label>
          <div class="col-md-8">
      <input id="correotitular" type="email" class="form-control" placeholder="Ej: pedroperez@hng.com" maxlength="80" >
      </div>
          </div>
       </div> 
     
    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">N&deg; Telefonico</label>
          <div class="col-md-8">
      <input id="teltitular"  class="form-control" placeholder="Ej: 04161234567" maxlength="11" onKeyPress="return acceptNum(event)">
      </div>
          </div>
       </div>      
       </div>      
     
     
     
     
     
     
<div class="row">  
  <div class="col-sm-6 col-lg-6">
    <div class="form-group">
      <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Entidad Bancaria</label>
      <div class="col-md-8">
        <select name="banco" id="banco" class="form-control" style="width:100%;">
        <option value="">Seleccione el Banco
        <option value="BANCAMIGA BANCO MICROFINANCIERO, C.A.">BANCAMIGA BANCO MICROFINANCIERO, C.A.
        <option value="BANCO ACTIVO">BANCO ACTIVO
        <option value="BANCO AGRICOLA">BANCO AGRICOLA
        <option value="BANCO BICENTENARIO">BANCO BICENTENARIO
        <option value="BANCO CARONI">BANCO CARONI
        <option value="BANCO CENTRAL DE VENEZUELA">BANCO CENTRAL DE VENEZUELA
        <option value="BANCO DE LAS FUERZAS ARMADAS(BANFANB)">BANCO DE LAS FUERZAS ARMADAS(BANFANB)
        <option value="BANCO DE VENEZUELA">BANCO DE VENEZUELA
        <option value="BANCO DEL CARIBE">BANCO DEL CARIBE
        <option value="BANCO DEL PUEBLO SOBERANO, C.A.">BANCO DEL PUEBLO SOBERANO, C.A.
        <option value="BANCO DEL SOL">BANCO DEL SOL
        <option value="BANCO DEL TESORO">BANCO DEL TESORO
        <option value="BANCO ESPIRITO SANTO, S.A.">BANCO ESPIRITO SANTO, S.A.
        <option value="BANCO EXTERIOR">BANCO EXTERIOR
        <option value="BANCO INDUSTRIAL DE VENEZUELA">BANCO INDUSTRIAL DE VENEZUELA
        <option value="BANCO INTERNACIONAL DE DESARROLLO">BANCO INTERNACIONAL DE DESARROLLO
        <option value="BANCO MERCANTIL">BANCO MERCANTIL
        <option value="BANCO NACIONAL DE CREDITO">BANCO NACIONAL DE CREDITO
        <option value="BANCO OCCIDENTAL DE DESCUENTO CA.">BANCO OCCIDENTAL DE DESCUENTO CA.
        <option value="BANCO PLAZA">BANCO PLAZA
        <option value="BANCO PROVINCIAL">BANCO PROVINCIAL
        <option value="BANCO SOFITASA">BANCO SOFITASA
        <option value="BANCO VENEZOLANO DE CREDITO">BANCO VENEZOLANO DE CREDITO
        <option value="BANCRECER">BANCRECER
        <option value="BANESCO">BANESCO
        <option value="BANGENTE">BANGENTE
        <option value="BANPLUS, BANCO COMERCIAL">BANPLUS, BANCO COMERCIAL
        <option value="CITIBANK">CITIBANK
        <option value="CORP BANCA">CORP BANCA
        <option value="EAP DEL SUR">EAP DEL SUR
        <option value="FONDO COMUN BANCO UNIVERSAL">FONDO COMUN BANCO UNIVERSAL
        <option value="INSTITUTO MUNICIPAL DE CREDITO P.">INSTITUTO MUNICIPAL DE CREDITO P.
        <option value="INVERUNION BANCO COMERCIAL">INVERUNION BANCO COMERCIAL
        <option value="MIBANCO BANCO DE DESARROLLO">MIBANCO BANCO DE DESARROLLO
        <option value="100% BANCO">100% BANCO
        </select>
      </div>
    </div>
  </div>
  <div class="col-sm-6 col-lg-6">
    <div class="form-group">
      <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Posee pago movil</label>
      <div class="col-md-8">
       &nbsp;<input type="checkbox" value="Si" id="pagomovil" style="transform:scale(2)">
      </div>
    </div>
  </div> 
</div> 
     
   <div class="row">

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">Tipo de Cuenta</label>
          <div class="col-md-8">
<select id="tipoc" class="form-control" style="width:100%;">
<option value="">Seleccione</option>
<option value="Corriente">Corriente</option>
<option value="Ahorro">Ahorro</option>
<option value="Otro">Otro</option>
</select>
      </div>
          </div>
       </div> 

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ccorreo" class="col-md-4 control-label" style="font-size:11px">N&deg; de Cuenta</label>
          <div class="col-md-8">
      <input id="ncuenta"  class="form-control" placeholder="Ej: 010012345678912345678901" maxlength="20" onKeyPress="return acceptNum(event)">
      </div>
          </div>
       </div>   
     
       </div>      
 
  
  <div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="form-group">
    <hr>
          <div class="col-md-12">
<center>
<button id="Rbancaria" class="btn btn-sm btn-success" onclick="return guardar_bancario(1)"><i class="fa fa-save"></i> Registrar</button>
<button id="Cbancaria" style="display:none" class="btn btn-sm btn-primary" onclick="return guardar_bancario(2)"><i class="fa fa-refresh"></i> Cambiar</button>
<button id="Canbancaria" style="display:none" class="btn btn-sm btn-danger" onclick="return cancelar(1)"><i class="fa fa-times-o"></i> Cancelar</button>
</center>
      </div>
          </div>
       </div>   
  </div>  
  

 </div> 

  <div id="addExt" class="addC" style="display:none">
<div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group alert alert-info">
          <b>Añadir Cuenta Extranjera</b>
        </div>
    </div>
</div>
	
	
		
	 <div class="row">

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="titular1" class="col-md-4 control-label" style="font-size:11px">Nombre del Titular</label>
          <div class="col-md-8">
<input class="form-control" id="titular1" style="width:100%;">
		  </div>
          </div>
       </div>

	  

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="cititular1" class="col-md-4 control-label" style="font-size:11px">CI / DNI / RIF / CUIL / PAS</label>
          <div class="col-md-8">
<input class="form-control" id="cititular1" placeholder="Ej: 80454566" style="width:100%;">
		  </div>
          </div>
       </div> 
	   
	   
       </div> 

	   
	   
	<div class="row">   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="correotitular1" class="col-md-4 control-label" style="font-size:11px">Correo Electronico</label>
          <div class="col-md-8">
		  <input id="correotitular1" type="email" class="form-control" placeholder="Ej: pedroperez@hng.com" maxlength="80" >
		  </div>
          </div>
       </div> 
	   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="teltitular1" class="col-md-4 control-label" style="font-size:11px">N&deg; Telefonico</label>
          <div class="col-md-8">
		  <input id="teltitular1"  class="form-control" placeholder="Ej: 14161234567 (Opcional)" value="<?php echo $_SESSION['tcasa']; ?>" maxlength="16" onKeyPress="return acceptNum(event)">
		  </div>
          </div>
       </div> 	   
       </div> 	   
	   

	   
	  <div class="row">  
	   
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="banco1" class="col-md-4 control-label" style="font-size:11px">Entidad Bancaria</label>
          <div class="col-md-8">
            <table class="table">
              <td style="width:70%">
                <input name="banco1" placeholder="Ej: Paypal / Zelle / BOA ..." id="banco1" class="form-control" style="width:100%;">
              </td>
              <td style="width:30%">
                <i class="fa fa-bank fa-2x" style="cursor: pointer;" title="Pulse para mostrar entidades bancarias" onclick="mostrar_bancos(this,1)"></i>
                <div id="detallesBanco" title="Escoger entidad bancaria" data-toggle="tooltip">
                    <div style="color:blue;"><b>BANCOS EN DOLARES Y EUROS</b></div>
                    <div onclick="cargar_entidad(this.innerHTML)">PayPal</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Zelle</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Payoneer</div>
                    <div onclick="cargar_entidad(this.innerHTML)">AirTM</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Uphold</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Bank of America</div>
                    <div onclick="cargar_entidad(this.innerHTML)">TransferWise</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banesco Panamá</div>
                    <!--cuentas argentinas -->
                    <div style="color:blue;"><b>BANCOS ARGENTINOS</b></div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Santander Río</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Galicia</div>
                    <div onclick="cargar_entidad(this.innerHTML)">BBVA Banco Francés</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Nación</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Macro</div>
                    <div onclick="cargar_entidad(this.innerHTML)">ICBC</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Citibank</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Hipotecario</div>
                    <div onclick="cargar_entidad(this.innerHTML)">HSBC</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Provincial</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Ciudad</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Credicoop</div>
                    <div onclick="cargar_entidad(this.innerHTML)">Banco Patagonia</div>   
                </div>
              </td>
            </table>
          </div>
        </div>
       </div>	
     
    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="divisa1" class="col-md-4 control-label" style="font-size:11px">(Moneda a Recibir)</label>
          <div class="col-md-8">
            <select id="divisa1" class="form-control">
              <?php echo $_SESSION['lista_moneda2']; ?>
            </select>
          </div>
        </div>
       </div> 	   
       </div> 
	   
	 <div class="row">

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="tipoc1" class="col-md-4 control-label" style="font-size:11px">Tipo de Cuenta</label>
          <div class="col-md-8">
<select id="tipoc1" class="form-control" style="width:100%;" onchange="noAplica(this)">
<option value="">Seleccione</option>
<option value="No Aplica">No Aplica</option>
<option value="De Cheques">De Cheques</option>
<option value="Corriente">Corriente</option>
<option value="Ahorro">Ahorro</option>
<option value="Caja de Ahorro">Caja de Ahorro</option>
<option value="Nomina">Nomina</option>
<option value="Valores">Valores</option>
<option value="Empresarial/Negocios">Empresarial / Negocios</option>
</select>
		  </div>
          </div>
       </div> 

	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nruta1" class="col-md-4 control-label" style="font-size:11px">Código del Banco / Numero de Ruta ABA/ACH/CBU</label>
          <div class="col-md-8">
      		  <input id="nruta1"  class="form-control" placeholder="Ej: 123456789" maxlength="24" onKeyPress="return stopRKey(event)">
      		</div>
        </div>
    </div> 	

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ncuenta1" class="col-md-4 control-label" style="font-size:11px">N&deg; de Cuenta / IBAN</label>
          <div class="col-md-8">
            <input id="ncuenta1"  class="form-control" placeholder="Ej: 01001234567891234" maxlength="24" onKeyPress="return stopRKey(event)">
          </div>
        </div>
    </div> 	   

    <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="npropia1" class="col-md-4 control-label" style="font-size:11px">Cuenta  Propia</label>
          <div class="col-md-8">
            <select id="npropia1" class="form-control"  style="width:100%;">
            <option value=""></option>
            <option value="Si">Si</option>
            <option value="No">No, Cuenta de Terceros</option>
            </select>
          </div>
        </div>
    </div>   
</div> 	   
	   

	
  <div class="row">
	  <div class="col-sm-12 col-lg-12">
        <div class="form-group">
		
          <div class="col-md-12">
<center>
<button id="Rbancaria1" class="btn btn-sm btn-success" onclick="return guardar_bancario_ext(1)"><i class="fa fa-save"></i> Registrar</button>
<button id="Cbancaria1" style="display:none" class="btn btn-sm btn-primary" onclick="return guardar_bancario_ext(2)"><i class="fa fa-refresh"></i> Cambiar</button>
<button id="Canbancaria1" style="display:none" class="btn btn-sm btn-danger" onclick="return cancelar(2)"><i class="fa fa-times-o"></i> Cancelar</button>
</center>
		  </div>
          </div>
       </div> 	
  </div> 	
  
</form>	
 </div> 

<hr>

<div class="panel panel-success">
  <div class="panel-heading"><center><b><big>CUENTAS REGISTRADAS</big></b></center></div>
  <div class="panel-body">
    <div id="listaCuentas" style="text-align:center;">... Sin Informacion Bancaria.</div>
  </div>
</div>


<br>
<hr>
<center>
<img src="images/bancos.png" class="img-responsive" style="width:50%">
</center>
</div>
</div>
</div>
  <link rel="stylesheet" type="text/css" href="bootstrap/datatables/datatables/datatables.min.css">
  <script type="text/javascript" src="bootstrap/datatables/datatables/datatables.min.js" ></script>
<script>
  parent.cerrar_carga();
  lista_cuentas_bancarias();

function reloadDatatableRetiro(){

 setTimeout(function(){

    $('.datatableRetiro').DataTable({
        pageLength: 5,
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });


  },2500);
}

</script>
</body>
</html>
