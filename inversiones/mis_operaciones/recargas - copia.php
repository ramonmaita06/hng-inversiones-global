<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--Archivos de calendario-->
	<script src="js/js_hng/src/js/jscal2.js"></script>
    <script src="js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  
  <link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css" media="screen">

  <link rel="stylesheet" href="js/alerta/dist/sweetalert.css">
  <script src="js/alerta/dist/sweetalert-dev.js"></script>
  <script src="js/jq.min.js"></script>
  <script src="js/bootstrap/js/bootstrap.min.js"></script>
  <script src="js/number_format.js"></script>




<script src="js/proceso_recarga.js"></script>
<script src="js/teclado.js"></script>
  <style>
  .ocultar{display:none}
  </style>
</head>
<body style="font-size:12px;">
<div class="panel panel-success" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading" align="center">
<big><big>RECARGAS DE BONOS</big></big>
</div>
<div class="panel-body">
<input type="hidden" id="tmoneda">
<br>

 <div  style="background:#fff"> 

<div  style="background:#C2C9D1"> 
<center><div class="btn-group">
<button id="boton1" class="btn btn-md btn-warning" onclick="return opciones_recarga(1)"><i class="fa fa-refresh"></i> Recargar</button>
<button id="boton2" class="btn btn-md btn-default" onclick="return opciones_recarga(2)"><i class="fa  fa-bar-chart-o"></i> Historico</button>
</div></center>
</div>
<br>


<div class="row mostrar_recarga" style="display:none;">
	<div class="col-ms-3 col-lg-3">
	<div class="form-group">
		<label class="col-md-4 control-label text-success">Moneda</label>
		<div class="col-md-8">
<select id="divisa" class="form-control">
  <?php 
    echo $_SESSION['lista_moneda2'];
  ?>
</select>
		</div>
		</div>
	</div>
	  <div class="col-sm-4 col-lg-4">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Inicial</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit0" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit0",
        trigger    : "DPC_edit0",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-4 col-lg-4">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Final</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit1" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit1",
        trigger    : "DPC_edit1",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-4 col-lg-4"  style="display:none">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Estatus</label>
          <div class="col-md-8">
<select id="esta" class="form-control" style="width:100%;">
<option value="0">En espera</option>
<option value="1">Aprobados</option>
<!--<option value="2">Rechazados</option>-->
</select>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-1 col-lg-1">
        <div class="form-group">
<center><button class="btn btn-success btn-md" onclick="return mostrar_recargas_fecha()"><i class="fa fa-search"></i></button></center>
          </div>
       </div>

</div>
<hr>

<div class="mostrar_recarga"  id="mostrar_recarga" style="display:none; width:100%; height:700px; overflow-x: hidden; overflow-y:auto; background:#fff;"></div>

<form class="form-horizontal"  id="form_recargas" role="form" onsubmit="return false;">

	 
	 
	<div class="row">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Tipo de Transacción</label>
          <div class="col-md-8">
<select name="tipot" id="tipot" class="form-control" style="width:100%;" onchange="return metodo_afiliacion(this)">
<option value="">Seleccione el tipo de pago
<option value="PRECARGADO">CÓDIGO PRECARGADO
<option value="DEPOSITO">DEPOSITO
<option value="TRANSFERENCIA">TRANSFERENCIA
<option value="PAGO MOVIL">PAGO MOVIL
<option value="PAGO DESDE EL EXTERIOR">PAGO DESDE EL EXTERIOR
</optgroup>
</select>			
			</div>
          </div>
       </div>
	   	  <div class="col-sm-6 col-lg-6" id="ccodp">
        <div class="form-group">
          <label for="codp" class="col-md-4 control-label">Serial del Código Precargado</label>
          <div class="col-md-8">
            <input id="codp" class="form-control" placeholder="Inserte código precargado" maxlength="8" onkeyup="return valida_codigo(this);"  onclick="return valida_codigo(this);">
			<i title="Pulse para limpiar el codigo" class="glyphicon glyphicon-qrcode" style="font-size:24px; position:absolute; right:5%; top:15%;cursor:pointer" onclick="$('#codp').attr({'readOnly':false}); $('#codp').val(''); $('#codp').focus()"></i>
			<input id="seguir" type="hidden" value="0">
			<input id="montop" type="hidden" value="0">
			</div>
          </div>
       </div>
	</div>   
	 <br>

     <div class="row metodo-dt">
         
       <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="bancoe" class="col-md-4 control-label">Banco Emisor <b>(De)</b></label>
          <div class="col-md-8">
<select name="bancoe" id="bancoe" class="form-control" style="width:100%;">
<option value="">Seleccione el Banco
<option value="BANCAMIGA">BANCAMIGA BANCO MICROFINANCIERO, C.A.
<option value="ACTIVO">BANCO ACTIVO
<option value="AGRICOLA">BANCO AGRICOLA
<option value="BICENTENARIO">BANCO BICENTENARIO
<option value="CARONI">BANCO CARONI
<option value="BCV">BANCO CENTRAL DE VENEZUELA
<option value="BANFANB">BANCO DE LAS FUERZAS ARMADAS(BANFANB)
<option value="VENEZUELA">BANCO DE VENEZUELA
<option value="DEL CARIBE">BANCO DEL CARIBE
<option value="DEL PUEBLO">BANCO DEL PUEBLO SOBERANO, C.A.
<option value="DEL SOL">BANCO DEL SOL
<option value="DEL TESORO">BANCO DEL TESORO
<option value="BES">BANCO ESPIRITO SANTO, S.A.
<option value="EXTERIOR">BANCO EXTERIOR
<option value="BIV">BANCO INDUSTRIAL DE VENEZUELA
<option value="BID">BANCO INTERNACIONAL DE DESARROLLO
<option value="MERCANTIL">BANCO MERCANTIL
<option value="BNC">BANCO NACIONAL DE CREDITO
<option value="BOD">BANCO OCCIDENTAL DE DESCUENTO CA.
<option value="PLAZA">BANCO PLAZA
<option value="PROVINCIAL">BANCO PROVINCIAL
<option value="SOFITASA">BANCO SOFITASA
<option value="BVC">BANCO VENEZOLANO DE CREDITO
<option value="BANCRECER">BANCRECER
<option value="BANESCO">BANESCO
<option value="BANGENTE">BANGENTE
<option value="BANPLUS">BANPLUS, BANCO COMERCIAL
<option value="CITIBANK">CITIBANK
<option value="CORP BANCA">CORP BANCA
<option value="EAP DEL SUR">EAP DEL SUR
<option value="FONDO COMUN">FONDO COMUN BANCO UNIVERSAL
<option value="IMC">INSTITUTO MUNICIPAL DE CREDITO P.
<option value="IBC">INVERUNION BANCO COMERCIAL
<option value="MBD">MIBANCO BANCO DE DESARROLLO
<option value="100% BANCO">100% BANCO
</select>
			</div>
          </div>
       </div>
           
         
         
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Banco Receptor <b>(Para)</b></label>
          <div class="col-md-8">
			<select name="bancod" id="bancod" class="form-control" style="width:100%;">
			<option value="">Seleccione el Banco
			<option value="BICENTENARIO">BANCO BICENTENARIO
			<option value="CARONI">BANCO CARONI
			<option value="VENEZUELA">BANCO DE VENEZUELA
			<option value="MERCANTIL">BANCO MERCANTIL
			<option value="BNC">BANCO NACIONAL DE CREDITO
			<option value="BVC">BANCO VENEZOLANO DE CREDITO
			<option value="BANESCO">BANESCO
			<option value="100% BANCO">100% BANCO
			<option value="PROVINCIAL">BANCO PROVINCIAL
			</select>
			</div>
          </div>
       </div>

	</div>   
	
	
     <div class="row metodo-dt">
        
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha de la Transacción</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit2" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit2",
        trigger    : "DPC_edit2",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="ape" class="col-md-4 control-label">Numero de Deposito o Transferencia</label>
          <div class="col-md-8">
            <input id="numero" class="form-control" type="number" maxlength="30">
			</div>
          </div>
       </div>

	</div>   
 	
	
     <div class="row metodo-dt">
	  <div class="col-sm-6 col-lg-6">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Monto (BsS.)</label>
          <div class="col-md-8">
		  <input id="monto" class="form-control" type="number" step="0.01"   maxlength="30"></div>
          </div>
       </div> 
	</div>      
 <br>
 <hr>
 <center>
<button class="btn btn-md btn-success" disabled id="btn_recargar" onclick="return registrar_recarga()"><i class="fa fa-save"></i> Recargar</button> </center>
  
  
</form>	
<br>
<br>
<hr>
<img src="images/bancos.png" class="img-responsive">
</div>
</div>
</div>
    <link rel="stylesheet" type="text/css" href="bootstrap/datatables/datatables/datatables.min.css">
  <script type="text/javascript" src="bootstrap/datatables/datatables/datatables.min.js" ></script>
<script>parent.cerrar_carga();
    setTimeout(function(){

      
    $('.datatable').DataTable({
        language: {
          "decimal": "",
          "emptyTable": "No hay información",
          "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
          "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
          "infoFiltered": "(Filtrado de _MAX_ total entradas)",
          "infoPostFix": "",
          "thousands": ",",
          "lengthMenu": "Mostrar _MENU_ Entradas",
          "loadingRecords": "Cargando...",
          "processing": "Procesando...",
          "search": "Buscar:",
          "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
    });
  },2500);
</script>

</body>
</html>