<?php

$serial = $_GET['serial'];
require_once('class/BCGFontFile.php');
require_once('class/BCGColor.php');
require_once('class/BCGDrawing.php');

require_once('class/BCGcode128.barcode.php');
header('Content-Type: image/png');

$colorFront = new BCGColor(0, 0, 0);
$colorBack = new BCGColor(255, 255, 255);

$code = new BCGcode128();
$code->setScale(6);
$code->setThickness(5);
$code->setForegroundColor($colorFront);
$code->setBackgroundColor($colorBack);
$code->parse($serial);

$drawing = new BCGDrawing('', $colorBack);
$drawing->setBarcode($code);

$drawing->draw();
$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);
?>