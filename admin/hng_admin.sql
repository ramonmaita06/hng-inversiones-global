-- phpMyAdmin SQL Dump
-- version 3.1.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-12-2019 a las 15:26:14
-- Versión del servidor: 5.1.30
-- Versión de PHP: 5.2.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `hng_admin`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `id_h` int(11) NOT NULL AUTO_INCREMENT,
  `id_u` int(11) NOT NULL,
  `operacion` varchar(80) NOT NULL,
  `detalles` varchar(250) NOT NULL,
  `fecha_h` datetime NOT NULL,
  PRIMARY KEY (`id_h`),
  KEY `id_u` (`id_u`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Volcar la base de datos para la tabla `historial`
--

INSERT INTO `historial` (`id_h`, `id_u`, `operacion`, `detalles`, `fecha_h`) VALUES
(1, 1, 'Retiro de Fondos', 'HNGI: Estimad@ Richard Tovar su retiro nro 50459733 por un monto de 56.212,50 BsS. fue procesado a su cuenta bancaria bajo el nro 123456 DESDE VENEZUELA', '2019-09-28 02:27:27'),
(2, 1, 'Liberacion de recarga ("INVERSIONES") - Aprobacion', 'Estimad@ sr(a) ALEJANDRA JOSE LUCES, nos complace en informarle que hemos recargado a su portafolio de inversiones la cantidad de 100.000,00 Bs.', '2019-09-28 02:27:42'),
(3, 1, 'Liberacion de recarga ("INVERSIONES") - Rechazado', 'Recarga rechazada para el cliente ARNALDO JOSE AVENDANO UZCATEGUI Informacion de recarga Ref: 000006280 Rechazada Monto: 000006280 Rechazada Banco: 000006280 Rechazada Fecha: 2019-07-18', '2019-09-28 02:28:07'),
(4, 1, 'Liberacion de Afiliacion ("INVERSIONES - Aprobado")', 'Afiliacion de Bermudez  Loida Milixa codigo: HNGI-6209, por un monto de afiliacion de: 10000.00 Banco: DE BNC A BDV Nro de referencia: 112123745 en fecha: 2019-07-18', '2019-09-28 02:28:46'),
(5, 1, 'Liberacion de Afiliacion ("INVERSIONES - Rechazado")', 'Estimad@ Bermudez  Loida Milixa su registro fue rechazado, debido a errores en los datos suministrados, verifique e intente de nuevo.', '2019-09-28 02:29:17'),
(6, 1, 'Retiro de Fondos (INVERSIONES) - Porcesado', 'HNGI: Estimad@ Pavel Davila su retiro nro 63914043 por un monto de 275.000,00 BsS. fue procesado a su cuenta bancaria bajo el nro 53646534 DESDE BANESCO', '2019-09-28 02:30:49'),
(7, 1, 'Liberacion de recarga ("INVERSIONES") - Aprobacion', 'Estimad@ sr(a) SANTA MUNOZ , nos complace en informarle que hemos recargado a su portafolio de inversiones la cantidad de 10.000,00 Bs.', '2019-09-29 22:42:37'),
(8, 1, 'Retiro de Fondos () - Procesado', 'HNGI: Estimad@ Hombres  su retiro nro 21367773 por un monto de 75.000,00 PesoCo. fue procesado a su cuenta bancaria bajo el nro 13212313 ', '2019-10-07 01:02:25'),
(9, 1, 'Retiro de Fondos () - Procesado', 'HNGI: Estimad@ Hombres  su retiro nro 21367773 por un monto de 75.000,00 PesoCo. fue procesado a su cuenta bancaria bajo el nro 13212313 ', '2019-10-07 01:02:36'),
(10, 1, 'Retiro de Fondos () - Procesado', 'HNGI: Estimad@ Hombres  su retiro nro 58223580 por un monto de 75.000,00 PesoCo. fue procesado a su cuenta bancaria bajo el nro 12332132 ', '2019-10-07 01:03:16'),
(11, 1, 'Liberacion de Afiliacion ("INVERSIONES - Aprobado")', 'Afiliacion de Campos De Garcia Reyna Eugenia codigo: HNGI-8022, por un monto de afiliacion de: 110000.00 Banco: DE BDV A MERCANTIL Nro de referencia: 44388993 en fecha: 2019-09-26', '2019-10-20 21:25:21'),
(12, 1, 'Liberacion de Afiliacion ("INVERSIONES - Aprobado")', 'Afiliacion de Davila Albarran Richard Alexander codigo: HNGI-8029, por un monto de afiliacion de: 500000.01 Banco: DE MERCANTIL A MERCANTIL Nro de referencia: 0025515264822 en fecha: 2019-10-02', '2019-10-20 21:37:08'),
(13, 1, 'Liberacion de Afiliacion ("INVERSIONES - Rechazado")', 'Estimad@ Davila Albarran Richard Alexander su registro fue rechazado, debido a errores en los datos suministrados, verifique e intente de nuevo.', '2019-10-20 21:43:50'),
(14, 1, 'Liberacion de Afiliacion ("INVERSIONES - Aprobado")', 'Afiliacion de Regterwtert Retreterw codigo: HNGI-8043, por un monto de afiliacion de: 25000.00 Banco: DE AGRICOLA A BDV Nro de referencia: 4523234234 en fecha: 2019-10-23', '2019-10-22 23:36:29'),
(15, 1, 'Liberacion de Afiliacion ("INVERSIONES - Aprobado")', 'Afiliacion de Sanchez Hernandez Luis Eduardo codigo: HNGI-8600, por un monto de afiliacion de: 25.00 Banco: DE pay pal A PAYPAL Nro de referencia: 3t659458uk731025p en fecha: 2019-10-19', '2019-10-26 17:15:41'),
(16, 1, 'Liberacion de recarga ("INVERSIONES") - Aprobacion', 'Estimad@ sr(a) EISWARD ESTIBEN MEJIAS ESCALANTE, nos complace en informarle que hemos recargado a su portafolio de inversiones la cantidad de 367.470,00 Bs.', '2019-10-27 22:58:38'),
(17, 1, 'Retiro de Fondos (INVERSIONES) - Rechazado', 'Motivo: El retiro Nº 31016928 no se pudo realizar el pago debido a no funciona', '2019-11-03 23:46:50'),
(18, 1, 'Retiro de Fondos (INVERSIONES) - Rechazado', 'Motivo: El retiro Nº 91577928 no se pudo realizar el pago debido a devolucion al portafolio autorizado por el  afiliado', '2019-11-08 12:41:21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `id_u` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(60) NOT NULL,
  `usuario` varchar(40) NOT NULL,
  `clave` varchar(32) NOT NULL,
  `telefono` varchar(16) NOT NULL,
  `privilegio` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  PRIMARY KEY (`id_u`),
  UNIQUE KEY `usuario` (`usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_u`, `nombres`, `usuario`, `clave`, `telefono`, `privilegio`, `fecha`) VALUES
(1, 'Dey Rodriguez ', 'deydeco@gmail.com', 'd510d89563883f1006d0260574e4ec36', '04148796899', 1, '2019-09-25 21:46:36'),
(2, 'Raquel Pinto', 'florangela438@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '04147723964', 2, '2019-09-25 21:46:36'),
(3, 'Israel Paredes', 'Israelparedes615@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '04123747450', 0, '2019-09-25 21:47:12');
