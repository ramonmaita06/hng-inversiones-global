<?php 

if(isset($_SESSION['cod_idp'])){
/*INICIO DE LA GENERACION DE CODIGO DE BARRA*/
$style = array(
    'position' => '',
    'align' => 'C',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => true,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);

// CODE 128 C
if(($_SESSION['cod_idp']=="") && (isset($_SESSION['cod_idp']))){
	$cod_id="SIN CÓDIGO";
	}
else if(($_SESSION['cod_idp']!="") && (isset($_SESSION['cod_idp']))){
	$cod_id=$_SESSION['cod_idp'];
	}
else {$cod_id="SIN CÓDIGO";}

if(strlen($cod_id)%2==1){$cod_id=$cod_id.' ';}

$pdf->Cell(0, 0, 'HNG C.A', 0, 1);
$pdf->write1DBarcode($cod_id, 'C128C', '', '', '', 12, 0.5, $style, 'N');
// set a barcode on the page footer
$pdf->setBarcode($cod_id);
}
//unset($_SESSION['cod_idp']);

/*FIN DE LA GENERACION DE CODIGO DE BARRA*/

?>