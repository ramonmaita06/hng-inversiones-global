<?php session_name("contador"); session_start();

// Include the main TCPDF library (search for installation path).
require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->setPrintHeader(false);

// set default header data


// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

date_default_timezone_set('America/Caracas');
$date=date('d-m-Y H:i:s a');

// Form validation functions
$js = <<<EOD
	print();
EOD;
// Add Javascript code
//$pdf->IncludeJS($js);

// set font
$pdf->SetFont('helvetica', '', 11);

// add a page
$pdf->AddPage();

$pdf->SetY(10);
$salida=$_SESSION["contrato"];
// -----------------------------------------------------------------------------

$pdf->SetFont('helvetica', '', 10);

// define barcode style
$style = array(
    'position' => '',
    'align' => 'C',
    'stretch' => false,
    'fitwidth' => true,
    'cellfitalign' => '',
    'border' => false,
    'hpadding' => 'auto',
    'vpadding' => 'auto',
    'fgcolor' => array(0,0,0),
    'bgcolor' => false, //array(255,255,255),
    'text' => true,
    'font' => 'helvetica',
    'fontsize' => 8,
    'stretchtext' => 4
);


// CODE 128 C
$pdf->Cell(0, 0, 'HNG C.A', 0, 1);
$pdf->write1DBarcode('I-VE0001', 'C128C', '', '', '', 12, 0.5, $style, 'N');

// set a barcode on the page footer
$pdf->setBarcode('I-VE0001');

// ---------------------------------------------------------
$pdf->writeHTML($salida, true, false, true, false, '');
//Close and output PDF document
$pdf->Output('barcode.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+