<?php session_name("hng"); session_start();
//include("../../php/conexion.php");

require_once('tcpdf/config/lang/eng.php');
require_once('tcpdf/tcpdf.php');



$cmeses = Array("00"=>"","1"=>"ENERO","2"=>"FEBRERO","3"=>"MARZO","4"=>"ABRIL","5"=>"MAYO","6"=>"JUNIO","7"=>"JULIO","8"=>"AGOSTO","9"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$meses = Array("00"=>"","01"=>"ENERO","02"=>"FEBRERO","03"=>"MARZO","04"=>"ABRIL","05"=>"MAYO","06"=>"JUNIO","07"=>"JULIO","08"=>"AGOSTO","09"=>"SEPTIEMBRE","10"=>"OCTUBRE","11"=>"NOVIEMBRE","12"=>"DICIEMBRE");
$nmeses = Array("00"=>"","ENERO"=>"01","FEBRERO"=>"02","MARZO"=>"03","ABRIL"=>"04","MAYO"=>"05","JUNIO"=>"06","JULIO"=>"07","AGOSTO"=>"08","SEPTIEMBRE"=>"09","OCTUBRE"=>"10","NOVIEMBRE"=>"11","DICIEMBRE"=>"12");



// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('');



$pdf->setPrintHeader(false);

// set font
$pdf->SetFont('helvetica', '',9);

// add a page
$pdf->AddPage('P');
$pdf->SetPrintFooter(true);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

date_default_timezone_set('America/Caracas');
$date=date('d-m-Y H:i:s a');

// Form validation functions
$js = <<<EOD
	print();
EOD;
// Add Javascript code
//$pdf->IncludeJS($js);

if($_SESSION['c6']=="Femenino"){$s="a"; $ciudadano="la ciudadana";}else{$s="a"; $ciudadano="el ciudadano";}

//$_SESSION['cod_idp']="SIN ASIGNACION";
include("codigo_barra.php");
$espacio="60";
$salida='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Documento sin t&iacute;tulo</title>
<style type="text/css">
<!--
.Estilo1 {
	color: #000099;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<table width="100%"><tr>
<td width="20%" align="left"></td>
<td width="60%"></td>
<td width="20%" align="right"><img src="../pdf/logo_oficial_new_contrato.png" width="120"></td>
</tr></table>

<p align="center"><b>CONTRATO DE AFILIACIÓN PARA LA COMPRA-VENTA DE MATERIALES APTOS PARA RECICLAJE</b></p>
<p align="center"><b>DISPOSICIONES GENERALES</b></p>

<p align="justify">
Entre, la Compañía “Hombres de Negocios Global” C.A. Sociedad de Comercio domiciliada en Ciudad Bolívar, Municipio Autónomo Heres del Estado Bolívar, debidamente inscrita ante el Registro Mercantil Segundo del Estado Bolívar, en fecha seis (06) de agosto del año 2014, quedando inserta en el Tomo: 32-A REGMESEGBO 304, Número: 36, siendo su última modificación en fecha, once (11) de enero del año 2016, quedando registrada en el Tomo: 1-A REGMESEGBO 304, Número: 47, con Registro de Información Fiscal (R.I.F.) N° J404527362, venezolano, mayor de edad, titular de la cédula de identidad N° V-13.131.611, procediendo con el carácter de Presidente de la Sociedad de Comercio “Hombres de Negocios Global” C.A. conforme se desprende de las Artículo 14° y Artículo 24° del Acta Constitutiva y Estatutos, quien en lo sucesivo y para los efectos del presente documento se denominará LA EMPRESA, por una parte y por la otra '.$ciudadano.' <u><span style="color:#000B3B;">'.strtoupper($_SESSION["c2"]).'</span></u>, venezolan'.$s.', mayor de edad, titular de la c&eacute;dula de identidad N&deg; <u><span style="color:#000B3B;">'.strtoupper($_SESSION["c3"]).'</span></u>, RIF <u><span style="color:#000B3B;">'.$_SESSION["c4"].'</span></u>,  persona natural quien en lo adelante se denominará EL VENDEDOR, a quien LA EMPRESA garantiza los servicios plasmados en el presente Contrato a EL VENDEDOR, el presente Contrato se regirá por las siguientes clausulas:
</p>


<br>

<p align="justify">
CLAUSULA 1. El Marco Legal de HOMBRES DE NEGOCIOS GLOBAL, C.A.: Para efectos del presente contrato HOMBRES DE NEGOCIOS GLOBAL, C.A. será denominado LA EMPRESA, la cual manifiesta ser una Sociedad Mercantil constituida bajo las leyes de la República Bolivariana De Venezuela, legal y debidamente inscrita en el Registro Mercantil Segundo de la Circunscripción Judicial del estado Bolívar bajo el Numero 36 Tomo 32-A de fecha 06 de Agosto de 2014, Inscrita en el Instituto Venezolano de los Seguros Sociales (I.V.S.S) bajo el Numero O61545173. Inscripción en el R.N.A, Aportante No 1060484212 del Instituto Nacional de Capacitación y Educación Socialista (I.N.C.E.S.), Inscrita ante el Registro Nacional de Empresas y Establecimientos Bajo el Número de Identificación Laboral (NIL) EN TRAMITES, Inscrito en el Servicio Municipal de Administración tributaria de la Alcaldía del Municipio Heres bajo licencia número (EN TRAMITE), Registro de Información Fiscal (RIF) . J-40452736-2, inscrita en el registro único de personas que desarrollan actividades económicas (RUPDAE) y por otra parte, el o la solicitante será denominado EL VENDEDOR.
</p>




<p align="justify">CLAUSULA 2: Sistema de compras Mensuales: LA EMPRESA mediante el presente contrato ofrece a EL VENDEDOR, la oportunidad de comprarle sus desechos con la finalidad de ser reciclados. EL VENDEDOR clasificará los desechos para la venta destinada a Reciclaje de materiales, a LA EMPRESA por tipo de material, seleccionando y separando según sea el tipo de material tomando en cuenta las características siguientes: Materiales Reciclables Ferrosos (chatarra) tales como: <span style="color:#000B3B;">COBRE, BRONCE, ACERO AL CARBONO, ACERO INOXIDABLE, LATÓN, y Materiales Reciclables No Ferrosos tales como: CARTÓN, PAPEL ALUMINIO, VIDRIO, PLÁSTICO, PLOMO, HUESO, ACEITE Y TODOS LOS ELEMENTOS CUYAS CARACTERÍSTICAS PERMITAN SER OBJETO DE RECICLAJE</span> nuevo o usado para ser transformado en productos útiles a adoptar el Sistema Clasificado en su reglón alternativo y apegado en forma planificada la prestación del Servicio a EL VENDEDOR que haya cumplido con lo estipulado en las Cláusulas y condiciones del presente contrato. </p>
<br>



<p align="justify">
CLAUSULA 3: Aceptación de EL VENDEDOR: EL VENDEDOR manifiesta su aceptación en incorporarse al Sistema de LA EMPRESA de compra Mensual para la obtención de los Materiales para Reciclaje ofrecidos por EL VENDEDOR destinados a los Servicios que promueve LA EMPRESA, que se regirá por un cronograma de Ruta establecido, de acuerdo al reporte que hagan los Vendedores (Grupo), ello con la finalidad de tener la garantía de recolectar cargas completas (camiones) en la Ruta del Sector o Parroquia.
</p>

<p align="justify">
MATERIALES: EL VENDEDOR se compromete a clasificar los desechos en cada reglón, la compra mínima de kilos mensuales de Materiales a Reciclar previamente clasificados, será la siguiente:
<br>
<table  border="0" style="font-weight:bold">
<tr>
<td>
<ul>
<li>100 Kgs. HIERRO PESADO </li>
<li>50 Kgs. ALUMINIO LIVIANO</li>
<li>100 Kgs. HIERRO LIVIANO</li>
<li>20 Kgs. COBRE </li>
<li>100 Kgs. CARTON </li>
<li>20 Kgs. BRONCE </li>
<li>100 Kgs. PAPEL </li>
<li>50 Kgs. ANTIMONIO</li>
<li>100 Kgs. VIDRIO LIMPIO</li>
<li>50 Kgs. HIERO COLADO</li>
</ul>
</td>

<td>
<ul>
<li>100 Kgs VIDRIO COLOR </li>
<li>50 Kgs. ACERO INOXIDABLE</li>
<li>100 Kgs. PLASTICO DURO </li>
<li>Und. BATERIA</li>
<li>50 Kgs. PACKAGING o PLASTICO SOPLADO </li>
<li>50 Kgs. PLOMO</li>
<li>50 Kgs. PLASTICO LIVIANO </li>
<li>100 Kgs. HUESO</li>
<li>50 Kgs. ALUMINIO DURO</li> 
<li>200 Ltr. ACEITE</li>
</ul>
</td>
</tr>
</table>
</p>

<p align="justify">En el supuesto de ser menor los kilos de los materiales, EL VENDEDOR tiene la opción de venderlos en el Sitio de Acopio para activar el plan de compensación ofrecido por LA EMPRESA. Los precios, para la compra de los Materiales a Reciclar estará publicado por LA EMPRESA en cada Portal (Oficina Virtual) de su Afiliado.</p>
<br>

<p align="justify">CLAUSULA 4. La integración del Grupo: EL VENDEDOR formará parte de un grupo DE VENDEDORES de sus Productos de Desechos para Reciclar, conformado por integrantes ilimitados en números de personas, por otra parte, deberá realizar el pago para activar el contrato de Inscripción y ventas de los Materiales plasmado en la Cláusula 3 y así poder participar en el evento de asignación del plan de cobranza por sus recomendaciones y tendrá toda la información de su grupo y las cuentas por pagar, cuenta por cobrar, en su Oficina Virtual por el portal www.hombresdenegociosglobalca.com asignado con su código de CONTRATO DE AFILIACIÓN PARA LA COMPRA-VENTA DE MATERIALES APTOS PARA RECICLAJE.</p>


<p align="justify">CLAUSULA 5. Constitución del Grupo: Un grupo se considera constituido cuando hayan sido aceptados por LA EMPRESA. EL VENDEDOR participará en el evento de asignación, como integrante del grupo, siempre que hayan aportado su cuota mensual y estén al día en sus pagos.</p>


<p align="justify">CLAUSULA 6. Precio de los Servicios a Adquirir: Luego de la Contratación de los Servicios a Adquirir, LA EMPRESA abonará el saldo a favor a EL VENDEDOR 15 días después del cierre de mes (los 30 días de cada mes a excepción del mes de febrero), el precio de los materiales para Reciclar será publicado en su Oficina Virtual por el portal www.hombresdenegociosglobalca.com asignado con su código, EL VENDEDOR, la diferencia y ajustes a que hubiere lugar para su venta, será igualmente publicado. EL VENDEDOR beneficiario o beneficiaria deberá cumplir con los requisitos exigidos, EL VENDEDOR acepta que LA EMPRESA, haga publicidad sobre el o los Servicios solicitados con los cuales ha sido beneficiado sin recibir por ello retribución alguna……</p>


<p align="justify">
CLAUSULA 7. EL VENDEDOR se obliga a cumplir sus compromisos y lograr sus objetivos en el Sistema de Ventas mensuales, aportando los siguientes conceptos que integran las ventas mensuales: 7.1. Aporte Neto: que representa la cantidad que EL VENDEDOR aportará como MONTO CONTRATADO para obtener los servicios, el monto será de Tres Mil Bolívares (Bs. 3.000,oo) por punto. 7.2. Costos Operativos: Es la cantidad que paga EL VENDEDOR en función de cubrir los gastos que resultan de las actividades del Servicio que presta LA EMPRESA. El monto será de Tres Mil Bolívares (Bs. 3.000,oo) por punto el cual se ajustará cada 90 días, tomando como referencia la Inflación en nuestro País. 7.3 Penalidad por atraso en el pago de activación de los Servicios: EL VENDEDOR no cobrará el monto que le corresponde por concepto de regalías, derivadas de sus recomendaciones de Afiliados. 7.4. EL VENDEDOR pagará la inscripción y los pagos mensuales de 1 punto por la cantidad de Tres Mil Bolívares (Bs. 3.000,oo). 7.5. EL VENDEDOR se compromete a clasificar los desechos en cada reglón. 7.6. Penalidad (suspensión): Se suspenderá el Servicio por no cumplir con la cantidad estipulada en el presente Contrato para la venta de Un Mil Doscientos (1.200) kilos al año de Materiales apto para Reciclaje entre no metales y metales ferrosos y no ferrosos, será suspendido el contrato (código ID Nº <u> <span style="color:#000B3B;">'.strtoupper($_SESSION["cod_idp"]).'</span> </u>) de manera eliminatoria del sistema de LA EMPRESA.
</p>

<p align="justify">CLAUSULA 8. Duración del Plan: Tendrá una duración de 12 meses y EL VENDEDOR deberá aportar la cantidad de 12 cuotas mensuales y consecutivas, EL VENDEDOR podrá realizar el pago total de las cuotas mensuales, de los gastos administrativos si estuviesen pendientes y hacer el finiquito del contrato. LA EMPRESA realizará la renovación del contrato de manera automática para el siguiente período. Para cancelar el convenio, EL VENDEDOR deberá notificar efectivamente y por escrito con noventa (90) días de anticipación a la fecha de vencimiento del convenio, a través de una carta privada que deberá ser firmada en indicación de recibida, donde indique los motivos o razones que la originaron. En caso contrario se procede a lo anteriormente expuesto la renovación CONTRATO DE AFILIACIÓN PARA SUMINISTRALES PRODUCTOS RECICLAJES e igual LA EMPRESA.</p>

<p align="justify">CLAUSULA 9. Secuencia de pago: Será de una (1) cuota mensual mínima ininterrumpida durante todo el Plan, permitiéndose abonos adelantados para reducir el monto pendiente y la fecha de vencimiento de los pagos será especificada en la solicitud de inscripción, no se aceptarán pagos fraccionados de la cuota mensual. En caso que la fecha de vencimiento de pagos coincida con un día festivo o no laborable, podrá efectuar su pago al siguiente día hábil.</p>


<p align="justify">CLAUSULA10. Opciones de pago de EL VENDEDOR PARA LA EMPRESA: Los pagos de cuotas deberán efectuarse con tarjeta de débito o crédito, puntos de ventas, transferencia electrónica, cheques endosados a nombre de HOMBRES DE NEGOCIOS GLOBAL C.A. o depositadas en las cuentas bancarias girada contra el Banco Mercantil. Cuenta Corriente Nro.0105 0134 21 1134145497. Girada contra el Banco Venezuela Cuenta Corriente Nro.0102 0414 38 0000441096. Girada contra el Banco Venezolano de crédito Cuenta Corriente Nro. 0104 0036 83 0360119874. Cuyo titular es LA EMPRESA, o en las Cuentas Bancarias Autorizadas por LA EMPRESA atendiendo la ubicación geográfica de EL VENDEDOR, las cuales estarán a disposición en la cartelera informativa, o en su página web www.hombresdenegociosglobalca.com o la guía del cliente y en la tarjeta de pago que EL VENDEDOR manifiesta recibir al momento de firmar el presente contrato. El ingreso de estas cantidades se amparará mediante facturas y recibos de pago.</p>


<p align="justify">CLAUSULA 11. Opciones de pago de LA EMPRESA PARA EL VENDEDOR: EL VENDEDOR debe presentar la factura para el cobro de las ventas (cuentas por cobrar) de materiales de reciclaje, LA EMPRESA efectuara el pago en la tarjeta de débito o crédito que le otorgar LA EMPRESA, transferencia electrónica, cheques endosados a nombre del EL VENDEDOR  identificado con el ID código <u> Nº <span style="color:#000B3B;">'.strtoupper($_SESSION["cod_idp"]).'</span> </u>.</p>


<p align="justify">CLAUSULA 12. Evento de Asignación: Mensualmente LA EMPRESA organizará una reunión que contará con la presencia opcional de cada VENDEDOR. En dicho evento sólo podrán participar aquellos VENDEDORES que estén solventes y se encuentre al día con sus pagos mensuales correspondientes. EL VENDEDOR deberá consignar el soporte de sus pago a la oficina principal de HOMBRES DE NEGOCIOS GLOBAL C.A o enviar un e-mail: pagos.hng@gmail.com dejando una copia en su poder como soporte. Los gastos de trámites administrativos y profesionales para la materialización del presente negocio de Servicios corren por cuenta de EL VENDEDOR.</p>


<p align="justify">CLAUSULA 13. El presente Contrato quedará rescindido de pleno derecho si se producen las circunstancias descritas a continuación: a) Terremoto, maremoto, erupciones volcánicas o cualquier otra catástrofe natural; b) Fisión o fusión nuclear, radiaciones ionizantes y contaminación radioactivas; c) Aplicación de la carta Democrática; d) Guerra declarada o no, invasión, guerra civil, revolución o insurrección o cualquier acción tomada por el Gobierno tendiente a combatir o defenderse de tales eventualidades, ante tales hechos fortuitos y de fuerza mayor quedará rescindido el presente Contrato, sin ninguna obligación pendiente para las partes.</p>


<p align="justify">CLAUSULA 14. Atraso de pagos de EL VENDEDOR: Se considerará en atraso, las cuotas totales pagadas con posterioridad a la fecha límite de pagos estipulada en la solicitud de inscripción, las abonadas con valor inferior al establecido en el presente contrato y las abonadas con cheques, o transferencias que el banco emisor no pague por cualquier motivo. EL VENDEDOR acepta que el atraso en los pagos mensuales correspondientes, ocasionará por consiguiente la consecuencia de pérdida definitiva de incentivo y bonos en el mes que ocurra el atraso.</p>


<p align="justify">CLAUSULA 15. Atraso de pagos del EL VENDEDOR: En el supuesto de que EL VENDEDOR dejase de abonar tres (3) cuotas de forma consecutiva, previo análisis del caso LA EMPRESA podrá dejar sin efecto el nexo contractual sin previo aviso y excluir del grupo EL VENDEDOR.</p>


<p align="justify">CLAUSULA 16: NOTIFICACIONES. “LAS PARTES” convienen en que las notificaciones y comunicaciones deben remitirse por escrito con acuse de recibo, a las siguientes direcciones: LA EMPRESA: cruce del paseo Meneses Av. Bolívar, en el Centro Comercial Meneses, Planta Baja Local Numero 4, Parroquia Catedral, Municipio Heres, Ciudad Bolívar, Estado Bolívar. Teléfonos: (0285) 415.03.25 - (0414) 853.79.99 e-mail: avisos.hng@gmail.com. EL VENDEDOR, Parroquia: <u><b>'.$_SESSION["c12"].'</b></u>, Municipio: <u><b>'.$_SESSION["c11"].'</b></u>, Ciudad: <u><b>'.$_SESSION["c10"].'</b></u>, Estado: <u><b>'.$_SESSION["c9"].'</b></u>, Dirección: <u><b>'.$_SESSION["c13"].'</b></u>, Teléfonos: <u><b>'.$_SESSION["c17"].'</b></u>, E-MAIL: <u><b>'.$_SESSION["c19"].'</b></u>.</p>



<p align="justify">CLAUSULA 17: Aceptación de explicación del Sistema: EL VENDEDOR declara haber recibido a satisfacción la asesoría adecuada, oportuna y veraz por el ejecutivo de negocios referente al Sistema de cuentas por pagar mensuales y cuentas por cobrar Mensuales y los precios de pago para la compra de los materiales reciclaje que promueve LA EMPRESA y leído cada una de las cláusulas del presente contrato aceptando las mismas, manifestándolo expresamente con su firma, (electrónica en la página Web) www.hombresdenegociosglobalca.com así mismo acepta que recibió copia de la Información sobre el Servicios, (Electrónica en la página Web),  www.hombresdenegociosglobalca.com Constancia de Explicación del sistema de pagos, Declaración de lectura de contrato, declaración jurada, Tarjeta de control de pagos, Guía del Cliente y una copia impresa del contrato de adhesión para el conocimiento de las condiciones y cláusulas del mismo antes de su suscripción.</p>


<p align="justify">CLAUSULA 18. JURISDICCIÓN PARA DIRIMIR LOS CONFLICTOS: Ambas partes se someten a la Jurisdicción de los Tribunales de Ciudad Bolívar, Municipio Heres del Estado Bolívar. AL FIRMAR, El Solicitante estará asumiendo que leyó y acepto las condiciones descritas en las Cláusulas del presente contrato.</p>





 <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
 <tr>
      <td width="250" colspan="2"  height="'.$espacio.'"  valign="top"></td>
    </tr>
    <tr>
      <td width="250"></td>
      <td width="250" align="center" valign="top">
	   <img src="../pdf/firma.jpg" width="80">
	   </td>
    </tr>
    <tr>
      <td width="250" valign="top" align="center">__________________________</td>
      <td width="250" valign="top" align="center">__________________________</td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center"><b><span id="ccliente2">'.strtoupper($_SESSION["c2"]).'</span><br />C.I.V : <b><span id="cci1">'.$_SESSION["c3"].'</span></b></b> </div></td>
      <td width="250" valign="top"><div align="center"><b>Jos&eacute; Gregorio    Paredes Andrades</b><br /><b>C.I.V: 13.131.611</b> </div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center">COD-ID: <b>'.strtoupper($_SESSION["cod_idp"]).'</b></div>
          <div align="center"><b>HUELLA </b></div></td>
      <td width="250" valign="top"><div align="center"><b>Hombres de Negocios Global C.A</b><br />
        <b>Rif: j-40452736-2</b></div></td>
    </tr>
    <tr>
      <td width="250" rowspan="2"  align="center">
	  <center>
	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  <table width="75" height="87" border="1" cellpadding="0" cellspacing="0" bordercolor="#000000">
          <tr>
            <td height="81">&nbsp;</td>
          </tr>
        </table>      
		</center>
		</td>
      <td width="250" valign="top"><div align="center">&nbsp;</div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center">&nbsp;</div></td>
    </tr>
    <tr>
      <td width="250" valign="top"><div align="center"><b>EL VENDEDOR</b></div>      </td>
      <td width="250" valign="top"><div align="center"><b>LA EMPRESA</b></div>      </td>
    </tr>
  </table>


</body>
</html>
';


//unset($_SESSION['contrato']);

// output the HTML content
$pdf->writeHTML($salida, true, false, true, false, '');


// ---------------------------------------------------------


$archivo="../contratos/".$_SESSION["cod_idp"]."_".$_SESSION["c2"]."_".$_SESSION["c3"].".pdf";

$archivoC=$_SESSION["cod_idp"]."_".$_SESSION["c2"]."_".$_SESSION["c3"].".pdf";



if($_SESSION["accion"]=="guardar"){
	$pdf->Output($archivo,'F'); fileperms($archivo); $_SESSION["acontrato"]=$archivoC;
	//unset($_SESSION["accion"]);
}else{
$pdf->Output($archivo,'I');	
}




//============================================================+
// END OF FILE                                                
//============================================================+


?>