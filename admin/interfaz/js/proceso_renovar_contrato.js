$(function(){
	cerrar_preloader();
	getData();
	$('.pagination li a').on('click', function(){
		event.preventDefault();
		preloader();
        // $('.items').html('<div class="loading"><img src="images/loading.gif" width="70px" height="70px"/><br/>Un momento por favor...</div>');

        var page = $(this).attr('data');		
        var dataString = 'page='+page;
        $.ajax({
            type: "POST",
            url: '../../php/proceso_renovar_contrato.php',
            data: {page:page,opc:4,parametro:null},
            dataType: 'json',
        })
        .done(function(data) {
        	preloader();
        	// console.log(data);
            $('#contenido-tabla').fadeIn(2000).html(data.datos);
            $('.pagination li').removeClass('active');
            $('.pagination li a[data="'+page+'"]').parent().addClass('active');

		})
		.fail(function(e) {
			console.log("error");
			console.log(e);
		})
		.always(function() {
			console.log("complete paginate");
			cerrar_preloader();
			// preloader();
		});
        // return false;
    });

	$('[data-toggle="tooltip"]').tooltip();
});

function getData(opc,parametro,inicio,fin) {
	preloader();
	if (opc == null) {
		opc = 4;
	}
	$.ajax({
		url: '../../php/proceso_renovar_contrato.php',
		type: 'POST',
		data: {opc:opc,parametro:parametro,inicio:inicio,fin:fin},
		dataType: 'json',
	})
	.done(function(data) {
		console.log("success");
		if (data.success == true) {
			$('#contenido-tabla').html(data.datos);
		}
	})
	.fail(function(e) {
		// console.log("error");
		console.log(e);
	})
	.always(function() {
		cerrar_preloader();
	});
}

function preloader() {
	$('.cortina_kyc').show('slow/400/fast');
}
function cerrar_preloader() {
	setTimeout(function(){
		$('.cortina_kyc').hide('slow/400/fast');
	},1500);
}