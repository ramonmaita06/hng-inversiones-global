function abrir_nota(){
	$("#cortina_nota,#ventana_nota").show(500,"linear");
}

function reenviar_nota(asunto,nota,id){
	$("#cortina_nota,#ventana_nota").show(500,"linear");
	$("#asunto").val(asunto);
	$("#nota").val(nota);
}


function filtronoti(opc){

$.post("php/proceso_notificacion.php",{opc:7, noti:opc}
,function(resp){
    $("#notas").html(resp.lista);
    parent.mostrar_notas();
},"json");

}



function mostrar_notas(){
	
	
$("#nnoti").html("<img src='images/cargar6.gif'>");
$("#lnotifica").html("<br><br><br><center><img src='images/cargar6.gif'></center>");


$.post("php/proceso_notificacion.php",{opc:1}
,function(resp){
if(resp.num>0){	
$("#nnotifica").show();
$("#mis_notas").html('<i class="fa fa-bell-o campana" style="font-size:20px; color:#D2C758;"></i>');
$("#nnotifica").html(resp.num);
}else{
	$("#mis_notas").html('<i class="fa fa-bell-o" style="font-size:20px"></i>');
	$("#nnotifica").hide();
}
$("#nnoti").html(resp.nnoti);
$("#lnotifica").html(resp.lista);
},"json");

	}mostrar_notas();
	


function leer_nota(id){
mis_carteras('php/gestion_noti.php');
setTimeout(function(){
$.post("php/proceso_notificacion.php",{opc:4,id:id}
,function(resp){
$("#cuerpo_noti").html(resp.lista);
mostrar_notas_gestion();
},"json");
},1500);
	}



function crear_nota(){
var asunto=$("#asunto").val();
var nota=$("#nota").val();
var para=$("#para").val();


if($("#para").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar cliente que enviara la notificacion", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#para").focus(); },3000); return false;
}


if($("#asunto").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el asunto de la notificación", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#asunto").focus(); },3000); return false;
}

if($("#nota").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "La nota no puede quedar vacia, inserte una nota.", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#nota").focus(); },3000); return false;
}


$.post("php/proceso_notificacion.php",{opc:5,para:para,asunto:asunto, nota:nota}
,function(resp){
	if(resp.salida==1){
	swal("",resp.mensaje,"success");
$("#cortina_nota,#ventana_nota").hide(500,"linear");
$("#asunto,#nota").val("");
	mostrar_notas_gestion();
	}else{swal("",resp.mensaje,"error");}
},"json");

	}	


function borrar_nota(id){


swal({
  title: "¿Esta segur@ que desea borrar la notificación?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
$.post("php/proceso_notificacion.php",{opc:6,id:id}
,function(resp){
	if(resp.salida==1){
	leer_nota_interna(id);
	setTimeout(function(){swal("",resp.mensaje,"success");},1000);
	}else{setTimeout(function(){swal("",resp.mensaje,"error");},1000);}
},"json");
} 
});
	}	

	
	
function mostrar_notas_gestion(){
	
	
$("#notas").html("<br><br><br><center><img src='images/cargar6.gif'></center>");


$.post("php/proceso_notificacion.php",{opc:3}
,function(resp){
$("#slnotas").html(resp.num);
$("#lnotas").html(resp.num1);
$("#notas").html(resp.lista);
parent.mostrar_notas();
},"json");

	}
	
	

function leer_nota_interna(id){

$.post("php/proceso_notificacion.php",{opc:4,id:id}
,function(resp){
	mostrar_notas_gestion();
$("#cuerpo_noti").html(resp.lista);
},"json");

	}