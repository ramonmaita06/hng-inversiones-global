$(document).ready(function() {
/*mostrar titles*/
    $('[data-toggle="tooltip"]').tooltip();  
});



function registrar_recarga(){



var tipot=$("#tipot").val();
var codp=$("#codp").val();
var bancod=$("#bancod").val();
var fechad=$("#DPC_edit2").val();
var numerod=$("#numero").val();
var montod=$("#monto").val();
var montop=$("#montop").val();


if($("#tipot").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el tipo de pago", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#tipot").focus(); },3000); return false;
}

if(tipot=="PRECARGADO"){

if($("#codp").val().length<8){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el codigo precargado", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#codp").focus(); },3000); return false;
}	

}else if(tipot!="PRECARGADO"){
	
if($("#bancod").val()==""){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar el banco donde realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#bancod").focus(); },3000); return false;
}
if($("#DPC_edit2").val()==""){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar la fecha cuando realizó la transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#DPC_edit2").focus(); },3000); return false;
}	
if($("#numero").val().length<6){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe introducir el numero de transacción", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#numero").focus(); },3000); return false;
}	


if($("#monto").val().length<4){ 
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto depositado o transferido", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}	
	
}



swal({
  title: "¿Esta segur@ que desea realizar la recarga?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: false,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
$.post("php/proceso_recargas.php",{opc:1,
tipot:tipot,codp:codp,bancod:bancod,fechad:fechad,
numerod:numerod,montod:montod,montop:montop
}
,function(resp){
	
	if(resp.salida==1){

		if(resp.opcion==1){swal("REGISTRO EXITOSO",resp.mensaje,"success");
		
		}
		else{swal("REGISTRO EXITOSO",resp.mensaje,"success");}	
	recarga_cofre1();
	}
	else{swal("REGISTRO FALLIDO",resp.mensaje,"error"); 	$("#mipie").show(); $("#ant").click();}
	
},"json");
  } else {
    swal("REGISTRO CANCELADO", "", "error");
	$("#mipie").show();
	$("#ant").click();
  }
});

	


}



function recarga_cofre1(){
		$("#cofre").html('<img src="images/cargar1.gif" style="width:20px">');
	$.post("php/planes_proceso_menu.php",{opc:2},function(resp){
/*bonos*/
		$("#cofre").html(resp.cofre);
/*notificaciones*/		
		$("#lnotifica").html(resp.notifica);	
		$("#nnotifica1").html("Tienes "+resp.nnotifica+" notificaciones");	
		$("#nnotifica").html(resp.nnotifica);	
		recarga_cofre();
	},"json");
	}

	



function opciones_recarga(opc){
		$("#mostrar_recarga").html('<center><br><br><br>Buscando...<br><img src="images/cargar1.gif" style="width:20px"></center>');
	$("#form_recargas,.mostrar_recarga").hide(500,"linear");
	
		$("#boton1,#boton2,#boton3").removeClass("btn-warning");
		$("#boton1,#boton2,#boton3").removeClass("btn-default");
		$(".mostrar_recarga,#mostrar_pago,#mostrar_afiliacion").hide(500,"linear");
	if(opc==1){
		$("#opc_recarga").val(opc);
		$("#boton1").addClass("btn-warning");
		$("#boton2,#boton3").addClass("btn-default");
		$("#mostrar_pago").show(500,"linear");
		mostrar_pagos();
		}
	if(opc==2){
		$("#boton2").addClass("btn-warning");
		$("#boton1,#boton3").addClass("btn-default");
		$(".mostrar_recarga").show(500,"linear");
		mostrar_recargas();
		}
	if(opc==3){
		$("#boton3").addClass("btn-warning");
		$("#boton1,#boton2").addClass("btn-default");
		$("#mostrar_afiliacion").show(500,"linear");
		$("#opc_recarga").val(opc);
		mostrar_pagos_afiliacion()
		}
}

	
function mostrar_pagos(){
var servicio=$("#servicio").val();
var banco=$("#banco").val();
	$("#mostrar_pago").html('<center><br><br><br>Buscando...<br><img src="images/cargar1.gif" style="width:20px"></center>');
$.post("php/proceso_recargas.php",{opc:4,servicio:servicio,banco:banco},function(resp){
$("#mostrar_pago").html(resp.listado);	
$('[data-toggle="tooltip"]').tooltip(); 
},"json");	
}mostrar_pagos();


function liberar(n,id,idb,div){
var servicio=$("#servicio").val();
swal({
  title: "¿Esta segur@ que desea realizar la operacion?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
		$("#mostrar_recarga").html('<center><br><br><br>Procesando...<br><img src="images/cargar1.gif" style="width:20px"></center>');
$.post("php/proceso_recargas.php",{opc:5, n:n, id:id, idb:idb,servicio:servicio,divisa:div},function(resp){
	if(resp.salida==1){
		mostrar_pagos(); 
		swal("LIBERACION DE PAGO",resp.mensaje,"success");	
}else{
		swal("LIBERACION DE PAGO",resp.mensaje,"error");	
}
},"json");	  
	  
  }
	else{
	swal("LIBERACION DE PAGO","PROCESO CANCELADO","error");	
		}  
});


}	






function mostrar_pagos_afiliacion(){
var servicio=$("#servicio").val();
var banco=$("#banco").val();
	$("#mostrar_afiliacion").html('<center><br><br><br>Buscando...<br><img src="images/cargar1.gif" style="width:20px"></center>');
$.post("php/proceso_recargas.php",{opc:6,servicio:servicio,banco:banco},function(resp){
$("#mostrar_afiliacion").html(resp.listado);	
$('[data-toggle="tooltip"]').tooltip(); 
},"json");	
}



function liberar_afiliacion(n,id){
servicio=$("#servicio").val();
swal({
  title: "¿Esta segur@ que desea realizar la operacion?",
  text: "",
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {

  
$.post("php/proceso_recargas.php",{opc:7, n:n, id:id,servicio:servicio},function(resp){
if(n==1){
	if(resp.salida==1){
		swal("LIBERACION DE PAGO DE AFILIACION","La liberacion del pago de afiliación fue exitosa. \n\n Se han cargado los bono al portafolio de inversiones.","success");
		mostrar_pagos_afiliacion();	

	}else{
		swal("LIBERACION DE PAGO DE AFILIACION","La operación no fue realizada debido a que el registro del usuario no existe","error");	
}
}else if(n==2){
	if(resp.salida==1){
		swal("LIBERACION DE PAGO DE AFILIACION",resp.mensaje,"success");

			mostrar_pagos_afiliacion();	
$.post("msn/proceso_liberar_pago.php",{opc:2},function(resp1){
		$.post("msn/api.php",{},function(resp2){},"json");
},"json");
	}else{
		swal("LIBERACION DE PAGO DE AFILIACION",resp.mensaje,"error");	
	}		
}

},"json");	    
  }
	else{
	swal("LIBERACION DE PAGO DE AFILIACION","PROCESO CANCELADO","error");	
		}  
});


}	


function portafolio_auto(){
$.post("php/procesos_planes.php",{opc:1,id_plan:1,metodo:"true"}
,function(resp){ },"json");
}









	
	
function mostrar_recargas(){
	$("#mostrar_recarga").html('<center><br><br><br>Buscando... Espere<br><img src="images/cargar1.gif" style="width:20px"></center>');
$.post("php/proceso_recargas.php",{opc:2},function(resp){
$("#mostrar_recarga").html(resp.listado);	
},"json");	
}	
	
	
	
	
	
	
	
	
	
	
function mostrar_recargas_fecha(){
	var fi=$("#DPC_edit0").val();
	var ff=$("#DPC_edit1").val();
	var esta=$("#esta").val();
	$("#mostrar_recarga").html('<center><br><br><br>Buscando...<br><img src="images/cargar1.gif" style="width:20px"></center>');
$.post("php/proceso_recargas.php",{opc:3, fi:fi, ff:ff, esta:esta},function(resp){
$("#mostrar_recarga").html(resp.listado);	
},"json");	
}	
	

	
	
	
	
	
	

function CorreoE(opc){
	
	$.post("php/correo.php",{},function(resp){
		
		
if(opc==1){
		if(resp.salida==1){swal("Envio de Correo","Se ha enviado el contrato de afiliacion a su direccion de correo electronico","success");
		mensajeTexto("Listo",1);}
		else{swal("Envio de Correo","No se pudo enviar el contrato de afiliacion a su direccion de correo electronico","error");}
		}
else{
		if(resp.salida==1){
		
		swal("Envio de Correo","Se ha enviado la informacion de pre-afiliacion a su direccion de correo electronico, y un mensaje de texto","success");
		mensajeTexto("Pre",1);
		}
		else{swal("Envio de Correo","No se pudo enviar la informacion de pre-afiliacion a su direccion de correo electronico.","error");}
		}
		
		//setTimeout(function(){ location.reload();},5000);
	},"json");
		
}



var envios=0;
function mensajeTexto(opc,n){
$.post("../msn/proceso.php",{opc:n, texto:opc},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
		setTimeout(function(){
		if(envios==0){ envios=1; mensajeTexto("Admin",2); }
		else{setTimeout(function(){ location.reload();},2000);}
		},1000);
},"json");	
}



function  metodo_afiliacion(opc){
	if(opc.value=="PRECARGADO"){
	$("#ccodp").show(500,'linear');
	$(".metodo-dt").hide(500,'linear');
	$("#btn_recargar").attr({"disabled":true});
	}else{
	$("#btn_recargar").attr({"disabled":false});	
	$("#ccodp").hide(500,'linear');
	$(".metodo-dt").show(500,'linear');	
	}
}

/*validar codigo precargado*/
function valida_codigo(cod){

	if(cod.value.length<8){return false;}
	$("#codp").attr({"readOnly":true});
$.post("php/valida_CP.php",{cod:cod.value},function(resp){
	if(resp.st==1){
	$("#btn_recargar").attr({"disabled":false});
	$("#codp").attr({"readOnly":true});
	$("#seguir").val(1);
	$("#montop").val(resp.montop);
	alerta("Validación exitosa",resp.msn,3000,false);
	}
	else if(resp.st==2){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==3){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
	else if(resp.st==0){
	$("#codp").attr({"readOnly":false});$("#seguir").val(0);
	alerta("Validación fallida",resp.msn,3000,false);
	}
},"json");
	
}



function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}




