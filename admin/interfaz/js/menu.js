function accesoModulo(opc){
	$("#cuerpo").load(opc);
}

function accesoModuloFrame(opc){
	$("#cuerpo").html('<iframe id="iframe" src="'+opc+'" width="100%" style="height:720px" frameborder="0"></iframe>');
	$("#modalService").modal('hide');
}

function ir_servicio_retiro(opc){
	accesoModulo(opc);
	$("#modalService").modal('hide');
}
