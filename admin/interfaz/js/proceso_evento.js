  function asistir(){
	
		
	var boleto = $("#boletos").val();
	var plural='';
	if(boleto > 1){ plural='s'; }
	swal({
	title: "¿Estas seguro de reservar "+boleto+" Boleto"+plural+"?",
	text: "La reserva de boletos es temporal, por lo que se recomienda realizar la compra lo antes posible.\n Usted puede adquirir hasta 10 boletos.",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar"
	 },

		function(isConfirm){
			if (isConfirm) {
				
				$.post("php/proceso_evento.php",{opcion:1, nboleto:boleto},function(resp){
					
				if(resp.salida == 1){
					setTimeout(function(){	
					swal("¡Hecho!",resp.mensaje,"success");
					},1000);
					
				}else{
					setTimeout(function(){	
					swal("¡Oop!",resp.mensaje,"error");
					},1000);	
				}	
				consultaboleto();
				},"json");

			} 
	});
}


function consultaboleto(){
	
	$.post("php/proceso_evento.php",{opcion:2},function(resp){
		/*mostrar opcion de precompra*/

		if(resp.boletoE == resp.venta || resp.cupos == 0 || resp.cupos == resp.disponible){
			$(".asistir").css({"display":"none"});
		}else{
			$(".asistir").css({"display":"inline"});
		}

		/*mostrar boletos comprados*/
		if(resp.boleto>0){
			$("#mostrarboletos").css({"display":"inline"});
		}else{
			$("#mostrarboletos").css({"display":"none"});
		}
		/*mostrar opcion de compra*/
		if(resp.disponible == 0){
			$("#paracomprar").html('').css({"display":"none"});
		}else{
			var plural='';
			if(resp.disponible>1){plural = 's';}
			$("#paracomprar").html('Usted tiene <label class="label label-success" id="reservado">'+resp.disponible+'</label> boleto'+plural+' reservados.  <b>¿Desea comprarlos?</b> <a href="#" onclick="return comprar()" class="btn btn-sm btn-success"   title="Comprar boletos"><i class="fa fa-shopping-cart"></i> Comprar</a> <a href="#" onclick="return cancelar_comprar()" class="btn btn-sm btn-danger"   title="Cancelar compra de boletos"><i class="fa fa-ban"></i> Cancelar</a>').css({"display":"inline"});
		}
		$("#cupos").html(resp.cupos);
		$("#nbol").html(resp.boleto);
		$("#nbol").html(resp.nboletos);
		$("#precio").html(number_format(resp.precio,2,",","."));
		$("#precio1").html(resp.precio);

	},"json");
	
}
function comprar(){
	var boleto = $("#reservado").html();
	var precio = $("#precio1").html();
	var plural='';
	if(boleto > 1){ plural='s'; }
	swal({
	title: "¿Estas seguro de comprar "+boleto+" Boleto"+plural+"?",
	text: "La compra del cada boleto se le deducirá de su portafolio de inversiones en bolivares.\n El monto a pagar es de "+number_format(eval(parseFloat(precio)*parseFloat(boleto)),2,",",".")+" BsS.",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar" 
},

	function(isConfirm){
		if (isConfirm) {
				
			$.post("php/proceso_evento.php",{opcion:3},function(resp){ 
				if(resp.salida == 1){
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"success"); },1000);
					
				}else{
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"error"); },1000);
					
				}
				consultaboleto();
			},"json");
		}
	});
}

function cancelar_comprar(){
	var boleto = $("#reservado").html();
	var plural='';
	if(boleto > 1){ plural='s'; }
	swal({
	title: "",
	text: "¿Estas seguro de cancelar la compra de "+boleto+" Boleto"+plural+"?",
	type: "info",
	showCancelButton: true,
	closeOnConfirm: false,
	showLoaderOnConfirm: true,
	confirmButtonText: "Si, Acepto",
	cancelButtonText: "No, Cancelar"
   },

	function(isConfirm){
		if (isConfirm) {
				
			$.post("php/proceso_evento.php",{opcion:4,nboleto:boleto},function(resp){ 
				if(resp.salida == 1){
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"success"); },1000);
					
				}else{
					setTimeout(function(){ swal("Notificacion",resp.mensaje,"error"); },1000);
					
				}
				consultaboleto();
			},"json");
		}
	});
}