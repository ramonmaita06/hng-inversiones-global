$(function(){
	cerrar_preloader();
	getData();
	$('.pagination li a').on('click', function(){
		event.preventDefault();
		preloader();
        // $('.items').html('<div class="loading"><img src="images/loading.gif" width="70px" height="70px"/><br/>Un momento por favor...</div>');

        var page = $(this).attr('data');		
        var dataString = 'page='+page;
        $.ajax({
            type: "POST",
            url: '../../php/proceso_verificar_identidad.php',
            data: {page:page,opc:4,parametro:null},
            dataType: 'json',
        })
        .done(function(data) {
        	preloader();
        	// console.log(data);
            $('#contenido-tabla').fadeIn(2000).html(data.datos);
            $('.pagination li').removeClass('active');
            $('.pagination li a[data="'+page+'"]').parent().addClass('active');

		})
		.fail(function(e) {
			console.log("error");
			console.log(e);
		})
		.always(function() {
			console.log("complete paginate");
			cerrar_preloader();
			// preloader();
		});
        // return false;
    });

	$('[data-toggle="tooltip"]').tooltip();

	$(document).on('hover', '.zoom', function(event) {
		$(this).addClass('transition');
	}, function() {
        $(this).removeClass('transition');
    }); 
	$(document).on('click', '#buscarCliente', function(event) {
		event.preventDefault();
		getData(4,$('#InputbuscarCliente').val());
		$('#InputbuscarCliente').val('');
	})

	$(document).on('click', '.ver-detalles', function(event) {
		event.preventDefault();
		preloader();
		var cod_cliente = $(this).data('id');
		$.ajax({
			url: '../../php/proceso_verificar_identidad.php',
			type: 'POST',
			data: {cod_cliente: cod_cliente, opc:1},
			dataType: 'json',
		})
		.done(function(data) {
			if (data.success == true) {
				$('#detalles').html(data.detalles);
				setTimeout(function(){
					$('#modalfoto').modal('show');
				},1000);
			}
		})
		.fail(function(e) {
			console.log("error");
			console.log(e);
		})
		.always(function() {
			cerrar_preloader();
		});
	});
	$(document).on('click', '#aprobar', function(event) {
		event.preventDefault();
		preloader();
		var id = $(this).data('id');
		$.ajax({
			url: '../../php/proceso_verificar_identidad.php',
			type: 'POST',
			data: {id: id, opc:2},
			dataType: 'json',
		})
		.done(function(data) {
			if (data.success == true) {
				$('#modalfoto').modal('hide');
				swal({title: "VERIFICAR IDENTIDAD", text:"Identidad verificada exitosamente." , timer: 4000,  showConfirmButton: true});
			}
		})
		.fail(function(e) {
			console.log(e);
		})
		.always(function() {
			getData();
			cerrar_preloader();	
		});
	});

	$(document).on('click', '#rechazar', function(event) {
		event.preventDefault();
		preloader();
		var id = $(this).data('id');
		$.ajax({
			url: '../../php/proceso_verificar_identidad.php',
			type: 'POST',
			data: {id: id, opc:3},
			dataType: 'json',
		})
		.done(function(data) {
			// console.log("success");
			if (data.success == true) {
				$('#modalfoto').modal('hide');
				swal({title: "VERIFICAR IDENTIDAD", text:"Solicitud rechazada." , timer: 4000,  showConfirmButton: true});
			}
		})
		.fail(function(e) {
			console.log(e.responseText);
			console.log(e);
		})
		.always(function() {
			getData();
			cerrar_preloader();
		});
	});
});

function getData(opc,parametro,inicio,fin) {
	preloader();
	if (opc == null) {
		opc = 4;
	}
	$.ajax({
		url: '../../php/proceso_verificar_identidad.php',
		type: 'POST',
		data: {opc:opc,parametro:parametro,inicio:inicio,fin:fin},
		dataType: 'json',
	})
	.done(function(data) {
		console.log("success");
		if (data.success == true) {
			$('#contenido-tabla').html(data.datos);
		}
	})
	.fail(function(e) {
		// console.log("error");
		console.log(e);
	})
	.always(function() {
		cerrar_preloader();
	});
}

function Dt(table) {
	table.dataTable({ 
		bDestroy: true,
		language: {
			"sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla =(",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            "buttons": {
                "copy": "Copiar",
                "colvis": "Visibilidad"
            }
        }
	});
}

function dDt(table) {
	table.dataTable().fnDestroy();
}

function preloader() {
	$('.cortina_kyc').show('slow/400/fast');
}
function cerrar_preloader() {
	setTimeout(function(){
		$('.cortina_kyc').hide('slow/400/fast');
	},1500);
}