<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html>
<meta charset="utf8">
<head>
<script src="js/referenciar.js"></script>
<style>
.imgr{
    -webkit-animation: 3s rotate linear infinite;
    animation: 3s rotate linear infinite;
    -webkit-transform-origin: 50% 50%;
    transform-origin: 50% 50%;
}
</style>
</head>
<body>
<div>

<div class="panel panel-warning">
<div class="panel-heading text-center"><big><big>REFERENCIAME</big></big></div>
<div class="panel-body">

<center><span class="texto">MI CÓDIGO: </span> <b><?php echo $_SESSION["cod_id"]; ?></b></center>
<br><form onsubmit="return false;">
Referenciar a:
<div class="input-group" style="width:100%">

<span><input type="email" style="height:48px; font-size:20px; width:100%;" id="correos"></span>
<span class="input-group-addon" onclick="return agregar_referencia();"><a class="glyphicon glyphicon-plus btn btn-md btn-success" style="margin:0px"></a> </span>

</div>
<br></form>
Correos: <div id="lcorreos" class="jumbotron" style="overflow-y:auto; overflow-x:auto; height:200px"></div>
<div id="lcorreosH" style="display:none" class="jumbotron"></div>

<br>
<center><button id="referencia" class="btn btn-primary btn-md" onclick="return enviar_referencia()"><i class="fa fa-send imgr"></i> Enviar</button></center>
</div>
</div>

</div>

</body>
</html>