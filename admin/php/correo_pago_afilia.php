<?php session_name("hng"); session_start();
require_once('AttachMailer.php'); 

$para=$reg["correo"];
$asunto="GESTION DE PAGOS HNG-".$_SESSION['servicio']."-ADMINISTRATIVO";
$mensaje="
<center>
<img style='width:320px;' src='https://hombresdenegociosglobalca.com/".strtolower($_SESSION['servicio'])."/images/logo_oficial.png'>
</center>
<br>";

if($_POST["n"]==1){
$mensaje.="<div style='font-size:14px;' align='justify'>Estimad@ sr(a) <b>".strtoupper($reg["nombre"])." ".strtoupper($reg["apellido"]).",</b> nos complace en informarle que su pago fue <span style='color:green'><b>PROCESADO Y LIBERADO</b></span> exitosamente.<br><br>Hemos recargado a su portafolio de bonos la cantidad de <b>".number_format($reg1["monto"],2,",",".")." Bonos.</b>";
}

else if($_POST["n"]==2){
$mensaje.="<div style='font-size:14px;' align='justify'>Estimad@ sr(a) <b>".strtoupper($reg["nombre"])." ".strtoupper($reg["apellido"]).",</b> por medio de la presente nos permitimos informarle que su pago fue <span style='color:red'><b>RECHAZADO</b></span> por inconsistencia de la informacion suministrada.<br><br>Lo invitamos a verificar y suministrar nuevamente la informacion del pago.";
}


$mensaje.="<br><br>
<h2><b>RESUMEN DE LA OPERACION</b></h2><br>
BANCO: <b>".strtoupper($reg["bancod"])."</b><br>
N&deg; DE OPERACION: <b>".strtoupper($reg["noperacion"])."</b><br>
MONTO: <b>".number_format($reg["monto"],2,",",".")."</b><br>
FECHA DE LA OPERACION: <b>".cambiar_fecha_es($reg["fechad"])."</b>
<br>
<hr>
<center>
<br>
<a style='text-decoration:none' href='https://hombresdenegociosglobalca.com/".strtolower($_SESSION['servicio'])."/'><img style='width:200px;' src='https://hombresdenegociosglobalca.com/".strtolower($_SESSION['servicio'])."/images/panel.png' title='Ir al panel de acceso'></a>
";


$mailer = new AttachMailer("HNG C.A. <avisos.hng@gmail.com>", $para, $asunto, $mensaje);
$mailer->send();

?>