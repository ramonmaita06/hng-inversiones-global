<?php session_name("hng"); session_start();
require_once('AttachMailer.php'); 
require_once('funciones.php'); 

//$para=$reg["correo"];
$para="deydeco@gmail.com";
$asunto="GESTION DE PAGOS HNG-".$_SESSION['servicio']."-ADMIN";
$mensaje="
<center>
<img style='width:320px;' src='https://hombresdenegociosglobalca.com/inversiones/images/logo_oficial.png'>
</center>
<br>";

if($_POST["n"]==1){
$mensaje.="<div style='font-size:14px;' align='justify'>Estimad@ sr(a) <b>".strtoupper($reg["nombre"])." ".strtoupper($reg["apellido"]).",</b> nos complace en informarle que su pago fue <span style='color:green'><b>PROCESADO Y LIBERADO</b></span> exitosamente.<br><br>Hemos recargado a su portafolio de bonos la cantidad de <b>".number_format($reg1["monto"],2,",",".")." Bonos.</b>";
}

else if($_POST["n"]==2){
$mensaje.="<div style='font-size:14px;' align='justify'>Estimad@ sr(a) <b>".strtoupper($reg["nombre"])." ".strtoupper($reg["apellido"]).",</b> por medio de la presente nos permitimos informarle que su pago fue <span style='color:red'><b>RECHAZADO</b></span> por inconsistencia de la informacion suministrada.<br><br>Lo invitamos a verificar y suministrar nuevamente la informacion del pago.";
}


$mensaje.="<br><br>
<h2><b>RESUMEN DE LA OPERACION</b></h2><br>
BANCO: <b>".strtoupper($reg1["banco"])."</b><br>
N&deg; DE OPERACION: <b>".strtoupper($reg1["referencia"])."</b><br>
MONTO: <b>".number_format($reg1["monto"],2,",",".")."</b><br>
FECHA DE LA OPERACION: <b>".cambiar_fecha_es($reg1["fecha"])."</b>
<br>
<hr>
<center>
<br>
<a style='text-decoration:none' href='https://hombresdenegociosglobalca.com/".strtolower($_SESSION['servicio'])."'><img style='width:200px;' src='https://hombresdenegociosglobalca.com/".strtolower($_SESSION['servicio'])."/images/panel.png' title='Ir al panel de acceso'></a>
";


$mensaje.='<center>
<h3><b>CUENTAS BANCARIAS JURIDICAS</b></h3>
<br>
<div style="border-bottom:1px grey solid; width:360px">
<img src="https://hombresdenegociosglobalca.com/inversiones/images/bv.jpg" style="width:120px">
<br>
BANCO DE VENEZUELA<bR>
HOMBRES DE NEGOCIOS GLOBAL C.A<bR>
RIF: J-40452736-2<bR>
CUENTA CORRIENTE<bR>
<b>N&deg; 01020414380000441069</b><bR>
pagos.hng@gmail.com <bR>
<span style="color:blue">Solo Depositos y Transferencias</span>
</div>
<br>
<div style="border-bottom:1px grey solid; width:360px">
<img src="https://hombresdenegociosglobalca.com/inversiones/images/mercantil.jpg" style="width:120px">
<br>
BANCO MERCANTIL<bR>
HOMBRES DE NEGOCIOS GLOBAL C.A<bR>
RIF: J-40452736-2<bR>
CUENTA CORRIENTE<bR>
<b>N&deg; 01050134211134145497</b><bR>
pagos.hng@gmail.com <bR>
<span style="color:blue">Solo Depositos y Transferencias</span>
</div>
<br>

<div style="border-bottom:1px grey solid; width:360px">
<img src="https://hombresdenegociosglobalca.com/inversiones/images/bvc.png" style="width:120px">
<br>
BANCO VENEZOLANO DE CREDITO<bR>
HOMBRES DE NEGOCIOS GLOBAL C.A<bR>
RIF: J-40452736-2<bR>
CUENTA CORRIENTE<bR>
<b>N&deg; 01040036830360119874</b><bR>
pagos.hng@gmail.com <bR>
<span style="color:blue">Solo Depositos y Transferencias</span>
</div>
<br>

<div style="border-bottom:1px grey solid; width:360px">
<img src="https://hombresdenegociosglobalca.com/inversiones/images/provincial.jpg" style="width:120px">
<br>
BANCO PROVINCIAL<bR>
HOMBRES DE NEGOCIOS GLOBAL C.A<bR>
RIF: J-40452736-2<bR>
CUENTA CORRIENTE<bR>
<b>N&deg; 01080165660100032528</b><bR>
pagos.hng@gmail.com <bR>
<span style="color:blue">Solo Depositos y Transferencias</span>
</div>
</center>';


$mailer = new AttachMailer("HNG C.A. <avisos.hng@gmail.com>", $para, $asunto, $mensaje);
$mailer->send();

?>