<?php session_name("hngadmin"); session_start();
date_default_timezone_set('America/Caracas');

include("../php/cnxI.php");


/**
 * 
 */
class VerificarIdentidad
{
	public function GetData($parametro)
	{
		if($parametro == null){
			$page = (isset($_POST['page']))? $_POST['page'] : 1;
			$rowsPerPage = 100;
			// $page = 1;
			$offset = ($page - 1) * $rowsPerPage;
			//echo "SELECT * FROM verificar_identidad where estatus = 1  order by fecha asc limit $offset, $rowsPerPage ";
			$sql = mysql_query("SELECT * FROM verificar_identidad where estatus = 1  AND id_admin = '".$_SESSION['id_u']."'  order by fecha asc limit $offset, $rowsPerPage ");
			
		}else{
			$sql = mysql_query("SELECT * FROM verificar_identidad where cod_cliente LIKE '%$parametro%'  AND id_admin = '".$_SESSION['id_u']."' order by fecha asc limit 50 ");
		}
		if (mysql_num_rows($sql)>0) {
			while ($row = mysql_fetch_array($sql)) {
				if($row['estatus'] == 1){
				 	$estatus = '<span class="label label-info"><i class="fa  fa-info-circle"></i> EN EVALUACION</span>';
				}
				if ($row['estatus'] == 2) {
				  $estatus = '<span class="label label-danger"><i class="fa fa-ban"></i> RECHAZADA</span>' ;
			  	}
			  	if($row['estatus'] == 0){
				 	$estatus = '<span class="label label-success"><i class="fa fa-check-circle"></i> VERIFICADA</span>';
				}  
				$html .= "<tr><td>".date_format(date_create($row['fecha']),"d/m/Y")."</td>
					<td>".$row['cod_cliente']."</td>
					<td>".$estatus."</td>
					<td><button class='btn btn-sm btn-primary ver-detalles'   data-id='".$row['cod_cliente']."'><i class='fa fa-eye'></i></button></td></tr>"	;
			}
		}else{
			$html.='<tr>
				<td colspan="4" aling="center" class="text-center">No se han encontrado resultados.</td>
			</tr>';
		}

		echo json_encode(array('success' => true, 'datos' =>$html ));
	}
	
	public function verDetalles()
	{
		$cons = mysql_query("SELECT * FROM verificar_identidad WHERE cod_cliente = '".$_POST['cod_cliente']."'  AND id_admin = '".$_SESSION['id_u']."' ");
		if (mysql_num_rows($cons) > 0 ) {
			while ($detalles = mysql_fetch_array($cons)) {
				$cons_c = mysql_query("SELECT * FROM cliente WHERE cod_id = '".$_POST['cod_cliente']."'");
				if (mysql_num_rows($cons_c) > 0) {
					while ($detalles_c = mysql_fetch_array($cons_c)) {
						$html = '<div class="container-fluid">
								<div class="row">
									<div class="row">
										<div class="col-md-12 col-sm-12 col-xs-12">
											<table width="100%"  class="table table-bordered table-strippedtablas">
												<thead class="text-center">
													<tr>
														<th>Cod. Cliente</th>
														<th style="text-aling: center;">ID</th>
														<th>Nombres</th>
														<th>Apellidos</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td>'.$detalles_c["cod_id"].'</td>
														<td>'.$detalles_c["cedula"].'</td>
														<td>'.$detalles_c["nombre"].'</td>
														<td>'.$detalles_c["apellido"].'</td>
													</tr>
													<tr>
														<th>Fecha de Nacimiento</th>
														<th>Sexo</th>
														<th>Pais de Nacimiento</th>
														<th>Pais de Residencia</th>
													</tr>
													<tr>
														<td>'.date_format(date_create($detalles_c["fechan"]),'d/m/Y').'</td>
														<td>'.$detalles_c["sexo"].'</td>
														<td>'.$detalles_c["nacion"].'</td>
														<td>'.$detalles_c["pais"].'</td>
													</tr>
												</tbody>
											</table>
										</div>	
									</div>';
									if($detalles['estatus'] != 0 && $detalles['estatus'] != 2){

									$html .= '<div class="col-md-4 col-sm-4 col-xs-3">
										<button class="btn btn-success adaptame" data-id="'.$detalles['id'].'" id="aprobar" data-toggle="tooltip" data-placement="bottom" data-original-title="KYC"  style="margin-top:50%;float:left;"><i class="fa fa-thumbs-up"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Aprobar <br>&nbsp;</span></button>
									</div>';
									}else{
										$html.='<div class="col-md-4 col-sm-4 col-xs-3"></div>';
									}
									$html .= '<div class="col-md-4 col-sm-4 col-xs-6">
										<img src="../../../inversiones/'.substr($detalles["foto"],3).'" alt="" width="" height="" class="img-responsive img-thumbnail zoom">
									</div>';
									if($detalles['estatus'] != 0 && $detalles['estatus'] != 2){
										$html.='<div class="col-md-4 col-sm-4 col-xs-3">
											<button class="btn btn-danger adaptame" data-id="'.$detalles['id'].'" id="rechazar" data-toggle="tooltip" data-placement="bottom" data-original-title="KYC"  style="margin-top:50%;float:right;"><i class="fa fa-thumbs-down"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Rechazar <br>&nbsp;</span></button>
										</div>';
									}else{
										$html.='<div class="col-md-4 col-sm-4 col-xs-3"></div>';
									}
									$html.='
								</div>
							</div>';
					}

					echo json_encode(array('success' => true, 'detalles' => $html));
				}
			}
		}
	}

	public function aprobar()
	{
		$cons = mysql_query("UPDATE verificar_identidad SET estatus=0 WHERE id='".$_POST['id']."'   AND id_admin = '".$_SESSION['id_u']."'");
		if (mysql_affected_rows()>0) {
			echo json_encode(array('success' => true));
		}
	}
	public function rechazar($id)
	{
		$cons = mysql_query("UPDATE verificar_identidad SET estatus=2 WHERE id=$id  AND id_admin = '".$_SESSION['id_u']."'");
		if (mysql_affected_rows()>0) {
			echo json_encode(array('success' => true));
		}
	}
}

$obj = new VerificarIdentidad;
if (isset($_POST['opc'])) {
	if ($_POST['opc'] == 1) {
		$obj->verDetalles();
	}
	if ($_POST['opc'] == 2) {
		$obj->aprobar();
	}
	if ($_POST['opc'] == 3) {
		$obj->rechazar($_POST['id']);
	}
	if ($_POST['opc'] == 4) {
		($_POST['parametro'] == null) ? $obj->GetData(null) : $obj->GetData($_POST['parametro']);
		
	}
}

?>