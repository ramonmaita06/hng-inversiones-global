<?php session_name("hng"); session_start(); 
if(isset($_SESSION["us"])){
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
   <link href="../css/sweetalert.css" rel="stylesheet">
    <link href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style_inversion.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	    <!-- jQuery 2.1.3 -->
    <!--Archivos de calendario-->
	<script src="../js/js_hng/src/js/jscal2.js"></script>
    <script src="../js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="../js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  </head>
  <body name="tope">
  
  
<div class="container">
  <nav class="nav">
  <center>
  <div id="ganancias" title="Ganancia de Inversion" onclick="return cofre()" style="display:none; cursor:pointer"><i class="fa fa-money"></i> 0,00</div>
  
  <input type="hidden" id="ganancias1">
  <br>
  <!--<button onclick="return opcionINV(1);" class="btn btn-success" title="Nueva Inversion"><i class=" fa fa-plus" style="font-size:24px"></i></button>-->
  <button onclick="return opcionINV(3);"  class="btn btn-danger" title="Historial de Inversiones"><i class="fa fa-clock-o" style="font-size:24px"></i></button>
  <button onclick="return opcionINV(4);"  class="btn btn-primary" title="Estado de Inversiones"><i class="fa fa-list-alt" style="font-size:24px"></i></button>
  <button onclick="return opcionINV(5);"  class="btn btn-info" title="Liberaciones de Inversiones"><i class="fa fa-book" style="font-size:24px"></i></button>
  <!--<button onclick="return opcionINV(6);"  class="btn btn-warning" title="Gestión de bonos de inversión"><i class="fa fa-money" style="font-size:24px"></i></button>-->
  </center>
 </nav>
 <hr>
 <br>


     
 
 
 
 <div id="cortina"></div>
<div id="detalles"></div>

 <div  id="pbusqueda" class="panel panel-warning"> 
<div class="panel-heading">
 <div class="row" > 

      <div class="col-sm12 col-lg-12">

        <div class="form-group">
		<div class="col-md-3" align="center">
		<label>N&deg; Referencia</label>
	<input class="form-control" onkeyup="return buscar_ref(this)" onclick="return buscar_ref(this)" type="search" id="refer">
		</div>
		<div class="col-md-3" align="center">
		<label>Desde</label>
	<input class="form-control" placeholder="dd/mm/aaaa" type="text" id="fci">
			 <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fci",
        trigger    : "fci",
        onSelect   : function() { this.hide()
		},
       // showTime   : 12,
         dateFormat : "%d/%m/%Y"
		  });//]]>
		  </script>
		</div>
		<div class="col-md-3" align="center">
		<label>Hasta</label>
	<input class="form-control" placeholder="dd/mm/aaaa" type="text" id="fcf">
				 <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "fcf",
        trigger    : "fcf",
        onSelect   : function() { this.hide()
		},
       // showTime   : 12,
        dateFormat : "%d/%m/%Y"
		  });//]]>
		  </script>
		</div>
		<div class="col-md-3" align="center">
<button class="btn btn-primary btn-lg" onclick="return buscar_fecha_ref()"><i class="fa fa-search"></i></button>
		</div>
       </div>
       </div>

		</div>
	</div>
     </div> 
  
<div id="titulomodulo" align="center"></div>
 
 
 <div id="cuerpoinfo"  style="width:100%;"><center><br><br><img src="../images/inversionista1.png"></center></div>
 
</div>
  
  
  
  
	<script src="../js/jq.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
 	<script src="../js/sweetalert.min.js"></script>
    <script src="../js/sweetalert.dev.js"></script>
    <script src="../js/wow.min.js"></script>
	<script src="../js/number_format.js"></script>
 	<script src="js/procesos.js"></script>
<script>parent.cerrar_carga();</script>
  </body>
</html>
<?php
}else{	echo "<script>location.href='../';</script>";} 
 ?>