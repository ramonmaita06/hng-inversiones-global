<?php session_name("hng"); session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/jquery.js"></script>
	<script src="../js/sweetalert.min.js"></script>
	<script src="js/proceso_liberar_bono.js"></script>
	 <link href="../css/sweetalert.css" rel="stylesheet">
	<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" /> 
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
</head>
<body>
	<div class="panel panel-success">
		<div class="panel panel-heading">
			<center><b>LIBERACION DE BONOS</b></center>
			<br>
			
				<div class="row">
					<div class="form-group col-md-12">
						<div class="col-md-8" id="listado_cliente">
						</div>
						<div class="col-md-2">
							<select id="divisa" class="form-control" style="width:auto">
								<option value="BsS">BsS</option>
								<option value="USD$">USD$</option>
							</select>
						</div>
						<div class="col-md-2">
							<button class="btn btn-success fa fa-search" onclick="listar_bonos()"></button>
						</div>
					</div>
				</div>
			
		</div>
		<div class="panel panel-body" id="listado">

		</div>
	</div>
</body>
</html>