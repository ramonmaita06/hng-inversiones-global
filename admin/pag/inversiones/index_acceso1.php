<?php session_name("hng"); session_start();

include("../php/cnxI.php");
include("php/funciones.php");
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>HNG-INVERSIONES</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
   <link href="../css/sweetalert.css" rel="stylesheet">
    <link href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style_inversion.css" rel="stylesheet" type="text/css" />

 </head>
  <body name="tope">
  <div class="panel panel-success">
<div class="panel-heading"><b>CLIENTES  HNG-INVERSIONES</b></div>
</div>
  <?php 

  
function cofre_general($id_c,$cod_id){
$cons=mysql_query("select sum(monto) as cofre from mis_depositos where id_c='".$id_c."' AND estatus='1' AND tipot!='BONO PRECARGADO'");
	$reg=mysql_fetch_array($cons);

$cons0=mysql_query("select sum(monto) as transferencias from mis_depositos where id_c='".$id_c."' AND estatus='2' AND tipot!='BONO PRECARGADO'");	
	$reg0=mysql_fetch_array($cons0);
$cons1=mysql_query("select sum(montop) as pagos from pagos where cod_id='".$cod_id."' AND estado='1'");
	$reg1=mysql_fetch_array($cons1);
$cons2=mysql_query("select sum(montop) as pagos from pagoshng where cod_id='".$cod_id."' AND estado='1'");
	$reg2=mysql_fetch_array($cons2);

	
$cons3=mysql_query("select sum(monto) as bonos from mis_depositos where id_c='".$id_c."' AND estatus='2' AND tipot='BONO PRECARGADO'");	
$reg3=mysql_fetch_array($cons3);

$cons4=mysql_query("select sum(monto_pi) as bonos from periodo_inversion where cod_id='".$cod_id."' AND estatus_pi=1");	
$reg4=mysql_fetch_array($cons4);

	
$micofre=(($reg["cofre"]+$reg3["bonos"]+$reg4["bonos"])-($reg0["transferencias"]+$reg1["pagos"]+$reg2["pagos"]));

	return $micofre;
}  
  
  
  
$cons=mysql_query("select * from cliente order by cod_id asc");
$lista='<table class="table table-striped table-hover table-bordered"><tr><th style="width:120px">CODIGO</th><th>NOMBRES Y APELLIDOS</th><th>CEDULA</th><th>CORREO</th><th>COFRE</th><th colspan="3">OPERACIONES</th></tr>';
$i=1;
$monto=0;
$mostrar='';
$mostrar1='';
$n=0;
$sel='';

while($info=mysql_fetch_array($cons)){

	$us=$info['correo'];
	$idc=$info['id_c'];
	$cons1=mysql_query("select distinct estatus from mis_retiros where id_c='".$info['id_c']."' AND estatus!=1");
if(mysql_num_rows($cons1)>0){	
$cons01=mysql_query("select count(estatus) AS estatus from mis_retiros where id_c='".$info['id_c']."' AND estatus=1");
$info01=mysql_fetch_array($cons01);
	if($info01['estatus']>0){$mt='none';}else{$mt='inline';}
	$sel='<table style="width:100%; " class=""><td><select  id="retiro'.$n.'" class="form-control">';
	while($info1=mysql_fetch_array($cons1)){
	if($info1['estatus']==1){$sel.=''; }else{

	if($info1['estatus']==0){	
	$cons2=mysql_query("select SUM(monto) AS monto from mis_retiros where id_c='".$info['id_c']."' AND estatus=0");
	$info2=mysql_fetch_array($cons2);
	$monto=masmenos($info2['monto']);
	$sel.='<option value="Retiro">Retiro: '.$monto.'</option>';  
	}
	else if($info1['estatus']==2){
	$cons2=mysql_query("select SUM(monto) AS monto from mis_retiros where id_c='".$info['id_c']."' AND estatus=2");
	$info2=mysql_fetch_array($cons2);
	$monto=masmenos($info2['monto']);
	$sel.='<option value="Confirma">Confirmacion: '.$monto.'</option>';
	}else{$sel=''; }

	}

	}
	$sel.='</select></td><td>
	
	<!--<button class="btn btn-info btn-md" onclick="return confirmar_retiro('.$n.','.$idc.');"><i class="fa fa-check"></i></button>-->
	
	<a href="http://inversiones.hombresdenegociosglobalca.com/php/proceso_pago_admin.php?us='.$us.'"  id="pagar'.$n.'"   class="btn btn-primary" title="Gestionar pago del cliente"><i class="fa fa-credit-card"></i> Pagar</a>
	
	</td></table>	

<!--<a href="http://transfer.hombresdenegociosglobalca.com/app/php/proceso_admin.php?us='.$us.'"  id="pagar'.$n.'"  style="display:none" class="btn btn-warning" title="Ver cuenta del cliente"><i class="fa fa-sign-in"></i> ir'.$n.'</a>-->
';


$n++;
}else{$sel=''; }

	
$lista.='<tr style="font-size:11px">
<td><a href="#" class="btn btn-info"><b>'.$info['cod_id'].'</b></a></td>
<td>'.$info['nombre'].' '.$info['apellido'].'</td>
<td>'.$info['cedula'].'</td>
<td>'.$info['correo'].'</td>
<td style="color: green;" align="right"><a href="#" class="btn btn-success"><b><i class="fa fa-money"></i> '.masmenos(cofre_general($info['id_c'],$info['cod_id'])).'</b></a></td>
<!--<td><a href="http://inversiones.hombresdenegociosglobalca.com/php/proceso_pago_admin.php?us='.$us.'"  target="1" class="btn btn-success" title="Pagar monto de retiro del cliente" style="display:'.$mostrar.';"><i class="fa fa-money"></i>  Pagar '.$monto.' bs.</a></td>

<td><a href="http://inversiones.hombresdenegociosglobalca.com/php/proceso_admin.php?us='.$us.'"  target="1" class="btn btn-warning" title="Ver cuenta del cliente"><i class="fa fa-sign-in"></i> Ver</a></td>-->

<td align="left">'.$sel.'</td>
<td><a href="http://inversiones.hombresdenegociosglobalca.com/php/proceso_admin.php?us='.$us.'"   target="1" class="btn btn-warning" title="Ver cuenta del cliente"><i class="fa fa-sign-in"></i> Ver</a></td>
</tr>';	
}
$lista.='</table>';

echo $lista;

 ?>
 
	<script src="../js/jq.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>    
    <script src="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
 	<script src="../js/sweetalert.min.js"></script>
    <script src="../js/sweetalert.dev.js"></script>
    <script src="../js/wow.min.js"></script>
	<script src="../js/number_format.js"></script>
 	<!--<script src="../inversiones/js/procesos.js"></script>-->
 	<script src="http://inversiones.hombresdenegociosglobalca.com/js/procesos.js"></script>
<script>parent.cerrar_carga();</script>
  </body>
  </html>