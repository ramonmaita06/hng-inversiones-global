function listar_clientes(){
	$.post("php/proceso_liberar_bono.php",{opc:2},function(resp){
		$("#listado_cliente").html(resp.listado);
	},"json");
}listar_clientes();

function listar_bonos(){
	var id_c = $("#id_c").val();
	var divisa = $("#divisa").val();
	$.post("php/proceso_liberar_bono.php",{opc:1, id_c:id_c, divisa:divisa},function(resp){
		$("#listado").html(resp.listado);
	},"json");
}

function liberar_bono(id, info){

swal({
  title: "¿Esta segur@ que desea liberar este bono?",
  text: info,
  type: "info",
  showCancelButton: true,
  closeOnConfirm: false,
  showLoaderOnConfirm: true,
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar"
},
function(isConfirm){
  if (isConfirm) {
		var divisa = $("#divisa").val();
			$.post("php/proceso_liberar_bono.php",{opc:3, id:id, divisa:divisa},function(resp){
				listar_bonos();
				//listar_clientes();
				swal("liberacion exitosa.","","success");
			},"json");
		}
	});
}