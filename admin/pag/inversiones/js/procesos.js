function imprimir(){
	$("nav,#pbusqueda,#bimprimir").hide();
	$("#totalg").css({"position":"relative"});
	print();
	$("nav,#pbusqueda,#bimprimir").show();
	$("#totalg").css({"position":"fixed"});
}


function cofre(){


$("#ganancias").html('<center><img src="../images/cargar.gif" style="width:18px"> Espere un momento...</center>');
	$.post("php/proceso.php",{opc:0},function(resp){
		
	$("#ganancias").html('<i class="fa fa-money"></i> '+resp.monto);
	$("#ganancias1").val(resp.monto1);
	},"json");
	
}


function lista_cofres(){
	$.post("php/proceso.php",{opc:4},function(resp){
	$("#portafolio1").html(resp.listado);
	$("#portafolio2").html(resp.listado);
	$("#montoportafolio1").val(resp.monto1);
	$("#montoportafolio2").val(resp.monto2);
	},"json");	
}



function preparar_tranferencia(){
	var ptf=document.getElementById("portafolio1").value;
	if(ptf==1){document.getElementById("portafolio2").value=2;}
	else if(ptf==2){document.getElementById("portafolio2").value=1;}
}


function transferir(){
	var monto=$("#montoinv").val();
	var ptf=$("#portafolio1").val();
	var montoptf1=$("#montoportafolio1").val();
	var montoptf2=$("#montoportafolio2").val();
	
	if((monto=="")||(monto<=0)){
		alerta("Notificación","Debe introducir un monto valido",5000,"success"); return false;
	}
	
	if(ptf==1){
	if(parseFloat(monto)>parseFloat(montoptf1)){
		alerta("Notificación","El monto introducido no debe ser mayor al monto de origen...",5000,"success"); return false;
	}}
	else if(ptf==2){
	if(parseFloat(monto)>parseFloat(montoptf2)){
		alerta("Notificación","El monto introducido no debe ser mayor al monto de origen.",5000,"success"); return false;
	}}	
	
	
swal({
  title: "¿Esta segur@ que desea realizar la transferencia?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
	
  if (isConfirm) {	
	$.post("php/proceso.php",{opc:5,ptf:ptf,monto:monto},function(resp){
	$("#montoinv").val("");
	//alerta("Notificación",resp.msn,5000,"success");
	swal("Notificación",resp.msn,"success");
	parent.menu_planes2();
	cofre();
	lista_cofres();
	setTimeout(function(){preparar_tranferencia();},1000);
	},"json");	
	
  }else {
    swal("PROCESO CANCELADO", "", "error");
  }
  
});	
	
}


var modulo_inv=1;
function mis_inversiones(){
modulo_inv=1;
cofre();
$("#titulomodulo").html("<code>MIS INVERSIONES</code>");
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar.gif" style="width:120px"><br>Espere un momento...</center>');
	$.post("php/proceso.php",{opc:1,estatus:1},function(resp){
/*bonos*/
		$("#cuerpoinfo").html(resp.listado);
	},"json");

}mis_inversiones();


function liberaciones_inversiones(){

$("#titulomodulo").html("<code>LIBERACION DE INVERSIONES</code>");
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar.gif" style="width:120px"><br>Espere un momento...</center>');
	$.post("php/proceso.php",{opc:1,estatus:2},function(resp){
/*bonos*/
		$("#cuerpoinfo").html(resp.listado);
	},"json");

}

function opcionINV(opc){
	$("#cortina,#recargar,#invertir").hide(100,"linear");
	if(opc==1){
	$("#ni").val(0);
	$("#monto").attr({"readOnly":false});
	$("#duracion").val("");	
	$("#monto").val("");
	$("#cortina,#invertir").show(100,"linear");}
	if(opc==2){$("#ni").val(1); $("#monto").attr({"readOnly":true}); $("#cortina,#invertir").show(100,"linear");}
	if(opc==3){historial_inversiones();}
	if(opc==4){mis_inversiones();}
	if(opc==5){liberaciones_inversiones();}
	if(opc==6){
		lista_cofres();
		$("#btnt").html("Espere...");
		$("#btntranferir").attr({"disabled":true});
		$("#cortina,#recargar").show(100,"linear");
		setTimeout(function(){
			preparar_tranferencia(); 
		$("#btntranferir").attr({"disabled":false});
		$("#btnt").html("Transferir");
		},1000); 
		
		}
}

function reinvertir(monto,id_inv){
$("#duracion").val();	
$("#monto").val(monto);	
$("#id_inv").val(id_inv);	
}


var hasta, actual, interes=0;
function calculo_inv(){
	var monto=$("#monto").val();
	var dura=$("#duracion").val();

	interes=eval((monto*dura)/100);
	$("#interes").html(number_format(interes,2,",","."));
	periodo_inv();

}

function periodo_inv(){
var dura=$("#duracion").val();	


 milisegundos=parseInt(dura*24*60*60*1000);
 fecha=new Date();
 dia=fecha.getDate();
 mes=fecha.getMonth()+1;
 anio=fecha.getFullYear();
 
if(dia<10){
    dia='0'+dia;
} 
if(mes<10){
    mes='0'+mes;
}
actual=dia+"/"+mes+"/"+anio;


  
 tiempo=fecha.getTime();
 total=fecha.setTime(parseInt(tiempo+milisegundos));
 dia=fecha.getDate();
 mes=fecha.getMonth()+1;
 anio=fecha.getFullYear();
if(dia<10){
    dia='0'+dia;
} 
if(mes<10){
    mes='0'+mes;
}
hasta=dia+"/"+mes+"/"+anio;

$("#periodo").html("Desde <b>"+actual+"</b> Hasta <b>"+hasta+"</b>");
}



function invertir(){
	var ni=$("#ni").val();
	var id_inv=$("#id_inv").val();
	var monto=$("#monto").val();
	var dura=$("#duracion").val();


if($("#monto").val().length<3){
swal({title: "COMPLETAR INFORMACION", text: "Debe agregar el monto a invertir", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}
if($("#duracion").val()==""){
swal({title: "COMPLETAR INFORMACION", text: "Debe seleccionar los dias a inversión", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#duracion").focus(); },3000); return false;
}

if(ni==0){
if(parseFloat($("#monto").val())>parseFloat($("#ganancias1").val())){
swal({title: "COMPLETAR INFORMACION", text: "La inversión no puede ser mayor al cofre de inversiones", timer: 2000,  showConfirmButton: false});setTimeout(function(){$("#monto").focus(); },3000); return false;
}}


swal({
  title: "¿Desea registrar la inversion?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {	
	$.post("php/proceso.php",{opc:2,ni:ni,id_inv:id_inv,
	monto:monto, dura:dura, interes:interes, hasta:hasta, actual:actual
	},function(resp){
		if(resp.salida==1){
		$("#invertir,#cortina").hide(100,"linear");
		mis_inversiones();
		swal("REGISTRO EXITOSO",resp.msn,"success");
		envio_mensaje();		
		}else{swal("REGISTRO FALLIDO",resp.msn,"error");}
	},"json");
} else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});
	
}



function envio_mensaje(){
$.post("../msn/procesoI.php",{opc:1},function(resp){
$.post("../msn/api.php",{},function(resp1){},"json");
},"json");	
}




function extraer_inv(id_inv){

swal({
  title: "¿Desea extraer toda la inversion?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {	
	$.post("php/proceso.php",{opc:3,id_inv:id_inv},function(resp){
	if(resp.salida==1){
	swal("REGISTRO EXITOSO",resp.msn,"success");
	mis_inversiones();
	}
	else{swal("REGISTRO FALLIDO",resp.msn,"error");}
	},"json");
} else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

}






function historial_inversiones(){
	modulo_inv=2;
$("#titulomodulo").html("<code>HISTORIAL DE INVERSIONES</code>");
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar.gif" style="width:120px"><br>Espere un momento...</center>');
$.post("php/proceso.php",{opc:6},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}


function buscar_ref(ref){

	if(ref.value.length<5){return false;}
$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar.gif" style="width:120px"><br>Espere un momento...</center>');
$.post("../php/proceso.php",{opc:7,ref:ref.value,mod:	modulo_inv},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}


function buscar_fecha_ref(){
var fi=$("#fci").val();
var ff=$("#fcf").val();

$("#cuerpoinfo").html('<center><br><br><img src="../images/cargar.gif" style="width:120px"><br>Espere un momento...</center>');
$.post("../php/proceso.php",{opc:7,ref:0,fi:fi,ff:ff,mod:modulo_inv},function(resp){
	$("#cuerpoinfo").html(resp.listado);

},"json");}




function liberar(monto,id){


$("#cortina,#detalles").hide();
swal({
  title: "¿Esta segur@ que desea liberar la inversion?",
  text: "",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Si, Acepto",
  cancelButtonText: "No, Cancelar",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
	
  if (isConfirm) {		
$.post("php/proceso.php",{opc:8,id:id},function(resp){
	if(resp.salida==1){
	swal("LIBERACION EXITOSA",resp.msn,"success");
	mis_inversiones();
	}
	else{swal("LIBERACION FALLIDA",resp.msn,"error");}
},"json");

} else {
    swal("REGISTRO CANCELADO", "", "error");
  }
});

	
}

function mostrar_cliente(id){
$.post("php/proceso.php",{opc:9,id:id},function(resp){
$("#cortina,#detalles").show(200,"linear");
$("#detalles").html(resp.detalles);
},"json");
}


function alerta(tt,msn,tm,btn){
	
swal({
  title: tt,
  text: msn,
  timer: tm,
  html:true,
  showConfirmButton: btn
});	
	
}