		  		
		  		<button onclick="accesoModulo('pag/mis_operaciones/pagos.php')" class="btn btn-success adaptame" title="Gestionar liberacion de recargas e inscripciones" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-unlock icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Liberaciones <br>de Pagos</span></button>
		  		
		  		<button onclick="$('#modalService').modal('show')" class="btn btn-info adaptame" title="Gestionar retiros de fondos de los clientes" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-money icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Gestión <br>de Retiros</span></button>

		  		<button disabled onclick="accesoModulo('pag/movimientos.php')" class="btn btn-warning adaptame" title="Ver movimientos de entrada y salida" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-line-chart icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Movimiento <br> E/S</span></button>

		  		<button disabled onclick="accesoModulo('pag/musuario.php')" class="btn btn-default adaptame" title="Ver movimientos de operaciones de usuarios" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-users icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Movimiento <br>de Usuarios</span></button>

		  		<button onclick="accesoModuloFrame('../inversiones/multicajero/index.php')" class="btn btn-danger adaptame" title="Ver movimientos de los cajeros" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-bank icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Gestión <br> MultiCajeros</span></button>