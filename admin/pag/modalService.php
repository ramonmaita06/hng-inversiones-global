<div class="modal fade" id="modalService" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:100000;">

    <div class="modal-dialog modal-sm" style="width:320px; z-index:100000;">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <span class="text-success" style="font-weight: bold;" id="modaltitle1">SELECCIONAR SERVICIOS</span>

        </div>
        <div class="modal-body">



          <div class="row">
            <div class="col-md-12">
              <button class="btn btn-success btn-lg col-lg-12" style="width:100%;" onclick="accesoModuloFrame('pag/inversiones/index_retiro.php')"><i class="pull-left fa fa-line-chart fa-1x text-success"></i> Inversiones</button>
            </div>
          
            <div class="col-md-12">
              <br>
              <button disabled class="btn btn-warning btn-lg col-lg-12" style="width:100%;" onclick="accesoModuloFrame('pag/equipate/index_retiro.php')"><i class="pull-left fa fa-laptop fa-1x text-success"></i> Equipate</button>
            </div>
          
            <div class="col-md-12">
              <br>
              <button disabled class="btn btn-info btn-lg col-lg-12" style="width:100%;" onclick="accesoModuloFrame('pag/equipate/index_retiro.php')"><i class="pull-left fa fa-pagelines fa-1x text-success"></i> Exequias</button>
            </div>
          </div>
          
          
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
  </div>