<?php session_name("hngadmin"); session_start(); 
date_default_timezone_set('America/Caracas');
include("../../php/cnxI.php");
// include("../../php/cnxadmin.php");
if(isset($_SESSION['usuario'])){

?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../interfaz/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../interfaz/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../../interfaz/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="../../interfaz/bootstrap/datatables/datatables/datatables.min.css">
	<title>ADMIN-HNG: <?php echo $_SESSION['nombres']; ?></title>
	<style>
		.tablas > thead{
			text-align: center !important;
		}
		img.zoom {
		    /*width: 350px;*/
		    /*height: 200px;*/
		    -webkit-transition: all .2s ease-in-out;
		    -moz-transition: all .2s ease-in-out;
		    -o-transition: all .2s ease-in-out;
		    -ms-transition: all .2s ease-in-out;
		}
		 
		img.zoom:hover {
		    -webkit-transform: scale(1.5); 
		    -moz-transform: scale(1.5);
		    -o-transform: scale(1.5);
		    transform: scale(1.5);
		}
		.pagination{
			margin: 0px !important;
		}
		.cortina_kyc{
		  position: fixed;
		  z-index:99999;
		  width: 100%;
		  height: 100%;
		  background: #fff;
		  opacity: .6;
		  left: 0%;
		  top: 0%;
		}
	</style>
</head>
<body>
<?php //include('../pag/modalService.php'); ?>
	
<!-- <div id="cuerpo"></div> -->

<div class="cortina_kyc">
  <div class="" style="position: fixed;top: 40%;left: 45%;">
    <img src="../../images/cargar1.gif" alt="" width="70px" height="70px">
    <br>
    <span class="text-center"> Cargando..</span>
  </div>
</div>
<div class="container-fluid">
	<div class="panel panel-success" style="position:fixed; width:100%; z-index:10000;">
		<div class="panel-heading" style="padding:0px">
			<table class="table table-bordered">
				<tbody>
					<tr><td colspan="3"><b>CLIENTES  HNG-INVERSIONES</b></td></tr>
						<tr>
							<td>
								

							</td>
						<td>
							<form onsubmit="return false;" method="post" autocomplete="off"><input type="search" class="form-control" id="InputbuscarCliente" title="Cod, Id-c, Nombres, Cedula, Correo, Telefonos." placeholder="Codigo de cliente"></form>
						</td>
						<td>
							<button class="btn btn-md btn-success" id="buscarCliente"><i class="fa fa-search"></i> Buscar</button>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="row" style="margin-top: 120px;">
		<div class="col-md-12">
			<div class="panel panel-warning">
				<div class="panel-heading with-border text-center">
					<big>CLIENTES PENDIENTES POR VERIFICAR</big>
					<!-- <br> -->
				</div>	
				<div class="panel-body">
					<div class="text-center">
						<?php  
							// $p = mysql_query("SELECT TOP 1 * FROM usuario order by id_u desc");
							// if (mysql_num_rows($p) > 0) {
							// 	while ($r = mysql_fetch_object($p)) {
							// 		echo "$r->nombres <br>";
							// 	}
							// }
							$r_x_paginas = 100;
							$consul = mysql_query("SELECT * FROM verificar_identidad WHERE estatus = 1 AND id_admin = '".$_SESSION['id_u']."' ");
							$n = mysql_num_rows($consul);
							if ($n > 0) {
								$n_paginas = ceil($n / $r_x_paginas);
							}

							if ($n_paginas > 1) {
			                    echo '<nav aria-label="Page navigation example">';
			                    echo '<ul class="pagination justify-content-end">';

			                    for ($i=1;$i<=$n_paginas;$i++) {
			                        $class_active = '';
			                        if ($i == 1) {
			                            $class_active = 'active';
			                        }
			                        echo '<li class="page-item '.$class_active.'"><a class="page-link" href="#" data="'.$i.'">'.$i.'</a></li>';
			                    }

			                    echo '</ul>';
			                    echo '</nav>';
			                }
						?>
					</div>
					<table class="table table-striped table-hover datatablses">
						<thead>
							<tr class="bg-success">
								<th>Fecha</th>
								<th>Código de Cliente</th>
								<th>Estatus</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody id="contenido-tabla">
							
						</tbody>
					</table>
					<div class="text-center">
						<?php  
							if ($n > 0) {
								$n_paginas = ceil($n / $r_x_paginas);
							}

							if ($n_paginas > 1) {
			                    echo '<nav aria-label="Page navigation example">';
			                    echo '<ul class="pagination justify-content-end">';

			                    for ($i=1;$i<=$n_paginas;$i++) {
			                        $class_active = '';
			                        if ($i == 1) {
			                            $class_active = 'active';
			                        }
			                        echo '<li class="page-item '.$class_active.'"><a class="page-link" href="#" data="'.$i.'">'.$i.'</a></li>';
			                    }

			                    echo '</ul>';
			                    echo '</nav>';
			                }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<!-- </div> -->



<div class="modal fade" id="modalfoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="z-index:100000;">

    <div class="modal-dialog modal-lg" style="z-index:100000; width: 90%">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <span class="text-success" style="font-weight: bold;" id="modaltitle1">VERIFICAR FOTO</span>

        </div>
        <div class="modal-body"id="detalles">

          
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        </div>
      </div>
      
    </div>
</div>

<script type="text/javascript" src="../../interfaz/js/jquery.js"></script>
<script type="text/javascript" src="../../interfaz/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../../interfaz/js/sweetalert.min.js"></script>
<script type="text/javascript" src="../../interfaz/bootstrap/datatables/datatables/datatables.min.js" ></script>
<script type="text/javascript" src="../../interfaz/js/jquery.mlens-1.7.min.js"></script>
<script type="text/javascript" src="../../interfaz/js/menu.js"></script>
<script type="text/javascript" src="../../interfaz/js/proceso_verificar_identidad.js"></script>
<script type="text/javascript">
	
</script>
</body>
</html>


<?php } else{
	echo '<script>location.href="./";</script>';
} ?>