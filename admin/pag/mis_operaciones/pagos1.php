<?php session_name("hng"); session_start(); ?>
<!doctype html>
<html lang="en">
<head>
  <title>Hombres de Negocios Global C.A.</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  

  <style>
  .ocultar{display:none}
  </style>
</head>
<body style="font-size:12px;">
<div class="panel panel-warning" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading" align="center">
<big><big>LIBERACION DE PAGOS</big></big>
</div>
<div class="panel-body">
<br>

 <div  style="background:#fff"> 

<div  style="background:#C2C9D1"> 
<center><div class="btn-group">
<select id="servicio" class="form-control">
<option value="DORADO">DORADO</option>
<option value="RECICLAJE">RECICLAJE</option>
<option value="EXEQUIAS">EXEQUIAS</option>
<option value="INVERSIONES" selected>INVERSIONES</option>
</select>
<br>


<button id="boton1" class="btn btn-md btn-warning" onclick="return opciones_recarga(1)"><i class="fa fa-dollar"></i> Pagos</button>
<button id="boton3" class="btn btn-md btn-default" onclick="return opciones_recarga(3)"><i class="fa fa-money"></i> Pago Afiliación</button>
<!--<button id="boton2" class="btn btn-md btn-default" onclick="return opciones_recarga(2)"><i class="fa  fa-bar-chart-o"></i> Historico</button>-->
</div></center>
</div>
<br>


<div class="row mostrar_recarga" style="display:none;">
	  <div class="col-sm-3 col-lg-3">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Inicial</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit0" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit0",
        trigger    : "DPC_edit0",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-3 col-lg-3">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Final</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit1" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit1",
        trigger    : "DPC_edit1",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-4 col-lg-4">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Estatus</label>
          <div class="col-md-8">
<select id="esta" class="form-control" style="width:100%;">
<option value="0">En espera</option>
<option value="1">Aprobados</option>
<option value="2">Rechazados</option>
</select>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-1 col-lg-1">
        <div class="form-group">
<center><button class="btn btn-success btn-md" onclick="return mostrar_recargas_fecha()"><i class="fa fa-search"></i></button></center>
          </div>
       </div>

</div>
<hr>

<div class="mostrar_pagos"  id="mostrar_pago" style=" width:100%; height:400px; overflow-y:auto; background:#fff;">Cargando Pagos...</div>

<div class="mostrar_afiliacion"  id="mostrar_afiliacion" style="display:none; width:100%; height:400px; overflow-y:auto; background:#fff;">Cargando registro de pagos de afiliación...</div>

<div class="mostrar_recarga"  id="mostrar_recarga" style="display:none; width:100%; height:400px; overflow-y:auto; background:#fff;"></div>



</div>
</div>
</div>



</body>
</html>