<?php session_name("hngadmin"); session_start(); ?>
<!doctype html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
  <title>Hombres de Negocios Global C.A.</title>
  
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--Archivos de calendario-->
	<script src="interfaz/js/js_hng/src/js/jscal2.js"></script>
    <script src="interfaz/js/js_hng/src/js/lang/es.js"></script>
    <link rel="stylesheet" type="text/css" href="interfaz/js/js_hng/src/css/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="interfaz/js/js_hng/src/css/border-radius.css" />
    <link rel="stylesheet" type="text/css" href="interfaz/js/js_hng/src/css/steel/steel.css" />
<!--Fin de Archivos de calendario-->
  
  <link rel="stylesheet" href="interfaz/js/bootstrap/css/bootstrap.min.css" media="screen">

  <link rel="stylesheet" href="interfaz/js/alerta/dist/sweetalert.css">
  <script src="interfaz/js/alerta/dist/sweetalert.min.js"></script>
  <script src="interfaz/js/jq.min.js"></script>
  <script src="interfaz/js/bootstrap/js/bootstrap.min.js"></script>
  <script src="interfaz/js/number_format.js"></script>




<script src="interfaz/js/proceso_recarga.js"></script>
<script src="interfaz/js/teclado.js"></script>
  <style>
  .ocultar{display:none}
  </style>
</head>
<body style="font-size:12px;">

<?php   include('../../php/modal.php'); ?>

<div class="panel panel-warning" style="background-image:url(images/fondo_red.png)">
<div class="panel-heading" align="center">
<big><big>LIBERACION DE PAGOS HNG</big></big>
</div>
<div class="panel-body">


<div  style="background:#fff"> 

<div  style="background:#C2C9D1"> 
<table class="">    
<tr>
  <td>
    <input type="hidden" id="opc_recarga">
    <select id="servicio" class="form-control" onchange="opciones_recarga($('#opc_recarga').val())">
    <!--<option value="DORADO">DORADO</option>
    <option value="RECICLAJE">RECICLAJE</option>-->
    <option value="CRUCERO">CRUCERO</option>
    <option value="EQUIPATE">EQUIPATE</option>
    <option value="EXEQUIAS">EXEQUIAS</option>
    <option value="INVERSIONES" selected="selected">INVERSIONES</option>
    </select>
  </td>
  <td>
    <div class="btn-group">
      <button id="boton1" class="btn btn-md btn-warning" onclick="return opciones_recarga(1)"><i class="fa fa-dollar"></i> Pagos</button>
      <button id="boton3" class="btn btn-md btn-default" onclick="return opciones_recarga(3)"><i class="fa fa-money"></i> Afiliación</button>
      <button id="boton2" class="btn btn-md btn-default" onclick="return opciones_recarga(2)" disabled><i class="fa  fa-bar-chart-o"></i> Historico</button>
    </div>
  </td>
</tr>
</table>

</div>
<br>


<div class="row mostrar_recarga" style="display:none;">
	  <div class="col-sm-3 col-lg-3">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Inicial</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit0" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit0",
        trigger    : "DPC_edit0",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-3 col-lg-3">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Fecha Final</label>
          <div class="col-md-8">
		  <input  class="form-control" id="DPC_edit1" placeholder="Ej.: dd-mm-aaaa">
		  <script type="text/javascript">//<![CDATA[
      Calendar.setup({
        inputField : "DPC_edit1",
        trigger    : "DPC_edit1",
        onSelect   : function() { this.hide() },
       // showTime   : 12,
        dateFormat : "%d-%m-%Y"
      });//]]>
</script>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-4 col-lg-4">
        <div class="form-group">
          <label for="nom" class="col-md-4 control-label">Estatus</label>
          <div class="col-md-8">
<select id="esta" class="form-control" style="width:100%;">
<option value="0">En espera</option>
<option value="1">Aprobados</option>
<option value="3">Rechazados</option>
</select>
		  </div>
          </div>
       </div> 
	   <div class="col-sm-1 col-lg-1">
        <div class="form-group">
<center><button class="btn btn-success btn-md" onclick="return mostrar_recargas_fecha()"><i class="fa fa-search"></i></button></center>
          </div>
       </div>

</div>

<center>
    
    <script>
        
        function consultar_ref(ref){
            var serv = $("#servicio").val();
            
            $.post("php/consultar_referencia.php",{opc:1, ref:ref, servicio: serv},function(resp){
                if(resp.salida == 1){
                    $("#lista_ref").show();
                    $("#lista_ref").html('<button onclick=$("#lista_ref").hide(); class="pull-right btn btn-sm btn-danger">Cerrar</button><br>'+resp.listado );
                }else{
                    $("#lista_ref").show();
                    $("#lista_ref").html('<button onclick=$("#lista_ref").hide(); class="pull-right btn btn-sm btn-danger">Cerrar</button><br>'+resp.listado );
                    setTimeout(function(){
                        $("#lista_ref").hide();    
                    },2000);
                }
            },"json");
        }
        
    </script>
    <div id="lista_ref" style="display:none; position:absolute; z-index:1000; width:100%; top:0%; left:0%; height: 180px; overflow-y:auto; background: white; border-bottom: 2px grey solid;"></div>
    <div class="row" style=" background: #C2C9D1; width:100%">
      <table>
        <tr>
          <td>
					<select name="banco" id="banco" class="form-control" style="width:100%;"  onchange="opciones_recarga($('#opc_recarga').val())">
            <option value="">Entidad Bancaria</option>
            <optgroup label="BANCOS NACIONALES">
						<option value="BICENTENARIO">BICENTENARIO
						<option value="CARONI">CARONI
						<option value="BDV">VENEZUELA
						<option value="MERCANTIL">MERCANTIL
						<option value="BNC">BNC
						<option value="BVC">VENEZOLANO DE CREDITO
						<option value="BANESCO">BANESCO
						<option value="100% BANCO">100% BANCO
						<option value="PROVINCIAL">PROVINCIAL
						</optgroup>  
            <optgroup label="BANCOS INTERNACIONALES">
            <option value="PAYPAL">PAYPAL
            <option value="ZELLE">ZELLE
            <option value="BANESCO PANAMA">BANESCO PANAMA
            <option value="COLPATRIA">COLPATRIA
            </optgroup> 
					</select>
          </td>
          <td>
            <input type="search" class="form-control" id="ref" placeholder="N. de Ref.">
          </td>
          <td>
            <i class="btn btn-success btn-md fa fa-search" onclick="consultar_ref($('#ref').val()), $('#ref').val('');"></i>
          </td>
        </tr>
      </table>
        </div>
    
        
</center>


<div class="mostrar_pagos"  id="mostrar_pago" style=" width:100%; height:560px; overflow-y:auto; background:#fff;">Cargando Pagos...</div>

<div class="mostrar_afiliacion"  id="mostrar_afiliacion" style="display:none; width:100%; height:560px; overflow-y:auto; background:#fff;">Cargando registro de pagos de afiliación...</div>

<div class="mostrar_recarga"  id="mostrar_recarga" style="display:none; width:100%; height:560px; overflow-y:auto; background:#fff;"></div>


</div>
</div>
</div>

<div class="modal fullscreen-modal" id="ModalComprobante" tabindex="-189" role="dialog" aria-labelledby="myModalLabe2l">

  <div class="modal-dialog modal-lg" style="width: 90%">

  <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header text-center">
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <i class="fa fa-eye  text-info" ></i> <span class="text-info" style="font-weight: bold;" id="modaltitle1">Ver Comprobante</span> <i class="fa fa-image  text-info"></i>

      </div>
      <div class="modal-body">

        <div class="cortina_kyc">
          <div class="" style="position: fixed;top: 40%;left: 40%;">
            <center>
              
              <img src="images/cargar.gif" alt="">
              <br>
              Cargando..!
            </center>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <img src="" id="img-comprobante" alt="Comprobante" class="img-responsive img-thumbnail" width="100%" height="100%">
            </div>
          </div>
        </div>
        

      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">


function searchCdo(cod_id,n){
    var title = '';
    if(n==9){ title='MIS PATROCINADORES';}
    if(n==10){ title='MIS REFERIDOS';}
$.post("php/proceso_recargas.php",{opc:n, cod_id:cod_id, servicio:'Inversiones'},function(resp){
    $('#myModal').modal('show');
    $("#titulomodal").html("<h3><i class='fa fa-sitemap'></i><i class='fa fa-users'></i> "+title+"</h3>");
    $("#vmodal").html(resp.listado);
    
},"json");
}



$(function(){
  $("#opc_recarga").val(1);
  $(document).on('click', '.ver-comprobante', function(event) {
    event.preventDefault();
    $('#img-comprobante').attr('src', '../inversiones/'+$(this).data('imagen'));
    console.log($(this).data('imagen'));
    $('#ModalComprobante').modal('show');

  });
});



</script>

</body>
</html>