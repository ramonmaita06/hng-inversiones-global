<?php session_name("hngadmin"); session_start(); 
date_default_timezone_set('America/Caracas');

if(isset($_SESSION['usuario'])){

?>
<!DOCTYPE html>
<html>

<head>
	<meta http-equiv="Expires" content="0">
	<meta http-equiv="Last-Modified" content="0">
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta name="description" content="Modal Window ">
    <meta charset="UTF-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="interfaz/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="interfaz/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="interfaz/css/sweetalert.css">
	<link rel="stylesheet" type="text/css" href="interfaz/bootstrap/datatables/datatables/datatables.min.css">
	<title>ADMIN-HNG: <?php echo $_SESSION['nombres']; ?></title>

	<style>
	.adaptame .icon{font-size:24px;}
	.ocultame2{display:inline;}
	.adaptame{	width:100px; }
	.adaptameImg{ width:220px; }
	.mimenu{width:100%;}
	 @media only screen and (max-width: 600px){
	.mimenu{position:absolute;}
	.adaptame{width:auto;}
	.adaptame .icon{font-size:20px;}
	.adaptameImg{ width:120px; }
	.ocultame2{display:none;}
	}

	#cuerpoCajero{
		width:100%;
		height:auto;
	}

	</style>
</head>
<body>
<?php include('pag/modalService.php'); ?>
<div class="panel" style="background: black; border-radius: 0px; ">
	<div class="panel-heading" style="padding:2px"> 

		<div class="pull-left">
			<img src="images/logo_oficial_new_acceso.png" class="adaptameImg"><br>
			<div class="ocultame2" style="color:white; font-size:12px"><i class="fa fa-user"></i> <?php echo $_SESSION['nombres']; ?><br>&nbsp;<br></div>
		</div>
		<center>
			<nav class="btn-group" style="z-index:1;">
				<?php if($_SESSION['priv'] == 1){ include('pag/menu1.php'); } ?>
				<?php if($_SESSION['priv'] == 2){ include('pag/menu2.php'); } ?>
				<?php if($_SESSION['priv'] == 0){ include('pag/menu3.php'); } ?>
				<?php if($_SESSION['priv'] == 3){ include('pag/menu4.php'); } ?>
				<button onclick="accesoModuloFrame('pag/mis_operaciones/verificar_cliente.php')" class="btn bg-teal-active adaptame" title="KYC" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-users icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">KYC <br>&nbsp;</span></button>
				<button onclick="accesoModuloFrame('pag/mis_operaciones/renovar_contrato.php')" class="btn btn-primary adaptame" title="Gestionar Renovaciones de  Contratos" data-toggle="tooltip"  data-placement="bottom"><i class=" fa fa-file icon"></i><br><span style="font-size:12px; font-weight:bold;" class="ocultame2">Renovar <br>Contratos</span></button>
		 	</nav>
			<button onclick="javascript:location.href='php/salir.php'" class="btn btn-md btn-link  pull-right"  data-placement="bottom" title="Pulse para cerrar sesion" data-toggle="tooltip"><i class="fa fa-power-off fa-2x" style="color:white"></i></button>
		</center>

		</div>
</div>
<div id="cuerpo"></div>











<script type="text/javascript" src="interfaz/js/jquery.js"></script>
<script type="text/javascript" src="interfaz/js/bootstrap.min.js"></script>
<script type="text/javascript" src="interfaz/js/sweetalert.min.js"></script>
<script type="text/javascript" src="interfaz/bootstrap/datatables/datatables/datatables.min.js" ></script>
<script type="text/javascript" src="interfaz/js/menu.js"></script>
<script type="text/javascript">
	$(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
	});
</script>
</body>
</html>


<?php } else{
	echo '<script>location.href="./";</script>';
} ?>